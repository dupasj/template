Before all starting, it’s important to know the main design pattern of the inner classes. In the classes we have multiple type of value type:

- a [field value](#field-value) (the value can be defined or not).
- a [list of values](#list-of-value)
- [relationship](#relation) between classes:
  - The relation can be one to one relationship.
  - The relation can be [one to many relationship](#one-to-many).

<a name="field-value"></a>
## When the field is a value.
Imagine that you have **an id (numerical value) within your `User` class**. The design pattern provides few following methods:

- <a name="field-value--setId"></a> `setId`: allows you to update or **assign the value to the field**. *This method allows one parameter which is the value that will be assigned.*
- <a name="field-value--unsafeId"></a> `unsafeId`: **gives the field value**. Unlike the [getId](#field-value--getId) method, **the method results an undefined value if the value is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
- <a name="field-value--getId"></a> `getId`: **give the field value**. Unlike the [unsafeId](#field-value--unsafeId) method, **we will throw an error if the value is not defined**.
- <a name="field-value--isIdDefined"></a> `isIdDefined`: **gives a boolean if the field is defined** or not.
- <a name="field-value--eraseId"></a> `eraseId`: **removes the assigned value** of the field: *the field will be flagged as undefined*.
- <a name="field-value--isIdEqualTo"></a> `isIdEqualTo`: **checks if the field is equal to your given value** and if the value is defined. *Note that if the given value is `undefined`, the method always results `false`*.

Here is a little flow of the `User` with the `id` as a numeric value.
````typescript  
const user = new User();  
  
user.isIdDefined(); // result false because the value is undefined  
user.isIdEqualTo(42); // result false because the value is undefined  
user.unsafeId(); // results undefined  
user.getId(); // @throws an error because the value is undefined  
  
user.setId(42); // Assigns the value to 42.  
  
user.isIdDefined(); // results true because the value is defined  
user.isIdEqualTo(42); // results true because the value is defined and equal to 42  
user.isIdEqualTo(5); // results false because the value is not equal to 42  
user.isIdEqualTo(undefined); // results false because the value is not equal to 42  
user.unsafeId(); // results 42  
user.getId(); // results 42  
  
user.eraseId(); // Removes the assigned value of the id field  
  
user.isIdDefined(); // results false because the value is undefined  
user.isIdEqualTo(42); // results false because the value is undefined  
user.unsafeId(); // results undefined  
user.getId(); // @throws an error because the value is undefined  
````  

Here are the possibilities of change:
- **If the value is a read-only feature**, the methods `eraseId` and `setId` won't exist. _In this case, the method `getId` will never throw an error._
- **If the value cannot be undefined**, the methods `eraseId`, `unsafeId`, and `isIdDefined` won't exist. _In this case, The method `getId` will never throw an error._

> Note that the value is a numeric value here, but the value can be any other type of value: depends on the expected behavior.

<a name="list-of-value"></a>
## When the field is a list of values.
Imagine you have a **tasks field (a list of string field) in your `Todo` class**. The design pattern provides few following methods:

- <a name="list-of-value--addTasks"></a>  `addTasks`: allows you to **add one or multiple values** to the array of tasks. Each argument of in the method will be added to the array of tasks, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in the array of tasks: the value is unique._
- <a name="list-of-value--removeTasks"></a> `removeTasks`: allows you to **remove one or multiple values** from the array of tasks. Each argument of in the method will be removed from the array of tasks, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
- <a name="list-of-value--containTasks"></a> `containTasks`: **checks if all the given value is included** in the array of tasks. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in the array of tasks or are `undefined`, the method results `false`: the method result `true` only if all the values are included in the array of tasks and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
- <a name="list-of-value--getTasks"></a> `getTasks`: **gives a copy of array of tasks**.
- <a name="list-of-value--unsafeTasks"></a> `unsafeTasks`: **gives the array of tasks without copy** the array. The array is not copied, so if you modify this array, you will modify the master array of tasks (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getTasks](#list-of-value--getTasks) method.
- <a name="list-of-value--getTasksSize"></a> `getTasksSize`: **gives the size of the array of tasks**.
- <a name="list-of-value--unsafeTask"></a> `unsafeTask`: **gives a positioned item in the array of tasks**. Unlike [getTask](#list-of-value--getTask) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
- <a name="list-of-value--getTask"></a> `getTask`: **gives a positioned item the array of tasks**. Unlike the [unsafeTask](#list-of-value--unsafeTask) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
- <a name="list-of-value--truncateTasks"></a> `truncateTasks`: **remove all values in the array of tasks**.  


  Here is a little flow of the `Todo` class with the `tasks` as list of string values.

````typescript  
const todo = new Todo();  
  
todo.containTasks("take a coffee"); // Results false because this task is not included in the list of tasks  
todo.containTasks(); // Returns true because no task is given, indeed all the tasks are included  
todo.getTasksSize(); // Results 0 (because the list of task s is empty)  
todo.unsafeTask(0); // Results undefined because the given position doesn't have value (because the list of task s is empty) 
todo.getTask(0); // @Throws an error because the given position doesn't have value (because the list of task s is empty) 
todo.unsafeTask(-1); // Results undefined because the given position doesn't have value (because the list of task s is empty) 
todo.getTask(-1); // @Throws an error because the given position doesn't have value (because the list of task s is empty) 
todo.getTasks(); // Results [] because the list of tasks is empty  
todo.unsafeTasks(); // Results [] because the list of task s is empty  
  
todo.addTasks("take a coffee",["be a good guy","write more documentation"]); // Add the following tasks: "take a coffee", "be a good guy", and "write more documentation".  
todo.addTasks("take a coffee") // Does nothing because the given task "take a coffee" is already added  
  
todo.getTasks(); // Results ["take a coffee", "be a good guy", "write more documentation"]  
todo.unsafeTasks(); // Results ["take a coffee", "be a good guy", "write more documentation"] (This is not a copy of the array list, DO NOT EDIT IT)  
  
todo.unsafeTask(0); // Results the first item: "take a coffee"  
todo.getTask(0); // Results the first item: "take a coffee"  
  
todo.unsafeTask(-2); // Results "be a good guy"  
todo.getTask(-2); // Results "be a good guy"  
  
todo.containTasks("take a coffee",["be a good guy","create more bugs"]); // Results false because all the given element is not in tasks list  
todo.containTasks("take a coffee",["be a good guy","write more documentation"]); // Results true because all thus tasks are included in the list  
todo.containTasks("take a coffee","be a good guy"); // Results true because this tasks are included in the tasks field  
  
todo.removeTasks("be a good guy","write more documentation"); // Removes the follwing tasks: "be a good guy","write more documentation"  
todo.removeTasks("be a good guy"); // Does nothing because the task "be a good guy" is absent in the list  
  
todo.getTasks(); // Results ["take a coffee"]  
todo.unsafeTasks(); // Results ["take a coffee"]  
todo.getTasksSize(); // Results 1  
  
todo.unsafeTask(0); // Results the first item: "take a coffee"  
todo.getTask(0); // Results the first item: "take a coffee"  
todo.unsafeTask(-1); // Results the last item: "take a coffee"  
todo.getTask(-1); // Results the last item: "take a coffee"  
  
todo.unsafeTask(-2); // Results an undefined value because the given position doesn't have value  
todo.getTask(-2); // @Throws an error because the given position doesn't have value  
````  

> Note that the value is a list of string here, but the value can be any other type of value: depends on the expected behavior.

<a name="relation"></a>
# Relations design pattern

<a name="one-to-many"></a>
## One to many design pattern
Imagine you have two classes: **the `Chicken` class who can have multiple `Egg` classes**.

<a name="one-to-many-parent"></a>
### The parent class (the Chicken class)
The `Chicken` class will look like [the list of value’s field](#list-of-value):

- <a name="one-to-many-chicken--linkEggs"></a>  `linkEggs`: allows you to **associate one or multiple `Egg` class instance** to the `Chicken` class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate `Chicken` class instance** ([Egg.unlinkChicken](#one-to-many-egg--unlinkChicken)), and **then associate the  `Egg` class instance** to the `Chicken` class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [Egg.linkChicken](#one-to-many-egg--linkChicken) to associate the `Chicken` class instance and the given `Egg` class instances from the both parts*.
- <a name="one-to-many-chicken--unlinkEggs"></a> `unlinkEggs`: allows you to **dissociate one or multiple `Egg` class instance** to the `Chicken` class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the `Chicken` class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [Egg.unlinkChicken](#one-to-many-egg--unlinkChicken) to dissociate the `Chicken` class instance and the given `Egg` class instances from the both parts*.
- <a name="one-to-many-chicken--getEggs"></a> `getEggs`: **Gives a copy of the array of the associated `Egg` class instances**.
- <a name="one-to-many-chicken--unsafeEggs"></a> `unsafeEggs`: **Gives an array of the associated `Egg` class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getEggs](one-to-many-chicken--getEggs) method.
- <a name="one-to-many-chicken--getEggsSize"></a> `getEggsSize`: **gives the number of the associated `Egg` class instances**.
- <a name="one-to-many-chicken--disposeEggs"></a> `disposeEggs`: **Dissociate all the associated `Egg` class instances**.  *Note that all the associated `Egg` class instances will lose their `Chicken` class association*.
- <a name="one-to-many-chicken--unsafeEgg"></a> `unsafeEgg`: **gives a positioned item in the array of the associated `Egg` class instances**. Unlike [getEgg](#one-to-many-chicken--getEgg) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
- <a name="one-to-many-chicken--getEgg"></a> `getEgg`: **gives a positioned item the array of the associated `Egg` class instances**. Unlike the [unsafeTask](#one-to-many-chicken--unsafeEgg) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
- <a name="one-to-many-chicken--areEggsLinked"></a> `areEggsLinked`: **checks if all the given `Egg` class instances are associated** with your `Chicken` class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your `Chicken` class instance or are `undefined`, the method results `false`: the method result `true` only if all the `Egg` class instances are associated with your `Chicken` class instance and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)_

<a name="one-to-many-child"></a>
### The child class (the Egg class)
The `Egg` class will look like [field value](#field-value):

- <a name="one-to-many-egg--linkChicken"></a> `linkChicken`: allows you to **associate a `Chicken` class instance** with your actual `Egg` class instance. *This method allows one parameter which is a `Chicken` class instance that will be associated*. *Note that the method will also run [Chicken.linkEggs](#one-to-many-chicken--linkEggs) to associate the `Chicken` class instance et the `Egg` class instance from the both parts*. *Note also that the method will also run [unlinkChicken](#one-to-many-egg--unlinkChicken) to dissociate the current `Chicken` class instance associated*.
- <a name="one-to-many-egg--unlinkChicken"></a> `unlinkChicken`: **dissociate the `Chicken` class instance** from your actual `Egg` class instance.  *Note that the method will also run [Chicken.unlinkEggs](unlinkEggs) to dissociate the `Chicken` class instance et the `Egg` class instance from the both parts*.
- <a name="one-to-many-egg--unsafeChicken"></a> `unsafeChicken`: **results the associated `Chicken` class instance**. Unlike the [getChicken](#one-to-many-egg--getChicken) method, **the method results an undefined value if the `Egg` class instance doesn't have `Chicken` class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
- <a name="one-to-many-egg--getChicken"></a> `getChicken`: **results the associated `Chicken` class instance**. Unlike the [unsafeChicken](#one-to-many-egg--unsafeChicken) method, **we will throw an error if the `Egg` class instance doesn't have `Chicken` class instance associate**.
- <a name="one-to-many-egg--hasChickenLinked"></a> `hasChickenLinked`: **gives a boolean if a `Chicken` class instance is associated** or not to our actual `Egg` class instance.
- <a name="one-to-many-egg--isLinkedWithChicken"></a> `isLinkedWithChicken`: **checks if the `Egg` class instance is associated with all the given `Chicken` class instance**. *Note that if the given value is `undefined`, the method always results `false`*.

````typescript
const chicken = new Chicken();
const egg = new Egg();

chicken.containEggs(egg); // Result false because this egg is not included in the eggs field
chicken.containEggs(); // Return true because no egg is given, indeed all the eggs are included
chicken.getEggsSize(); // Result 0 because the list is empty
chicken.unsafeEgg(0); // result undefined because the given position doesn't have value
chicken.getEgg(0); // @Throw an error because the given position doesn't have value
chicken.unsafeEgg(-1); // result undefined because the given position doesn't have value
chicken.getEgg(-1); // @Throw an error because the given position doesn't have value
chicken.getEggs(); // Result [] because the list is empty
chicken.unsafeEggs(); // Result [] because the list is empty

egg.isChickenDefined(); // result false because the value is undefined
egg.isChickenEqualTo(42); // result false because the value is undefined
egg.unsafeChicken(); // result undefined
egg.getChicken(); // @throw an error because the value is undefined

chicken.linkEggs(egg); // Associate the egg and the chicken class.
egg.linkChicken(chicken); // Do nothing because the the chicken is already linked to the egg 

egg.linkChicken(chicken); // Dissociate the egg and the chicken class.
chicken.linkEggs(egg); // Do nothing because the the egg is already dissociate from the egg 

````