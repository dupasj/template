module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testPathIgnorePatterns: ["./test/__LIB/","./src/","./result/"],
  "setupFilesAfterEnv": ["jest-extended/all"],
};