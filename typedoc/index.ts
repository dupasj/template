import * as Path from "path";
import * as fs from "fs";
import * as BabelType from "@babel/types";
import * as BabelCore from "@babel/core";
import {randomUUID} from "crypto";

type Splitter = {
    split: string[],
    encapsulating: [string[],string[]][],
    string: string[],
    ignore: string[],
}

const splitter = (content: string,options: Splitter) => {
    options.ignore = options.ignore.map(i => i.trim()).filter(i => i.length > 0).sort((a,b) => a.length - b.length);
    options.split = options.split.map(i => i.trim()).filter(i => i.length > 0).sort((a,b) => a.length - b.length);
    options.string = options.string.map(i => i.trim()).filter(i => i.length > 0).sort((a,b) => a.length - b.length);

    options.encapsulating = options.encapsulating.map(encapsulating => {
        return [
            encapsulating[0].map(i => i.trim()).filter(i => i.length > 0).sort((a,b) => a.length - b.length),
            encapsulating[1].map(i => i.trim()).filter(i => i.length > 0).sort((a,b) => a.length - b.length)
        ] as [string[],string[]]
    }).filter(item => item[0].length > 0 && item[1].length > 0);

    const capsuling_start = options.encapsulating.map(encapsulating => {
        return encapsulating[0]
    }).flat(1).sort((a,b) => a.length - b.length);

    let capsuling: {
        deep: number,
        capsuling: [string[],string[]],
    }|null = null;
    let split = 0;
    let in_string: string|null = null;

    const results: string[] = [];

    for(let i=0;i<content.length;i++){
        {
            const escaping = options.ignore.find(ignore => {
                return Array.from(ignore).every((char,index) => {
                    return content[i+index] === char;
                });
            });

            if (escaping){
                i+=escaping.length;
                continue;
            }
        }

        if (in_string){
            const escaping = Array.from(in_string).every((char,index) => {
                return content[i+index] === char;
            });

            if (escaping){
                i+=in_string.length - 1;
                in_string = null;
                continue;
            }

            continue;
        }

        {
            const _string = options.string.find(ignore => {
                return Array.from(ignore).every((char,index) => {
                    return content[i+index] === char;
                });
            });

            if (_string){
                i+=_string.length;
                in_string = _string;
                continue;
            }
        }

        if (capsuling){
            const end = capsuling.capsuling[1].find(end => {
                return Array.from(end).every((char,index) => {
                    return content[i+index] === char;
                });
            });

            if (end){
                i+=end.length - 1;
                capsuling.deep --;
                if (capsuling.deep <= 0){
                    capsuling = null;
                }

                continue;
            }

            const start = capsuling.capsuling.find(end => {
                return Array.from(end[0]).every((char,index) => {
                    return content[i+index] === char;
                });
            });

            if (start){
                i+=start.length - 1;
                capsuling.deep ++;
            }

            continue;
        }

        {
            const start = capsuling_start.find(start => {
                return Array.from(start).every((char,index) => {
                    return content[i+index] === char;
                });
            });

            if (start){
                i+=start.length - 1;
                capsuling = {
                    deep: 1,
                    capsuling: options.encapsulating.find(i => i[0].includes(start))!
                };
                continue;
            }
        }

        const match = options.split.find(split => {
            return Array.from(split).every((char,index) => {
                return content[i+index] === char;
            });
        });

        if (match){

            results.push(content.slice(split,i))
            results.push(match)

            i+=match.length - 1;
            split = i + 1;
        }
    }

    results.push(content.slice(split))

    return results;
};


const tiny = (content: string) => {
    do{
        if (content[content.length - 1] === "*" || content[content.length - 1] === "/"){
            content = content.slice(0,-1);
        }
        content = content.trim();
    } while (content[content.length - 1] === "*" || content[content.length - 1] === "/")

    const rules: [RegExp,string][] = [
        //header rules
        [/#{6}\s?([^\n]+)/g, "<h6>$1</h6>"],
        [/#{5}\s?([^\n]+)/g, "<h5>$1</h5>"],
        [/#{4}\s?([^\n]+)/g, "<h4>$1</h4>"],
        [/#{3}\s?([^\n]+)/g, "<h3>$1</h3>"],
        [/#{2}\s?([^\n]+)/g, "<h2>$1</h2>"],
        [/#{1}\s?([^\n]+)/g, "<h1>$1</h1>"],

        //bold, italics and paragragh rules
        [/\*\*\s?([^\n]+)\*\*/g, "<b>$1</b>"],
        [/\*\s?([^\n]+)\*/g, "<i>$1</i>"],
        [/__([^_]+)__/g, "<b>$1</b>"],
        [/_([^_`]+)_/g, "<i>$1</i>"],
        [/([^\n]+\n?)/g, "<p>$1</p>"],

        //links
        [
            /\[([^\]]+)\]\(([^)]+)\)/g,
            '<a href="$2" style="color:#2A5DB0;text-decoration: none;">$1</a>',
        ],

        //highlights
        [
            /(`)(\s?[^\n,]+\s?)(`)/g,
            '<a style="background-color:grey;color:black;text-decoration: none;border-radius: 3px;padding:0 2px;">$2</a>',
        ],

        //Lists
        [/([^\n]+)(\+)([^\n]+)/g, "<ul><li>$3</li></ul>"],
        [/([^\n]+)(\*)([^\n]+)/g, "<ul><li>$3</li></ul>"],

        //Image
        [
            /!\[([^\]]+)\]\(([^)]+)\s"([^")]+)"\)/g,
            '<img src="$2" alt="$1" title="$3" />',
        ],
    ];

    /*
    rules.forEach(([rule, template]) => {
        content = content.replace(rule, template)
    })
     */

    return content;
};

const TAGS = [
    "@deprecated",
    "@param",

    "@throws",
    "@typeParam",



    "@prop",
    "@property",


    "@enum",

    "@public",
    "@private",
    "@protected",
    "@internal",
    "@readonly",
    "@alpha",
    "@beta",
    "@experimental",
    "@hidden",
    "@ignore",

    "@remarks",
    "@returns",
    "@return",
    "@todo",
    "@group",
    "@category",
    "@see",
    "@summary",
    "@description",
]

const entrypoint = "C:\\Users\\dupas\\IdeaProjects\\template\\result\\project.ts";
enum ANNOTATION {
    PARENTHESES = "PARENTHESES",
    THIS = "THIS",
    ANY = "ANY",
    STRING = "STRING",
    NUMBER = "NUMBER",
    BIG_NUMBER = "BIG_NUMBER",
    UNKNOWN = "UNKNOWN",
    BOOLEAN = "BOOLEAN",
    UNION = "UNION",
    ARRAY = "ARRAY",
    ARRAY_VALUES = "ARRAY_VALUES",
    UNDEFINED = "UNDEFINED",
    REFERENCE = "REFERENCE",
}
enum TYPE {
    CLASS = "CLASS",
    METHOD = "METHOD",
    PARAMETER = "PARAMETER",
    TYPE = "TYPE",
    TYPE_PARAMETER = "TYPE_PARAMETER",
    PROPERTY = "PROPERTY",
}
enum KIND {
    LET = "LET",
    VAR = "VAR",
    CONST = "CONST",
}
enum SCOPE {
    PRIVATE = "PRIVATE",
    PROTECTED = "PROTECTED",
    PUBLIC = "PUBLIC",
}

type NODES<T extends (BabelType.Node|null|undefined) = (BabelType.Node|null|undefined)> = [BabelType.File,BabelType.Program,... (BabelType.Node|null|undefined)[],T];

type Declaration = ClassDeclaration|PropertyDeclaration|MethodDeclaration|ParameterDeclaration|TypeDeclaration|TypeParameterDeclaration;

interface _Annotation {
    type: ANNOTATION
}
type Annotation = ArrayValuesAnnotation|ParenthesizesAnnotation|ArrayAnnotation|ThisAnnotation|NumberAnnotation|UndefinedAnnotation|StringAnnotation|UnknownAnnotation|BooleanAnnotation|BigNumberAnnotation|AnyAnnotation|UnionAnnotation|ReferenceAnnotation;

interface ArrayAnnotation extends _Annotation {
    type: ANNOTATION.ARRAY
    value: _Annotation
}
interface ArrayValuesAnnotation extends _Annotation {
    type: ANNOTATION.ARRAY_VALUES
    values: _Annotation[]
}
interface ParenthesizesAnnotation extends _Annotation {
    type: ANNOTATION.PARENTHESES
    value: _Annotation
}
interface ThisAnnotation extends _Annotation {
    type: ANNOTATION.THIS
    ref: string|null
}
interface NumberAnnotation extends _Annotation {
    type: ANNOTATION.NUMBER
}
interface UndefinedAnnotation extends _Annotation {
    type: ANNOTATION.UNDEFINED
}
interface StringAnnotation extends _Annotation {
    type: ANNOTATION.STRING
}
interface UnknownAnnotation extends _Annotation {
    type: ANNOTATION.UNKNOWN
}
interface BooleanAnnotation extends _Annotation {
    type: ANNOTATION.BOOLEAN
}
interface BigNumberAnnotation extends _Annotation {
    type: ANNOTATION.BIG_NUMBER
}
interface AnyAnnotation extends _Annotation {
    type: ANNOTATION.ANY
}
interface UnionAnnotation extends _Annotation {
    type: ANNOTATION.UNION
    values: _Annotation[]
}
interface ReferenceAnnotation extends _Annotation {
    type: ANNOTATION.REFERENCE,
    ref: string|null
    name: string|null
}

interface _Declaration {
    type: TYPE,
    id: string
    name: string
}
interface _VariableDeclaration extends _Declaration {
    constraint: Annotation|null
}
interface ParameterDeclaration extends _VariableDeclaration {
    constraint: Annotation|null
    type: TYPE.PARAMETER
    optional: boolean,
    spread: boolean,
    default: Annotation|null
}
interface TypeParameterDeclaration extends _Declaration {
    type: TYPE.TYPE_PARAMETER
    constraint: Annotation|null
    default: Annotation|null
}
interface TypeDeclaration extends _Declaration {
    constraint: Annotation|null
    typeParameters: string[]
    type: TYPE.TYPE
}

enum VERSION {
    DEPRECATED = "DEPRECATED",
    BETA = "BETA",
    EXPERIMENTAL = "EXPERIMENTAL",
    ALPHA = "ALPHA"
}

type DOC = {
    params: { [key: string]: string },
    summary: string | null
    description: string | null
    return: string | null
    see: string[],
    remarks: string[]
    todo: string[]
    categories: string[]
    hidden: boolean,
    ignore: boolean,
    version: VERSION | null,
    scope: SCOPE,
    internal: boolean,
    readonly: boolean,
}

const parseDoc = (content: string) => {
    const doc: DOC = {
        params: {},
        summary: null,
        description: null,
        return: null,
        see: [],
        remarks: [],
        todo: [],
        categories: [],
        hidden: false,
        ignore: false,
        version: null,
        scope: SCOPE.PRIVATE,
        internal: false,
        readonly: false
    }

    const split = splitter(content, {
        split: TAGS,
        encapsulating: [
            [
                ["["],
                ["]"]
            ],
            [
                ["{"],
                ["}"]
            ],
            [
                ["("],
                [")"]
            ]
        ],
        string: ["\'",'"','\`'],
        ignore: ["\\"]
    });

    for(let i=1;(i + 1)<split.length;i+=2){
        const tag = split[i];

        if (tag === "@description"){
            if (doc.description !== null){
                console.trace("You have more than one description");
            }

            doc.description = tiny(split[i+1]);
            continue;
        }

        if (tag === "@summary"){
            if (doc.summary !== null){
                console.trace("You have more than one summary");
            }

            doc.summary = tiny(split[i+1]);
            continue;
        }

        if (tag === "@return" || tag === "@returns"){
            if (doc.return !== null){
                console.trace("You have more than one return");
            }

            doc.return = tiny(split[i+1]);
            continue;
        }

        if (tag === "@see"){
            doc.see.push(tiny(split[i+1]));
            continue;
        }

        if (tag === "@todo"){
            doc.todo.push(tiny(split[i+1]));
            continue;
        }

        if (tag === "@group" || tag === "@category"){
            doc.categories.push(tiny(split[i+1]));
            continue;
        }

        if (tag === "@remarks"){
            doc.remarks.push(tiny(split[i+1]));
            continue;
        }

        if (tag === "@internal"){
            doc.internal = true;
            continue;
        }

        if (tag === "@ignore"){
            doc.ignore = true;
            continue;
        }

        if (tag === "@hidden"){
            doc.hidden = true;
            continue;
        }

        if (tag === "@experimental"){
            doc.version = VERSION.EXPERIMENTAL;
            continue;
        }

        if (tag === "@beta"){
            doc.version = VERSION.BETA;
            continue;
        }

        if (tag === "@alpha"){
            doc.version = VERSION.ALPHA;
            continue;
        }

        if (tag === "@private"){
            doc.scope = SCOPE.PRIVATE;
            continue;
        }

        if (tag === "@protected"){
            doc.scope = SCOPE.PROTECTED;
            continue;
        }
        if (tag === "@public"){
            doc.scope = SCOPE.PUBLIC;
            continue;
        }

        if (tag === "@readonly"){
            doc.readonly = true;
            continue;
        }

        if (tag === "@deprecated"){
            doc.version = VERSION.DEPRECATED;
            continue;
        }

        if (tag === "@property" || tag === "@prop"){

            continue;
        }
    }

    return doc;
}

interface PropertyDeclaration extends _Declaration {
    type: TYPE.PROPERTY,
    constraint: Annotation|null
    abstract: boolean
    scope: SCOPE
    default: Annotation|null

    description: string|null
    summary: string|null
    hidden: boolean
    ignore: boolean
    readonly: boolean
    version: VERSION|null
    see: string[],
    remarks: string[]
    todo: string[]
    categories: string[]

}
interface MethodDeclaration extends _Declaration {
    type: TYPE.METHOD,
    result: Annotation|null
    typeParameters: string[]
    parameters: string[]
    abstract: boolean
    scope: SCOPE

    return: string|null
    description: string|null
    summary: string|null
    hidden: boolean
    ignore: boolean
    readonly: boolean
    version: VERSION|null
    see: string[],
    remarks: string[]
    todo: string[]
    categories: string[]
}
interface ClassDeclaration extends _Declaration {
    type: TYPE.CLASS,
    abstract: boolean,
    typeParameters: string[]
    children: string[]

    description: string|null
    summary: string|null
    hidden: boolean
    ignore: boolean
    readonly: boolean
    version: VERSION|null
    see: string[],
    remarks: string[]
    todo: string[]
    categories: string[]
}

interface Project {
    files: {[key: string]: File}
    declarations: {[key: string]: Declaration}
}
interface File {
    [key: string]: Annotation
}

(async () => {
    const project: Project = {
        declarations: {},
        files: {},
    }
    const __cache = {
        classes: new Map<BabelType.ClassDeclaration,ClassDeclaration>(),
        types: new Map<BabelType.TSTypeAliasDeclaration,TypeDeclaration>(),
        typesParameters: new Map<BabelType.TSTypeParameter|BabelType.TypeParameter,TypeParameterDeclaration>(),
    };

    const events: {[key: string]: ((file: File) => void)[]} = {};
    const processFile = async (filename: string,callback: ((file: File) => void)) => {
        const processAnnotation = async (nodes: NODES): Promise<Annotation> => {
            const type = nodes[nodes.length - 1];

            if (!BabelType.isNode(type)){
                return {
                    type: ANNOTATION.ANY
                };
            }

            if (BabelType.isIdentifier(type)){
                return processIdentifierAnnotation([
                    ... nodes,
                    type,
                ])
            }

            if (BabelType.isClassDeclaration(type)){
                const declaration = await processClassDeclaration([
                    ... nodes,
                    type,
                ]);

                return {
                    type: ANNOTATION.REFERENCE,
                    name: declaration.name,
                    ref: declaration.id,
                }
            }

            if (BabelType.isTSTypeAnnotation(type) || BabelType.isTypeAnnotation(type)){
                return processAnnotation([... nodes,type.typeAnnotation]);
            }

            if (BabelType.isTSThisType(type) || BabelType.isThisTypeAnnotation(type)){
                return {
                    type: ANNOTATION.THIS,
                    ref: null
                }
            }

            if (BabelType.isTSUnknownKeyword(type)){
                return {
                    type: ANNOTATION.UNKNOWN
                }
            }

            if (BabelType.isTSNumberKeyword(type) || BabelType.isNumberTypeAnnotation(type)){
                return {
                    type: ANNOTATION.NUMBER
                }
            }

            if (BabelType.isTSBooleanKeyword(type) || BabelType.isBooleanTypeAnnotation(type)){
                return {
                    type: ANNOTATION.BOOLEAN
                }
            }

            if (BabelType.isTSUndefinedKeyword(type)){
                return {
                    type: ANNOTATION.UNDEFINED
                }
            }

            if (BabelType.isTSBigIntKeyword(type)){
                return {
                    type: ANNOTATION.BIG_NUMBER
                }
            }

            if (BabelType.isTSStringKeyword(type) || BabelType.isStringTypeAnnotation(type)){
                return {
                    type: ANNOTATION.STRING
                }
            }

            if (BabelType.isTSUnionType(type) || BabelType.isUnionTypeAnnotation(type)){
                const values: Annotation[] = [];
                for(const _type of type.types){
                    values.push(await processAnnotation([... nodes,_type]));
                }

                return {
                    type: ANNOTATION.UNION,
                    values: values
                }
            }

            if (BabelType.isTSArrayType(type) || BabelType.isArrayTypeAnnotation(type)){
                return {
                    type: ANNOTATION.ARRAY,
                    value: await processAnnotation([... nodes,type.elementType])
                }
            }

            if (BabelType.isTSTypeReference(type)){
                return processAnnotation([... nodes,type.typeName])
            }

            if (BabelType.isTSParenthesizedType(type)){
                return {
                    type: ANNOTATION.PARENTHESES,
                    value: await processAnnotation([... nodes,type.typeAnnotation])
                }
            }
            if (BabelType.isArrayExpression(type)){
                const values: Annotation[] = [];
                for(const _type of type.elements){
                    values.push(await processAnnotation([... nodes,_type]));
                }

                return {
                    type: ANNOTATION.ARRAY_VALUES,
                    values: values
                }
            }

            console.trace(`Unexpected node with type ${type.type} in ${filename} while trying to process annotation`)
            return {
                type: ANNOTATION.ANY
            }
        }
        const processTypeParameter = async (nodes: NODES<BabelType.TypeParameter|BabelType.TSTypeParameter>): Promise<TypeParameterDeclaration> => {
            const typeParameterNode = nodes[nodes.length - 1] as BabelType.TypeParameter|BabelType.TSTypeParameter;

            const _cache = __cache.typesParameters.get(typeParameterNode);
            if (_cache){
                return _cache;
            }

            const typeParameterDeclaration: TypeParameterDeclaration = {
                type: TYPE.TYPE_PARAMETER,
                constraint: null,
                default: null,
                id: randomUUID(),
                name: typeParameterNode.name
            };
            __cache.typesParameters.set(typeParameterNode,typeParameterDeclaration);
            project.declarations[typeParameterDeclaration.id] = typeParameterDeclaration;

            if (BabelType.isTSTypeParameter(typeParameterNode)){
                typeParameterDeclaration.constraint = await processAnnotation([
                    ... nodes,
                    typeParameterNode.constraint,
                ])
            }else{

            }

            typeParameterDeclaration.default = await processAnnotation([
                ... nodes,
                typeParameterNode.default,
            ])

            return typeParameterDeclaration;
        }
        const processClassDeclaration = async (nodes: NODES<BabelType.ClassDeclaration>): Promise<ClassDeclaration> => {
            const classNode = nodes[nodes.length - 1] as BabelType.ClassDeclaration;

            const _cache = __cache.classes.get(classNode);
            if (_cache){
                return _cache;
            }

            const doc = parseDoc((classNode.leadingComments ?? []).map(i => i.value).join(""));

            const classDeclaration: ClassDeclaration = {
                categories: doc.categories,
                description: doc.description,
                hidden: doc.hidden,
                ignore: doc.ignore,
                readonly: doc.readonly,
                remarks: doc.remarks,
                see: doc.see,
                summary: doc.summary,
                todo: doc.todo,
                version: doc.version,
                type: TYPE.CLASS,
                abstract: classNode.abstract ?? false,
                id: randomUUID(),
                children: [],
                typeParameters: [],
                name: classNode.id.name
            };
            __cache.classes.set(classNode,classDeclaration);
            project.declarations[classDeclaration.id] = classDeclaration;

            if (classNode.typeParameters){
                if (BabelType.isTypeParameterDeclaration(classNode.typeParameters) || BabelType.isTSTypeParameterDeclaration(classNode.typeParameters)){
                    for(const parameter of classNode.typeParameters.params){
                        const typeParameter = await processTypeParameter([
                            ... nodes,
                            classNode.typeParameters,
                            parameter,
                        ])
                        classDeclaration.typeParameters.push(typeParameter.id)
                    }
                }else{
                    console.trace(`Unexpected node with type ${classNode.typeParameters.type} in ${filename} while trying to to process the type parameter in the class ${classDeclaration.name}`)
                }
            }

            for(const child of classNode.body.body){
                if (BabelType.isClassMethod(child)){

                    const doc = parseDoc((child.leadingComments ?? []).map(i => i.value).join(""));

                    const methodDeclaration: MethodDeclaration = {
                        categories: doc.categories,
                        description: doc.description,
                        return: doc.return,
                        hidden: doc.hidden,
                        ignore: doc.ignore,
                        readonly: doc.readonly,
                        remarks: doc.remarks,
                        see: doc.see,
                        summary: doc.summary,
                        todo: doc.todo,
                        version: doc.version,

                        abstract: child.abstract ?? false,
                        scope: doc.scope ?? (() => {
                            const access = child.access ?? child.accessibility;

                            if (access === "private"){
                                return SCOPE.PRIVATE
                            }

                            if (access === "protected"){
                                return SCOPE.PROTECTED
                            }

                            return SCOPE.PUBLIC;
                        })(),
                        type: TYPE.METHOD,
                        result: null,
                        id: randomUUID(),
                        typeParameters: [],
                        parameters: [],
                        name: (() => {
                            if (BabelType.isIdentifier(child.key)){
                                return child.key.name;
                            }
                            if (BabelType.isBigIntLiteral(child.key)){
                                return child.key.value;
                            }
                            if (BabelType.isStringLiteral(child.key)){
                                return child.key.value;
                            }
                            if (BabelType.isNumericLiteral(child.key)){
                                return child.key.value.toString();
                            }

                            console.trace(`Unexpected node with type ${child.key.type} in ${filename} while trying to to get the name of a method in ${classDeclaration.name} class`)
                            return "";
                        })()
                    };
                    classDeclaration.children.push(methodDeclaration.id);
                    project.declarations[methodDeclaration.id] = methodDeclaration;

                    if (child.typeParameters){
                        if (BabelType.isTypeParameterDeclaration(child.typeParameters) || BabelType.isTSTypeParameterDeclaration(child.typeParameters)){
                            for(const parameter of child.typeParameters.params){
                                const typeParameter = await processTypeParameter([
                                    ... nodes,
                                    classNode.body,
                                    child,
                                    child.typeParameters,
                                    parameter,
                                ])
                                methodDeclaration.typeParameters.push(typeParameter.id)
                            }
                        }else{
                            console.trace(`Unexpected node with type ${child.typeParameters.type} in ${filename} while trying to to process the type parameter in the method ${classDeclaration.name}.${methodDeclaration.name}`)
                        }
                    }

                    for(const parameter of child.params){
                        if (BabelType.isIdentifier(parameter)){
                            const constraint = await processAnnotation([
                                ... nodes,
                                classNode.body,
                                child,
                                parameter,
                                parameter.typeAnnotation
                            ])

                            const parameterDeclaration: ParameterDeclaration = {
                                type: TYPE.PARAMETER,
                                id: randomUUID(),
                                optional: parameter.optional ?? false,
                                spread: false,
                                name: parameter.name,
                                constraint: constraint,
                                default: null,
                            };

                            methodDeclaration.parameters.push(parameterDeclaration.id);
                            project.declarations[parameterDeclaration.id] = parameterDeclaration;
                            continue;
                        }
                        if (BabelType.isRestElement(parameter)){
                            const left = parameter.argument;

                            if (BabelType.isIdentifier(left)){
                                const type = left.typeAnnotation ?? parameter.typeAnnotation;
                                const constraint: Annotation = type ? await processAnnotation([
                                    ... nodes,
                                    classNode.body,
                                    child,
                                    parameter,
                                    left,
                                    type
                                ]) : {
                                    type: ANNOTATION.ARRAY,
                                    value: {
                                        type: ANNOTATION.ANY
                                    }
                                }

                                const parameterDeclaration: ParameterDeclaration = {
                                    type: TYPE.PARAMETER,
                                    id: randomUUID(),
                                    optional: parameter.optional ?? false,
                                    spread: true,
                                    name: left.name,
                                    default: null,
                                    constraint: constraint
                                };

                                methodDeclaration.parameters.push(parameterDeclaration.id);
                                project.declarations[parameterDeclaration.id] = parameterDeclaration;

                                continue;
                            }

                            console.trace(`Unexpected node with type ${left.type} in ${filename} while trying to to process the left side of the spread argument in the method ${classDeclaration.name}.${methodDeclaration.name}`)

                            continue;
                        }

                        console.trace(`Unexpected node with type ${parameter.type} in ${filename} while trying to to process the argument in the method ${classDeclaration.name}.${methodDeclaration.name}`)
                    }

                    methodDeclaration.result = await processAnnotation([
                        ... nodes,
                        classNode.body,
                        child,
                        child.returnType
                    ])

                    continue;
                }
                if (BabelType.isClassProperty(child)){
                    const doc = parseDoc((child.leadingComments ?? []).map(i => i.value).join(""));

                    const propertyDeclaration: PropertyDeclaration = {
                        categories: doc.categories,
                        description: doc.description,
                        hidden: doc.hidden,
                        ignore: doc.ignore,
                        readonly: doc.readonly,
                        remarks: doc.remarks,
                        see: doc.see,
                        summary: doc.summary,
                        todo: doc.todo,
                        version: doc.version,

                        type: TYPE.PROPERTY,
                        default: null,
                        constraint: null,
                        id: randomUUID(),
                        abstract: child.abstract ?? false,
                        scope: doc.scope ?? (() => {
                            const access = child.accessibility;

                            if (access === "private"){
                                return SCOPE.PRIVATE
                            }

                            if (access === "protected"){
                                return SCOPE.PROTECTED
                            }

                            return SCOPE.PUBLIC;
                        })(),

                        name: (() => {
                            if (BabelType.isIdentifier(child.key)){
                                return child.key.name;
                            }
                            if (BabelType.isBigIntLiteral(child.key)){
                                return child.key.value;
                            }
                            if (BabelType.isStringLiteral(child.key)){
                                return child.key.value;
                            }
                            if (BabelType.isNumericLiteral(child.key)){
                                return child.key.value.toString();
                            }

                            console.trace(`Unexpected node with type ${child.key.type} in ${filename} while trying to to get the name of a method in ${classDeclaration.name} class`)
                            return "";
                        })()
                    };
                    classDeclaration.children.push(propertyDeclaration.id);
                    project.declarations[propertyDeclaration.id] = propertyDeclaration;


                    if (child.typeAnnotation){
                        propertyDeclaration.constraint = await processAnnotation([
                            ... nodes,
                            classNode.body,
                            child,
                            child.typeAnnotation
                        ])
                    }
                    if (child.value){
                        propertyDeclaration.default = await processAnnotation([
                            ... nodes,
                            classNode.body,
                            child,
                            child.value
                        ])
                    }

                    continue
                }

                console.trace(`Unexpected node with type ${child.type} in ${filename} while trying to process class ${classDeclaration.name} declaration body`)
            }

            return classDeclaration;
        }
        const processTypeDeclaration = async (nodes: NODES<BabelType.TSTypeAliasDeclaration>): Promise<TypeDeclaration> => {
            const typeNode = nodes[nodes.length - 1] as BabelType.TSTypeAliasDeclaration;

            const _cache = __cache.types.get(typeNode);
            if (_cache){
                return _cache;
            }

            const typeDeclaration: TypeDeclaration = {
                typeParameters: [],
                constraint: null,
                name: typeNode.id.name,
                type: TYPE.TYPE,
                id: randomUUID()
            };
            __cache.types.set(typeNode,typeDeclaration);
            project.declarations[typeDeclaration.id] = typeDeclaration;

            if (typeNode.typeParameters){
                for(const parameter of typeNode.typeParameters.params){
                    const typeParameter = await processTypeParameter([
                        ... nodes,
                        typeNode.typeParameters,
                        parameter,
                    ])
                    typeDeclaration.typeParameters.push(typeParameter.id)
                }
            }

            typeDeclaration.constraint = await processAnnotation([
                ... nodes,
                typeNode.typeAnnotation,
            ])

            return typeDeclaration;
        }
        const processIdentifierAnnotation = async (nodes: NODES<BabelType.Identifier>): Promise<Annotation> => {
            const id = nodes[nodes.length - 1] as BabelType.Identifier;

            if (id.name === "undefined"){
                return {
                    type: ANNOTATION.UNDEFINED
                }
            }

            const item = async (nodes: NODES) => {
                const node = nodes[nodes.length - 1];

                if (!BabelType.isNode(node)){
                    return null;
                }

                if (
                    BabelType.isExportDefaultDeclaration(node)
                ){
                    return null;
                }


                if (BabelType.isExportNamedDeclaration(node)){
                    return item([
                        ... nodes,
                        node.declaration
                    ]);
                }
                if (BabelType.isTSTypeAliasDeclaration(node)){
                    if (node.id.name !== id.name){
                        return null;
                    }
                    const declaration = await processTypeDeclaration([
                        ... nodes,
                        node,
                    ]);

                    return {
                        type: ANNOTATION.REFERENCE,
                        name: declaration.name,
                        ref: declaration.id,
                    }
                }
                if (BabelType.isClassDeclaration(node)){
                    if (node.id.name !== id.name){
                        return null;
                    }
                    const declaration = await processClassDeclaration([
                        ... nodes,
                        node,
                    ]);

                    return {
                        type: ANNOTATION.REFERENCE,
                        name: declaration.name,
                        ref: declaration.id,
                    }
                }
                if (BabelType.isImportDeclaration(node)){
                    for(const specifier of node.specifiers){
                        if (BabelType.isImportDefaultSpecifier(specifier)){
                            if (specifier.local.name === id.name){
                                const source = node.source.value;
                                const resolved = require.resolve(Path.isAbsolute(source) ? source : Path.join(Path.dirname(filename),source));


                                const ref: AnyAnnotation = {
                                    type: ANNOTATION.ANY
                                }
                                await processFile(resolved,(_file) => {
                                    if (!("default" in _file)){
                                        console.trace(`Unexpected to not find default export from ${resolved} because the identifier ${id.name} in ${filename} is an import default`)
                                        return;
                                    }

                                    const _ref = _file.default
                                    for(const key in _ref){
                                        ref[key] = _ref[key];
                                    }
                                })
                                return ref
                            }

                            continue;
                        }
                        if (BabelType.isImportSpecifier(specifier)){
                            if (specifier.local.name === id.name){
                                const imported = BabelType.isIdentifier(specifier.imported) ? specifier.imported.name : specifier.imported.value;

                                const source = node.source.value;
                                const resolved = require.resolve(Path.isAbsolute(source) ? source : Path.join(Path.dirname(filename),source));

                                const ref: AnyAnnotation = {
                                    type: ANNOTATION.ANY
                                }
                                await processFile(resolved,(_file) => {
                                    if (!(imported in _file)){
                                        console.trace(`Unexpected to not find ${imported} export from ${resolved} because the identifier ${id.name} in ${filename} is an import ${imported}`)
                                        return;
                                    }

                                    const _ref = _file[imported]
                                    for(const key in _ref){
                                        ref[key] = _ref[key];
                                    }
                                })
                                return ref
                            }
                            continue;
                        }

                        console.trace(`Unexpected specifier with type ${specifier.type} in ${filename} while trying to process identifier annotation`)
                    }

                    return null;
                }

                console.trace(`Unexpected node with type ${node.type} in ${filename} while trying to process identifier annotation`)
            }

            for(let i=(nodes.length - 1);i>=0;i--){
                const node = nodes[i];

                if (BabelType.isClassDeclaration(node) || BabelType.isTSTypeAliasDeclaration(node) || BabelType.isClassMethod(node)){
                    if (BabelType.isTypeParameterDeclaration(node.typeParameters) || BabelType.isTSTypeParameterDeclaration(node.typeParameters)){
                        for(const type of node.typeParameters.params){
                            if (type.name === id.name){

                                const typeParameterDeclaration = await processTypeParameter([
                                    nodes[0],
                                    nodes[1],
                                    ... nodes.slice(2,i),
                                    type
                                ]);

                                return {
                                    type: ANNOTATION.REFERENCE,
                                    ref: typeParameterDeclaration.id,
                                    name: type.name
                                };
                            }
                        }
                    }
                }

                if (!BabelType.isBlock(node)){
                    continue
                }

                for(const statement of node.body){
                    const test = await item([
                        nodes[0],
                        nodes[1],
                        ... nodes.slice(2,i),
                        statement
                    ]);

                    if (test !== null){
                        return test;
                    }
                }
            }

            console.trace(`We cannot find the identifier ${id.name} in ${filename} while trying to process annotation: `+nodes.map(node => node?.type))
            return {
                type: ANNOTATION.ANY
            }
        }

        const _file = project.files[filename];
        if (_file){
            return callback(_file);
        }

        const _callbacks = events[filename];
        if (_callbacks){
            _callbacks.push(callback);
            return;
        }

        const callbacks: ((file: File) => void)[] = [];
        events[filename] = callbacks;

        const file: File = {};

        const buffer = await fs.promises.readFile(filename);
        const ast: BabelType.File|null = await BabelCore.parseAsync(buffer.toString(), {
            presets: ["@babel/preset-typescript"],
            filename: filename,
        });
        if (ast === null){
            throw new Error("Failed to parse "+filename+". Babel results `null`.");
        }

        for(const node of ast.program.body){
            if (
                BabelType.isTSTypeAliasDeclaration(node)
                ||
                BabelType.isImportDeclaration(node)
                ||
                BabelType.isClassDeclaration(node)
            ){
                continue
            }
            if (BabelType.isExportDefaultDeclaration(node)){
                const declaration = node.declaration

                file["default"] = await processAnnotation([ast,ast.program,node,declaration]);
                continue;
            }
            if (BabelType.isExportNamedDeclaration(node)){
                if (BabelType.isDeclaration(node.declaration)){
                    const declaration = node.declaration
                    const annotation = await processAnnotation([ast,ast.program,node,declaration]);

                    if (annotation.type === ANNOTATION.REFERENCE){
                        file[annotation.name ?? ""] = annotation;
                    }
                    continue
                }

                for(const specifier of node.specifiers){
                    console.trace(`Unexpected specifier with type ${specifier.type} in ${filename} while trying to process export declaration`)
                }

                continue;
            }

            console.trace(`Unexpected node with type ${node.type} in ${entrypoint} while trying to find export declaration`)
        }

        project.files[filename] = file;

        for(const item of callbacks){
            item(file);
        }
        callback(file);
    }
    await new Promise((resolve) => {
        processFile(entrypoint,resolve)
    });

    console.log(JSON.stringify(project,null,"\t"));
})();