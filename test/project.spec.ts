import testOneToMany from "./__LIB/one-to-many";
import flat from "../result/util/deep-array/flat";
import Mock = jest.Mock;
import defaultOneToManyError from "./__LIB/one-to-many/util/default-error";
import OutOfRange from "../result/error/out-of-range";
import UnlinkedValue from "../result/error/unlinked-value";
import Project from "../result/project";
import Action from "../result/action";
import Model from "../result/model";
import Route from "../result/route";


jest.mock("../result/util/deep-array/flat",() => {
    const flat = jest.requireActual("../result/util/deep-array/flat").default;
    return {
        __esModule: true,
        default: jest.fn().mockImplementation(flat)
    }
});

describe("Project", () => {
    testOneToMany({
        flat: () => flat as Mock,
        error: defaultOneToManyError(UnlinkedValue),
        parent: {
            class: Project
        },
        children: {
            class: Action
        },
    })
    testOneToMany({
        flat: () => flat as Mock,
        parent: {
            class: Project,
        },
        children: {
            class: Model
        },
        error: defaultOneToManyError(UnlinkedValue),
    })
    testOneToMany({
        flat: () => flat as Mock,
        parent: {
            class: Project,
            property: "routes"
        },
        children: {
            class: Route,
        },
        error: defaultOneToManyError(UnlinkedValue),
    })
})