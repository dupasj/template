import testField from "../__LIB/field";
import OutOfRange from "../../result/error/out-of-range";
import UndefinedValue from "../../result/error/undefined-value";

describe("OutOfRange", () => {
    testField({
        class: OutOfRange,
        property: "property"
    })
    testField({
        class: OutOfRange,
        property: "classname"
    })
    testField({
        class: OutOfRange,
        property: "position"
    })
    testField({
        class: UndefinedValue,
        property: "method"
    })
});