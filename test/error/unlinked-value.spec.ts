import testField from "../__LIB/field";
import UnlinkedValue from "../../result/error/unlinked-value";

describe("UnlinkedValue", () => {
    testField({
        class: UnlinkedValue,
        property: "property"
    })
    testField({
        class: UnlinkedValue,
        property: "classname"
    })
    testField({
        class: UnlinkedValue,
        property: "wanted_classname"
    })
    testField({
        class: UnlinkedValue,
        property: "method"
    })
});