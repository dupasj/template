import testField from "../__LIB/field";
import UndefinedValue from "../../result/error/undefined-value";

describe("UndefinedValue", () => {
    testField({
        class: UndefinedValue,
        property: "property"
    })
    testField({
        class: UndefinedValue,
        property: "classname"
    })
    testField({
        class: UndefinedValue,
        property: "method"
    })
});