import testOneToMany from "../__LIB/one-to-many";
import {ProjectRoute, ProjectRouteTest} from "../../result/project";
import UnlinkedValue from "../../result/error/unlinked-value";
import testField from "../__LIB/field";
import flat from "../../result/util/deep-array/flat";
import Mock = jest.Mock;
import defaultOneToManyError from "../__LIB/one-to-many/util/default-error";

jest.mock("../../result/util/deep-array/flat",() => {
    const flat = jest.requireActual("../../result/util/deep-array/flat").default;
    return {
        __esModule: true,
        default: jest.fn().mockImplementation(flat)
    }
});

describe("Route", () => {
    testOneToMany({
        flat: () => (flat as Mock),
        parent: {
            class: ProjectRoute,
            property: "tests"
        },
        children: {
            class: ProjectRouteTest,
            property: "route"
        },
        error: defaultOneToManyError(UnlinkedValue)
    })
    testField({
        class: ProjectRoute,
        property: "key"
    })
})