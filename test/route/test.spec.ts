import testField from "../__LIB/field";
import RouteTest from "../../result/route/test";

describe("RouteTest", () => {
    testField({
        class: RouteTest,
        property: "method"
    })
    testField({
        class: RouteTest,
        property: "pattern"
    })
})