import TestListParameter from "./util/type/parameter/general";
import parameterList from "./util/transform-parameter";
import testListAdd from "./util/add";
import testListRemove from "./util/remove";
import testListSize from "./util/size";
import testListTruncate from "./util/truncate";
import testListContain from "./util/contain";

const testList = (option: TestListParameter) => {
    const computed = parameterList(option);
    const name = computed.class.name;

    describe(`The methods related to ${name}.${computed.property}`, () => {
        if (computed.overrider.add){
            testListAdd(computed.overrider.add)
        }
        if (computed.overrider.remove){
            testListRemove(computed.overrider.remove)
        }
        if (computed.overrider.size){
            testListSize(computed.overrider.size)
        }
        if (computed.overrider.truncate){
            testListTruncate(computed.overrider.truncate)
        }
        if (computed.overrider.contain){
            testListContain(computed.overrider.contain)
        }
    });
}

export default testList;