import TestListTruncateOption from "./option";
import TestListItem from "../../../../type/computed/item";

interface TestListTruncate extends TestListItem<TestListTruncateOption>{

}

export default TestListTruncate;