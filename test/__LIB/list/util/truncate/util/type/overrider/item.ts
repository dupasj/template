import TestListTruncateOption from "../computed/option";
import TestListItemOverrider from "../../../../type/overrider/item";
import TestListTruncateOptionOverrider from "./option";

interface TestListTruncateOverrider extends TestListItemOverrider<TestListTruncateOptionOverrider,TestListTruncateOption>{

}

export default TestListTruncateOverrider;