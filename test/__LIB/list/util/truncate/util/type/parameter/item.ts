import TestListTruncateOptionParameter from "./option";
import TestListItemParameter from "../../../../type/parameter/item";
import TestListTruncateOption from "../computed/option";

interface TestListTruncateParameter extends TestListItemParameter<TestListTruncateOptionParameter,TestListTruncateOption>{

}

export default TestListTruncateParameter;