import TestListTruncateParameter from "./type/parameter/item";
import TestListTruncate from "./type/computed/item";
import parameterListBase from "../../transform-base";

const parameterListTruncate = (input: TestListTruncateParameter): TestListTruncate => {
    const base = parameterListBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterListTruncate;