import TestListTruncateOverrider from "./type/overrider/item";
import TestListTruncate from "./type/computed/item";
import TestListBase from "../../type/computed/base";
import overriderListBase from "../../transform-overrider";

const overriderListTruncate = (base: TestListBase, overrider: TestListTruncateOverrider): TestListTruncate => {
    const computed = overriderListBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderListTruncate;