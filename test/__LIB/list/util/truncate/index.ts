import TestListTruncateParameter from "./util/type/parameter/item";
import parameterListTruncate from "./util/transform-parameter";

const testListTruncate = (input: TestListTruncateParameter) => {
    const computed = parameterListTruncate(input);

    const name = computed.class.name;
    const property = computed.property;
    const remove = computed.method.remove;
    const unsafe = computed.method.list.unsafe;
    const truncate = computed.method.truncate;

    if (typeof truncate !== "string"){
        return;
    }

    describe(`Test the ${name}.${truncate}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.builder(computed);

            expect(instance[truncate]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.builder(computed);

            expect(typeof instance[truncate]).toBe("function");
        });
        it(`The method return himself`, () => {
            const instance = computed.builder(computed);

            expect(instance[truncate]()).toBe(instance);
        });

        it("The method truncate the array property", () => {
            const instance = computed.builder(computed);

            const length = Math.floor(10 + Math.random() * 50);
            const value = computed.generator(computed);
            for(let i=0;i<length;i++){
                instance[property].push(value);
            }

            instance[truncate]();

            expect(instance[property].length).toBe(0);
        });
        if (typeof remove === "string"){
            it("The method use the "+name+"."+remove+" method to remove all the items", () => {
                const instance = computed.builder(computed);

                const length = Math.floor(10 + Math.random() * 50);
                const value = computed.generator(computed);
                for(let i=0;i<length;i++){
                    instance[property].push(value);
                }

                const spy = jest.spyOn(instance,remove)

                instance[truncate]();

                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenNthCalledWith(1,instance[property]);
            });
            if (typeof unsafe === "string"){
                it("The method use the "+name+"."+remove+" method to remove all the items resulted by the "+name+"."+unsafe+" method", () => {
                    const instance = computed.builder(computed);

                    const _property = {
                        length: Math.floor(10 + Math.random() * 50),
                        value: instance[property],
                    }
                    for(let i=0;i<_property.length;i++){
                        _property.value.push(computed.generator(computed));
                    }

                    const mock = {
                        length: Math.floor(10 + Math.random() * 50),
                        value: instance[property],
                    }
                    for(let i=0;i<mock.length;i++){
                        mock.value.push(computed.generator(computed));
                    }
                    if (mock.length === _property.length){
                        mock.value.length ++;
                        mock.value.push(computed.generator(computed));
                    }

                    instance[unsafe] = jest.fn().mockReturnValue(mock.value);

                    const spy = jest.spyOn(instance,remove)

                    instance[truncate]();

                    expect(spy).toHaveBeenCalled();
                    expect(spy).toHaveBeenNthCalledWith(1,mock.value);
                });
            }
        }

        computed.after(computed);
    });
};

export default testListTruncate;