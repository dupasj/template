import Mock = jest.Mock;
import TestListItem from "../computed/item";
import TestListBase from "../computed/base";

interface TestListBaseOverrider {
    property?: string,
    class?: {new(): Object},
    is_static?: boolean,
    generator?(option: TestListBase): any,
    builder?(option: TestListBase): {[key: string]: any},
    flat?: ((option: TestListBase) => Mock)|Mock|false|null,
    method?: {
        add?: string|null|false,
        remove?: string|null|false,
        contain?: string|null|false,
        size?: string|null|false,
        truncate?: string|null|false,
        list?: {
            unsafe?: string|null|false,
            getter?: string|null|false,
        }|false|null,
        item?: {
            unsafe?: string|null|false,
            getter?: string|null|false,
        }|false|null
    },
    before?: (option: TestListItem) => void,
    after?: (option: TestListItem) => void,
}

export default TestListBaseOverrider;