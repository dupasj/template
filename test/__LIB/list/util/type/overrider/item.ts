import TestListBaseOverrider from "./base";
import TestListItem from "../computed/item";

interface TestListItemOverrider<Option extends { [key: string]: any } = {},ComputedOption extends { [key: string]: any } = Option> extends TestListBaseOverrider {
    option?: Option,

    before?: (option: TestListItem<ComputedOption>) => void,
    after?: (option: TestListItem<ComputedOption>) => void,
}

export default TestListItemOverrider;