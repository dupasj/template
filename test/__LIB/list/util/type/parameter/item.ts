import TestListBaseParemeter from "./base";
import TestListItem from "../computed/item";

interface TestListItemParameter<Option extends { [key: string]: any } = {},ComputedOption extends { [key: string]: any } = Option> extends TestListBaseParemeter {
    option?: Option,

    before?: (option: TestListItem<ComputedOption>) => void,
    after?: (option: TestListItem<ComputedOption>) => void,
}

export default TestListItemParameter;