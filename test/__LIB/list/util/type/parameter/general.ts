import TestListBaseParameter from "./base";
import TestListContainOverrider from "../../contain/util/type/overrider/item";
import TestListSizeOverrider from "../../size/util/type/overrider/item";
import TestListAddOverrider from "../../add/util/type/overrider/item";
import TestListTruncateOverrider from "../../truncate/util/type/overrider/item";
import TestListRemoveOverrider from "../../remove/util/type/overrider/item";
import TestList from "../computed/general";
import Mock = jest.Mock;

interface TestListParameter extends TestListBaseParameter {
    overrider?: {
        contain?: TestListContainOverrider|false|null,
        size?: TestListSizeOverrider|false|null,
        add?: TestListAddOverrider|false|null,
        truncate?: TestListTruncateOverrider|false|null,
        remove?: TestListRemoveOverrider|false|null,
    },
    before?: (option: TestList) => void,
    after?: (option: TestList) => void,
}

export default TestListParameter;