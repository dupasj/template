import Mock = jest.Mock;
import TestListBase from "../computed/base";
import TestListItem from "../computed/item";

interface TestListBaseParameter {
    property: string,
    class: {new(): Object},
    is_static?: boolean,
    flat: ((option: TestListItem) => Mock)|Mock|false|null,
    generator?(option: TestListItem): any,
    builder?(option: TestListItem): {[key: string]: any},
    method?: {
        list?: {
            unsafe?: string|false|null,
            getter?: string|false|null,
        }|false|null,
        item?: {
            unsafe?: string|false|null,
            getter?: string|false|null,
        }|false|null,
        size?: string|false|null,
        add?: string|false|null,
        remove?: string|false|null,
        truncate?: string|false|null,
        contain?: string|false|null,
    },
    before?: (option: TestListBase) => void,
    after?: (option: TestListBase) => void,
}

export default TestListBaseParameter;