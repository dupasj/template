import TestListBase from "./base";

interface TestListItem<Option extends { [key: string]: any } = {}> extends TestListBase {
    option: Option,

    before: (option: TestListItem<Option>) => void,
    after: (option: TestListItem<Option>) => void,
}

export default TestListItem;