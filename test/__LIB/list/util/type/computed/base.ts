import Mock = jest.Mock;
import TestListItem from "./item";

interface TestListBase {
    property: string,
    class: {new(): Object},
    is_static: boolean,
    generator(option: TestListItem): any,
    builder(option: TestListItem): {[key: string]: any},
    flat: ((option: TestListItem) => Mock)|false,
    method: {
        list: {
            unsafe: string|false,
            getter: string|false,
        },
        item: {
            unsafe: string|false,
            getter: string|false,
        },
        size: string|false,
        add: string|false,
        remove: string|false,
        truncate: string|false,
        contain: string|false,
    },
    before: (option: TestListBase) => void,
    after: (option: TestListBase) => void,
}

export default TestListBase;