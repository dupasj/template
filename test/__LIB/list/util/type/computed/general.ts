import TestListBase from "./base";
import TestListContain from "../../contain/util/type/computed/item";
import TestListSize from "../../size/util/type/computed/item";
import TestListAdd from "../../add/util/type/computed/item";
import TestListTruncate from "../../truncate/util/type/computed/item";
import TestListRemove from "../../remove/util/type/computed/item";

interface TestList extends TestListBase {
    overrider: {
        contain: TestListContain|false,
        size: TestListSize|false,
        add: TestListAdd|false,
        truncate: TestListTruncate|false,
        remove: TestListRemove|false,
    },

    before: (option: TestList) => void,
    after: (option: TestList) => void,
}

export default TestList;