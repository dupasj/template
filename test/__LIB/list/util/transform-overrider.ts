import TestListBaseOverrider from "./type/overrider/base";
import TestListBase from "./type/computed/base";

const overriderListBase = (base: TestListBase, overrider: TestListBaseOverrider): TestListBase => {
    return {
        class: overrider.class ?? base.class,
        builder: overrider.builder ?? base.builder,
        generator: overrider.builder ?? base.generator,
        flat: (() => {
            const flat = overrider.flat;

            if (flat === false  || flat === null){
                return false;
            }

            return flat ?? base.flat;
        })(),
        is_static: overrider.is_static ?? base.is_static,
        method: {
            list: (() => {
                const list = overrider?.method?.list;
                if (list === null || list === false){
                    return {
                        unsafe: false,
                        getter: false,
                    }
                }

                return {
                    getter: (() => {
                        const getter = list?.getter;
                        if (getter === null || getter === false){
                            return false;
                        }

                        return getter ?? base.method.list.getter;
                    })(),
                    unsafe: (() => {
                        const unsafe = list?.unsafe;
                        if (unsafe === null || unsafe === false){
                            return false;
                        }

                        return unsafe ?? base.method.list.unsafe;
                    })(),
                }
            })(),
            item: (() => {
                const item = overrider?.method?.item;
                if (item === null || item === false){
                    return {
                        unsafe: false,
                        getter: false,
                    }
                }

                return {
                    getter: (() => {
                        const getter = item?.getter;
                        if (getter === null || getter === false){
                            return false;
                        }

                        return getter ?? base.method.item.getter;
                    })(),
                    unsafe: (() => {
                        const unsafe = item?.unsafe;
                        if (unsafe === null || unsafe === false){
                            return false;
                        }

                        return unsafe ?? base.method.item.unsafe;
                    })(),
                }
            })(),
            size: (() => {
                const size = overrider?.method?.size;
                if (size === null || size === false){
                    return false;
                }

                return size ?? base.method.size;
            })(),
            contain: (() => {
                const contain = overrider?.method?.size;
                if (contain === null || contain === false){
                    return false;
                }

                return contain ?? base.method.contain;
            })(),
            remove: (() => {
                const remove = overrider?.method?.remove;
                if (remove === null || remove === false){
                    return false;
                }

                return remove ?? base.method.remove;
            })(),
            add: (() => {
                const add = overrider?.method?.add;
                if (add === null || add === false){
                    return false;
                }

                return add ?? base.method.add;
            })(),
            truncate: (() => {
                const truncate = overrider?.method?.truncate;
                if (truncate === null || truncate === false){
                    return false;
                }

                return truncate ?? base.method.truncate;
            })(),
        },
        property: overrider.property ?? base.property,
        after: overrider.after ?? ((option: any) => {}),
        before: overrider.before ?? ((option: any) => {}),
    }
};

export default overriderListBase;