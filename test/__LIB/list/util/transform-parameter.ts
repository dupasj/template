import TestList from "./type/computed/general";
import TestListParameter from "./type/parameter/general";
import parameterListBase from "./transform-base";
import overriderListContain from "./contain/util/transform-overrider";
import overriderListSize from "./size/util/transform-overrider";
import overriderListAdd from "./add/util/transform-overrider";
import overriderListRemove from "./remove/util/transform-overrider";
import overriderListTruncate from "./truncate/util/transform-overrider";

const parameterList = (base: TestListParameter): TestList => {
    const temporary = parameterListBase(base);

    return {
        ... temporary,
        overrider: {
            size: (() => {
                const size = base.overrider?.size;

                if (size === false || size === null){
                    return false;
                }

                return overriderListSize(temporary,size ?? {})
            })(),
            contain: (() => {
                const contain = base.overrider?.contain;

                if (contain === false || contain === null){
                    return false;
                }

                return overriderListContain(temporary,contain ?? {})
            })(),
            remove: (() => {
                const remove = base.overrider?.remove;

                if (remove === false || remove === null){
                    return false;
                }

                return overriderListRemove(temporary,remove ?? {})
            })(),
            add: (() => {
                const add = base.overrider?.add;

                if (add === false || add === null){
                    return false;
                }

                return overriderListAdd(temporary,add ?? {})
            })(),
            truncate: (() => {
                const truncate = base.overrider?.truncate;

                if (truncate === false || truncate === null){
                    return false;
                }

                return overriderListTruncate(temporary,truncate ?? {})
            })(),
        },
    }
};

export default parameterList;