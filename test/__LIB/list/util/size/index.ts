import TestListSizeParameter from "./util/type/parameter/item";
import parameterListSize from "./util/transform-parameter";

const testListSize = (input: TestListSizeParameter) => {
    const computed = parameterListSize(input);

    const name = computed.class.name;
    const size = computed.method.size;
    const unsafe = computed.method.list.unsafe;
    const property = computed.property;

    if (typeof size !== "string"){
        return;
    }

    describe(`Test the ${name}.${size}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.builder(computed);

            expect(instance[size]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.builder(computed);

            expect(typeof instance[size]).toBe("function");
        });

        it("The method return the property array size", () => {
            const instance = computed.builder(computed);

            const length = Math.floor(10 + Math.random() * 50);
            const value = computed.generator(computed);
            for(let i=0;i<length;i++){
                instance[property].push(value);
            }

            expect(instance[size]()).toBe(instance[property].length);
        });

        if (typeof unsafe === "string"){
            it("The method return the expect copy of the result of "+name+"."+unsafe+" value result", () => {
                const instance = computed.builder(computed);

                const _property = {
                    length: Math.floor(10 + Math.random() * 50),
                    value: instance[property],
                }
                for(let i=0;i<_property.length;i++){
                    _property.value.push(computed.generator(computed));
                }

                const mock = {
                    length: Math.floor(10 + Math.random() * 50),
                    value: [] as any[],
                }
                for(let i=0;i<mock.length;i++){
                    mock.value.push(computed.generator(computed));
                }
                if (mock.length === _property.length){
                    mock.value.length ++;
                    mock.value.push(computed.generator(computed));
                }

                instance[unsafe] = jest.fn().mockReturnValue(mock.value);

                const result = instance[size]();

                expect(result).not.toBe(_property.length);
                expect(result).toBe(mock.length);
            });
        }

        computed.after(computed);
    });
};

export default testListSize;