import TestListSizeOption from "../computed/option";
import TestListItemOverrider from "../../../../type/overrider/item";
import TestListSizeOptionOverrider from "./option";

interface TestListSizeOverrider extends TestListItemOverrider<TestListSizeOptionOverrider,TestListSizeOption>{

}

export default TestListSizeOverrider;