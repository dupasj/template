import TestListSizeOptionParameter from "./option";
import TestListItemParameter from "../../../../type/parameter/item";
import TestListSizeOption from "../computed/option";

interface TestListSizeParameter extends TestListItemParameter<TestListSizeOptionParameter,TestListSizeOption>{

}

export default TestListSizeParameter;