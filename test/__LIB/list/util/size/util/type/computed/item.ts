import TestListSizeOption from "./option";
import TestListItem from "../../../../type/computed/item";

interface TestListSize extends TestListItem<TestListSizeOption>{

}

export default TestListSize;