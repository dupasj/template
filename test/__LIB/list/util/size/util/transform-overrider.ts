import TestListSizeOverrider from "./type/overrider/item";
import TestListSize from "./type/computed/item";
import TestListBase from "../../type/computed/base";
import overriderListBase from "../../transform-overrider";

const overriderListSize = (base: TestListBase, overrider: TestListSizeOverrider): TestListSize => {
    const computed = overriderListBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderListSize;