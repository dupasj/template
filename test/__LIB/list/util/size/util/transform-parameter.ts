import TestListSizeParameter from "./type/parameter/item";
import TestListSize from "./type/computed/item";
import parameterListBase from "../../transform-base";

const parameterListSize = (input: TestListSizeParameter): TestListSize => {
    const base = parameterListBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterListSize;