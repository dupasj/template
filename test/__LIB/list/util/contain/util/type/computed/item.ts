import TestListContainOption from "./option";
import TestListItem from "../../../../type/computed/item";

interface TestListContain extends TestListItem<TestListContainOption>{

}

export default TestListContain;