import TestListContainOptionParameter from "./option";
import TestListItemParameter from "../../../../type/parameter/item";
import TestListContainOption from "../computed/option";

interface TestListContainParameter extends TestListItemParameter<TestListContainOptionParameter,TestListContainOption>{

}

export default TestListContainParameter;