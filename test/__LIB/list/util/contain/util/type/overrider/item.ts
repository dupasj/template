import TestListContainOption from "../computed/option";
import TestListItemOverrider from "../../../../type/overrider/item";
import TestListContainOptionOverrider from "./option";

interface TestListContainOverrider extends TestListItemOverrider<TestListContainOptionOverrider,TestListContainOption>{

}

export default TestListContainOverrider;