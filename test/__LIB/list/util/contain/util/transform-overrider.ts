import TestListContainOverrider from "./type/overrider/item";
import TestListContain from "./type/computed/item";
import TestListBase from "../../type/computed/base";
import overriderListBase from "../../transform-overrider";

const overriderListContain = (base: TestListBase, overrider: TestListContainOverrider): TestListContain => {
    const computed = overriderListBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderListContain;