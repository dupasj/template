import TestListContainParameter from "./type/parameter/item";
import TestListContain from "./type/computed/item";
import parameterListBase from "../../transform-base";

const parameterListContain = (input: TestListContainParameter): TestListContain => {
    const base = parameterListBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterListContain;