import TestListRemoveOverrider from "./type/overrider/item";
import TestListRemove from "./type/computed/item";
import TestListBase from "../../type/computed/base";
import overriderListBase from "../../transform-overrider";

const overriderListRemove = (base: TestListBase, overrider: TestListRemoveOverrider): TestListRemove => {
    const computed = overriderListBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderListRemove;