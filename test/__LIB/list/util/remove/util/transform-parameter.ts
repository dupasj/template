import TestListRemoveParameter from "./type/parameter/item";
import TestListRemove from "./type/computed/item";
import parameterListBase from "../../transform-base";

const parameterListRemove = (input: TestListRemoveParameter): TestListRemove => {
    const base = parameterListBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterListRemove;