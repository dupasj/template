import TestListRemoveOption from "../computed/option";
import TestListItemOverrider from "../../../../type/overrider/item";
import TestListRemoveOptionOverrider from "./option";

interface TestListRemoveOverrider extends TestListItemOverrider<TestListRemoveOptionOverrider,TestListRemoveOption>{

}

export default TestListRemoveOverrider;