import TestListRemoveOption from "./option";
import TestListItem from "../../../../type/computed/item";

interface TestListRemove extends TestListItem<TestListRemoveOption>{

}

export default TestListRemove;