import TestListRemoveOptionParameter from "./option";
import TestListItemParameter from "../../../../type/parameter/item";
import TestListRemoveOption from "../computed/option";

interface TestListRemoveParameter extends TestListItemParameter<TestListRemoveOptionParameter,TestListRemoveOption>{

}

export default TestListRemoveParameter;