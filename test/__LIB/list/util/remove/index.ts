import TestListRemoveParameter from "./util/type/parameter/item";
import parameterListRemove from "./util/transform-parameter";
import SpyInstance = jest.SpyInstance;

const testListRemove = (input: TestListRemoveParameter) => {
    const computed = parameterListRemove(input);

    const name = computed.class.name;
    const property = computed.property;
    const remove = computed.method.remove;
    const contain = computed.method.contain;
    const flat = computed.flat;

    if (typeof remove !== "string"){
        return;
    }

    describe(`Test the ${name}.${remove}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.builder(computed);

            expect(instance[remove]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.builder(computed);

            expect(typeof instance[remove]).toBe("function");
        });

        it("The method remove the given item value", () => {
            const instance = computed.builder(computed);

            const length = Math.floor(10 + Math.random() * 50);
            for(let i=0;i<length;i++){
                const value = computed.generator(computed);
                instance[property].push(value);
            }

            const rand = instance[property][Math.floor(instance[property].length * Math.random())];

            instance[remove](rand);

            expect(instance[property]).not.toContain(rand);
        });

        describe("The method return himself", () => {
            it(`The method return himself when no value has been removed`, () => {
                const instance = computed.builder(computed);
                expect(instance[remove](computed.generator(computed))).toBe(instance);
            });
            it(`The method return himself when the given value has been removed`, () => {
                const instance = computed.builder(computed);

                const length = Math.floor(10 + Math.random() * 50);
                for(let i=0;i<length;i++){
                    const value = computed.generator(computed);
                    instance[property].push(value);
                }

                const rand = instance[property][Math.floor(instance[property].length * Math.random())];

                expect(instance[remove](rand)).toBe(instance);
            });
        })


        describe("indexOf and splice methods implementation",() => {
            describe("indexOf method implementation",() => {
                it("The method use indexOf method to remove the given item value", () => {
                    const instance = computed.builder(computed);

                    const length = Math.floor(10 + Math.random() * 50);
                    for(let i=0;i<length;i++){
                        const value = computed.generator(computed);
                        instance[property].push(value);
                    }

                    const index = Math.floor(instance[property].length * Math.random());
                    const rand = instance[property][index];

                    const spy = jest.spyOn(instance[property],"indexOf");

                    instance[remove](rand);

                    expect(spy).toHaveBeenCalledTimes(2);
                });
                it("The method doesn't use indexOf method if the value isn't in the list", () => {
                    const instance = computed.builder(computed);

                    const length = Math.floor(10 + Math.random() * 50);
                    for(let i=0;i<length;i++){
                        const value = computed.generator(computed);
                        instance[property].push(value);
                    }

                    const spy = jest.spyOn(instance[property],"indexOf");

                    instance[remove](computed.generator(computed));

                    expect(spy).not.toHaveBeenCalled();
                });

                if (typeof contain === "string"){
                    it("The method doesn't use indexOf method if the method "+contain+" result false", () => {
                        const instance = computed.builder(computed);

                        const length = Math.floor(10 + Math.random() * 50);
                        for(let i=0;i<length;i++){
                            const value = computed.generator(computed);
                            instance[property].push(value);
                        }

                        instance[contain] = jest.fn().mockReturnValue(false);
                        const spy = jest.spyOn(instance[property],"indexOf");

                        instance[remove](computed.generator(computed));

                        expect(spy).not.toHaveBeenCalled();
                    });
                }
            });

            describe("splice method implementation",() => {
                it("The method doesn't use splice method if the value isn't in the list", () => {
                    const instance = computed.builder(computed);

                    const length = Math.floor(10 + Math.random() * 50);
                    for(let i=0;i<length;i++){
                        const value = computed.generator(computed);
                        instance[property].push(value);
                    }

                    const spy = jest.spyOn(instance[property],"splice");

                    instance[remove](computed.generator(computed));

                    expect(spy).not.toHaveBeenCalled();
                });
                it("The method use splice method to remove the given item value", () => {
                    const instance = computed.builder(computed);

                    const length = Math.floor(10 + Math.random() * 50);
                    for(let i=0;i<length;i++){
                        const value = computed.generator(computed);
                        instance[property].push(value);
                    }

                    const index = Math.floor(instance[property].length * Math.random());
                    const rand = instance[property][index];

                    const spy = jest.spyOn(instance[property],"splice");

                    instance[remove](rand);

                    expect(spy).toHaveBeenCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1,index,1);
                });

                if (typeof contain === "string"){
                    it("The method doesn't use splice method if the method "+contain+" result false", () => {
                        const instance = computed.builder(computed);

                        const length = Math.floor(10 + Math.random() * 50);
                        for(let i=0;i<length;i++){
                            const value = computed.generator(computed);
                            instance[property].push(value);
                        }

                        instance[contain] = jest.fn().mockReturnValue(false);
                        const spy = jest.spyOn(instance[property],"splice");

                        instance[remove](computed.generator(computed));

                        expect(spy).not.toHaveBeenCalled();
                    });
                }
            })

            it("The method will remove the duplicated value", () => {
                const instance = computed.builder(computed);

                function shuffle(array) {
                    let currentIndex = array.length,  randomIndex;

                    while (currentIndex != 0) {
                        randomIndex = Math.floor(Math.random() * currentIndex);
                        currentIndex--;

                        // And swap it with the current element.
                        [array[currentIndex], array[randomIndex]] = [
                            array[randomIndex], array[currentIndex]];
                    }

                    return array;
                }

                const length = Math.floor(10 + Math.random() * 50);
                for(let i=0;i<length;i++){
                    const value = computed.generator(computed);
                    const r = Math.random() * 5 + 1;
                    for(let n=0;n<r;n++){
                        instance[property].push(value);
                    }
                }

                shuffle(instance[property]);

                const index = Math.floor(instance[property].length * Math.random());
                const rand = instance[property][index];

                instance[remove](rand);

                expect(instance[property]).not.toContain(rand);
            });
            it("The method will always use the splice method if the indexOf method doesn't return -1, the splice method use the indexOf result", () => {
                const instance = computed.builder(computed);

                const length = Math.floor(10 + Math.random() * 50);
                const indexes: any[] = [];
                for(let i=0;i<length;i++){
                    const value = computed.generator(computed);
                    indexes.push(value);
                }


                const value = computed.generator(computed);
                instance[property].push(value);

                const spy = {
                    index: jest.spyOn(instance[property],"indexOf"),
                    splice: jest.spyOn(instance[property],"splice"),
                };

                let count = 0;
                spy.index.mockImplementation(() => {
                    return indexes[count++] ?? -1;
                })

                instance[remove](value);
                expect(spy.index).toHaveBeenCalledTimes(length+1);
                expect(spy.splice).toHaveBeenCalledTimes(length);

                for(let i=0;i<indexes.length;i++){
                    expect(spy.index).toHaveBeenNthCalledWith(i+1,value);
                    expect(spy.splice).toHaveBeenNthCalledWith(i+1,indexes[i],1);
                }
            });
        })

        if (flat){
            describe("flat function implementation",() => {
                it("The method use the flat function pass the argument into it", () => {
                    const spy = flat(computed);
                    spy.mockClear();

                    const value = computed.generator(computed);
                    const instance = computed.builder(computed);

                    instance[remove](value);

                    expect(spy).toHaveBeenNthCalledWith(1, [value]);
                })

                if (typeof contain === "string") {
                    it("The method use the flat result function to loop remove function", () => {
                        const instance = computed.builder(computed);

                        const length = Math.floor(10 + Math.random() * 50);
                        const mock: any[] = [];

                        for (let i = 0; i < length; i++) {
                            const value = computed.generator(computed);
                            mock.push(value);
                        }

                        const spy = {
                            indexOf: jest.spyOn(instance[property], "indexOf"),
                            contain: jest.spyOn(instance, contain).mockReturnValue(true),
                            flat: (() => {
                                const spy = flat(computed);
                                spy.mockClear();

                                return spy.mockReturnValueOnce(mock);
                            })(),
                        }

                        instance[remove]();

                        expect(spy.contain).toHaveBeenCalledTimes(mock.length);
                        expect(spy.indexOf).toHaveBeenCalledTimes(mock.length);
                        mock.forEach((item, index) => {
                            expect(spy.contain).toHaveBeenNthCalledWith(index + 1, item);
                            expect(spy.indexOf).toHaveBeenNthCalledWith(index + 1, item);
                        });
                    })
                }
            });
        }

        computed.after(computed);
    });
};

export default testListRemove;