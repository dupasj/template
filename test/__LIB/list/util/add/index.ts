import TestListAddParameter from "./util/type/parameter/item";
import parameterListAdd from "./util/transform-parameter";

const testListAdd = (input: TestListAddParameter) => {
    const computed = parameterListAdd(input);

    const name = computed.class.name;
    const property = computed.property;
    const add = computed.method.add;
    const contain = computed.method.contain;
    const flat = computed.flat;

    if (typeof add !== "string"){
        return;
    }

    describe(`Test the ${name}.${add}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.builder(computed);

            expect(instance[add]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.builder(computed);

            expect(typeof instance[add]).toBe("function");
        });

        it("The method add the given items value", () => {
            const instance = computed.builder(computed);

            const values: any[] = [];

            const length = Math.floor(10 + Math.random() * 50);
            for(let i=0;i<length;i++){
                const value = computed.generator(computed);
                values.push(value);
            }

            instance[add](values);

            for(const value of values){
                expect(instance[property]).toContain(value);
            }
        });
        it("The method cannot have duplicate items", () => {
            const instance = computed.builder(computed);

            const values: any[] = [];

            function shuffle(array) {
                let currentIndex = array.length,  randomIndex;

                while (currentIndex != 0) {
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex--;

                    // And swap it with the current element.
                    [array[currentIndex], array[randomIndex]] = [
                        array[randomIndex], array[currentIndex]];
                }

                return array;
            }

            const length = Math.floor(10 + Math.random() * 50);
            for(let i=0;i<length;i++){
                const value = computed.generator(computed);
                const r = Math.random() * 5 + 1;
                for(let n=0;n<r;n++){
                    values.push(value);
                }
            }

            instance[add](values);

            const occurrences = instance[property].reduce(function (acc, curr) {
                return acc[curr] ? ++acc[curr] : acc[curr] = 1, acc
            }, {});

            for(const value of values){
                expect(occurrences[value]).toBe(1);
            }
        });

        describe("The method return himself", () => {
            it(`The method return himself when no value has been added`, () => {
                const instance = computed.builder(computed);

                const value = computed.generator(computed);
                instance[property] = [value];

                instance[add](value);

                expect(instance[add](value)).toBe(instance);
            });
            it(`The method return himself when the given value has been added`, () => {
                const instance = computed.builder(computed);

                const value = computed.generator(computed);

                instance[add](value);

                expect(instance[add](value)).toBe(instance);
            });
        })

        describe("push method implementation",() => {
            it("The method use push method to add the given item value", () => {
                const instance = computed.builder(computed);

                const value = computed.generator(computed);

                const spy = jest.spyOn(instance[property],"push");

                instance[add](value);

                expect(spy).toHaveBeenCalledTimes(1);
                expect(spy).toHaveBeenNthCalledWith(1,value);
            });
            it("The method doesn't use push method if the is in the list", () => {
                const instance = computed.builder(computed);

                const value = computed.generator(computed);
                instance[property] = [value];

                const spy = jest.spyOn(instance[property],"push");

                instance[add](value);

                expect(spy).not.toHaveBeenCalled();
            });

            if (typeof contain === "string"){
                it("The method doesn't use push method if the method "+contain+" result true", () => {
                    const instance = computed.builder(computed);

                    const value = computed.generator(computed);

                    const spy = jest.spyOn(instance[property],"push");
                    instance[contain] = jest.fn().mockReturnValue(true)

                    instance[add](value);

                    expect(spy).not.toHaveBeenCalled();
                });
                it("The method use push method if the method "+contain+" result false", () => {
                    const instance = computed.builder(computed);

                    const value = computed.generator(computed);
                    instance[property] = [value]

                    const spy = jest.spyOn(instance[property],"push");
                    instance[contain] = jest.fn().mockReturnValue(false)


                    instance[add](value);

                    expect(spy).toHaveBeenCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1,value);
                });
            }
        });

        if (flat){
            describe("flat function implementation",() => {
                it("The method use the flat function pass the argument into it", () => {
                    const spy = flat(computed);
                    spy.mockClear();

                    const value = computed.generator(computed);
                    const instance = computed.builder(computed);

                    instance[add](value);

                    expect(spy).toHaveBeenNthCalledWith(1, [value]);
                })

                if (typeof contain === "string") {
                    it("The method use the flat result function to loop add function", () => {
                        const instance = computed.builder(computed);

                        const length = Math.floor(10 + Math.random() * 50);
                        const mock: any[] = [];

                        for (let i = 0; i < length; i++) {
                            const value = computed.generator(computed);
                            mock.push(value);
                        }

                        const spy = {
                            push: jest.spyOn(instance[property], "push"),
                            contain: jest.spyOn(instance, contain).mockReturnValue(false),
                            flat: (() => {
                                const spy = flat(computed);
                                spy.mockClear();

                                return spy.mockReturnValueOnce(mock);
                            })(),
                        }

                        instance[add]();

                        expect(spy.contain).toHaveBeenCalledTimes(mock.length);
                        expect(spy.push).toHaveBeenCalledTimes(mock.length);
                        mock.forEach((item, index) => {
                            expect(spy.contain).toHaveBeenNthCalledWith(index + 1, item);
                            expect(spy.push).toHaveBeenNthCalledWith(index + 1, item);
                        });
                    })
                }
            });
        }



        computed.after(computed);
    });
};

export default testListAdd;