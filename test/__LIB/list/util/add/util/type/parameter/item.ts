import TestListAddOptionParameter from "./option";
import TestListItemParameter from "../../../../type/parameter/item";
import TestListAddOption from "../computed/option";

interface TestListAddParameter extends TestListItemParameter<TestListAddOptionParameter,TestListAddOption>{

}

export default TestListAddParameter;