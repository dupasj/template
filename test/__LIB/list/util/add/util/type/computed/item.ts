import TestListAddOption from "./option";
import TestListItem from "../../../../type/computed/item";

interface TestListAdd extends TestListItem<TestListAddOption>{

}

export default TestListAdd;