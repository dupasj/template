import TestListAddOption from "../computed/option";
import TestListItemOverrider from "../../../../type/overrider/item";
import TestListAddOptionOverrider from "./option";

interface TestListAddOverrider extends TestListItemOverrider<TestListAddOptionOverrider,TestListAddOption>{

}

export default TestListAddOverrider;