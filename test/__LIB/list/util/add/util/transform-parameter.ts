import TestListAddParameter from "./type/parameter/item";
import TestListAdd from "./type/computed/item";
import parameterListBase from "../../transform-base";

const parameterListAdd = (input: TestListAddParameter): TestListAdd => {
    const base = parameterListBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterListAdd;