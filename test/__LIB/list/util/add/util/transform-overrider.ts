import TestListAddOverrider from "./type/overrider/item";
import TestListBase from "../../type/computed/base";
import TestListAdd from "./type/computed/item";
import overriderListBase from "../../transform-overrider";

const overriderListAdd = (base: TestListBase, overrider: TestListAddOverrider): TestListAdd => {
    const computed = overriderListBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderListAdd;