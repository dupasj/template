import pluralize from "pluralize";
import TestListBaseParameter from "./type/parameter/base";
import TestListBase from "./type/computed/base";
import {randomUUID} from "crypto";
import * as ChangeCase from "change-case";

const parameterListBase = (input: TestListBaseParameter): TestListBase => {
    const is_static = input.is_static ?? false;

    return {
        flat: (() => {
            const flat = input.flat;

            if (flat === false  || flat === null){
                return false;
            }

            if (typeof flat !== "function"){
                return () => flat;
            }

            return flat;
        })(),
        class: input.class,
        property: input.property,
        is_static: is_static,
        method: {
            list: (() => {
                const list = input.method?.list;
                if (list === null || list === false){
                    return {
                        unsafe: false,
                        getter: false,
                    }
                }

                return {
                    getter: (() => {
                        const getter = list?.getter;
                        if (getter === null || getter === false){
                            return false;
                        }

                        return getter ?? "get" + ChangeCase.pascalCase(pluralize(input.property));
                    })(),
                    unsafe: (() => {
                        const unsafe = list?.unsafe;
                        if (unsafe === null || unsafe === false){
                            return false;
                        }

                        return unsafe ?? "unsafe" + ChangeCase.pascalCase(pluralize(input.property));
                    })(),
                }
            })(),
            item: (() => {
                const item = input.method?.item;
                if (item === null || item === false){
                    return {
                        unsafe: false,
                        getter: false,
                    }
                }

                return {
                    getter: (() => {
                        const getter = item?.getter;
                        if (getter === null || getter === false){
                            return false;
                        }

                        return getter ?? "get" + ChangeCase.pascalCase(pluralize.singular(input.property));
                    })(),
                    unsafe: (() => {
                        const unsafe = item?.unsafe;
                        if (unsafe === null || unsafe === false){
                            return false;
                        }

                        return unsafe ?? "unsafe" + ChangeCase.pascalCase(pluralize.singular(input.property));
                    })(),
                }
            })(),
            size: (() => {
                const size = input.method?.size;
                if (size === null || size === false){
                    return false;
                }

                return size ?? "get"+ChangeCase.pascalCase(input.property)+"Size";
            })(),
            contain: (() => {
                const contain = input.method?.contain;
                if (contain === null || contain === false){
                    return false;
                }

                return contain ?? "contain"+ChangeCase.pascalCase(input.property);
            })(),
            remove: (() => {
                const remove = input.method?.remove;
                if (remove === null || remove === false){
                    return false;
                }

                return remove ?? "remove"+ChangeCase.pascalCase(input.property);
            })(),
            add: (() => {
                const add = input.method?.add;
                if (add === null || add === false){
                    return false;
                }

                return add ?? "add"+ChangeCase.pascalCase(input.property);
            })(),
            truncate: (() => {
                const truncate = input.method?.truncate;
                if (truncate === null || truncate === false){
                    return false;
                }

                return truncate ?? "truncate"+ChangeCase.pascalCase(input.property);
            })(),
        },
        after: input.after ?? (() => {}),
        before: input.before ?? (() => {}),
        builder: input.builder ?? (() => {
            if (is_static){
                return Object.assign({},input.class);
            }

            return new input.class;
        }),
        generator(option: any): any {
            return randomUUID()
        }
    }
};

export default parameterListBase;