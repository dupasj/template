const spyOnValueProperty = function(obj, prop, accessType: "get"|"set" = 'get') {
    let descriptor = Object.getOwnPropertyDescriptor(obj, prop)
    let value = descriptor!.value
    let newDescriptor = {
        ...descriptor,
        get: () => value,
        set: (newValue) => value = newValue
    }
    delete newDescriptor.value
    delete newDescriptor.writable
    Object.defineProperty(obj, prop, newDescriptor)
    return jest.spyOn(obj, prop, accessType)
}

export default spyOnValueProperty;