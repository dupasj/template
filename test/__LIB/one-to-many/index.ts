import TestOneToManyParameter from "./util/parameter/general";
import parameterOneToMany from "./util/transform-parameter";
import testOneToManyParentLink from "./parent/link";
import testOneToManyParentSize from "./parent/size";
import testOneToManyParentDispose from "./parent/dispose";
import testOneToManyParentContain from "./parent/contain";
import testOneToManyParentUnlink from "./parent/unlink";
import testOneToManyChildrenIs from "./children/is";
import testOneToManyChildrenDefined from "./children/defined";
import testOneToManyChildrenUnlink from "./children/unlink";
import testOneToManyChildrenLink from "./children/link";
import testOneToManyChildrenUnsafe from "./children/unsafe";
import testOneToManyChildrenGetter from "./children/getter";

const testOneToMany = (option: TestOneToManyParameter) => {
    const computed = parameterOneToMany(option);
    const parent = computed.parent.class.name;
    const child = computed.children.class.name;

    describe(`The methods related to the on to many relation between ${parent}.${computed.parent.property} and ${child}.${computed.children.property}`, () => {
        describe(`The methods related to ${parent}.${computed.parent.property}`, () => {
            if (computed.overrider.parent.link){
                testOneToManyParentLink(computed.overrider.parent.link)
            }
            if (computed.overrider.parent.size){
                testOneToManyParentSize(computed.overrider.parent.size)
            }
            if (computed.overrider.parent.dispose){
                testOneToManyParentDispose(computed.overrider.parent.dispose)
            }
            if (computed.overrider.parent.contain){
                testOneToManyParentContain(computed.overrider.parent.contain)
            }
            if (computed.overrider.parent.unlink){
                testOneToManyParentUnlink(computed.overrider.parent.unlink)
            }
        });

        describe(`The methods related to ${child}.${computed.children.property}`, () => {
            if (computed.overrider.children.link){
                testOneToManyChildrenLink(computed.overrider.children.link)
            }
            if (computed.overrider.children.unlink){
                testOneToManyChildrenUnlink(computed.overrider.children.unlink)
            }
            if (computed.overrider.children.defined){
                testOneToManyChildrenDefined(computed.overrider.children.defined)
            }
            if (computed.overrider.children.is){
                testOneToManyChildrenIs(computed.overrider.children.is)
            }
            if (computed.overrider.children.unsafe){
                testOneToManyChildrenUnsafe(computed.overrider.children.unsafe)
            }
            if (computed.overrider.children.getter){
                testOneToManyChildrenGetter(computed.overrider.children.getter)
            }
        });
    });
}

export default testOneToMany;