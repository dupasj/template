import TestOneToManyBaseParameter from "./parameter/base";
import TestOneToManyBase from "./computed/base";
import * as ChangeCase from "change-case";
import pluralize from "pluralize";

const parameterOneToManyBase = (input: TestOneToManyBaseParameter): TestOneToManyBase => {
    const property = {
        parent: input.parent.property ?? ChangeCase.snakeCase(pluralize(input.children.class.name)),
        children: input.children.property ?? ChangeCase.snakeCase(pluralize.singular(input.parent.class.name)),
    }

   return {
       parent: {
           class: input.parent.class,
           property: property.parent,
           is_static: input.parent.is_static ?? false,

           method: {
               list: (() => {
                   const list = input.parent.method?.list;
                   if (list === null || list === false){
                       return {
                           unsafe: false,
                           getter: false,
                       }
                   }

                   return {
                       getter: (() => {
                           const getter = list?.getter;
                           if (getter === null || getter === false){
                               return false;
                           }

                           return getter ?? "get" + ChangeCase.pascalCase(pluralize(property.parent));
                       })(),
                       unsafe: (() => {
                           const unsafe = list?.unsafe;
                           if (unsafe === null || unsafe === false){
                               return false;
                           }

                           return unsafe ?? "unsafe" + ChangeCase.pascalCase(pluralize(property.parent));
                       })(),
                   }
               })(),
               item: (() => {
                   const item = input.parent.method?.item;
                   if (item === null || item === false){
                       return {
                           unsafe: false,
                           getter: false,
                       }
                   }

                   return {
                       getter: (() => {
                           const getter = item?.getter;
                           if (getter === null || getter === false){
                               return false;
                           }

                           return getter ?? "get" + ChangeCase.pascalCase(pluralize.singular(property.parent));
                       })(),
                       unsafe: (() => {
                           const unsafe = item?.unsafe;
                           if (unsafe === null || unsafe === false){
                               return false;
                           }

                           return unsafe ?? "unsafe" + ChangeCase.pascalCase(pluralize.singular(property.parent));
                       })(),
                   }
               })(),
               size: (() => {
                   const size = input.parent.method?.size;
                   if (size === null || size === false){
                       return false;
                   }

                   return size ?? "get"+ChangeCase.pascalCase(property.parent)+"Size";
               })(),
               contain: (() => {
                   const contain = input.parent.method?.contain;
                   if (contain === null || contain === false){
                       return false;
                   }

                   return contain ?? "are"+ChangeCase.pascalCase(property.parent)+"Linked";
               })(),
               unlink: (() => {
                   const unlink = input.parent.method?.unlink;
                   if (unlink === null || unlink === false){
                       return false;
                   }

                   return unlink ?? "unlink"+ChangeCase.pascalCase(property.parent);
               })(),
               link: (() => {
                   const link = input.parent.method?.link;
                   if (link === null || link === false){
                       return false;
                   }

                   return link ?? "link"+ChangeCase.pascalCase(property.parent);
               })(),
               dispose: (() => {
                   const dispose = input.parent.method?.dispose;
                   if (dispose === null || dispose === false){
                       return false;
                   }

                   return dispose ?? "dispose"+ChangeCase.pascalCase(property.parent);
               })(),
           },

           builder: input.parent.builder ?? ((option) => {
               if (option.parent.is_static){
                   return Object.assign({},option.parent.class);
               }

               return new option.parent.class;
           }),
       },
       children: {
           class: input.children.class,
           property: property.children,
           is_static: input.children.is_static ?? false,


           method: {
               defined: (() => {
                   const defined = input.children?.method?.defined;
                   if (defined === null || defined === false){
                       return false;
                   }

                   return defined ?? "has"+ChangeCase.pascalCase(property.children)+"Linked";
               })(),
               is: (() => {
                   const is = input.children?.method?.is;
                   if (is === null || is === false){
                       return false;
                   }

                   return is ?? "isLinkedWith"+ChangeCase.pascalCase(property.children);
               })(),
               unsafe: (() => {
                   const unsafe = input.children?.method?.unsafe;
                   if (unsafe === null || unsafe === false){
                       return false;
                   }

                   return unsafe ?? "unsafe"+ChangeCase.pascalCase(property.children);
               })(),
               getter: (() => {
                   const getter = input?.children.method?.getter;
                   if (getter === null || getter === false){
                       return false;
                   }

                   return getter ?? "get"+ChangeCase.pascalCase(property.children);
               })(),
               link: (() => {
                   const link = input.children.method?.link;
                   if (link === null || link === false){
                       return false;
                   }

                   return link ?? "link"+ChangeCase.pascalCase(property.children);
               })(),
               unlink: (() => {
                   const unlink = input.children.method?.unlink;
                   if (unlink === null || unlink === false){
                       return false;
                   }

                   return unlink ?? "unlink"+ChangeCase.pascalCase(property.children);
               })(),
           },
           builder: input.children.builder ?? ((option) => {
               if (option.children.is_static){
                   return Object.assign({},option.children.class);
               }

               return new option.children.class;
           }),
       },
       error: (() => {
           const error = input.error;

           if (error === false  || error === null){
               return false;
           }

           return error;
       })(),
       flat: (() => {
           const flat = input.flat;

           if (flat === false  || flat === null){
               return false;
           }

           return flat;
       })(),
       after: input.after ?? (() => {}),
       before: input.before ?? (() => {}),
   }
};

export default parameterOneToManyBase;