import Mock = jest.Mock;
import TestOneToManyBaseparameter from "./base";
import TestOneToManyItem from "../computed/item";

interface TestOneToManyItemOverrider<Option extends {[key: string]: any} = {},ComputedOption extends {[key: string]: any} = Option> extends  TestOneToManyBaseparameter {
    before?: (option: TestOneToManyItem<ComputedOption>) => void,
    after?: (option: TestOneToManyItem<ComputedOption>) => void,
    option?: Option
}

export default TestOneToManyItemOverrider;