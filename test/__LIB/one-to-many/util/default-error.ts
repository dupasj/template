import UnlinkedValue from "../../../../result/error/unlinked-value";
import UndefinedValue from "../../../../result/error/undefined-value";
import TestOneToManyBase from "./computed/base";

const defaultOneToManyError = (_constructor: {new (): Object}) => {
    return (computed: TestOneToManyBase,result: any) => {
        const getter = computed.children.method.getter;

        expect(result).toBeInstanceOf(_constructor);
        if (getter){
            expect((result as UnlinkedValue).unsafeMethod()).toBe(getter);
        }
        expect((result as UnlinkedValue).unsafeWantedClassname()).toBe(computed.parent.class.name);
        expect((result as UndefinedValue).unsafeClassname()).toBe(computed.children.class.name);
        expect((result as UndefinedValue).unsafeProperty()).toBe(computed.children.property);
    };
};

export default defaultOneToManyError;