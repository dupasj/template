import TestOneToManyBase from "./computed/base";
import TestOneToManyItemOverrider from "./overrider/item";

const overriderOneToManyBase = (input: TestOneToManyBase, overrider: TestOneToManyItemOverrider): TestOneToManyBase => {
   return {
       parent: {
           class: overrider.parent?.class ?? input.parent.class,
           property: overrider.parent?.property ?? input.parent.property,
           is_static: overrider.parent?.is_static ?? input.parent.is_static,
           builder: overrider.parent?.builder ?? input.parent.builder,

           method: {
               list: (() => {
                   const list = overrider.parent?.method?.list;
                   if (list === null || list === false){
                       return {
                           unsafe: false,
                           getter: false,
                       }
                   }

                   return {
                       getter: (() => {
                           const getter = list?.getter;
                           if (getter === null || getter === false){
                               return false;
                           }

                           return getter ?? input.parent.method.list.getter;
                       })(),
                       unsafe: (() => {
                           const unsafe = list?.unsafe;
                           if (unsafe === null || unsafe === false){
                               return false;
                           }

                           return unsafe ?? input.parent.method.list.unsafe;
                       })(),
                   }
               })(),
               item: (() => {
                   const item = overrider.parent?.method?.item;
                   if (item === null || item === false){
                       return {
                           unsafe: false,
                           getter: false,
                       }
                   }

                   return {
                       getter: (() => {
                           const getter = item?.getter;
                           if (getter === null || getter === false){
                               return false;
                           }

                           return getter ?? input.parent.method.item.getter;
                       })(),
                       unsafe: (() => {
                           const unsafe = item?.unsafe;
                           if (unsafe === null || unsafe === false){
                               return false;
                           }

                           return unsafe ?? input.parent.method.item.unsafe;
                       })(),
                   }
               })(),
               size: (() => {
                   const size = overrider.parent?.method?.size;
                   if (size === null || size === false){
                       return false;
                   }

                   return size ?? input.parent.method.size;
               })(),
               contain: (() => {
                   const contain = overrider.parent?.method?.contain;
                   if (contain === null || contain === false){
                       return false;
                   }

                   return contain ?? input.parent.method.contain;
               })(),
               unlink: (() => {
                   const unlink = overrider.parent?.method?.unlink;
                   if (unlink === null || unlink === false){
                       return false;
                   }

                   return unlink ?? input.parent.method.unlink;
               })(),
               link: (() => {
                   const link = overrider.parent?.method?.link;
                   if (link === null || link === false){
                       return false;
                   }

                   return link ?? input.parent.method.link;
               })(),
               dispose: (() => {
                   const dispose = overrider.parent?.method?.dispose;
                   if (dispose === null || dispose === false){
                       return false;
                   }

                   return dispose ?? input.parent.method.dispose;
               })(),
           },
       },
       children: {
           class: overrider.children?.class ?? input.children.class,
           property: overrider.children?.property ?? input.children.property,
           is_static: overrider.children?.is_static ?? input.children.is_static,
           builder: overrider.children?.builder ?? input.children.builder,

           method: {
               defined: (() => {
                   const defined = overrider.children?.method?.defined;
                   if (defined === null || defined === false){
                       return false;
                   }

                   return defined ?? input.children.method.defined;
               })(),
               is: (() => {
                   const is = overrider.children?.method?.is;
                   if (is === null || is === false){
                       return false;
                   }

                   return is ?? input.children.method.is;
               })(),
               unsafe: (() => {
                   const unsafe = overrider.children?.method?.unsafe;
                   if (unsafe === null || unsafe === false){
                       return false;
                   }

                   return unsafe ?? input.children.method.unsafe;
               })(),
               getter: (() => {
                   const getter = overrider.children?.method?.getter;
                   if (getter === null || getter === false){
                       return false;
                   }

                   return getter ?? input.children.method.getter;
               })(),
               link: (() => {
                   const link = overrider.children?.method?.link;
                   if (link === null || link === false){
                       return false;
                   }

                   return link ?? input.children.method.link;
               })(),
               unlink: (() => {
                   const unlink = overrider.children?.method?.unlink;
                   if (unlink === null || unlink === false){
                       return false;
                   }

                   return unlink ?? input.children.method.unlink;
               })(),
           },
       },
       error: (() => {
           const error = overrider.error;

           if (error === false  || error === null){
               return false;
           }

           return error ?? input.flat;
       })(),
       flat: (() => {
           const flat = overrider.flat;

           if (flat === false  || flat === null){
               return false;
           }

           return flat ?? input.flat;
       })(),
       after: overrider.after ?? (() => {}),
       before: overrider.before ?? (() => {}),
   }
};

export default overriderOneToManyBase;