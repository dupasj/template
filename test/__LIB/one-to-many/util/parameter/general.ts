import TestOneToManyBaseParameter from "./base";
import TestOneToManyParentLinkParameter from "../../parent/link/util/type/parameter/item";
import TestOneToManyParentSizeParameter from "../../parent/size/util/type/parameter/item";
import TestOneToManyParentDisposeParameter from "../../parent/dispose/util/type/parameter/item";
import TestOneToManyParentContainParameter from "../../parent/contain/util/type/parameter/item";
import TestOneToManyParentUnlinkParameter from "../../parent/unlink/util/type/parameter/item";
import TestOneToManyChildrenGetterOverrider from "../../children/getter/util/type/overrider/item";
import TestOneToManyChildrenUnlinkOverrider from "../../children/unlink/util/type/overrider/item";
import TestOneToManyChildrenLinkOverrider from "../../children/link/util/type/overrider/item";
import TestOneToManyChildrenIsOverrider from "../../children/is/util/type/overrider/item";
import TestOneToManyChildrenDefinedOverrider from "../../children/defined/util/type/overrider/item";
import TestOneToManyChildrenUnsafeOverrider from "../../children/unsafe/util/type/overrider/item";

interface TestOneToManyParameter extends TestOneToManyBaseParameter{
    overrider?: {
        parent?: {
            link?: TestOneToManyParentLinkParameter|false|null,
            size?: TestOneToManyParentSizeParameter|false|null,
            dispose?: TestOneToManyParentDisposeParameter|false|null,
            contain?: TestOneToManyParentContainParameter|false|null,
            unlink?: TestOneToManyParentUnlinkParameter|false|null,
        }|false|null,
        children?: {
            defined?: TestOneToManyChildrenDefinedOverrider|false|null,
            is?: TestOneToManyChildrenIsOverrider|false|null,
            link?: TestOneToManyChildrenLinkOverrider|false|null,
            unlink?: TestOneToManyChildrenUnlinkOverrider|false|null,
            getter?: TestOneToManyChildrenGetterOverrider|false|null,
            unsafe?: TestOneToManyChildrenUnsafeOverrider|false|null
        }|false|null
    }
}

export default TestOneToManyParameter