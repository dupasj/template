import TestOneToManyBase from "../computed/base";
import Mock = jest.Mock;
import TestOneToManyItem from "../computed/item";

interface TestOneToManyBaseParameter {
    flat: false|null|((option: TestOneToManyItem) => any),
    error: false|null|((option: any,error: any) => void)

    parent: {
        property?: string,
        class: {new(): Object},
        is_static?: boolean,
        builder?: (option: TestOneToManyItem) => {[key: string]: any},

        method?: {
            link?: string|false|null,
            unlink?: string|false|null,
            linked?: string|false|null,
            contain?: string|false|null,
            size?: string|false|null,
            dispose?: string|false|null,
            list?: {
                unsafe?: string|false|null,
                getter?: string|false|null,
            }|false|null,
            item?: {
                unsafe?: string|false|null,
                getter?: string|false|null,
            }|false|null
        }
    },
    children: {
        property?: string,
        class: {new(): Object},
        is_static?: boolean,
        builder?: (option: TestOneToManyItem) => {[key: string]: any},

        method?: {
            link?: string|false|null,
            unlink?: string|false|null,
            is?: string|false|null,
            defined?: string|false|null,
            unsafe?: string|false|null,
            getter?: string|false|null,
        }
    },

    before?: (option: TestOneToManyBase) => void,
    after?: (option: TestOneToManyBase) => void,
}

export default TestOneToManyBaseParameter