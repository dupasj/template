import Mock = jest.Mock;
import TestOneToManyBaseparameter from "./base";
import TestOneToManyItem from "../computed/item";

interface TestOneToManyItemParameter<Option extends {[key: string]: any} = {},ComputedOption extends {[key: string]: any} = Option> extends  TestOneToManyBaseparameter {
    flat: false|((option: TestOneToManyItem<ComputedOption>) => Mock),
    error: false|null|((option: any,error: any) => void),
    before?: (option: TestOneToManyItem<ComputedOption>) => void,
    after?: (option: TestOneToManyItem<ComputedOption>) => void,
    option?: Option
}

export default TestOneToManyItemParameter;