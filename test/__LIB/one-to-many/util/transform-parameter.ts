import parameterOneToManyBase from "./transform-base";
import TestOneToManyParameter from "./parameter/general";
import TestOneToMany from "./computed/general";
import overriderOneToManyParentLink from "../parent/link/util/transform-overrider";
import overriderOneToManyParentSize from "../parent/size/util/transform-overrider";
import overriderOneToManyParentDispose from "../parent/dispose/util/transform-overrider";
import overriderOneToManyParentContain from "../parent/contain/util/transform-overrider";
import overriderOneToManyParentUnlink from "../parent/unlink/util/transform-overrider";
import overriderOneToManyChildrenUnlink from "../children/unlink/util/transform-overrider";
import overriderOneToManyChildrenLink from "../children/link/util/transform-overrider";
import overriderOneToManyChildrenUnsafe from "../children/unsafe/util/transform-overrider";
import overriderOneToManyChildrenGetter from "../children/getter/util/transform-overrider";
import overriderOneToManyChildrenIs from "../children/is/util/transform-overrider";
import overriderOneToManyChildrenDefined from "../children/defined/util/transform-overrider";

const parameterOneToMany = (base: TestOneToManyParameter): TestOneToMany => {
    const temporary = parameterOneToManyBase(base);

    return {
        ... temporary,

        overrider: {
            children: (() => {
                const children = base.overrider?.children;

                if (children === null || children === false){
                    return {
                        link: false,
                        unlink: false,
                        is: false,
                        defined: false,
                        getter: false,
                        unsafe: false
                    }
                }

                return {
                    unlink: (() => {
                        const unlink = children?.unlink;

                        if (unlink === false || unlink === null){
                            return false;
                        }

                        return overriderOneToManyChildrenUnlink(temporary,unlink ?? {})
                    })(),
                    link: (() => {
                        const link = children?.link;

                        if (link === false || link === null){
                            return false;
                        }

                        return overriderOneToManyChildrenLink(temporary,link ?? {})
                    })(),
                    unsafe: (() => {
                        const unsafe = children?.unsafe;

                        if (unsafe === false || unsafe === null){
                            return false;
                        }

                        return overriderOneToManyChildrenUnsafe(temporary,unsafe ?? {})
                    })(),
                    is: (() => {
                        const is = children?.is;

                        if (is === false || is === null){
                            return false;
                        }

                        return overriderOneToManyChildrenIs(temporary,is ?? {})
                    })(),
                    getter: (() => {
                        const getter = children?.getter;

                        if (getter === false || getter === null){
                            return false;
                        }

                        return overriderOneToManyChildrenGetter(temporary,getter ?? {})
                    })(),
                    defined: (() => {
                        const defined = children?.defined;

                        if (defined === false || defined === null){
                            return false;
                        }

                        return overriderOneToManyChildrenDefined(temporary,defined ?? {})
                    })(),
                }
            })(),
            parent: (() => {
                const parent = base.overrider?.parent;

                if (parent === null || parent === false){
                    return {
                        link: false,
                        size: false,
                        dispose: false,
                        contain: false,
                        unlink: false,
                    }
                }

                return {
                    unlink: (() => {
                        const unlink = parent?.unlink;

                        if (unlink === false || unlink === null){
                            return false;
                        }

                        return overriderOneToManyParentUnlink(temporary,unlink ?? {})
                    })(),
                    link: (() => {
                        const link = parent?.link;

                        if (link === false || link === null){
                            return false;
                        }

                        return overriderOneToManyParentLink(temporary,link ?? {})
                    })(),
                    size: (() => {
                        const size = parent?.size;

                        if (size === false || size === null){
                            return false;
                        }

                        return overriderOneToManyParentSize(temporary,size ?? {})
                    })(),
                    dispose: (() => {
                        const dispose = parent?.dispose;

                        if (dispose === false || dispose === null){
                            return false;
                        }

                        return overriderOneToManyParentDispose(temporary,dispose ?? {})
                    })(),
                    contain: (() => {
                        const contain = parent?.contain;

                        if (contain === false || contain === null){
                            return false;
                        }

                        return overriderOneToManyParentContain(temporary,contain ?? {})
                    })()
                }
            })()
        }
    }
};

export default parameterOneToMany;