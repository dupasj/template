import Mock = jest.Mock;
import TestOneToManyBase from "./base";

interface TestOneToManyItem<Option extends {[key: string]: any} = {}> extends  TestOneToManyBase {
    flat: false|((option: TestOneToManyItem<Option>) => Mock),
    before: (option: TestOneToManyItem<Option>) => void,
    after: (option: TestOneToManyItem<Option>) => void,
    option: Option
}

export default TestOneToManyItem