import Mock = jest.Mock;
import TestOneToManyItem from "./item";

interface TestOneToManyBase {
    flat: false|((option: TestOneToManyItem) => Mock),
    error: false|((option: any,error: any) => void),

    parent: {
        property: string,
        class: {new(): Object},
        is_static: boolean,
        builder: (option: TestOneToManyItem) => {[key: string]: any},

        method: {
            link: string|false,
            unlink: string|false,
            contain: string|false,
            size: string|false,
            dispose: string|false,
            list: {
                unsafe: string|false,
                getter: string|false,
            },
            item: {
                unsafe: string|false,
                getter: string|false,
            }
        }
    },
    children: {
        property: string,
        class: {new(): Object},
        is_static: boolean,
        builder: (option: TestOneToManyItem) => {[key: string]: any},

        method: {
            link: string|false,
            unlink: string|false,
            is: string|false,
            defined: string|false,
            unsafe: string|false,
            getter: string|false,
        }
    },

    before: (option: TestOneToManyBase) => void,
    after: (option: TestOneToManyBase) => void,
}

export default TestOneToManyBase