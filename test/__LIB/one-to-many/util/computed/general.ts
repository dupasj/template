import TestOneToManyBase from "./base";
import TestOneToManyParentLink from "../../parent/link/util/type/computed/item";
import TestOneToManyParentSize from "../../parent/size/util/type/computed/item";
import TestOneToManyParentDispose from "../../parent/dispose/util/type/computed/item";
import TestOneToManyParentContain from "../../parent/contain/util/type/computed/item";
import TestOneToManyParentUnlink from "../../parent/unlink/util/type/computed/item";
import TestOneToManyChildrenDefined from "../../children/defined/util/type/computed/item";
import TestOneToManyChildrenIs from "../../children/is/util/type/computed/item";
import TestOneToManyChildrenLink from "../../children/link/util/type/computed/item";
import TestOneToManyChildrenUnlink from "../../children/unlink/util/type/computed/item";
import TestOneToManyChildrenGetter from "../../children/getter/util/type/computed/item";
import TestOneToManyChildrenUnsafe from "../../children/unsafe/util/type/computed/item";

interface TestOneToMany extends TestOneToManyBase{
    before: (option: TestOneToMany) => void,
    after: (option: TestOneToMany) => void,

    overrider: {
        parent: {
            link: TestOneToManyParentLink|false,
            size: TestOneToManyParentSize|false
            dispose: TestOneToManyParentDispose|false
            unlink: TestOneToManyParentUnlink|false
            contain: TestOneToManyParentContain|false
        },
        children: {
            defined: TestOneToManyChildrenDefined|false,
            is: TestOneToManyChildrenIs|false,
            link: TestOneToManyChildrenLink|false,
            unlink: TestOneToManyChildrenUnlink|false,
            getter: TestOneToManyChildrenGetter|false,
            unsafe: TestOneToManyChildrenUnsafe|false
        }
    }
}

export default TestOneToMany