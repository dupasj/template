import TestOneToManyBase from "../../../util/computed/base";
import TestOneToManyParentUnlinkOverrider from "./type/overrider/item";
import TestOneToManyParentUnlink from "./type/computed/item";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyParentUnlink = (base: TestOneToManyBase, overrider: TestOneToManyParentUnlinkOverrider): TestOneToManyParentUnlink => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        ... computed,
        option: {},
    };
}

export default overriderOneToManyParentUnlink;