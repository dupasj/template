import TestOneToManyParentUnlinkParameter from "./type/parameter/item";
import TestOneToManyParentUnlink from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyParentUnlink = (input: TestOneToManyParentUnlinkParameter): TestOneToManyParentUnlink => {
    const base = parameterOneToManyBase(input);

    return {
        ... base,
        option: {},
    };
}

export default parameterOneToManyParentUnlink;