import TestOneToManyItemOverrider from "../../../../../util/overrider/item";
import TestOneToManyParentUnlinkOption from "../computed/option";
import TestOneToManyParentUnlinkOptionOverrider from "./option";

interface TestOneToManyParentUnlinkOverrider extends TestOneToManyItemOverrider<TestOneToManyParentUnlinkOptionOverrider,TestOneToManyParentUnlinkOption>{

}

export default TestOneToManyParentUnlinkOverrider;