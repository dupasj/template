import TestOneToManyItemParameter from "../../../../../util/parameter/item";
import TestOneToManyParentUnlinkOptionParameter from "./option";
import TestOneToManyParentUnlinkOption from "../computed/option";

interface TestOneToManyParentUnlinkParameter extends TestOneToManyItemParameter<TestOneToManyParentUnlinkOptionParameter,TestOneToManyParentUnlinkOption>{

}

export default TestOneToManyParentUnlinkParameter;