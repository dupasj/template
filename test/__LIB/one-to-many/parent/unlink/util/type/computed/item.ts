import TestOneToManyParentUnlinkOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyParentUnlink extends TestOneToManyItem<TestOneToManyParentUnlinkOption>{

}

export default TestOneToManyParentUnlink;