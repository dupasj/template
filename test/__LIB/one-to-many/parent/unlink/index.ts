import TestOneToManyParentUnlinkParameter from "./util/type/parameter/item";
import parameterOneToManyParentUnlink from "./util/transform-parameter";
import Mock = jest.Mock;

const testOneToManyParentUnlink = (input: TestOneToManyParentUnlinkParameter) => {
    const computed = parameterOneToManyParentUnlink(input);

    const name = computed.parent.class.name;
    const child = computed.children.class.name;
    const property = computed.parent.property;
    const remove = computed.parent.method.unlink;
    const childrenUnlink = computed.children.method.unlink;
    const contain = computed.parent.method.contain;
    const flat = computed.flat;

    if (typeof remove !== "string"){
        return;
    }

    describe(`Test the ${name}.${remove}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.parent.builder(computed);

            expect(instance[remove]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.parent.builder(computed);

            expect(typeof instance[remove]).toBe("function");
        });

        it("The method remove the given item value", () => {
            const instance = computed.parent.builder(computed);

            const length = Math.floor(10 + Math.random() * 50);
            for(let i=0;i<length;i++){
                const value = computed.children.builder(computed);
                instance[property].push(value);
            }

            const rand = instance[property][Math.floor(instance[property].length * Math.random())];

            instance[remove](rand);

            expect(instance[property]).not.toContain(rand);
        });

        describe("The method return himself", () => {
            it(`The method return himself when no value has been removed`, () => {
                const instance = computed.parent.builder(computed);
                expect(instance[remove](computed.children.builder(computed))).toBe(instance);
            });
            it(`The method return himself when the given value has been removed`, () => {
                const instance = computed.parent.builder(computed);

                const length = Math.floor(10 + Math.random() * 50);
                for(let i=0;i<length;i++){
                    const value = computed.children.builder(computed);
                    instance[property].push(value);
                }

                const rand = instance[property][Math.floor(instance[property].length * Math.random())];

                expect(instance[remove](rand)).toBe(instance);
            });
        })


        describe("indexOf and splice methods implementation",() => {
            describe("indexOf method implementation",() => {
                it("The method use indexOf method to remove the given item value", () => {
                    const instance = computed.parent.builder(computed);

                    const length = Math.floor(10 + Math.random() * 50);
                    for(let i=0;i<length;i++){
                        const value = computed.children.builder(computed);
                        instance[property].push(value);
                    }

                    const index = Math.floor(instance[property].length * Math.random());
                    const rand = instance[property][index];

                    const spy = jest.spyOn(instance[property],"indexOf");

                    instance[remove](rand);

                    expect(spy).toHaveBeenCalledTimes(2);
                });
                it("The method doesn't use indexOf method if the value isn't in the list", () => {
                    const instance = computed.parent.builder(computed);

                    const length = Math.floor(10 + Math.random() * 50);
                    for(let i=0;i<length;i++){
                        const value = computed.children.builder(computed);
                        instance[property].push(value);
                    }

                    const spy = jest.spyOn(instance[property],"indexOf");

                    instance[remove](computed.children.builder(computed));

                    expect(spy).not.toHaveBeenCalled();
                });

                if (typeof contain === "string"){
                    it("The method doesn't use indexOf method if the method "+contain+" result false", () => {
                        const instance = computed.parent.builder(computed);

                        const length = Math.floor(10 + Math.random() * 50);
                        for(let i=0;i<length;i++){
                            const value = computed.children.builder(computed);
                            instance[property].push(value);
                        }

                        instance[contain] = jest.fn().mockReturnValue(false);
                        const spy = jest.spyOn(instance[property],"indexOf");

                        instance[remove](computed.children.builder(computed));

                        expect(spy).not.toHaveBeenCalled();
                    });
                }
            });

            describe("splice method implementation",() => {
                it("The method doesn't use splice method if the value isn't in the list", () => {
                    const instance = computed.parent.builder(computed);

                    const length = Math.floor(10 + Math.random() * 50);
                    for(let i=0;i<length;i++){
                        const value = computed.children.builder(computed);
                        instance[property].push(value);
                    }

                    const spy = jest.spyOn(instance[property],"splice");

                    instance[remove](computed.children.builder(computed));

                    expect(spy).not.toHaveBeenCalled();
                });
                it("The method use splice method to remove the given item value", () => {
                    const instance = computed.parent.builder(computed);

                    const length = Math.floor(10 + Math.random() * 50);
                    for(let i=0;i<length;i++){
                        const value = computed.children.builder(computed);
                        instance[property].push(value);
                    }

                    const index = Math.floor(instance[property].length * Math.random());
                    const rand = instance[property][index];

                    const spy = jest.spyOn(instance[property],"splice");

                    instance[remove](rand);

                    expect(spy).toHaveBeenCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1,index,1);
                });

                if (typeof contain === "string"){
                    it("The method doesn't use splice method if the method "+contain+" result false", () => {
                        const instance = computed.parent.builder(computed);

                        const length = Math.floor(10 + Math.random() * 50);
                        for(let i=0;i<length;i++){
                            const value = computed.children.builder(computed);
                            instance[property].push(value);
                        }

                        instance[contain] = jest.fn().mockReturnValue(false);
                        const spy = jest.spyOn(instance[property],"splice");

                        const index = Math.floor(instance[property].length * Math.random());
                        const rand = instance[property][index];

                        instance[remove](rand);

                        expect(spy).not.toHaveBeenCalled();
                    });
                }
            })

            it("The method will remove the duplicated value", () => {
                const instance = computed.parent.builder(computed);

                function shuffle(array) {
                    let currentIndex = array.length,  randomIndex;

                    while (currentIndex != 0) {
                        randomIndex = Math.floor(Math.random() * currentIndex);
                        currentIndex--;

                        // And swap it with the current element.
                        [array[currentIndex], array[randomIndex]] = [
                            array[randomIndex], array[currentIndex]];
                    }

                    return array;
                }

                const length = Math.floor(10 + Math.random() * 50);
                for(let i=0;i<length;i++){
                    const value = computed.children.builder(computed);
                    const r = Math.random() * 5 + 1;
                    for(let n=0;n<r;n++){
                        instance[property].push(value);
                    }
                }

                shuffle(instance[property]);

                const index = Math.floor(instance[property].length * Math.random());
                const rand = instance[property][index];

                instance[remove](rand);

                expect(instance[property]).not.toContain(rand);
            });
            it("The method will always use the splice method if the indexOf method doesn't return -1, the splice method use the indexOf result", () => {
                const instance = computed.parent.builder(computed);

                const length = Math.floor(10 + Math.random() * 50);
                const indexes: any[] = [];
                for(let i=0;i<length;i++){
                    const value = computed.children.builder(computed);
                    indexes.push(value);
                }


                const value = computed.children.builder(computed);
                instance[property].push(value);

                const spy = {
                    index: jest.spyOn(instance[property],"indexOf"),
                    splice: jest.spyOn(instance[property],"splice"),
                };

                let count = 0;
                spy.index.mockImplementation(() => {
                    return indexes[count++] ?? -1;
                })

                instance[remove](value);
                expect(spy.index).toHaveBeenCalledTimes(length+1);
                expect(spy.splice).toHaveBeenCalledTimes(length);

                for(let i=0;i<indexes.length;i++){
                    expect(spy.index).toHaveBeenNthCalledWith(i+1,value);
                    expect(spy.splice).toHaveBeenNthCalledWith(i+1,indexes[i],1);
                }
            });
        })

        if (flat){
            describe("flat function implementation",() => {
                it("The method use the flat function pass the argument into it", () => {
                    const spy = flat(computed);
                    spy.mockClear();

                    const value = computed.children.builder(computed);
                    const instance = computed.parent.builder(computed);

                    instance[remove](value);

                    expect(spy).toHaveBeenNthCalledWith(1, [value]);
                })

                it("The method use the flat result function to loop remove function", () => {
                    const instance = computed.parent.builder(computed);

                    const length = Math.floor(10 + Math.random() * 50);
                    const mock: any[] = [];
                    const spies: Mock[] = [];

                    for (let i = 0; i < length; i++) {
                        const value = computed.children.builder(computed);

                        if (childrenUnlink){
                            const spy = jest.fn();
                            value[childrenUnlink] = spy;
                            spies.push(spy);
                        }

                        mock.push(value);
                    }

                    const spy = {
                        indexOf: jest.spyOn(instance[property], "indexOf"),
                        contain: (() => {
                            if (contain){
                                return jest.spyOn(instance, contain).mockReturnValue(true)
                            }

                            return null;
                        })(),
                        flat: (() => {
                            const spy = flat(computed);
                            spy.mockClear();

                            return spy.mockReturnValueOnce(mock);
                        })(),
                    }

                    instance[remove]();

                    if (spy.contain){
                        expect(spy.contain).toHaveBeenCalledTimes(mock.length);
                    }
                    expect(spy.indexOf).toHaveBeenCalledTimes(mock.length);
                    mock.forEach((item, index) => {
                        if (spy.contain) {
                            expect(spy.contain).toHaveBeenNthCalledWith(index + 1, item);
                        }
                        expect(spy.indexOf).toHaveBeenNthCalledWith(index + 1, item);
                    });
                    spies.forEach(spy => {
                        expect(spy).toHaveBeenCalledTimes(1);
                        expect(spy).toHaveBeenNthCalledWith(1);
                    });
                })
            });
        }

        if (childrenUnlink){
            describe(`Test the implementation with the ${child}.${childrenUnlink} and the ${name}.${remove}`,() => {
                it(`The method doesn't use ${child}.${childrenUnlink} method if the value isn't in the list`, () => {
                    const instance = computed.parent.builder(computed);

                    const spies: Mock[] = [];

                    const length = Math.floor(10 + Math.random() * 50);
                    for(let i=0;i<length;i++){
                        const value = computed.children.builder(computed);

                        const spy = jest.fn();
                        value[childrenUnlink] = spy;
                        spies.push(spy);

                        instance[property].push(value);
                    }

                    instance[remove](computed.children.builder(computed));

                    spies.forEach(spy => {
                        expect(spy).not.toHaveBeenCalled();
                    });
                });
                it(`The method use ${child}.${childrenUnlink} method when the value is removed`, () => {
                    const instance = computed.parent.builder(computed);

                    const length = Math.floor(10 + Math.random() * 50);
                    for(let i=0;i<length;i++){
                        const value = computed.children.builder(computed);

                        instance[property].push(value);
                    }

                    const index = Math.floor(instance[property].length * Math.random());
                    const rand = instance[property][index];

                    const spy = jest.fn();
                    rand[childrenUnlink] = spy;

                    instance[remove](rand);

                    expect(spy).toHaveBeenCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1);
                });

                if (typeof contain === "string"){
                    it(`The method doesn't use ${child}.${childrenUnlink} method if the method ${name}.${contain} result false`, () => {
                        const instance = computed.parent.builder(computed);

                        const length = Math.floor(10 + Math.random() * 50);
                        for(let i=0;i<length;i++){
                            const value = computed.children.builder(computed);

                            instance[property].push(value);
                        }

                        instance[contain] = jest.fn().mockReturnValue(false);

                        const index = Math.floor(instance[property].length * Math.random());
                        const rand = instance[property][index];

                        const spy = jest.fn();
                        rand[childrenUnlink] = spy;

                        instance[remove](rand);

                        expect(spy).not.toHaveBeenCalled();
                    });

                    it(`The method use ${child}.${childrenUnlink} method if the method ${name}.${contain} result true`, () => {
                        const instance = computed.parent.builder(computed);

                        instance[contain] = jest.fn().mockReturnValue(true);

                        const rand = computed.children.builder(computed);
                        const spy = jest.fn();
                        rand[childrenUnlink] = spy;

                        instance[remove](rand);

                        expect(spy).toHaveBeenCalledTimes(1);
                        expect(spy).toHaveBeenNthCalledWith(1);
                    });
                }
            })
        }

        computed.after(computed);
    });
};

export default testOneToManyParentUnlink;