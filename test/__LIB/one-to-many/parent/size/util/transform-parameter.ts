import TestOneToManyParentSizeParameter from "./type/parameter/item";
import TestOneToManyParentSize from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyParentSize = (input: TestOneToManyParentSizeParameter): TestOneToManyParentSize => {
    const base = parameterOneToManyBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterOneToManyParentSize;