import TestOneToManyParentSizeOption from "../computed/option";
import TestOneToManyParentItemOverrider from "../../../../type/overrider/item";

interface TestOneToManyParentSizeOverrider extends TestOneToManyParentItemOverrider<TestOneToManyParentSizeOption>{

}

export default TestOneToManyParentSizeOverrider;