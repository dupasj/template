import TestOneToManyParentSizeOptionParameter from "./option";
import TestOneToManyItemParameter from "../../../../../util/parameter/item";

interface TestOneToManyParentSizeParameter extends TestOneToManyItemParameter<TestOneToManyParentSizeOptionParameter>{

}

export default TestOneToManyParentSizeParameter;