import TestOneToManyParentSizeOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyParentSize extends TestOneToManyItem<TestOneToManyParentSizeOption>{

}

export default TestOneToManyParentSize;