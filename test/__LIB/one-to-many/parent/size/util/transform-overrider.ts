import TestOneToManyParentSizeOverrider from "./type/overrider/item";
import TestOneToManyParentSize from "./type/computed/item";
import TestOneToManyBase from "../../../util/computed/base";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyParentSize = (base: TestOneToManyBase, overrider: TestOneToManyParentSizeOverrider): TestOneToManyParentSize => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderOneToManyParentSize;