import TestOneToManyParentDisposeOverrider from "./type/overrider/item";
import TestOneToManyParentDispose from "./type/computed/item";
import TestOneToManyBase from "../../../util/computed/base";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyParentDispose = (base: TestOneToManyBase, overrider: TestOneToManyParentDisposeOverrider): TestOneToManyParentDispose => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderOneToManyParentDispose;