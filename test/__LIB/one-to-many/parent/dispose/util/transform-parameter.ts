import TestOneToManyParentDisposeParameter from "./type/parameter/item";
import TestOneToManyParentDispose from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyParentDispose = (input: TestOneToManyParentDisposeParameter): TestOneToManyParentDispose => {
    const base = parameterOneToManyBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterOneToManyParentDispose;