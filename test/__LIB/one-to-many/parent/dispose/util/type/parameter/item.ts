import TestOneToManyParentDisposeOptionParameter from "./option";
import TestOneToManyItemParameter from "../../../../../util/parameter/item";

interface TestOneToManyParentDisposeParameter extends TestOneToManyItemParameter<TestOneToManyParentDisposeOptionParameter>{

}

export default TestOneToManyParentDisposeParameter;