import TestOneToManyParentDisposeOption from "../computed/option";
import TestOneToManyItemOverrider from "../../../../../util/overrider/item";
import TestOneToManyParentDisposeOptionOverrider from "./option";

interface TestOneToManyParentDisposeOverrider extends TestOneToManyItemOverrider<TestOneToManyParentDisposeOptionOverrider,TestOneToManyParentDisposeOption>{

}

export default TestOneToManyParentDisposeOverrider;