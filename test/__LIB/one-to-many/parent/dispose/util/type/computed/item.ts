import TestOneToManyParentDisposeOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyParentDispose extends TestOneToManyItem<TestOneToManyParentDisposeOption>{

}

export default TestOneToManyParentDispose;