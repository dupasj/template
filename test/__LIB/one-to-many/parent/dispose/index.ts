import TestOneToManyParentDisposeParameter from "./util/type/parameter/item";
import parameterOneToManyParentDispose from "./util/transform-parameter";

const testOneToManyParentDispose = (input: TestOneToManyParentDisposeParameter) => {
    const computed = parameterOneToManyParentDispose(input);

    const name = computed.parent.class.name;
    const property = computed.parent.property;
    const remove = computed.parent.method.unlink;
    const unsafe = computed.parent.method.list.unsafe;
    const dispose = computed.parent.method.dispose;

    if (typeof dispose !== "string"){
        return;
    }

    describe(`Test the ${name}.${dispose}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.parent.builder(computed);

            expect(instance[dispose]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.parent.builder(computed);

            expect(typeof instance[dispose]).toBe("function");
        });
        it(`The method return himself`, () => {
            const instance = computed.parent.builder(computed);

            expect(instance[dispose]()).toBe(instance);
        });

        it("The method dispose the array property", () => {
            const instance = computed.parent.builder(computed);

            const length = Math.floor(10 + Math.random() * 50);
            const value = computed.children.builder(computed);
            for(let i=0;i<length;i++){
                instance[property].push(value);
            }

            instance[dispose]();

            expect(instance[property].length).toBe(0);
        });
        if (typeof remove === "string"){
            it("The method use the "+name+"."+remove+" method to remove all the items", () => {
                const instance = computed.parent.builder(computed);

                const length = Math.floor(10 + Math.random() * 50);
                const value = computed.children.builder(computed);
                for(let i=0;i<length;i++){
                    instance[property].push(value);
                }

                const spy = jest.spyOn(instance,remove)

                instance[dispose]();

                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenNthCalledWith(1,instance[property]);
            });
            if (typeof unsafe === "string"){
                it("The method use the "+name+"."+remove+" method to remove all the items resulted by the "+name+"."+unsafe+" method", () => {
                    const instance = computed.parent.builder(computed);

                    const _property = {
                        length: Math.floor(10 + Math.random() * 50),
                        value: instance[property],
                    }
                    for(let i=0;i<_property.length;i++){
                        _property.value.push(computed.children.builder(computed));
                    }

                    const mock = {
                        length: Math.floor(10 + Math.random() * 50),
                        value: instance[property],
                    }
                    for(let i=0;i<mock.length;i++){
                        mock.value.push(computed.children.builder(computed));
                    }
                    if (mock.length === _property.length){
                        mock.value.length ++;
                        mock.value.push(computed.children.builder(computed));
                    }

                    instance[unsafe] = jest.fn().mockReturnValue(mock.value);

                    const spy = jest.spyOn(instance,remove)

                    instance[dispose]();

                    expect(spy).toHaveBeenCalled();
                    expect(spy).toHaveBeenNthCalledWith(1,mock.value);
                });
            }
        }

        computed.after(computed);
    });
};

export default testOneToManyParentDispose;