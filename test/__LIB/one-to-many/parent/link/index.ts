import TestOneToManyParentLinkParameter from "./util/type/parameter/item";
import parameterOneToManyParentLink from "./util/transform-parameter";
import Spy = jasmine.Spy;
import Mock = jest.Mock;

const testOneToManyParentLink = (input: TestOneToManyParentLinkParameter) => {
    const computed = parameterOneToManyParentLink(input);

    const name = computed.parent.class.name;
    const property = computed.parent.property;
    const link = computed.parent.method.link;
    const childrenLink = computed.children.method.link;
    const childrenName = computed.children.class.name;
    const contain = computed.parent.method.contain;
    const flat = computed.flat;

    if (typeof link !== "string"){
        return;
    }

    describe(`Test the ${name}.${link}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.parent.builder(computed);

            expect(instance[link]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.parent.builder(computed);

            expect(typeof instance[link]).toBe("function");
        });

        it("The method add the given items value", () => {
            const instance = computed.parent.builder(computed);

            const values: any[] = [];

            const length = Math.floor(10 + Math.random() * 50);
            for(let i=0;i<length;i++){
                const value = computed.children.builder(computed);
                values.push(value);
            }

            instance[link](values);

            for(const value of values){
                expect(instance[property]).toContain(value);
            }
        });
        it("The method cannot have duplicate items", () => {
            const instance = computed.parent.builder(computed);

            const values: any[] = [];

            function shuffle(array) {
                let currentIndex = array.length,  randomIndex;

                while (currentIndex != 0) {
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex--;

                    // And swap it with the current element.
                    [array[currentIndex], array[randomIndex]] = [
                        array[randomIndex], array[currentIndex]];
                }

                return array;
            }

            const length = Math.floor(10 + Math.random() * 50);
            for(let i=0;i<length;i++){
                const value = computed.children.builder(computed);
                const r = Math.random() * 5 + 1;
                for(let n=0;n<r;n++){
                    values.push(value);
                }
            }

            instance[link](values);

            const map = new Map<any,number>();
            for(const value of instance[property]){
                const acc = map.get(value) ?? 0;

                map.set(value,acc + 1);
            }

            for(const value of map.values()){
                expect(value).toBe(1);
            }
        });

        describe("The method return himself", () => {
            it(`The method return himself when no value has been added`, () => {
                const instance = computed.parent.builder(computed);

                const value = computed.children.builder(computed);
                instance[property] = [value];

                instance[link](value);

                expect(instance[link](value)).toBe(instance);
            });
            it(`The method return himself when the given value has been added`, () => {
                const instance = computed.parent.builder(computed);

                const value = computed.children.builder(computed);

                instance[link](value);

                expect(instance[link](value)).toBe(instance);
            });
        })

        describe("push method implementation",() => {
            it("The method use push method to add the given item value", () => {
                const instance = computed.parent.builder(computed);

                const value = computed.children.builder(computed);

                const spy = jest.spyOn(instance[property],"push");

                instance[link](value);

                expect(spy).toHaveBeenCalledTimes(1);
                expect(spy).toHaveBeenNthCalledWith(1,value);
            });
            it("The method doesn't use push method if the is in the list", () => {
                const instance = computed.parent.builder(computed);

                const value = computed.children.builder(computed);
                instance[property] = [value];

                const spy = jest.spyOn(instance[property],"push");

                instance[link](value);

                expect(spy).not.toHaveBeenCalled();
            });

            if (typeof contain === "string"){
                it("The method doesn't use push method if the method "+contain+" result true", () => {
                    const instance = computed.parent.builder(computed);

                    const value = computed.children.builder(computed);

                    const spy = jest.spyOn(instance[property],"push");
                    instance[contain] = jest.fn().mockReturnValue(true)

                    instance[link](value);

                    expect(spy).not.toHaveBeenCalled();
                });
                it("The method use push method if the method "+contain+" result false", () => {
                    const instance = computed.parent.builder(computed);

                    const value = computed.children.builder(computed);
                    instance[property] = [value]

                    const spy = jest.spyOn(instance[property],"push");
                    instance[contain] = jest.fn().mockReturnValueOnce(false).mockReturnValue(true)


                    instance[link](value);

                    expect(spy).toHaveBeenCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1,value);
                });
            }
        });

        if (flat){
            describe("flat function implementation",() => {
                it("The method use the flat function pass the argument into it", () => {
                    const spy = flat(computed);
                    spy.mockClear();

                    const value = computed.children.builder(computed);
                    const instance = computed.parent.builder(computed);

                    instance[link](value);

                    expect(spy).toHaveBeenNthCalledWith(1, [value]);
                })

                if (typeof contain === "string") {
                    it("The method use the flat result function to loop", () => {
                        const instance = computed.parent.builder(computed);

                        const length = Math.floor(10 + Math.random() * 50);
                        const mock: any[] = [];
                        const spies: Mock[] = [];

                        for (let i = 0; i < length; i++) {
                            const value = computed.children.builder(computed);

                            if (childrenLink){
                                const spy = jest.fn();
                                spies.push(spy);
                                value[childrenLink] = spy;
                            }

                            mock.push(value);
                        }

                        const spy = {
                            push: jest.spyOn(instance[property], "push"),
                            contain: jest.spyOn(instance, contain).mockReturnValue(false),
                            flat: (() => {
                                const spy = flat(computed);
                                spy.mockClear();

                                return spy.mockReturnValueOnce(mock);
                            })(),
                        }

                        instance[link]();

                        expect(spy.contain).toHaveBeenCalledTimes(mock.length);
                        expect(spy.push).toHaveBeenCalledTimes(mock.length);
                        mock.forEach((item, index) => {
                            expect(spy.contain).toHaveBeenNthCalledWith(index + 1, item);
                            expect(spy.push).toHaveBeenNthCalledWith(index + 1, item);
                        });
                        spies.forEach((spy) => {
                            expect(spy).toHaveBeenCalledTimes(1);
                        });
                    })
                }
            });
        }

        if (childrenLink){
            describe(`Test the implementation with the ${childrenName}.${childrenLink} and the ${name}.${link}`,() => {
                describe("push method implementation",() => {
                    it(`The method use the ${childrenName}.${childrenLink} method to add the given item value`, () => {
                        const instance = computed.parent.builder(computed);

                        const value = computed.children.builder(computed);

                        const spy = jest.spyOn(value,childrenLink);

                        instance[link](value);

                        expect(spy).toHaveBeenCalledTimes(1);
                        expect(spy).toHaveBeenNthCalledWith(1,instance);
                    });
                    it(`The method is not use ${childrenName}.${childrenLink} method if the is in the list`, () => {
                        const instance = computed.parent.builder(computed);

                        const value = computed.children.builder(computed);
                        instance[property] = [value];

                        const spy = jest.spyOn(value,childrenLink);

                        instance[link](value);

                        expect(spy).not.toHaveBeenCalled();
                    });

                    if (typeof contain === "string"){
                        it(`The method doesn't use ${childrenName}.${childrenLink} method method if the method ${name}.${contain} result true`, () => {
                            const instance = computed.parent.builder(computed);

                            const value = computed.children.builder(computed);

                            const spy = jest.spyOn(value,childrenLink);
                            instance[contain] = jest.fn().mockReturnValue(true)

                            instance[link](value);

                            expect(spy).not.toHaveBeenCalled();
                        });
                        it(`The method doesn't use ${childrenName}.${childrenLink} method method if the method ${name}.${contain} result true`, () => {
                            const instance = computed.parent.builder(computed);

                            const value = computed.children.builder(computed);
                            instance[property] = [value]

                            const spy = jest.spyOn(value,childrenLink);
                            instance[contain] = jest.fn().mockReturnValueOnce(false).mockReturnValue(true)

                            instance[link](value);

                            expect(spy).toHaveBeenCalledTimes(1);
                            expect(spy).toHaveBeenNthCalledWith(1,instance);
                        });
                    }
                });
            })
        }

        computed.after(computed);
    });
};

export default testOneToManyParentLink;