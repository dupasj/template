import TestOneToManyItemOverrider from "../../../../../util/overrider/item";
import TestOneToManyParentLinkOptionOverrider from "./option";
import TestOneToManyParentLinkOption from "../computed/option";

interface TestOneToManyParentLinkOverrider extends TestOneToManyItemOverrider<TestOneToManyParentLinkOptionOverrider,TestOneToManyParentLinkOption>{

}

export default TestOneToManyParentLinkOverrider;