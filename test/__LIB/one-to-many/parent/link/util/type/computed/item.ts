import TestOneToManyParentLinkOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyParentLink extends TestOneToManyItem<TestOneToManyParentLinkOption>{

}

export default TestOneToManyParentLink;