import TestOneToManyParentLinkOptionParameter from "./option";
import TestOneToManyItemParameter from "../../../../../util/parameter/item";

interface TestOneToManyParentLinkParameter extends TestOneToManyItemParameter<TestOneToManyParentLinkOptionParameter>{

}

export default TestOneToManyParentLinkParameter;