import TestOneToManyParentLinkParameter from "./type/parameter/item";
import TestOneToManyParentLink from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyParentLink = (input: TestOneToManyParentLinkParameter): TestOneToManyParentLink => {
    const base = parameterOneToManyBase(input);

    return {
        ... base,
        option: {},
    };
}

export default parameterOneToManyParentLink;