import TestOneToManyBase from "../../../util/computed/base";
import TestOneToManyParentLinkOverrider from "./type/overrider/item";
import TestOneToManyParentLink from "./type/computed/item";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyParentLink = (base: TestOneToManyBase, overrider: TestOneToManyParentLinkOverrider): TestOneToManyParentLink => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        ... computed,
        option: {},
    };
}

export default overriderOneToManyParentLink;