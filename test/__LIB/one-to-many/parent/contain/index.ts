import TestOneToManyParentContainParameter from "./util/type/parameter/item";
import parameterOneToManyParentContain from "./util/transform-parameter";

const testOneToManyParentContain = (input: TestOneToManyParentContainParameter) => {
    const computed = parameterOneToManyParentContain(input);

    const name = computed.parent.class.name;
    const property = computed.parent.property;
    const contain = computed.parent.method.contain;
    const flat = computed.flat;

    if (typeof contain !== "string"){
        return;
    }

    describe(`Test the ${name}.${contain}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.parent.builder(computed);

            expect(instance[contain]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.parent.builder(computed);

            expect(typeof instance[contain]).toBe("function");
        });

        it(`The method return true when argument is not given`, () => {
            const instance = computed.parent.builder(computed);

            instance[property] = computed.children.builder(computed);

            expect(instance[contain]()).toBeTruthy();
        });
        it(`The method return true when all the given argument is contained`, () => {
            const instance = computed.parent.builder(computed);

            const length = Math.floor(10 + Math.random() * 50);
            for(let i=0;i<length;i++){
                const value = computed.children.builder(computed);
                instance[property].push(value);
            }

            function shuffle(array) {
                let currentIndex = array.length,  randomIndex;

                while (currentIndex != 0) {
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex--;

                    // And swap it with the current element.
                    [array[currentIndex], array[randomIndex]] = [
                        array[randomIndex], array[currentIndex]];
                }

                return array;
            }

            const shuffled = shuffle(Array.from(instance[property]))

            expect(instance[contain](shuffled)).toBeTruthy();
        });

        it(`The method return false is one of all the argument is undefined`, () => {
            const instance = computed.parent.builder(computed);

            const length = Math.floor(10 + Math.random() * 50);
            for(let i=0;i<length;i++){
                const value = computed.children.builder(computed);
                instance[property].push(value);
            }

            function shuffle(array) {
                let currentIndex = array.length,  randomIndex;

                while (currentIndex != 0) {
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex--;

                    // And swap it with the current element.
                    [array[currentIndex], array[randomIndex]] = [
                        array[randomIndex], array[currentIndex]];
                }

                return array;
            }

            const shuffled = shuffle(Array.from([... instance[property], undefined]))

            expect(instance[contain](shuffled)).toBeFalsy();
        });

        it(`The method return false is one of all the argument is not contained`, () => {
            const instance = computed.parent.builder(computed);

            const length = Math.floor(10 + Math.random() * 50);
            for(let i=0;i<length;i++){
                const value = computed.children.builder(computed);
                instance[property].push(value);
            }

            function shuffle(array) {
                let currentIndex = array.length,  randomIndex;

                while (currentIndex != 0) {
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex--;

                    // And swap it with the current element.
                    [array[currentIndex], array[randomIndex]] = [
                        array[randomIndex], array[currentIndex]];
                }

                return array;
            }

            const shuffled = shuffle(Array.from([... instance[property], computed.children.builder(computed)]))

            expect(instance[contain](shuffled)).toBeFalsy();
        });

        if (flat){
            describe("flat function implementation",() => {
                it("The method use the flat function pass the argument into it", () => {
                    const spy = flat(computed);
                    spy.mockClear();

                    const value = computed.children.builder(computed);
                    const instance = computed.parent.builder(computed);

                    instance[contain](value);

                    expect(spy).toHaveBeenNthCalledWith(1, [value]);
                })
                it("The method use the every's result of the result of the flat function", () => {
                    const mocked = [];

                    {
                        const spy = flat(computed);
                        spy.mockClear();

                        spy.mockReturnValueOnce(mocked);
                    }

                    const value = computed.children.builder(computed) as any;
                    jest.spyOn(mocked,"every").mockReturnValue(value);

                    const instance = computed.parent.builder(computed);

                    expect(instance[contain]()).toBe(value)
                })
            });
        }

        computed.after(computed);
    });
};

export default testOneToManyParentContain;