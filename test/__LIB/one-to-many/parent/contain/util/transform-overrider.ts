import TestOneToManyParentContainOverrider from "./type/overrider/item";
import TestOneToManyParentContain from "./type/computed/item";
import TestOneToManyBase from "../../../util/computed/base";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyParentContain = (base: TestOneToManyBase, overrider: TestOneToManyParentContainOverrider): TestOneToManyParentContain => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderOneToManyParentContain;