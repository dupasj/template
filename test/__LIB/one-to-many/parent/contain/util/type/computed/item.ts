import TestOneToManyParentContainOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyParentContain extends TestOneToManyItem<TestOneToManyParentContainOption>{

}

export default TestOneToManyParentContain;