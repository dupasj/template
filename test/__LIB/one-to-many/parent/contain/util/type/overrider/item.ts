import TestOneToManyParentContainOption from "../computed/option";
import TestOneToManyParentContainOptionOverrider from "./option";
import TestOneToManyItemOverrider from "../../../../../util/overrider/item";

interface TestOneToManyParentContainOverrider extends TestOneToManyItemOverrider<TestOneToManyParentContainOptionOverrider,TestOneToManyParentContainOption>{

}

export default TestOneToManyParentContainOverrider;