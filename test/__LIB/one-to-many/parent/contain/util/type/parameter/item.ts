import TestOneToManyParentContainOptionParameter from "./option";
import TestOneToManyItemParameter from "../../../../../util/parameter/item";
import TestOneToManyParentContainOption from "../computed/option";

interface TestOneToManyParentContainParameter extends TestOneToManyItemParameter<TestOneToManyParentContainOptionParameter,TestOneToManyParentContainOption>{

}

export default TestOneToManyParentContainParameter;