import TestOneToManyParentContainParameter from "./type/parameter/item";
import TestOneToManyParentContain from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyParentContain = (input: TestOneToManyParentContainParameter): TestOneToManyParentContain => {
    const base = parameterOneToManyBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterOneToManyParentContain;