import parameterOneToManyChildrenDefined from "./util/transform-parameter";
import TestOneToManyChildrenDefinedParameter from "./util/type/parameter/item";

const testOneToManyChildrenDefined = (input: TestOneToManyChildrenDefinedParameter) => {
    const computed = parameterOneToManyChildrenDefined(input);

    const name = computed.children.class.name;
    const property = computed.children.property;

    const defined = computed.children.method.defined;

    if (typeof defined !== "string"){
        return;
    }

    describe(`Test the method ${name}.${defined}`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.children.builder(computed);

            expect(instance[defined]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.children.builder(computed);

            expect(typeof instance[defined]).toBe("function");
        });
        it(`The method returns false when the ${name}.${property} value is undefined`, () => {
            const instance = computed.children.builder(computed);

            instance[property] = undefined;

            expect(instance[defined]()).toBeFalsy();
        });
        it(`The method returns false when the ${name}.${property} does not exist`, () => {
            const instance = computed.children.builder(computed);

            delete instance[property];

            expect(instance[defined]()).toBeFalsy();
        });
        it(`The method returns true when the ${name}.${property} value is defined`, () => {
            const instance = computed.children.builder(computed);

            instance[property] = computed.parent.builder(computed);

            expect(instance[defined]()).toBeTruthy();
        });

        computed.after(computed);
    });
};

export default testOneToManyChildrenDefined;