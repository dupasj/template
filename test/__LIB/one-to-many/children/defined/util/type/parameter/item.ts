import TestOneToManyChildrenDefinedOptionParameter from "./option";
import TestOneToManyItemParameter from "../../../../../util/parameter/item";
import TestOneToManyChildrenDefinedOption from "../computed/option";

interface TestOneToManyChildrenDefinedParameter extends TestOneToManyItemParameter<TestOneToManyChildrenDefinedOptionParameter,TestOneToManyChildrenDefinedOption>{

}

export default TestOneToManyChildrenDefinedParameter;