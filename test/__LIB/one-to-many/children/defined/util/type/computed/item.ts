import TestOneToManyChildrenDefinedOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyChildrenDefined extends TestOneToManyItem<TestOneToManyChildrenDefinedOption>{

}

export default TestOneToManyChildrenDefined;