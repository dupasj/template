import TestOneToManyChildrenDefinedOption from "../computed/option";
import TestOneToManyChildrenDefinedOptionOverrider from "./option";
import TestOneToManyItemOverrider from "../../../../../util/overrider/item";

interface TestOneToManyChildrenDefinedOverrider extends TestOneToManyItemOverrider<TestOneToManyChildrenDefinedOptionOverrider,TestOneToManyChildrenDefinedOption>{

}

export default TestOneToManyChildrenDefinedOverrider;