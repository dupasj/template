import TestOneToManyChildrenDefinedOverrider from "./type/overrider/item";
import TestOneToManyChildrenDefined from "./type/computed/item";
import TestOneToManyBase from "../../../util/computed/base";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyChildrenDefined = (base: TestOneToManyBase, overrider: TestOneToManyChildrenDefinedOverrider): TestOneToManyChildrenDefined => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderOneToManyChildrenDefined;