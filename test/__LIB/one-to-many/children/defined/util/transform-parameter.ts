import TestOneToManyChildrenDefinedParameter from "./type/parameter/item";
import TestOneToManyChildrenDefined from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyChildrenDefined = (input: TestOneToManyChildrenDefinedParameter): TestOneToManyChildrenDefined => {
    const base = parameterOneToManyBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterOneToManyChildrenDefined;