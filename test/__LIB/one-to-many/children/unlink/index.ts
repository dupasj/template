import TestOneToManyChildrenUnlinkParameter from "./util/type/parameter/item";
import parameterOneToManyChildrenUnlink from "./util/transform-parameter";
import spyOnValueProperty from "../../../util/spy-on-property";

const testOneToManyChildrenUnlink = (input: TestOneToManyChildrenUnlinkParameter) => {
    const computed = parameterOneToManyChildrenUnlink(input);

    const name = computed.children.class.name;
    const property = computed.children.property;

    const erase = computed.children.method.unlink;

    if (typeof erase !== "string"){
        return;
    }

    describe(`Test the ${name}.${erase}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.children.builder(computed);

            expect(instance[erase]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.children.builder(computed);

            expect(typeof instance[erase]).toBe("function");
        });
        it(`The method return himself when the value is undefined`, () => {
            const instance = computed.children.builder(computed);

            expect(instance[erase]()).toBe(instance);
        });
        it(`The method return himself when the value is defined`, () => {
            const instance = computed.children.builder(computed);

            expect(instance[erase]()).toBe(instance);
        });

        it(`The method set the ${name}.${property}'s value as undefined when the value is defined`, () => {
            const instance = computed.children.builder(computed);

            instance[property] = computed.parent.builder(computed);

            const spy = spyOnValueProperty(instance,property,"set");

            instance[erase]();

            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenNthCalledWith(1,undefined);
        });
        it(`The method set the ${name}.${property}'s value as undefined when the value is undefined`, () => {
            const instance = computed.children.builder(computed);

            const spy = spyOnValueProperty(instance,property,"set");

            instance[erase]();

            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenNthCalledWith(1,undefined);
        });


        computed.after(computed);
    });
};

export default testOneToManyChildrenUnlink;