import TestOneToManyChildrenUnlinkOverrider from "./type/overrider/item";
import TestOneToManyChildrenUnlink from "./type/computed/item";
import TestOneToManyBase from "../../../util/computed/base";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyChildrenUnlink = (base: TestOneToManyBase, overrider: TestOneToManyChildrenUnlinkOverrider): TestOneToManyChildrenUnlink => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderOneToManyChildrenUnlink;