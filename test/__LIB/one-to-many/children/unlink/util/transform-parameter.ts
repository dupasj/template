import TestOneToManyChildrenUnlinkParameter from "./type/parameter/item";
import TestOneToManyChildrenUnlink from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyChildrenUnlink = (input: TestOneToManyChildrenUnlinkParameter): TestOneToManyChildrenUnlink => {
    const base = parameterOneToManyBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterOneToManyChildrenUnlink;