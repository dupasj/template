import TestOneToManyChildrenUnlinkOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyChildrenUnlink extends TestOneToManyItem<TestOneToManyChildrenUnlinkOption>{

}

export default TestOneToManyChildrenUnlink;