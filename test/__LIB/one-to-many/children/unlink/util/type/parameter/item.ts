import TestOneToManyChildrenUnlinkOptionParameter from "./option";
import TestOneToManyItemParameter from "../../../../../util/parameter/item";
import TestOneToManyChildrenUnlinkOption from "../computed/option";

interface TestOneToManyChildrenUnlinkParameter extends TestOneToManyItemParameter<TestOneToManyChildrenUnlinkOptionParameter,TestOneToManyChildrenUnlinkOption>{

}

export default TestOneToManyChildrenUnlinkParameter;