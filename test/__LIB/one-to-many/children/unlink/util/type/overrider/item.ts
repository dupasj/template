import TestOneToManyChildrenUnlinkOption from "../computed/option";
import TestOneToManyChildrenUnlinkOptionOverrider from "./option";
import TestOneToManyItemOverrider from "../../../../../util/overrider/item";

interface TestOneToManyChildrenUnlinkOverrider extends TestOneToManyItemOverrider<TestOneToManyChildrenUnlinkOptionOverrider,TestOneToManyChildrenUnlinkOption>{

}

export default TestOneToManyChildrenUnlinkOverrider;