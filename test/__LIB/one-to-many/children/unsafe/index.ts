import TestOneToManyChildrenUnsafe from "./util/type/computed/item";
import parameterOneToManyChildrenUnsafe from "./util/transform-parameter";

const testOneToManyChildrenUnsafe = (input: TestOneToManyChildrenUnsafe) => {
    const computed = parameterOneToManyChildrenUnsafe(input);

    const name = computed.children.class.name;
    const unsafe = computed.children.method.unsafe;
    const property = computed.children.property;

    if (typeof unsafe !== "string"){
        return;
    }

    describe(`Test the ${name}.${unsafe}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.children.builder(computed);

            expect(instance[unsafe]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.children.builder(computed);

            expect(typeof instance[unsafe]).toBe("function");
        });
        it(`The method return undefined when the ${name}.${property} value is undefined`, () => {
            const instance = computed.children.builder(computed);

            instance[property] = undefined;

            expect(instance[unsafe]()).toBeFalsy();
        });
        it(`The method return undefined when the ${name}.${property} value does not exist`, () => {
            const instance = computed.children.builder(computed);

            delete instance[property];

            expect(instance[unsafe]()).toBeFalsy();
        });
        it(`The method return the ${name}.${property}'s value when the ${name}.${property} value is defined`, () => {
            const instance = computed.children.builder(computed);

            const value = computed.parent.builder(computed);

            instance[property] = value;

            expect(instance[unsafe]()).toBe(value);
        });

        computed.after(computed);
    });
};

export default testOneToManyChildrenUnsafe;