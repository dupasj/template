import TestOneToManyChildrenUnsafeOptionParameter from "./option";
import TestOneToManyItemParameter from "../../../../../util/parameter/item";
import TestOneToManyChildrenUnsafeOption from "../computed/option";

interface TestOneToManyChildrenUnsafeParameter extends TestOneToManyItemParameter<TestOneToManyChildrenUnsafeOptionParameter,TestOneToManyChildrenUnsafeOption>{

}

export default TestOneToManyChildrenUnsafeParameter;