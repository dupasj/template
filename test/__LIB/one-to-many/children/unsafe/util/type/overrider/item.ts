import TestOneToManyChildrenUnsafeOption from "../computed/option";
import TestOneToManyChildrenUnsafeOptionOverrider from "./option";
import TestOneToManyItemOverrider from "../../../../../util/overrider/item";

interface TestOneToManyChildrenUnsafeOverrider extends TestOneToManyItemOverrider<TestOneToManyChildrenUnsafeOptionOverrider,TestOneToManyChildrenUnsafeOption>{

}

export default TestOneToManyChildrenUnsafeOverrider;