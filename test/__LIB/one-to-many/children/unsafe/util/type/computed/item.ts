import TestOneToManyChildrenUnsafeOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyChildrenUnsafe extends TestOneToManyItem<TestOneToManyChildrenUnsafeOption>{

}

export default TestOneToManyChildrenUnsafe;