import TestOneToManyChildrenUnsafeOverrider from "./type/overrider/item";
import TestOneToManyChildrenUnsafe from "./type/computed/item";
import TestOneToManyBase from "../../../util/computed/base";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyChildrenUnsafe = (base: TestOneToManyBase, overrider: TestOneToManyChildrenUnsafeOverrider): TestOneToManyChildrenUnsafe => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderOneToManyChildrenUnsafe;