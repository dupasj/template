import TestOneToManyChildrenUnsafeParameter from "./type/parameter/item";
import TestOneToManyChildrenUnsafe from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyChildrenUnsafe = (input: TestOneToManyChildrenUnsafeParameter): TestOneToManyChildrenUnsafe => {
    const base = parameterOneToManyBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterOneToManyChildrenUnsafe;