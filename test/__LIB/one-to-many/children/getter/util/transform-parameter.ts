import TestOneToManyChildrenGetterParameter from "./type/parameter/item";
import TestOneToManyChildrenGetter from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyChildrenGetter = (input: TestOneToManyChildrenGetterParameter): TestOneToManyChildrenGetter => {
    const base = parameterOneToManyBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterOneToManyChildrenGetter;