import TestOneToManyChildrenGetterOptionParameter from "./option";
import TestOneToManyItemParameter from "../../../../../util/parameter/item";
import TestOneToManyChildrenGetterOption from "../computed/option";

interface TestOneToManyChildrenGetterParameter extends TestOneToManyItemParameter<TestOneToManyChildrenGetterOptionParameter,TestOneToManyChildrenGetterOption>{

}

export default TestOneToManyChildrenGetterParameter;