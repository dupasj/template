import TestOneToManyChildrenGetterOption from "../computed/option";
import TestOneToManyChildrenGetterOptionOverrider from "./option";
import TestOneToManyItemOverrider from "../../../../../util/overrider/item";

interface TestOneToManyChildrenGetterOverrider extends TestOneToManyItemOverrider<TestOneToManyChildrenGetterOptionOverrider,TestOneToManyChildrenGetterOption>{

}

export default TestOneToManyChildrenGetterOverrider;