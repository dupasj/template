import TestOneToManyChildrenGetterOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyChildrenGetter extends TestOneToManyItem<TestOneToManyChildrenGetterOption>{

}

export default TestOneToManyChildrenGetter;