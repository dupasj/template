import TestOneToManyChildrenGetterOverrider from "./type/overrider/item";
import TestOneToManyChildrenGetter from "./type/computed/item";
import TestOneToManyBase from "../../../util/computed/base";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyChildrenGetter = (base: TestOneToManyBase, overrider: TestOneToManyChildrenGetterOverrider): TestOneToManyChildrenGetter => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderOneToManyChildrenGetter;