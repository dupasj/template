import TestOneToManyChildrenGetterParameter from "./util/type/parameter/item";
import parameterOneToManyChildrenGetter from "./util/transform-parameter";

const testOneToManyChildrenGetter = (input: TestOneToManyChildrenGetterParameter) => {
    const computed = parameterOneToManyChildrenGetter(input);

    const name = computed.children.class.name;
    const property = computed.children.property;

    const error = computed.error;
    const unsafe = computed.children.method.unsafe;
    const getter = computed.children.method.getter;

    if (typeof getter !== "string"){
        return;
    }

    describe(`Test the ${name}.${getter} method`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.children.builder(computed);

            expect(instance[getter]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.children.builder(computed);

            expect(typeof instance[getter]).toBe("function");
        });


        describe(`Test the ${name}.${getter} method return value`,() => {
            it(`The method returns the ${name}.${property}'s value`, () => {
                const instance = computed.children.builder(computed);

                const value = computed.parent.builder(computed);
                instance[property] = value;

                expect(instance[getter]()).toBe(value);
            });
            if (typeof unsafe === "string") {
                it(`The method calls the ${name}.${unsafe} method`, () => {
                    const instance = computed.children.builder(computed);

                    instance[property] = computed.parent.builder(computed);
                    const spy = jest.spyOn(instance,unsafe);

                    instance[getter]();

                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1);
                });
                it(`The method returns the value from the ${name}.${unsafe} method result, not from the ${name}.${property} property's value`, () => {
                    const instance = computed.children.builder(computed);

                    instance[property] = computed.parent.builder(computed);
                    const value = computed.parent.builder(computed);

                    instance[unsafe] = jest.fn().mockReturnValue(value);

                    expect(instance[getter]()).toBe(value);
                });
            }
        });

        if (error){
            it(`The method throws an error if the value from the ${name}.${property} property's value is undefined`, () => {
                const instance = computed.children.builder(computed);

                const result = (() => {
                    try{
                        instance[getter]();
                    }catch (e){
                        return e;
                    }

                    return undefined;
                })();

                error(computed,result);
            });

            if (unsafe){
                it(`The method throws an error if the value from the ${name}.${unsafe}() method result is undefined, not if the ${name}.${property} property's value is undefined`, () => {
                    const instance = computed.children.builder(computed);

                    instance[property] = computed.parent.builder(computed);
                    instance[unsafe] = jest.fn().mockReturnValue(undefined);

                    const result = (() => {
                        try{
                            instance[getter]();
                        }catch (e){
                            return e;
                        }

                        return undefined;
                    })();

                    error(computed,result);
                });
            }
        }

        computed.after(computed);
    });
};

export default testOneToManyChildrenGetter;