import TestOneToManyChildrenLinkOverrider from "./type/overrider/item";
import TestOneToManyChildrenLink from "./type/computed/item";
import TestOneToManyBase from "../../../util/computed/base";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyChildrenLink = (base: TestOneToManyBase, overrider: TestOneToManyChildrenLinkOverrider): TestOneToManyChildrenLink => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderOneToManyChildrenLink;