import TestOneToManyChildrenLinkParameter from "./type/parameter/item";
import TestOneToManyChildrenLink from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyChildrenLink = (input: TestOneToManyChildrenLinkParameter): TestOneToManyChildrenLink => {
    const base = parameterOneToManyBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterOneToManyChildrenLink;