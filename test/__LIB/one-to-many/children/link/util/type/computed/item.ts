import TestOneToManyChildrenLinkOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyChildrenLink extends TestOneToManyItem<TestOneToManyChildrenLinkOption>{

}

export default TestOneToManyChildrenLink;