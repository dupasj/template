import TestOneToManyChildrenLinkOptionParameter from "./option";
import TestOneToManyItemParameter from "../../../../../util/parameter/item";
import TestOneToManyChildrenLinkOption from "../computed/option";

interface TestOneToManyChildrenLinkParameter extends TestOneToManyItemParameter<TestOneToManyChildrenLinkOptionParameter,TestOneToManyChildrenLinkOption>{

}

export default TestOneToManyChildrenLinkParameter;