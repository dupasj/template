import TestOneToManyChildrenLinkOption from "../computed/option";
import TestOneToManyChildrenLinkOptionOverrider from "./option";
import TestOneToManyItemOverrider from "../../../../../util/overrider/item";

interface TestOneToManyChildrenLinkOverrider extends TestOneToManyItemOverrider<TestOneToManyChildrenLinkOptionOverrider,TestOneToManyChildrenLinkOption>{

}

export default TestOneToManyChildrenLinkOverrider;