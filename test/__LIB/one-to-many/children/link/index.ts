import TestOneToManyChildrenLinkParameter from "./util/type/parameter/item";
import parameterOneToManyChildrenLink from "./util/transform-parameter";
import spyOnValueProperty from "../../../util/spy-on-property";

const testOneToManyChildrenLink = (input: TestOneToManyChildrenLinkParameter) => {
    const computed = parameterOneToManyChildrenLink(input);

    const name = computed.children.class.name;
    const property = computed.children.property;

    const is = computed.children.method.is;
    const unlink = computed.children.method.unlink;
    const link = computed.children.method.link;
    const parentLink = computed.parent.method.link;
    const parentName = computed.parent.class.name;
    const parentContain = computed.parent.method.contain;

    if (typeof link !== "string"){
        return;
    }

    describe(`Test the ${name}.${link} method`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.children.builder(computed);

            expect(instance[link]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.children.builder(computed);

            expect(typeof instance[link]).toBe("function");
        });

        describe(`Test the ${name}.${link} method return value`,() => {
            it(`The method returns himself when the value doesn't change`, () => {
                const instance = computed.children.builder(computed);

                const value = computed.parent.builder(computed);
                instance[property] = value;

                expect(instance[link](value)).toBe(instance);
            });
            it(`The method returns himself when the value was undefined`, () => {
                const instance = computed.children.builder(computed);

                expect(instance[link](computed.parent.builder(computed))).toBe(instance);
            });
            it(`The method returns himself when the value change`, () => {
                const instance = computed.children.builder(computed);

                instance[property] = computed.parent.builder(computed);

                expect(instance[link](computed.parent.builder(computed))).toBe(instance);
            });

            if (is){
                it(`The method returns himself when the method ${name}.${is} returns true`, () => {
                    const instance = computed.children.builder(computed);

                    instance[property] = computed.parent.builder(computed);
                    instance[is] = jest.fn().mockReturnValue(true);

                    expect(instance[link](computed.parent.builder(computed))).toBe(instance);
                });
                it(`The method returns himself when the method ${name}.${is} returns false`, () => {
                    const instance = computed.children.builder(computed);

                    const value = computed.parent.builder(computed);
                    instance[property] = value;
                    instance[is] = jest.fn().mockReturnValue(false);

                    expect(instance[link](value)).toBe(instance);
                });
            }
        });
        describe(`Test the ${name}.${link} method value assignment`,() => {
            it(`The method's return does not assign the value when the value doesn't change`, () => {
                const instance = computed.children.builder(computed);

                const value = computed.parent.builder(computed);
                instance[property] = value;

                const spy = spyOnValueProperty(instance,property,"set");

                instance[link](value);

                expect(spy).not.toBeCalled();
            });
            it(`The method assigns the value when the value was undefined`, () => {
                const instance = computed.children.builder(computed);
                const spy = spyOnValueProperty(instance,property,"set");

                if (unlink){
                    instance[unlink] = jest.fn();
                }

                const value = computed.parent.builder(computed);
                instance[link](value);

                expect(spy).toBeCalledTimes(1);
                expect(spy).toHaveBeenNthCalledWith(1,value);
            });
            it(`The method assigns the value when the value change`, () => {
                const instance = computed.children.builder(computed);

                instance[property] = computed.parent.builder(computed);
                if (unlink){
                    instance[unlink] = jest.fn();
                }

                const value = computed.parent.builder(computed);


                const spy = spyOnValueProperty(instance,property,"set");

                instance[link](value);

                expect(spy).toBeCalledTimes(1);
                expect(spy).toHaveBeenNthCalledWith(1,value);
            });

            if (is){
                it(`The method doesn't assign the value when the method ${name}.${is} returns true`, () => {
                    const instance = computed.children.builder(computed);

                    instance[property] = computed.parent.builder(computed);
                    instance[is] = jest.fn().mockReturnValue(true);
                    if (unlink){
                        instance[unlink] = jest.fn();
                    }

                    const spy = spyOnValueProperty(instance,property,"set");

                    const value = computed.parent.builder(computed);
                    instance[link](value);


                    expect(spy).not.toHaveBeenCalled();
                });
                it(`The method assigns the value when the method ${name}.${is} returns false`, () => {
                    const instance = computed.children.builder(computed);

                    const value = computed.parent.builder(computed);
                    instance[property] = value;
                    instance[is] = jest.fn().mockReturnValue(false);
                    if (unlink){
                        instance[unlink] = jest.fn();
                    }

                    const spy = spyOnValueProperty(instance,property,"set");

                    instance[link](value);

                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1,value);
                });
            }
        });



        if (unlink){
            describe(`Test the ${name}.${unlink} integration in the method ${name}.${link}`,() => {
                it(`The method's return does not run ${name}.${unlink} method when the value doesn't change`, () => {
                    const instance = computed.children.builder(computed);

                    const value = computed.parent.builder(computed);
                    instance[property] = value;

                    const spy = jest.spyOn(instance,unlink);

                    instance[link](value);

                    expect(spy).not.toBeCalled();
                });
                it(`The method runs the method ${name}.${unlink} when the value was undefined`, () => {
                    const instance = computed.children.builder(computed);
                    const spy = jest.spyOn(instance,unlink);

                    instance[link](computed.parent.builder(computed));

                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1);
                });
                it(`The method runs the method ${name}.${unlink} when the value change`, () => {
                    const instance = computed.children.builder(computed);

                    instance[property] = computed.parent.builder(computed);
                    const spy = jest.spyOn(instance,unlink);

                    instance[link](computed.parent.builder(computed));

                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1);
                });

                it(`The method runs the method ${name}.${unlink} method before the value assignment when the value was undefined`, () => {
                    const instance = computed.children.builder(computed);
                    const before = jest.fn().mockReturnValue(instance);
                    instance[unlink] = before;
                    const after = spyOnValueProperty(instance,property,"set");

                    instance[link](computed.parent.builder(computed));

                    // @ts-ignore
                    expect(before).toHaveBeenCalledBefore(after);
                });
                it(`The method runs the method ${name}.${unlink} before the value assignment when the value change`, () => {
                    const instance = computed.children.builder(computed);
                    instance[property] = computed.parent.builder(computed);

                    const before = jest.fn().mockReturnValue(instance);
                    instance[unlink] = before;
                    const after = spyOnValueProperty(instance,property,"set");

                    instance[link](computed.parent.builder(computed));

                    // @ts-ignore
                    expect(before).toHaveBeenCalledBefore(after);
                });


                if (is){
                    it(`The method's return does not run ${name}.${unlink} method when the method ${name}.${is} returns true`, () => {
                        const instance = computed.children.builder(computed);

                        instance[property] = computed.parent.builder(computed);
                        instance[is] = jest.fn().mockReturnValue(true);

                        const spy = jest.spyOn(instance,unlink);

                        instance[link](computed.parent.builder(computed));

                        expect(spy).not.toBeCalled();
                    });
                    it(`The method runs the method ${name}.${unlink} when the method ${name}.${is} returns false`, () => {
                        const instance = computed.children.builder(computed);

                        const value = computed.parent.builder(computed);
                        instance[property] = value;
                        instance[is] = jest.fn().mockReturnValue(false);

                        const spy = jest.spyOn(instance,unlink);

                        instance[link](value);

                        expect(spy).toBeCalledTimes(2);
                        expect(spy).toHaveBeenNthCalledWith(1);
                        expect(spy).toHaveBeenNthCalledWith(2);
                    });

                    it(`The method runs the method ${name}.${unlink} before the value assignment when the method ${name}.${is} returns false`, () => {
                        const instance = computed.children.builder(computed);

                        const value = computed.parent.builder(computed);
                        instance[property] = value;
                        instance[is] = jest.fn().mockReturnValue(false);

                        const before = jest.fn().mockReturnValue(instance);
                        instance[unlink] = before;
                        const after = spyOnValueProperty(instance,property,"set");

                        // @ts-ignore
                        instance[link](computed.parent.builder(computed));

                        // @ts-ignore
                        expect(before).toHaveBeenCalledBefore(after);
                    });
                }
            });
        }

        if (parentContain){
            describe(`The implementation between ${parentName}.${parentContain} and ${name}.${link}`,() => {
                it(`The method uses ${parentName}.${parentContain} if the value doesn't exit`,() => {
                    const instance = computed.children.builder(computed);

                    const value = computed.parent.builder(computed);
                    const spy = jest.spyOn(value,parentContain);

                    instance[link](value);

                    expect(spy).toHaveBeenCalled();
                    expect(spy).toHaveBeenNthCalledWith(1,instance);
                });
                it(`The method doesn't use ${parentName}.${parentContain} if the value exit`,() => {
                    const instance = computed.children.builder(computed);

                    const value = computed.parent.builder(computed);
                    const spy = jest.spyOn(value,parentContain);

                    instance[property] = value;

                    instance[link](value);

                    expect(spy).not.toHaveBeenCalled();
                });

                if (is){
                    it(`The method uses ${parentName}.${parentLink} if method ${name}.${is} return false`,() => {
                        const instance = computed.children.builder(computed);

                        const value = computed.parent.builder(computed);

                        instance[is] = jest.fn().mockReturnValueOnce(false).mockReturnValue(true);

                        const spy = jest.spyOn(value,parentContain);

                        instance[link](value);

                        expect(spy).toHaveBeenCalled();
                        expect(spy).toHaveBeenNthCalledWith(1,instance);
                    });
                    it(`The method doesn't use ${parentName}.${parentLink} if method ${name}.${is} return true`,() => {
                        const instance = computed.children.builder(computed);
                        instance[is] = jest.fn().mockReturnValue(true);

                        const value = computed.parent.builder(computed);
                        const spy = jest.spyOn(value,parentContain);

                        instance[link](value);

                        expect(spy).not.toHaveBeenCalled();
                    });
                }
            });
        }

        if (parentLink){
            describe(`The implementation between ${parentName}.${parentLink} and ${name}.${link}`,() => {
                it(`The method uses ${parentName}.${parentLink} if the value doesn't exit`,() => {
                    const instance = computed.children.builder(computed);

                    const value = computed.parent.builder(computed);
                    const spy = jest.spyOn(value,parentLink);

                    instance[link](value);

                    expect(spy).toHaveBeenCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1,instance);
                });
                it(`The method doesn't use ${parentName}.${parentLink} if the value exit`,() => {
                    const instance = computed.children.builder(computed);

                    const value = computed.parent.builder(computed);
                    const spy = jest.spyOn(value,parentLink);

                    instance[property] = value;

                    instance[link](value);

                    expect(spy).not.toHaveBeenCalled();
                });

                if (parentContain){
                    it(`The method uses ${parentName}.${parentLink} if method ${parentName}.${parentContain} return false`,() => {
                        const instance = computed.children.builder(computed);

                        const value = computed.parent.builder(computed);

                        value[parentContain] = jest.fn().mockReturnValueOnce(false).mockReturnValue(true);

                        const spy = jest.spyOn(value,parentLink);

                        instance[link](value);

                        expect(spy).toHaveBeenCalledTimes(1);
                        expect(spy).toHaveBeenNthCalledWith(1,instance);
                    });
                    it(`The method doesn't use ${parentName}.${parentLink} if method ${parentName}.${parentContain} return true`,() => {
                        const instance = computed.children.builder(computed);

                        const value = computed.parent.builder(computed);
                        const spy = jest.spyOn(value,parentLink);

                        value[parentContain] = jest.fn().mockReturnValue(true);

                        instance[link](value);

                        expect(spy).not.toHaveBeenCalled();
                    });
                }
            });
        }

        computed.after(computed);
    });
};

export default testOneToManyChildrenLink;