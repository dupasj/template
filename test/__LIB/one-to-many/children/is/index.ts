import TestOneToManyChildrenIsParameter from "./util/type/parameter/item";
import parameterOneToManyChildrenIs from "./util/transform-parameter";

const testOneToManyChildrenIs = (input: TestOneToManyChildrenIsParameter) => {
    const computed = parameterOneToManyChildrenIs(input);

    const name = computed.children.class.name;
    const property = computed.children.property;

    const is = computed.children.method.is;
    const unsafe = computed.children.method.unsafe;

    if (typeof is !== "string"){
        return;
    }

    describe(`Test the ${name}.${is}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.children.builder(computed);

            expect(instance[is]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.children.builder(computed);

            expect(typeof instance[is]).toBe("function");
        });

        it(`The method return false when the given argument is undefined`, () => {
            const instance = computed.children.builder(computed);

            instance[property] = computed.parent.builder(computed);

            expect(instance[is](undefined)).toBeFalsy();
        });
        it(`The method return false when argument is not given`, () => {
            const instance = computed.children.builder(computed);

            instance[property] = computed.parent.builder(computed);

            expect(instance[is]()).toBeFalsy();
        });


        it(`The method return false when the ${name}.${property} value and the given argument are undefined`, () => {
            const instance = computed.children.builder(computed);

            expect(instance[is](undefined)).toBeFalsy();
        });
        it(`The method return false when the ${name}.${property} value is undefined and argument is not given`, () => {
            const instance = computed.children.builder(computed);

            expect(instance[is]()).toBeFalsy();
        });
        it(`The method return false when the ${name}.${property}'s value and the given argument do not match`, () => {
            const instance = computed.children.builder(computed);

            instance[property] = computed.parent.builder(computed);

            expect(instance[is](computed.parent.builder(computed))).toBeFalsy();
        });
        it(`The method return true when the ${name}.${property}'s value and the given argument match`, () => {
            const instance = computed.children.builder(computed);

            const value = computed.parent.builder(computed);
            instance[property] = value;

            expect(instance[is](value)).toBeTruthy();
        });

        if (unsafe){
            it(`The method does not call ${name}.${unsafe} when the given argument is undefined`, () => {
                const instance = computed.children.builder(computed);

                const spy = jest.spyOn(instance,unsafe);
                instance[is](undefined);

                expect(spy).not.toBeCalled();
            });
            it(`The method does not call ${name}.${unsafe}() when the given argument is not given`, () => {
                const instance = computed.children.builder(computed);

                const spy = jest.spyOn(instance,unsafe);
                instance[is]();

                expect(spy).not.toBeCalled();
            });
            it(`The method calls ${name}.${unsafe} when the given argument is given`, () => {
                const instance = computed.children.builder(computed);

                const spy = jest.spyOn(instance,unsafe);
                instance[is](computed.parent.builder(computed));

                expect(spy).toBeCalledTimes(1);
                expect(spy).toHaveBeenNthCalledWith(1);
            });
            it(`The method returns true when the ${name}.${unsafe}'s result and the given argument match, not the ${name}.${property} property value`, () => {
                const instance = computed.children.builder(computed);

                instance[property] = computed.parent.builder(computed);

                const value = computed.parent.builder(computed);
                instance[unsafe] = jest.fn().mockReturnValue(value);

                expect(instance[is](value)).toBeTruthy();
            });
            it(`The method return false when the ${name}.${unsafe}'s result and the given argument do not match, not the ${name}.${property} property value`, () => {
                const instance = computed.children.builder(computed);

                const value = computed.parent.builder(computed);

                instance[property] = value;
                instance[unsafe] = jest.fn().mockReturnValue(computed.parent.builder(computed));

                expect(instance[is](value)).toBeFalsy();
            });
        }

        computed.after(computed);
    });
};

export default testOneToManyChildrenIs;