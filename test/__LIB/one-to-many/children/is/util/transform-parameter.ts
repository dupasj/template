import TestOneToManyChildrenIsParameter from "./type/parameter/item";
import TestOneToManyChildrenIs from "./type/computed/item";
import parameterOneToManyBase from "../../../util/transform-base";

const parameterOneToManyChildrenIs = (input: TestOneToManyChildrenIsParameter): TestOneToManyChildrenIs => {
    const base = parameterOneToManyBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterOneToManyChildrenIs;