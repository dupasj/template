import TestOneToManyChildrenIsOptionParameter from "./option";
import TestOneToManyItemParameter from "../../../../../util/parameter/item";
import TestOneToManyChildrenIsOption from "../computed/option";

interface TestOneToManyChildrenIsParameter extends TestOneToManyItemParameter<TestOneToManyChildrenIsOptionParameter,TestOneToManyChildrenIsOption>{

}

export default TestOneToManyChildrenIsParameter;