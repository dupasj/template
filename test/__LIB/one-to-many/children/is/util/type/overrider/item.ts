import TestOneToManyChildrenIsOption from "../computed/option";
import TestOneToManyChildrenIsOptionOverrider from "./option";
import TestOneToManyItemOverrider from "../../../../../util/overrider/item";

interface TestOneToManyChildrenIsOverrider extends TestOneToManyItemOverrider<TestOneToManyChildrenIsOptionOverrider,TestOneToManyChildrenIsOption>{

}

export default TestOneToManyChildrenIsOverrider;