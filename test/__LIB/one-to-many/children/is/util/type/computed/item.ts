import TestOneToManyChildrenIsOption from "./option";
import TestOneToManyItem from "../../../../../util/computed/item";

interface TestOneToManyChildrenIs extends TestOneToManyItem<TestOneToManyChildrenIsOption>{

}

export default TestOneToManyChildrenIs;