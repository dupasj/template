import TestOneToManyChildrenIsOverrider from "./type/overrider/item";
import TestOneToManyChildrenIs from "./type/computed/item";
import TestOneToManyBase from "../../../util/computed/base";
import overriderOneToManyBase from "../../../util/transform-overrider";

const overriderOneToManyChildrenIs = (base: TestOneToManyBase, overrider: TestOneToManyChildrenIsOverrider): TestOneToManyChildrenIs => {
    const computed = overriderOneToManyBase(base,overrider)

    return {
        option: {},
        ... computed
    };
}

export default overriderOneToManyChildrenIs;