import TestFieldSetterParameter from "./util/type/parameter/item";
import parameterFieldSetter from "./util/transform-parameter";
import spyOnValueProperty from "../../util/spy-on-property";

const testFieldSetter = (input: TestFieldSetterParameter) => {
    const computed = parameterFieldSetter(input);

    const name = computed.class.name;
    const property = computed.property;

    const is = computed.method.is;
    const erase = computed.method.erase;
    const setter = computed.method.setter;

    if (typeof setter !== "string"){
        return;
    }

    describe(`Test the ${name}.${setter} method`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.builder(computed);

            expect(instance[setter]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.builder(computed);

            expect(typeof instance[setter]).toBe("function");
        });

        describe(`Test the ${name}.${setter} method return value`,() => {
            it(`The method returns himself when the value doesn't change`, () => {
                const instance = computed.builder(computed);

                const value = computed.generator(computed);
                instance[property] = value;

                expect(instance[setter](value)).toBe(instance);
            });
            it(`The method returns himself when the value was undefined`, () => {
                const instance = computed.builder(computed);

                expect(instance[setter](computed.generator(computed))).toBe(instance);
            });
            it(`The method returns himself when the value change`, () => {
                const instance = computed.builder(computed);

                instance[property] = computed.generator(computed);

                expect(instance[setter](computed.generator(computed))).toBe(instance);
            });

            if (is){
                it(`The method returns himself when the method ${name}.${is} returns true`, () => {
                    const instance = computed.builder(computed);

                    instance[property] = computed.generator(computed);
                    instance[is] = jest.fn().mockReturnValue(true);

                    expect(instance[setter](computed.generator(computed))).toBe(instance);
                });
                it(`The method returns himself when the method ${name}.${is} returns false`, () => {
                    const instance = computed.builder(computed);

                    const value = computed.generator(computed);
                    instance[property] = value;
                    instance[is] = jest.fn().mockReturnValue(false);

                    expect(instance[setter](value)).toBe(instance);
                });
            }
        });
        describe(`Test the ${name}.${setter} method value assignment`,() => {
            it(`The method's return does not assign the value when the value doesn't change`, () => {
                const instance = computed.builder(computed);

                const value = computed.generator(computed);
                instance[property] = value;

                const spy = spyOnValueProperty(instance,property,"set");

                instance[setter](value);

                expect(spy).not.toBeCalled();
            });
            it(`The method assigns the value when the value was undefined`, () => {
                const instance = computed.builder(computed);
                const spy = spyOnValueProperty(instance,property,"set");

                if (erase){
                    instance[erase] = jest.fn();
                }

                const value = computed.generator(computed);
                instance[setter](value);

                expect(spy).toBeCalledTimes(1);
                expect(spy).toHaveBeenNthCalledWith(1,value);
            });
            it(`The method assigns the value when the value change`, () => {
                const instance = computed.builder(computed);

                instance[property] = computed.generator(computed);
                if (erase){
                    instance[erase] = jest.fn();
                }

                const value = computed.generator(computed);


                const spy = spyOnValueProperty(instance,property,"set");

                instance[setter](value);

                expect(spy).toBeCalledTimes(1);
                expect(spy).toHaveBeenNthCalledWith(1,value);
            });

            if (is){
                it(`The method doesn't assign the value when the method ${name}.${is} returns true`, () => {
                    const instance = computed.builder(computed);

                    instance[property] = computed.generator(computed);
                    instance[is] = jest.fn().mockReturnValue(true);
                    if (erase){
                        instance[erase] = jest.fn();
                    }

                    const spy = spyOnValueProperty(instance,property,"set");

                    const value = computed.generator(computed);
                    instance[setter](value);


                    expect(spy).not.toHaveBeenCalled();
                });
                it(`The method assigns the value when the method ${name}.${is} returns false`, () => {
                    const instance = computed.builder(computed);

                    const value = computed.generator(computed);
                    instance[property] = value;
                    instance[is] = jest.fn().mockReturnValue(false);
                    if (erase){
                        instance[erase] = jest.fn();
                    }

                    const spy = spyOnValueProperty(instance,property,"set");

                    instance[setter](value);

                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1,value);
                });
            }
        });



        if (erase){
            describe(`Test the ${name}.${erase} integration in the method ${name}.${setter}`,() => {
                it(`The method's return does not run ${name}.${erase} method when the value doesn't change`, () => {
                    const instance = computed.builder(computed);

                    const value = computed.generator(computed);
                    instance[property] = value;

                    const spy = jest.spyOn(instance,erase);

                    instance[setter](value);

                    expect(spy).not.toBeCalled();
                });
                it(`The method runs the method ${name}.${erase} when the value was undefined`, () => {
                    const instance = computed.builder(computed);
                    const spy = jest.spyOn(instance,erase);

                    instance[setter](computed.generator(computed));

                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1);
                });
                it(`The method runs the method ${name}.${erase} when the value change`, () => {
                    const instance = computed.builder(computed);

                    instance[property] = computed.generator(computed);
                    const spy = jest.spyOn(instance,erase);

                    instance[setter](computed.generator(computed));

                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1);
                });

                it(`The method runs the method ${name}.${erase} method before the value assignment when the value was undefined`, () => {
                    const instance = computed.builder(computed);
                    const before = jest.fn().mockReturnValue(instance);
                    instance[erase] = before;
                    const after = spyOnValueProperty(instance,property,"set");

                    instance[setter](computed.generator(computed));

                    // @ts-ignore
                    expect(before).toHaveBeenCalledBefore(after);
                });
                it(`The method runs the method ${name}.${erase} before the value assignment when the value change`, () => {
                    const instance = computed.builder(computed);
                    instance[property] = computed.generator(computed);

                    const before = jest.fn().mockReturnValue(instance);
                    instance[erase] = before;
                    const after = spyOnValueProperty(instance,property,"set");

                    instance[setter](computed.generator(computed));

                    // @ts-ignore
                    expect(before).toHaveBeenCalledBefore(after);
                });


                if (is){
                    it(`The method's return does not run ${name}.${erase} method when the method ${name}.${is} returns true`, () => {
                        const instance = computed.builder(computed);

                        instance[property] = computed.generator(computed);
                        instance[is] = jest.fn().mockReturnValue(true);

                        const spy = jest.spyOn(instance,erase);

                        instance[setter](computed.generator(computed));

                        expect(spy).not.toBeCalled();
                    });
                    it(`The method runs the method ${name}.${erase} when the method ${name}.${is} returns false`, () => {
                        const instance = computed.builder(computed);

                        const value = computed.generator(computed);
                        instance[property] = value;
                        instance[is] = jest.fn().mockReturnValue(false);

                        const spy = jest.spyOn(instance,erase);

                        instance[setter](value);

                        expect(spy).toBeCalledTimes(1);
                        expect(spy).toHaveBeenNthCalledWith(1);
                    });

                    it(`The method runs the method ${name}.${erase} before the value assignment when the method ${name}.${is} returns false`, () => {
                        const instance = computed.builder(computed);

                        const value = computed.generator(computed);
                        instance[property] = value;
                        instance[is] = jest.fn().mockReturnValue(false);

                        const before = jest.fn().mockReturnValue(instance);
                        instance[erase] = before;
                        const after = spyOnValueProperty(instance,property,"set");

                        instance[setter](computed.generator(value));

                        // @ts-ignore
                        expect(before).toHaveBeenCalledBefore(after);
                    });
                }
            });
        }

        computed.after(computed);
    });
};

export default testFieldSetter;