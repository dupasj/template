import TestFieldSetter from "./type/computed/item";
import TestFieldSetterOverrider from "./type/overrider/item";
import TestFieldBase from "../../util/type/computed/base";
import overriderFieldBaseItem from "../../util/transform-overrider";

const overriderFieldSetter = (base: TestFieldBase, overrider: TestFieldSetterOverrider): TestFieldSetter => {
    const temporary = overriderFieldBaseItem(base,overrider);

    return {
        option: {},
        ... temporary
    };
}

export default overriderFieldSetter;