import TestFieldSetterParameter from "./type/parameter/item";
import TestFieldSetter from "./type/computed/item";
import parameterFieldBase from "../../util/transform-base";

const parameterFieldSetter = (input: TestFieldSetterParameter): TestFieldSetter => {
    const base = parameterFieldBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterFieldSetter;