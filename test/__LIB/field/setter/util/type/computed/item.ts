import TestFieldItem from "../../../../util/type/computed/item";
import TestFieldSetterOption from "./option";

interface TestFieldSetter extends TestFieldItem<TestFieldSetterOption>{

}

export default TestFieldSetter;