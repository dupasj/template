import TestFieldItemOverrider from "../../../../util/type/overrider/item";
import TestFieldSetterOption from "../computed/option";
import TestFieldSetterOptionOverrider from "./option";

interface TestFieldSetterOverrider extends TestFieldItemOverrider<TestFieldSetterOptionOverrider,TestFieldSetterOption>{

}

export default TestFieldSetterOverrider;