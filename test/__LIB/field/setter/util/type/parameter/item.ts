import TestFieldItemParameter from "../../../../util/type/parameter/item";
import TestFieldSetterOptionParameter from "./option";
import TestFieldSetterOption from "../computed/option";

interface TestFieldSetterParameter extends TestFieldItemParameter<TestFieldSetterOptionParameter,TestFieldSetterOption>{

}

export default TestFieldSetterParameter;