import TestFieldUnsafeParameter from "./util/type/parameter/item";
import parameterFieldUnsafe from "./util/transform-parameter";

const testFieldUnsafe = (input: TestFieldUnsafeParameter) => {
    const computed = parameterFieldUnsafe(input);

    const name = computed.class.name;
    const unsafe = computed.method.unsafe;
    const property = computed.property;

    if (typeof unsafe !== "string"){
        return;
    }

    describe(`Test the ${name}.${unsafe}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.builder(computed);

            expect(instance[unsafe]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.builder(computed);

            expect(typeof instance[unsafe]).toBe("function");
        });
        it(`The method return undefined when the ${name}.${property} value is undefined`, () => {
            const instance = computed.builder(computed);

            instance[property] = undefined;

            expect(instance[unsafe]()).toBeFalsy();
        });
        it(`The method return undefined when the ${name}.${property} value does not exist`, () => {
            const instance = computed.builder(computed);

            delete instance[property];

            expect(instance[unsafe]()).toBeFalsy();
        });
        it(`The method return the ${name}.${property}'s value when the ${name}.${property} value is defined`, () => {
            const instance = computed.builder(computed);

            const value = computed.generator(computed);

            instance[property] = value;

            expect(instance[unsafe]()).toBe(value);
        });

        computed.after(computed);
    });
};

export default testFieldUnsafe;