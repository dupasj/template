import TestFieldItemOverrider from "../../../../util/type/overrider/item";
import TestFieldUnsafeOption from "../computed/option";
import TestFieldUnsafeOptionOverrider from "./option";

interface TestFieldUnsafeOverrider extends TestFieldItemOverrider<TestFieldUnsafeOptionOverrider,TestFieldUnsafeOption>{

}

export default TestFieldUnsafeOverrider;