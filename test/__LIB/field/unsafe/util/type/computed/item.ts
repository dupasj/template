import TestFieldItem from "../../../../util/type/computed/item";
import TestFieldUnsafeOption from "./option";

interface TestFieldUnsafe extends TestFieldItem<TestFieldUnsafeOption>{

}

export default TestFieldUnsafe;