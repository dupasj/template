import TestFieldItemParameter from "../../../../util/type/parameter/item";
import TestFieldUnsafeOptionParameter from "./option";
import TestFieldUnsafeOption from "../computed/option";

interface TestFieldUnsafeParameter extends TestFieldItemParameter<TestFieldUnsafeOptionParameter,TestFieldUnsafeOption>{

}

export default TestFieldUnsafeParameter;