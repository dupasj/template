import TestFieldUnsafeOverrider from "./type/overrider/item";
import TestFieldBase from "../../util/type/computed/base";
import TestFieldUnsafe from "./type/computed/item";
import overriderFieldBaseItem from "../../util/transform-overrider";

const overriderFieldUnsafe = (base: TestFieldBase, overrider: TestFieldUnsafeOverrider): TestFieldUnsafe => {
    const temporary = overriderFieldBaseItem(base,overrider);

    return {
        option: {},
        ... temporary
    };
}

export default overriderFieldUnsafe;