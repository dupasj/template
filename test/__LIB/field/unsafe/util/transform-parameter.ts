import TestFieldUnsafeParameter from "./type/parameter/item";
import TestFieldUnsafe from "./type/computed/item";
import parameterFieldBase from "../../util/transform-base";

const parameterFieldUnsafe = (input: TestFieldUnsafeParameter): TestFieldUnsafe => {
    const base = parameterFieldBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterFieldUnsafe;