import TestFieldGetterParameter from "./util/type/parameter/item";
import parameterFieldGetter from "./util/transform-parameter";

const testFieldGetter = (input: TestFieldGetterParameter) => {
    const computed = parameterFieldGetter(input);

    const name = computed.class.name;
    const property = computed.property;

    const error = computed.option.error;
    const unsafe = computed.method.unsafe;
    const getter = computed.method.getter;

    if (typeof getter !== "string"){
        return;
    }

    describe(`Test the ${name}.${getter} method`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.builder(computed);

            expect(instance[getter]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.builder(computed);

            expect(typeof instance[getter]).toBe("function");
        });


        describe(`Test the ${name}.${getter} method return value`,() => {
            it(`The method returns the ${name}.${property}'s value`, () => {
                const instance = computed.builder(computed);

                const value = computed.generator(computed);
                instance[property] = value;

                expect(instance[getter]()).toBe(value);
            });
            if (typeof unsafe === "string") {
                it(`The method calls the ${name}.${unsafe} method`, () => {
                    const instance = computed.builder(computed);

                    instance[property] = computed.generator(computed);
                    const spy = jest.spyOn(instance,unsafe);

                    instance[getter]();

                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toHaveBeenNthCalledWith(1);
                });
                it(`The method returns the value from the ${name}.${unsafe} method result, not from the ${name}.${property} property's value`, () => {
                    const instance = computed.builder(computed);

                    instance[property] = computed.generator(computed);
                    const value = computed.generator(computed);

                    instance[unsafe] = jest.fn().mockReturnValue(value);

                    expect(instance[getter]()).toBe(value);
                });
            }
        });

        if (error){
            it(`The method throws an error if the value from the ${name}.${property} property's value is undefined`, () => {
                const instance = computed.builder(computed);

                const result = (() => {
                    try{
                        instance[getter]();
                    }catch (e){
                        return e;
                    }

                    return undefined;
                })();

                error(computed,result);
            });

            if (unsafe){
                it(`The method throws an error if the value from the ${name}.${unsafe}() method result is undefined, not if the ${name}.${property} property's value is undefined`, () => {
                    const instance = computed.builder(computed);

                    instance[property] = computed.generator(computed);
                    instance[unsafe] = jest.fn().mockReturnValue(undefined);

                    const result = (() => {
                        try{
                            instance[getter]();
                        }catch (e){
                            return e;
                        }

                        return undefined;
                    })();

                    error(computed,result);
                });
            }
        }

        computed.after(computed);
    });
};

export default testFieldGetter;