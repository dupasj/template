import TestFieldGetter from "./type/computed/item";
import TestFieldGetterOverrider from "./type/overrider/item";
import TestFieldBase from "../../util/type/computed/base";
import overriderFieldBaseItem from "../../util/transform-overrider";

const overriderFieldGetter = (base: TestFieldBase, overrider: TestFieldGetterOverrider): TestFieldGetter => {
    const temporary = overriderFieldBaseItem(base,overrider);

    return {
        option: {
            error: false // TODO
        },
        ... temporary
    };
}

export default overriderFieldGetter;