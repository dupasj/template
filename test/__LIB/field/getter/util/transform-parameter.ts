import TestFieldGetterParameter from "./type/parameter/item";
import TestFieldGetter from "./type/computed/item";
import parameterFieldBase from "../../util/transform-base";

const parameterFieldGetter = (input: TestFieldGetterParameter): TestFieldGetter => {
    const base = parameterFieldBase(input);

    return {
        option: {
            error: (() => {
                const error = input.option?.error;
                if (error === null || error === false){
                    return false;
                }

                return error ?? ((option, error) => {

                })
            })()
        },
        ... base
    };
}

export default parameterFieldGetter;