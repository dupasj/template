import TestFieldItemParameter from "../../../../util/type/parameter/item";
import TestFieldGetterOptionParameter from "./option";
import TestFieldGetterOption from "../computed/option";

interface TestFieldGetterParameter extends TestFieldItemParameter<TestFieldGetterOptionParameter,TestFieldGetterOption>{

}

export default TestFieldGetterParameter;