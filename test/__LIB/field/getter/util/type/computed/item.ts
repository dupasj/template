import TestFieldItem from "../../../../util/type/computed/item";
import TestFieldGetterOption from "./option";

interface TestFieldGetter extends TestFieldItem<TestFieldGetterOption>{

}

export default TestFieldGetter;