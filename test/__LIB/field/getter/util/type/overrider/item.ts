import TestFieldItemOverrider from "../../../../util/type/overrider/item";
import TestFieldGetterOption from "../computed/option";
import TestFieldGetterOptionOverrider from "./option";

interface TestFieldGetterOverrider extends TestFieldItemOverrider<TestFieldGetterOptionOverrider,TestFieldGetterOption>{

}

export default TestFieldGetterOverrider;