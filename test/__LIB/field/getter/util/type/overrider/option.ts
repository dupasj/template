type TestFieldGetterOptionOverrider = {
    error?: null|false|((option: any,error: any) => void)
};

export default TestFieldGetterOptionOverrider;