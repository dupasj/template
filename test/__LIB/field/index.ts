import testFieldSetter from "./setter";
import testFieldIs from "./is";
import testFieldGetter from "./getter";
import testFieldUnsafe from "./unsafe";
import testFieldErase from "./erase";
import testFieldDefined from "./defined";
import TestFieldParameter from "./util/type/parameter/general";
import parameterField from "./util/transform-parameter";

const testField = (option: TestFieldParameter) => {
    const computed = parameterField(option);
    const name = computed.class.name;

    describe(`The methods related to ${name}.${computed.property}`, () => {
        if (computed.overrider.erase){
            testFieldErase(computed.overrider.erase)
        }
        if (computed.overrider.defined){
            testFieldDefined(computed.overrider.defined)
        }
        if (computed.overrider.unsafe){
            testFieldUnsafe(computed.overrider.unsafe)
        }
        if (computed.overrider.getter){
            testFieldGetter(computed.overrider.getter)
        }
        if (computed.overrider.setter){
            testFieldSetter(computed.overrider.setter)
        }
        if (computed.overrider.is){
            testFieldIs(computed.overrider.is)
        }
    });
}

export default testField;