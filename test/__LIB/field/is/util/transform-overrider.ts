import TestFieldIs from "./type/computed/item";
import TestFieldIsOverrider from "./type/overrider/item";
import TestFieldBase from "../../util/type/computed/base";
import overriderFieldBaseItem from "../../util/transform-overrider";

const overriderFieldIs = (base: TestFieldBase, overrider: TestFieldIsOverrider): TestFieldIs => {
    const temporary = overriderFieldBaseItem(base,overrider);

    return {
        option: {},
        ... temporary
    };
}

export default overriderFieldIs;