import TestFieldItemParameter from "../../../../util/type/parameter/item";
import TestFieldIsOptionParameter from "./option";
import TestFieldIsOption from "../computed/option";

interface TestFieldIsParameter extends TestFieldItemParameter<TestFieldIsOptionParameter,TestFieldIsOption>{

}

export default TestFieldIsParameter;