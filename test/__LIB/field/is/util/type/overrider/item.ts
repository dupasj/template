import TestFieldItemOverrider from "../../../../util/type/overrider/item";
import TestFieldIsOption from "../computed/option";
import TestFieldIsOptionOverrider from "./option";

interface TestFieldIsOverrider extends TestFieldItemOverrider<TestFieldIsOptionOverrider,TestFieldIsOption>{

}

export default TestFieldIsOverrider;