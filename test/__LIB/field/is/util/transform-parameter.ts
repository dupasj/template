import TestFieldIsParameter from "./type/parameter/item";
import TestFieldIs from "./type/computed/item";
import parameterFieldBase from "../../util/transform-base";

const parameterFieldIs = (input: TestFieldIsParameter): TestFieldIs => {
    const base = parameterFieldBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterFieldIs;