import TestFieldBase from "./type/computed/base";
import TestFieldBaseOverrider from "./type/overrider/base";

const overriderFieldBaseItem = (base: TestFieldBase, overrider: TestFieldBaseOverrider): TestFieldBase => {
    return {
        class: overrider.class ?? base.class,
        property: overrider.property ?? base.property,
        is_static: overrider.is_static ?? base.is_static,
        method: {
            is: (() => {
                const is = overrider.method?.is;
                if (is === null || is === false){
                    return false;
                }

                return is ?? base.method.is;
            })(),
            defined: (() => {
                const defined = overrider.method?.defined;
                if (defined === null || defined === false){
                    return false;
                }

                return defined ?? base.method.defined;
            })(),
            unsafe: (() => {
                const unsafe = overrider.method?.unsafe;
                if (unsafe === null || unsafe === false){
                    return false;
                }

                return unsafe ?? base.method.unsafe;
            })(),
            getter: (() => {
                const getter = overrider.method?.getter;
                if (getter === null || getter === false){
                    return false;
                }

                return getter ?? base.method.getter;
            })(),
            setter: (() => {
                const setter = overrider.method?.setter;
                if (setter === null || setter === false){
                    return false;
                }

                return setter ?? base.method.setter;
            })(),
            erase: (() => {
                const erase = overrider.method?.erase;
                if (erase === null || erase === false){
                    return false;
                }

                return erase ?? base.method.erase;
            })(),
        },
        after: overrider.after ?? (() => {}),
        before: overrider.before ?? (() => {}),
        builder: overrider.builder ?? base.builder,
        generator: overrider.generator ?? base.generator,
    }
};

export default overriderFieldBaseItem;