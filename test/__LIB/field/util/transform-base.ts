import TestFieldBaseParameter from "./type/parameter/base";
import TestFieldBase from "./type/computed/base";
import * as ChangeCase from "change-case";
import {randomUUID} from "crypto";

const parameterFieldBase = (input: TestFieldBaseParameter): TestFieldBase => {
    return {
        class: input.class,
        property: input.property,
        is_static: input.is_static ?? false,
        method: {
            is: (() => {
                const is = input.method?.is;
                if (is === null || is === false){
                    return false;
                }

                return is ?? "is"+ChangeCase.pascalCase(input.property)+"EqualTo";
            })(),
            defined: (() => {
                const defined = input.method?.defined;
                if (defined === null || defined === false){
                    return false;
                }

                return defined ?? "is"+ChangeCase.pascalCase(input.property)+"Defined";
            })(),
            unsafe: (() => {
                const unsafe = input.method?.unsafe;
                if (unsafe === null || unsafe === false){
                    return false;
                }

                return unsafe ?? "unsafe"+ChangeCase.pascalCase(input.property);
            })(),
            getter: (() => {
                const getter = input.method?.getter;
                if (getter === null || getter === false){
                    return false;
                }

                return getter ?? "get"+ChangeCase.pascalCase(input.property);
            })(),
            setter: (() => {
                const setter = input.method?.setter;
                if (setter === null || setter === false){
                    return false;
                }

                return setter ?? "set"+ChangeCase.pascalCase(input.property);
            })(),
            erase: (() => {
                const erase = input.method?.erase;
                if (erase === null || erase === false){
                    return false;
                }

                return erase ?? "erase"+ChangeCase.pascalCase(input.property);
            })(),
        },
        after: input.after ?? (() => {}),
        before: input.before ?? (() => {}),
        builder: input.builder ?? ((option) => {
            if (option.is_static){
                return Object.assign({},option.class);
            }

            return new option.class;
        }),
        generator() {
            return randomUUID()
        }
    }
};

export default parameterFieldBase;