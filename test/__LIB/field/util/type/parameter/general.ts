import TestFieldBaseParameter from "./base";
import TestFieldUnsafeOverrider from "../../../unsafe/util/type/overrider/item";
import TestFieldGetterOverrider from "../../../getter/util/type/overrider/item";
import TestFieldSetterOverrider from "../../../setter/util/type/overrider/item";
import TestFieldDefinedOverrider from "../../../defined/util/type/overrider/item";
import TestFieldIsOverrider from "../../../is/util/type/overrider/item";
import TestFieldEraseOverrider from "../../../erase/util/type/overrider/item";
import TestField from "../computed/general";

interface TestFieldParameter extends TestFieldBaseParameter {
    overrider?: {
        unsafe?: null|false|TestFieldUnsafeOverrider,
        getter?: null|false|TestFieldGetterOverrider,
        setter?: null|false|TestFieldSetterOverrider,
        defined?: null|false|TestFieldDefinedOverrider,
        is?: null|false|TestFieldIsOverrider,
        erase?: null|false|TestFieldEraseOverrider,
    },
    before?: (option: TestField) => void,
    after?: (option: TestField) => void,
}

export default TestFieldParameter;