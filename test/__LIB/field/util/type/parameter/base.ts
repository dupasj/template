import TestFieldBase from "../computed/base";
import TestFieldItem from "../computed/item";

interface TestFieldBaseParameter {
    property: string,
    class: {new(): Object},
    is_static?: boolean,
    generator?(option: TestFieldItem): any,
    builder?(option: TestFieldItem): {[key: string]: any},

    method?: {
        unsafe?: string|null|false,
        is?: string|null|false,
        defined?: string|null|false,
        getter?: string|null|false,
        setter?: string|null|false,
        erase?: string|null|false,
    },
    before?: (option: TestFieldBase) => void,
    after?: (option: TestFieldBase) => void,
}

export default TestFieldBaseParameter;