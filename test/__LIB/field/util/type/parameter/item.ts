import TestFieldBaseParemeter from "./base";
import TestFieldItem from "../computed/item";

interface TestFieldItemParameter<Option extends { [key: string]: any } = {},ComputedOption extends { [key: string]: any } = Option> extends TestFieldBaseParemeter {
    option?: Option,

    before?: (option: TestFieldItem<ComputedOption>) => void,
    after?: (option: TestFieldItem<ComputedOption>) => void,
}

export default TestFieldItemParameter;