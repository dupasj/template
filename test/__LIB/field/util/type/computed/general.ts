import TestFieldBase from "./base";
import TestFieldUnsafe from "../../../unsafe/util/type/computed/item";
import TestFieldDefined from "../../../defined/util/type/computed/item";
import TestFieldGetter from "../../../getter/util/type/computed/item";
import TestFieldSetter from "../../../setter/util/type/computed/item";
import TestFieldIs from "../../../is/util/type/computed/item";
import TestFieldErase from "../../../erase/util/type/computed/item";

interface TestField extends TestFieldBase {
    overrider:{
        unsafe: TestFieldUnsafe|false,
        defined: TestFieldDefined|false,
        getter: TestFieldGetter|false,
        setter: TestFieldSetter|false,
        is: TestFieldIs|false,
        erase: TestFieldErase|false,
    },
    before: (option: TestField) => void,
    after: (option: TestField) => void,
}

export default TestField;