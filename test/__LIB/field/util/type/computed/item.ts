import TestFieldBase from "./base";

interface TestFieldItem<Option extends { [key: string]: any } = {}> extends TestFieldBase {
    option: Option,

    before: (option: TestFieldItem<Option>) => void,
    after: (option: TestFieldItem<Option>) => void,
}

export default TestFieldItem;