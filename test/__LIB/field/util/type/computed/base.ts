import TestFieldItem from "./item";

interface TestFieldBase {
    property: string,
    class: {new(): Object},
    is_static: boolean,
    generator(option: TestFieldItem): any,
    builder(option: TestFieldItem): {[key: string]: any},
    method: {
        unsafe: string|false,
        is: string|false,
        defined: string|false,
        getter: string|false,
        setter: string|false,
        erase: string|false,
    },
    before: (option: TestFieldBase) => void,
    after: (option: TestFieldBase) => void,
}

export default TestFieldBase;