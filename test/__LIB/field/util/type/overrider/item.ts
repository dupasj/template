import TestFieldBaseOverrider from "./base";
import TestFieldItem from "../computed/item";

interface TestFieldItemOverrider<Option extends { [key: string]: any } = {},ComputedOption extends { [key: string]: any } = Option> extends TestFieldBaseOverrider {
    option?: Option,

    before?: (option: TestFieldItem<ComputedOption>) => void,
    after?: (option: TestFieldItem<ComputedOption>) => void,
}

export default TestFieldItemOverrider;