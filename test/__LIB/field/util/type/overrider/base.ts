import TestFieldBase from "../computed/base";

interface TestFieldBaseOverrider {
    property?: string,
    class?: {new(): Object},
    is_static?: boolean,
    generator?(option: any): any,
    builder?(option: any): {[key: string]: any},
    method?: {
        unsafe?: string|null|false,
        defined?: string|null|false,
        getter?: string|null|false,
        setter?: string|null|false,
        erase?: string|null|false,
        is?: string|null|false,
    },
    before?: (option: TestFieldBase) => void,
    after?: (option: TestFieldBase) => void,
}

export default TestFieldBaseOverrider;