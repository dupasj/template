import TestField from "./type/computed/general";
import TestFieldParameter from "./type/parameter/general";
import parameterFieldBase from "./transform-base";
import overriderFieldErase from "../erase/util/transform-overrider";
import overriderFieldSetter from "../setter/util/transform-overrider";
import overriderFieldIs from "../is/util/transform-overrider";
import overriderFieldGetter from "../getter/util/transform-overrider";
import overriderFieldUnsafe from "../unsafe/util/transform-overrider";

const parameterField = (base: TestFieldParameter): TestField => {
    const temporary = parameterFieldBase(base);

    return {
        overrider: {
            unsafe: (() => {
                const unsafe = base.overrider?.unsafe;

                if (unsafe === false || unsafe === null){
                    return false;
                }

                return overriderFieldUnsafe(temporary,unsafe ?? {})
            })(),
            setter: (() => {
                const setter = base.overrider?.setter;

                if (setter === false || setter === null){
                    return false;
                }

                return overriderFieldSetter(temporary,setter ?? {})
            })(),
            getter: (() => {
                const getter = base.overrider?.getter;

                if (getter === false || getter === null){
                    return false;
                }

                return overriderFieldGetter(temporary,getter ?? {})
            })(),
            erase: (() => {
                const erase = base.overrider?.erase;

                if (erase === false || erase === null){
                    return false;
                }

                return overriderFieldErase(temporary,erase ?? {})
            })(),
            is: (() => {
                const is = base.overrider?.is;

                if (is === false || is === null){
                    return false;
                }

                return overriderFieldIs(temporary,is ?? {})
            })(),
            defined: (() => {
                const defined = base.overrider?.defined;

                if (defined === false || defined === null){
                    return false;
                }

                return overriderFieldIs(temporary,defined ?? {})
            })(),
        },
        ... temporary,
    }
};

export default parameterField;