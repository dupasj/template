import TestFieldEraseParameter from "./type/parameter/item";
import TestFieldErase from "./type/computed/item";
import parameterFieldBase from "../../util/transform-base";

const parameterFieldErase = (input: TestFieldEraseParameter): TestFieldErase => {
    const base = parameterFieldBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterFieldErase;