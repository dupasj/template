import TestFieldErase from "./type/computed/item";
import TestFieldEraseOverrider from "./type/overrider/item";
import TestFieldBase from "../../util/type/computed/base";
import overriderFieldBaseItem from "../../util/transform-overrider";

const overriderFieldErase = (base: TestFieldBase, overrider: TestFieldEraseOverrider): TestFieldErase => {
    const temporary = overriderFieldBaseItem(base,overrider);

    return {
        option: {},
        ... temporary
    };
}

export default overriderFieldErase;