import TestFieldItemOverrider from "../../../../util/type/overrider/item";
import TestFieldEraseOptionOverrider from "./option";
import TestFieldEraseOption from "../computed/option";

interface TestFieldEraseOverrider extends TestFieldItemOverrider<TestFieldEraseOptionOverrider,TestFieldEraseOption>{

}

export default TestFieldEraseOverrider;