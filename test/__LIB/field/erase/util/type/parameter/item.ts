import TestFieldItemParameter from "../../../../util/type/parameter/item";
import TestFieldEraseOptionParameter from "./option";
import TestFieldEraseOption from "../computed/option";

interface TestFieldEraseParameter extends TestFieldItemParameter<TestFieldEraseOptionParameter,TestFieldEraseOption>{

}

export default TestFieldEraseParameter;