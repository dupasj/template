import TestFieldItem from "../../../../util/type/computed/item";
import TestFieldEraseOption from "./option";

interface TestFieldErase extends TestFieldItem<TestFieldEraseOption>{

}

export default TestFieldErase;