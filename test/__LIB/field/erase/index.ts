import TestFieldEraseParameter from "./util/type/parameter/item";
import parameterFieldErase from "./util/transform-parameter";
import spyOnValueProperty from "../../util/spy-on-property";

const testFieldErase = (input: TestFieldEraseParameter) => {
    const computed = parameterFieldErase(input);

    const name = computed.class.name;
    const property = computed.property;

    const erase = computed.method.erase;

    if (typeof erase !== "string"){
        return;
    }

    describe(`Test the ${name}.${erase}() methods`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.builder(computed);

            expect(instance[erase]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.builder(computed);

            expect(typeof instance[erase]).toBe("function");
        });
        it(`The method return himself when the value is undefined`, () => {
            const instance = computed.builder(computed);

            expect(instance[erase]()).toBe(instance);
        });
        it(`The method return himself when the value is defined`, () => {
            const instance = computed.builder(computed);

            expect(instance[erase]()).toBe(instance);
        });

        it(`The method set the ${name}.${property}'s value as undefined when the value is defined`, () => {
            const instance = computed.builder(computed);

            instance[property] = computed.generator(computed);

            const spy = spyOnValueProperty(instance,property,"set");

            instance[erase]();

            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenNthCalledWith(1,undefined);
        });
        it(`The method set the ${name}.${property}'s value as undefined when the value is undefined`, () => {
            const instance = computed.builder(computed);

            const spy = spyOnValueProperty(instance,property,"set");

            instance[erase]();

            expect(spy).toBeCalledTimes(1);
            expect(spy).toHaveBeenNthCalledWith(1,undefined);
        });


        computed.after(computed);
    });
};

export default testFieldErase;