import TestFieldDefinedParameter from "./type/parameter/item";
import TestFieldDefined from "./type/computed/item";
import parameterFieldBase from "../../util/transform-base";

const parameterFieldDefined = (input: TestFieldDefinedParameter): TestFieldDefined => {
    const base = parameterFieldBase(input);

    return {
        option: {},
        ... base
    };
}

export default parameterFieldDefined;