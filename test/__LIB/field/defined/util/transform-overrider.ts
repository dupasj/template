import TestFieldDefined from "./type/computed/item";
import TestFieldDefinedOverrider from "./type/overrider/item";
import TestFieldBase from "../../util/type/computed/base";
import overriderFieldBaseItem from "../../util/transform-overrider";

const overriderFieldDefined = (base: TestFieldBase, overrider: TestFieldDefinedOverrider): TestFieldDefined => {
    const temporary = overriderFieldBaseItem(base,overrider);

    return {
        option: {},
        ... temporary
    };
}

export default overriderFieldDefined;