import TestFieldItemParameter from "../../../../util/type/parameter/item";
import TestFieldDefinedOptionParameter from "./option";
import TestFieldDefinedOption from "../computed/option";

interface TestFieldDefinedParameter extends TestFieldItemParameter<TestFieldDefinedOptionParameter,TestFieldDefinedOption>{

}

export default TestFieldDefinedParameter;