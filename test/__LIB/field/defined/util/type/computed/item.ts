import TestFieldItem from "../../../../util/type/computed/item";
import TestFieldDefinedOption from "./option";

interface TestFieldDefined extends TestFieldItem<TestFieldDefinedOption>{

}

export default TestFieldDefined;