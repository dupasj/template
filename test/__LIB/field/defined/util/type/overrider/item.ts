import TestFieldItemOverrider from "../../../../util/type/overrider/item";
import TestFieldDefinedOption from "../computed/option";
import TestFieldDefinedOptionOverrider from "./option";

interface TestFieldDefinedOverrider extends TestFieldItemOverrider<TestFieldDefinedOptionOverrider,TestFieldDefinedOption>{

}

export default TestFieldDefinedOverrider;