import TestFieldDefinedParameter from "./util/type/parameter/item";
import parameterFieldDefined from "./util/transform-parameter";

const testFieldDefined = (input: TestFieldDefinedParameter) => {
    const computed = parameterFieldDefined(input);

    const name = computed.class.name;
    const property = computed.property;

    const defined = computed.method.defined;

    if (typeof defined !== "string"){
        return;
    }

    describe(`Test the method ${name}.${defined}`, () => {
        computed.before(computed);

        it("The method exists", () => {
            const instance = computed.builder(computed);

            expect(instance[defined]).not.toBeUndefined();
        });
        it("The method is a function", () => {
            const instance = computed.builder(computed);

            expect(typeof instance[defined]).toBe("function");
        });
        it(`The method returns false when the ${name}.${property} value is undefined`, () => {
            const instance = computed.builder(computed);

            instance[property] = undefined;

            expect(instance[defined]()).toBeFalsy();
        });
        it(`The method returns false when the ${name}.${property} does not exist`, () => {
            const instance = computed.builder(computed);

            delete instance[property];

            expect(instance[defined]()).toBeFalsy();
        });
        it(`The method returns true when the ${name}.${property} value is defined`, () => {
            const instance = computed.builder(computed);

            instance[property] = computed.generator(computed);

            expect(instance[defined]()).toBeTruthy();
        });

        computed.after(computed);
    });
};

export default testFieldDefined;