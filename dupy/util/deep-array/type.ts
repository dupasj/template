type DeepArray<T> = T|(T|DeepArray<T>)[]

export default DeepArray;