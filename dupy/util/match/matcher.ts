import DeepArray from "../deep-array/type";
import flat from "../deep-array/flat";
import Match from "./type";

const matcher = (matchers: DeepArray<Match>,plain: undefined|Match) => {
    if (typeof plain === "undefined"){
        return false;
    }

    for(const matcher of flat(matchers)){
        if (matcher === plain){
            return true;
        }
        if (typeof plain === "string"){
            if (typeof matcher === "function" && matcher(plain)){
                return true;
            }
            if (matcher instanceof RegExp && matcher.test(plain)){
                return true;
            }
        }
    }

    return false;
}


export default matcher;