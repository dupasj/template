type Match = string|RegExp|((test: string) => true);

export default Match;