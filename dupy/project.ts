import ActionConfiguration from "./action";
import DeepArray from "./util/deep-array/type";
import Route from "./route";
import flat from "./util/deep-array/flat";
import OutOfRange from "./error/internal/out-of-range";
import ModelConfiguration from "./model";
import DupyObject from "./dupy-object";
import Translate from "./translate";

class Project extends Translate{
    isProject(): this is Project {
        return true;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [ActionConfiguration](ActionConfiguration) and the actual [Project](Project)**.
     * @description **An array of saved relations between [ActionConfiguration](ActionConfiguration) class instances and the actual [Project](Project) class instance**.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category property
     * @protected
     * @see Project.areActionsLinked
     * @see Project.disposeActions
     * @see Project.unlinkActions
     * @see Project.unsafeActions
     * @see Project.getActions
     * @see Project.linkActions
     * @see Project.getAction
     * @see Project.unsafeAction
     * @see Project.getActionsSize
     */
    protected actions: ActionConfiguration[] = [];


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the number of the associated [ActionConfiguration](ActionConfiguration) class instances**.
     * @description **Gives the size of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances**.
     * @return **Returns the number of the associated [ActionConfiguration](ActionConfiguration) class instances**.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category length
     * @public
     * @see Project.areActionsLinked
     * @see Project.disposeActions
     * @see Project.unlinkActions
     * @see Project.unsafeActions
     * @see Project.getActions
     * @see Project.linkActions
     * @see Project.getAction
     * @see Project.unsafeAction
     * @see Project.actions
     */
    getActionsSize(): number{
        return this.unsafeActions().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration)** class instances. Unlike [getAction](Project.getAction) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances**. Unlike [getAction](Project.getAction) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances**. Unlike [getAction](Project.getAction) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration), you can give `-1` for example. If you want to get the first item of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration), you can give `0`.*
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category unsafe
     * @category value
     * @public
     * @see Project.areActionsLinked
     * @see Project.disposeActions
     * @see Project.unlinkActions
     * @see Project.unsafeActions
     * @see Project.getActions
     * @see Project.linkActions
     * @see Project.getAction
     * @see Project.getActionsSize
     * @see Project.actions
     */
    unsafeAction(position: number): ActionConfiguration|undefined{
        if (position < 0){
            const new_position = this.getActionsSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeAction(new_position);
        }

        return this.actions[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Project--unsafeAction) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Project--unsafeAction) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Project--unsafeAction) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration), you can give `-1` for example. If you want to get the first item of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration), you can give `0`.*
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category getter
     * @category value
     * @public
     * @see Project.areActionsLinked
     * @see Project.disposeActions
     * @see Project.unlinkActions
     * @see Project.unsafeActions
     * @see Project.getActions
     * @see Project.linkActions
     * @see Project.unsafeAction
     * @see Project.getActionsSize
     * @see Project.actions
     */
    getAction(position: number): ActionConfiguration{
        const action = this.unsafeAction(position);
        if (typeof action === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getAction");
            error.setClassname("Project");
            error.setProperty("actions");
            error.setPosition(position);
            throw error;
        }
        return action;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associate one or multiple [ActionConfiguration](ActionConfiguration) class instances** from your actual [Project](Project) class instance.
     * @description Allows you to **associate one or multiple [ActionConfiguration](ActionConfiguration) class instances** from your actual [Project](Project) class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate [Project](Project) class instance** ([ActionConfiguration.unlinkProject](ActionConfiguration.unlinkProject)), and **then associate the  [ActionConfiguration](ActionConfiguration) class instances** to the [Project](Project) class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [ActionConfiguration.linkProject](ActionConfiguration.linkProject) to associate the [Project](Project) class instance and the [ActionConfiguration](ActionConfiguration) class instances from the both parts*.
     * @return Returns the actual [ActionConfiguration](ActionConfiguration) class instance.
     * @param actions All the given [ActionConfiguration](ActionConfiguration) class instances that you to associate to your actual [Project](Project) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category associate
     * @public
     * @see Project.areActionsLinked
     * @see Project.disposeActions
     * @see Project.unlinkActions
     * @see Project.unsafeActions
     * @see Project.getActions
     * @see Project.getAction
     * @see Project.unsafeAction
     * @see Project.getActionsSize
     * @see Project.actions
     * @see ActionConfiguration.linkProject
     */
    linkActions(... actions: DeepArray<ActionConfiguration>[]): this{
        for (const action of flat(actions)){
            if (this.areActionsLinked(action)){
                continue;
            }


            this.actions.push(action);


            action.linkProject(this);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances**.
     * @description **Gives a copy of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances**.
     * @return **Returns a copy of the [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances**.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category getter
     * @category iterable
     * @public
     * @see Project.areActionsLinked
     * @see Project.disposeActions
     * @see Project.unlinkActions
     * @see Project.unsafeActions
     * @see Project.linkActions
     * @see Project.getAction
     * @see Project.unsafeAction
     * @see Project.getActionsSize
     * @see Project.actions
     */
    getActions(): ActionConfiguration[]{
        return Array.from(this.unsafeActions());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives an [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances without copy**.
     * @description **Gives an [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getActions](Project.getActions) method.
     * @return **Returns an [array](Project.actions) of the associated [ActionConfiguration](ActionConfiguration) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getActions](Project.getActions) method.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category unsafe
     * @category iterable
     * @public
     * @see Project.areActionsLinked
     * @see Project.disposeActions
     * @see Project.unlinkActions
     * @see Project.getActions
     * @see Project.linkActions
     * @see Project.getAction
     * @see Project.unsafeAction
     * @see Project.getActionsSize
     * @see Project.actions
     */
    unsafeActions(): ActionConfiguration[]{
        return this.actions;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate one or multiple [ActionConfiguration](ActionConfiguration) class instances**.
     * @description Allows you to **dissociate one or multiple [ActionConfiguration](ActionConfiguration) class instances** from your actual [Project](Project) class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the [Project](Project) class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [ActionConfiguration.unlinkProject](ActionConfiguration.unlinkProject) to dissociate the [Project](Project) class instance and the given [ActionConfiguration](ActionConfiguration) class instances from the both parts*.
     * @return Returns the actual [ActionConfiguration](ActionConfiguration) class instance.
     * @param actions All the given [ActionConfiguration](ActionConfiguration) class instances that you to dissociate from your actual [Project](Project) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category dissociate
     * @public
     * @see Project.areActionsLinked
     * @see Project.disposeActions
     * @see Project.unsafeActions
     * @see Project.getActions
     * @see Project.linkActions
     * @see Project.getAction
     * @see Project.unsafeAction
     * @see Project.getActionsSize
     * @see Project.actions
     * @see ActionConfiguration.unlinkProject
     */
    unlinkActions(... actions: DeepArray<ActionConfiguration>[]): this{
        for (const action of flat(actions)){
            if (!this.areActionsLinked(action)){
                continue;
            }

            while (true){
                const index = this.actions.indexOf(action);
                if (index < 0){
                    break;
                }

                this.actions.splice(index, 1);
            }

            action.unlinkProject();
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate all the associated [ActionConfiguration](ActionConfiguration) class instances**.
     * @description **Dissociate all the associated [ActionConfiguration](ActionConfiguration) class instances**.  *Note that all the associated [ActionConfiguration](ActionConfiguration) class instances will lose their [Project](Project) class instance association*.
     * @return Returns the actual [ActionConfiguration](ActionConfiguration) class instance.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category truncate
     * @public
     * @see Project.areActionsLinked
     * @see Project.unlinkActions
     * @see Project.unsafeActions
     * @see Project.getActions
     * @see Project.linkActions
     * @see Project.getAction
     * @see Project.unsafeAction
     * @see Project.getActionsSize
     * @see Project.actions
     */
    disposeActions(): this{
        this.unlinkActions(this.unsafeActions());
        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [ActionConfiguration](ActionConfiguration) class instances are associated**.
     * @description **Checks if all the given [ActionConfiguration](ActionConfiguration) class instances are associated** with your actual [Project](Project) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [Project](Project) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [ActionConfiguration](ActionConfiguration) class instances are associated with your [Project](Project) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @return **Return a boolean if all the given [ActionConfiguration](ActionConfiguration) class instances are associated** with your actual [Project](Project) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [Project](Project) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [ActionConfiguration](ActionConfiguration) class instances are associated with your [Project](Project) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @param actions All the given [ActionConfiguration](ActionConfiguration) class instances that you want to check their relation *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category defined
     * @public
     * @see Project.disposeActions
     * @see Project.unlinkActions
     * @see Project.unsafeActions
     * @see Project.getActions
     * @see Project.linkActions
     * @see Project.getAction
     * @see Project.unsafeAction
     * @see Project.getActionsSize
     * @see Project.actions
     */
    areActionsLinked(... actions: DeepArray<ActionConfiguration|undefined>[]): boolean{
        return flat(actions).every((action) => {
            if (typeof action === "undefined"){
                return false;
            }

            return this.unsafeActions().includes(action);
        });
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [ModelConfiguration](ModelConfiguration) and the actual [Project](Project)**.
     * @description **An array of saved relations between [ModelConfiguration](ModelConfiguration) class instances and the actual [Project](Project) class instance**.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category property
     * @protected
     * @see Project.areModelsLinked
     * @see Project.disposeModels
     * @see Project.unlinkModels
     * @see Project.unsafeModels
     * @see Project.getModels
     * @see Project.linkModels
     * @see Project.getModel
     * @see Project.unsafeModel
     * @see Project.getModelsSize
     */
    protected models: ModelConfiguration[] = [];


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the number of the associated [ModelConfiguration](ModelConfiguration) class instances**.
     * @description **Gives the size of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances**.
     * @return **Returns the number of the associated [ModelConfiguration](ModelConfiguration) class instances**.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category length
     * @public
     * @see Project.areModelsLinked
     * @see Project.disposeModels
     * @see Project.unlinkModels
     * @see Project.unsafeModels
     * @see Project.getModels
     * @see Project.linkModels
     * @see Project.getModel
     * @see Project.unsafeModel
     * @see Project.models
     */
    getModelsSize(): number{
        return this.unsafeModels().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration)** class instances. Unlike [getModel](Project.getModel) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances**. Unlike [getModel](Project.getModel) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances**. Unlike [getModel](Project.getModel) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration), you can give `-1` for example. If you want to get the first item of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration), you can give `0`.*
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category unsafe
     * @category value
     * @public
     * @see Project.areModelsLinked
     * @see Project.disposeModels
     * @see Project.unlinkModels
     * @see Project.unsafeModels
     * @see Project.getModels
     * @see Project.linkModels
     * @see Project.getModel
     * @see Project.getModelsSize
     * @see Project.models
     */
    unsafeModel(position: number): ModelConfiguration|undefined{
        if (position < 0){
            const new_position = this.getModelsSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeModel(new_position);
        }

        return this.models[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Project--unsafeModel) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Project--unsafeModel) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Project--unsafeModel) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration), you can give `-1` for example. If you want to get the first item of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration), you can give `0`.*
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category getter
     * @category value
     * @public
     * @see Project.areModelsLinked
     * @see Project.disposeModels
     * @see Project.unlinkModels
     * @see Project.unsafeModels
     * @see Project.getModels
     * @see Project.linkModels
     * @see Project.unsafeModel
     * @see Project.getModelsSize
     * @see Project.models
     */
    getModel(position: number): ModelConfiguration{
        const model = this.unsafeModel(position);
        if (typeof model === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getModel");
            error.setClassname("Project");
            error.setProperty("models");
            error.setPosition(position);
            throw error;
        }
        return model;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associate one or multiple [ModelConfiguration](ModelConfiguration) class instances** from your actual [Project](Project) class instance.
     * @description Allows you to **associate one or multiple [ModelConfiguration](ModelConfiguration) class instances** from your actual [Project](Project) class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate [Project](Project) class instance** ([ModelConfiguration.unlinkProject](ModelConfiguration.unlinkProject)), and **then associate the  [ModelConfiguration](ModelConfiguration) class instances** to the [Project](Project) class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [ModelConfiguration.linkProject](ModelConfiguration.linkProject) to associate the [Project](Project) class instance and the [ModelConfiguration](ModelConfiguration) class instances from the both parts*.
     * @return Returns the actual [ModelConfiguration](ModelConfiguration) class instance.
     * @param models All the given [ModelConfiguration](ModelConfiguration) class instances that you to associate to your actual [Project](Project) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category associate
     * @public
     * @see Project.areModelsLinked
     * @see Project.disposeModels
     * @see Project.unlinkModels
     * @see Project.unsafeModels
     * @see Project.getModels
     * @see Project.getModel
     * @see Project.unsafeModel
     * @see Project.getModelsSize
     * @see Project.models
     * @see ModelConfiguration.linkProject
     */
    linkModels(... models: DeepArray<ModelConfiguration>[]): this{
        for (const model of flat(models)){
            if (this.areModelsLinked(model)){
                continue;
            }


            this.models.push(model);


            model.linkProject(this);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances**.
     * @description **Gives a copy of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances**.
     * @return **Returns a copy of the [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances**.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category getter
     * @category iterable
     * @public
     * @see Project.areModelsLinked
     * @see Project.disposeModels
     * @see Project.unlinkModels
     * @see Project.unsafeModels
     * @see Project.linkModels
     * @see Project.getModel
     * @see Project.unsafeModel
     * @see Project.getModelsSize
     * @see Project.models
     */
    getModels(): ModelConfiguration[]{
        return Array.from(this.unsafeModels());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives an [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances without copy**.
     * @description **Gives an [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getModels](Project.getModels) method.
     * @return **Returns an [array](Project.models) of the associated [ModelConfiguration](ModelConfiguration) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getModels](Project.getModels) method.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category unsafe
     * @category iterable
     * @public
     * @see Project.areModelsLinked
     * @see Project.disposeModels
     * @see Project.unlinkModels
     * @see Project.getModels
     * @see Project.linkModels
     * @see Project.getModel
     * @see Project.unsafeModel
     * @see Project.getModelsSize
     * @see Project.models
     */
    unsafeModels(): ModelConfiguration[]{
        return this.models;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate one or multiple [ModelConfiguration](ModelConfiguration) class instances**.
     * @description Allows you to **dissociate one or multiple [ModelConfiguration](ModelConfiguration) class instances** from your actual [Project](Project) class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the [Project](Project) class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [ModelConfiguration.unlinkProject](ModelConfiguration.unlinkProject) to dissociate the [Project](Project) class instance and the given [ModelConfiguration](ModelConfiguration) class instances from the both parts*.
     * @return Returns the actual [ModelConfiguration](ModelConfiguration) class instance.
     * @param models All the given [ModelConfiguration](ModelConfiguration) class instances that you to dissociate from your actual [Project](Project) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category dissociate
     * @public
     * @see Project.areModelsLinked
     * @see Project.disposeModels
     * @see Project.unsafeModels
     * @see Project.getModels
     * @see Project.linkModels
     * @see Project.getModel
     * @see Project.unsafeModel
     * @see Project.getModelsSize
     * @see Project.models
     * @see ModelConfiguration.unlinkProject
     */
    unlinkModels(... models: DeepArray<ModelConfiguration>[]): this{
        for (const model of flat(models)){
            if (!this.areModelsLinked(model)){
                continue;
            }

            while (true){
                const index = this.models.indexOf(model);
                if (index < 0){
                    break;
                }

                this.models.splice(index, 1);
            }

            model.unlinkProject();
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate all the associated [ModelConfiguration](ModelConfiguration) class instances**.
     * @description **Dissociate all the associated [ModelConfiguration](ModelConfiguration) class instances**.  *Note that all the associated [ModelConfiguration](ModelConfiguration) class instances will lose their [Project](Project) class instance association*.
     * @return Returns the actual [ModelConfiguration](ModelConfiguration) class instance.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category truncate
     * @public
     * @see Project.areModelsLinked
     * @see Project.unlinkModels
     * @see Project.unsafeModels
     * @see Project.getModels
     * @see Project.linkModels
     * @see Project.getModel
     * @see Project.unsafeModel
     * @see Project.getModelsSize
     * @see Project.models
     */
    disposeModels(): this{
        this.unlinkModels(this.unsafeModels());
        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [ModelConfiguration](ModelConfiguration) class instances are associated**.
     * @description **Checks if all the given [ModelConfiguration](ModelConfiguration) class instances are associated** with your actual [Project](Project) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [Project](Project) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [ModelConfiguration](ModelConfiguration) class instances are associated with your [Project](Project) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @return **Return a boolean if all the given [ModelConfiguration](ModelConfiguration) class instances are associated** with your actual [Project](Project) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [Project](Project) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [ModelConfiguration](ModelConfiguration) class instances are associated with your [Project](Project) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @param models All the given [ModelConfiguration](ModelConfiguration) class instances that you want to check their relation *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category defined
     * @public
     * @see Project.disposeModels
     * @see Project.unlinkModels
     * @see Project.unsafeModels
     * @see Project.getModels
     * @see Project.linkModels
     * @see Project.getModel
     * @see Project.unsafeModel
     * @see Project.getModelsSize
     * @see Project.models
     */
    areModelsLinked(... models: DeepArray<ModelConfiguration|undefined>[]): boolean{
        return flat(models).every((model) => {
            if (typeof model === "undefined"){
                return false;
            }

            return this.unsafeModels().includes(model);
        });
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [Route](Route) and the actual [Project](Project)**.
     * @description **An array of saved relations between [Route](Route) class instances and the actual [Project](Project) class instance**.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category property
     * @protected
     * @see Project.areRoutesLinked
     * @see Project.disposeRoutes
     * @see Project.unlinkRoutes
     * @see Project.unsafeRoutes
     * @see Project.getRoutes
     * @see Project.linkRoutes
     * @see Project.getRoute
     * @see Project.unsafeRoute
     * @see Project.getRoutesSize
     */
    protected routes: Route[] = [];


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the number of the associated [Route](Route) class instances**.
     * @description **Gives the size of the [array](Project.routes) of the associated [Route](Route) class instances**.
     * @return **Returns the number of the associated [Route](Route) class instances**.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category length
     * @public
     * @see Project.areRoutesLinked
     * @see Project.disposeRoutes
     * @see Project.unlinkRoutes
     * @see Project.unsafeRoutes
     * @see Project.getRoutes
     * @see Project.linkRoutes
     * @see Project.getRoute
     * @see Project.unsafeRoute
     * @see Project.routes
     */
    getRoutesSize(): number{
        return this.unsafeRoutes().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](Project.routes) of the associated [Route](Route)** class instances. Unlike [getRoute](Project.getRoute) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of the [array](Project.routes) of the associated [Route](Route) class instances**. Unlike [getRoute](Project.getRoute) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](Project.routes) of the associated [Route](Route) class instances**. Unlike [getRoute](Project.getRoute) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](Project.routes) of the associated [Route](Route), you can give `-1` for example. If you want to get the first item of the [array](Project.routes) of the associated [Route](Route), you can give `0`.*
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category unsafe
     * @category value
     * @public
     * @see Project.areRoutesLinked
     * @see Project.disposeRoutes
     * @see Project.unlinkRoutes
     * @see Project.unsafeRoutes
     * @see Project.getRoutes
     * @see Project.linkRoutes
     * @see Project.getRoute
     * @see Project.getRoutesSize
     * @see Project.routes
     */
    unsafeRoute(position: number): Route|undefined{
        if (position < 0){
            const new_position = this.getRoutesSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeRoute(new_position);
        }

        return this.routes[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](Project.routes) of the associated [Route](Route) class instances**. Unlike the [unsafeTask](#one-to-many-Project--unsafeRoute) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item of the [array](Project.routes) of the associated [Route](Route) class instances**. Unlike the [unsafeTask](#one-to-many-Project--unsafeRoute) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](Project.routes) of the associated [Route](Route) class instances**. Unlike the [unsafeTask](#one-to-many-Project--unsafeRoute) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](Project.routes) of the associated [Route](Route), you can give `-1` for example. If you want to get the first item of the [array](Project.routes) of the associated [Route](Route), you can give `0`.*
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category getter
     * @category value
     * @public
     * @see Project.areRoutesLinked
     * @see Project.disposeRoutes
     * @see Project.unlinkRoutes
     * @see Project.unsafeRoutes
     * @see Project.getRoutes
     * @see Project.linkRoutes
     * @see Project.unsafeRoute
     * @see Project.getRoutesSize
     * @see Project.routes
     */
    getRoute(position: number): Route{
        const route = this.unsafeRoute(position);
        if (typeof route === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getRoute");
            error.setClassname("Project");
            error.setProperty("routes");
            error.setPosition(position);
            throw error;
        }
        return route;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associate one or multiple [Route](Route) class instances** from your actual [Project](Project) class instance.
     * @description Allows you to **associate one or multiple [Route](Route) class instances** from your actual [Project](Project) class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate [Project](Project) class instance** ([Route.unlinkProject](Route.unlinkProject)), and **then associate the  [Route](Route) class instances** to the [Project](Project) class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [Route.linkProject](Route.linkProject) to associate the [Project](Project) class instance and the [Route](Route) class instances from the both parts*.
     * @return Returns the actual [Route](Route) class instance.
     * @param routes All the given [Route](Route) class instances that you to associate to your actual [Project](Project) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category associate
     * @public
     * @see Project.areRoutesLinked
     * @see Project.disposeRoutes
     * @see Project.unlinkRoutes
     * @see Project.unsafeRoutes
     * @see Project.getRoutes
     * @see Project.getRoute
     * @see Project.unsafeRoute
     * @see Project.getRoutesSize
     * @see Project.routes
     * @see Route.linkProject
     */
    linkRoutes(... routes: DeepArray<Route>[]): this{
        for (const route of flat(routes)){
            if (this.areRoutesLinked(route)){
                continue;
            }


            this.routes.push(route);


            route.linkProject(this);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of the [array](Project.routes) of the associated [Route](Route) class instances**.
     * @description **Gives a copy of the [array](Project.routes) of the associated [Route](Route) class instances**.
     * @return **Returns a copy of the [array](Project.routes) of the associated [Route](Route) class instances**.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category getter
     * @category iterable
     * @public
     * @see Project.areRoutesLinked
     * @see Project.disposeRoutes
     * @see Project.unlinkRoutes
     * @see Project.unsafeRoutes
     * @see Project.linkRoutes
     * @see Project.getRoute
     * @see Project.unsafeRoute
     * @see Project.getRoutesSize
     * @see Project.routes
     */
    getRoutes(): Route[]{
        return Array.from(this.unsafeRoutes());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives an [array](Project.routes) of the associated [Route](Route) class instances without copy**.
     * @description **Gives an [array](Project.routes) of the associated [Route](Route) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getRoutes](Project.getRoutes) method.
     * @return **Returns an [array](Project.routes) of the associated [Route](Route) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getRoutes](Project.getRoutes) method.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category unsafe
     * @category iterable
     * @public
     * @see Project.areRoutesLinked
     * @see Project.disposeRoutes
     * @see Project.unlinkRoutes
     * @see Project.getRoutes
     * @see Project.linkRoutes
     * @see Project.getRoute
     * @see Project.unsafeRoute
     * @see Project.getRoutesSize
     * @see Project.routes
     */
    unsafeRoutes(): Route[]{
        return this.routes;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate one or multiple [Route](Route) class instances**.
     * @description Allows you to **dissociate one or multiple [Route](Route) class instances** from your actual [Project](Project) class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the [Project](Project) class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [Route.unlinkProject](Route.unlinkProject) to dissociate the [Project](Project) class instance and the given [Route](Route) class instances from the both parts*.
     * @return Returns the actual [Route](Route) class instance.
     * @param routes All the given [Route](Route) class instances that you to dissociate from your actual [Project](Project) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category dissociate
     * @public
     * @see Project.areRoutesLinked
     * @see Project.disposeRoutes
     * @see Project.unsafeRoutes
     * @see Project.getRoutes
     * @see Project.linkRoutes
     * @see Project.getRoute
     * @see Project.unsafeRoute
     * @see Project.getRoutesSize
     * @see Project.routes
     * @see Route.unlinkProject
     */
    unlinkRoutes(... routes: DeepArray<Route>[]): this{
        for (const route of flat(routes)){
            if (!this.areRoutesLinked(route)){
                continue;
            }

            while (true){
                const index = this.routes.indexOf(route);
                if (index < 0){
                    break;
                }

                this.routes.splice(index, 1);
            }

            route.unlinkProject();
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate all the associated [Route](Route) class instances**.
     * @description **Dissociate all the associated [Route](Route) class instances**.  *Note that all the associated [Route](Route) class instances will lose their [Project](Project) class instance association*.
     * @return Returns the actual [Route](Route) class instance.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category truncate
     * @public
     * @see Project.areRoutesLinked
     * @see Project.unlinkRoutes
     * @see Project.unsafeRoutes
     * @see Project.getRoutes
     * @see Project.linkRoutes
     * @see Project.getRoute
     * @see Project.unsafeRoute
     * @see Project.getRoutesSize
     * @see Project.routes
     */
    disposeRoutes(): this{
        this.unlinkRoutes(this.unsafeRoutes());
        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [Route](Route) class instances are associated**.
     * @description **Checks if all the given [Route](Route) class instances are associated** with your actual [Project](Project) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [Project](Project) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [Route](Route) class instances are associated with your [Project](Project) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @return **Return a boolean if all the given [Route](Route) class instances are associated** with your actual [Project](Project) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [Project](Project) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [Route](Route) class instances are associated with your [Project](Project) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @param routes All the given [Route](Route) class instances that you want to check their relation *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category defined
     * @public
     * @see Project.disposeRoutes
     * @see Project.unlinkRoutes
     * @see Project.unsafeRoutes
     * @see Project.getRoutes
     * @see Project.linkRoutes
     * @see Project.getRoute
     * @see Project.unsafeRoute
     * @see Project.getRoutesSize
     * @see Project.routes
     */
    areRoutesLinked(... routes: DeepArray<Route|undefined>[]): boolean{
        return flat(routes).every((route) => {
            if (typeof route === "undefined"){
                return false;
            }

            return this.unsafeRoutes().includes(route);
        });
    }
}

export default Project