import UndefinedValue from "../error/internal/undefined-value";
import Route from "./index";
import UnlinkedValue from "../error/internal/unlinked-value";

class RouteMatcher{
    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @RouteMatcher.method.
     * @description Save @RouteMatcher.method.
     * @category @RouteMatcher.method
     * @category property
     * @category field
     * @protected
     * @see RouteMatcher.setMethod
     * @see RouteMatcher.getMethod
     * @see RouteMatcher.unsafeMethod
     * @see RouteMatcher.eraseMethod
     * @see RouteMatcher.isMethodDefined
     * @see RouteMatcher.isMethodEqualTo
     */
    protected method: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @RouteMatcher.method**.
     * @description Allows you to update or **assign the value of @RouteMatcher.method**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [RouteMatcher](RouteMatcher) class instance.
     * @param method The new value to assign to @RouteMatcher.method.
     * @category @RouteMatcher.method
     * @category setter
     * @category field
     * @public
     * @see RouteMatcher.method
     * @see RouteMatcher.getMethod
     * @see RouteMatcher.unsafeMethod
     * @see RouteMatcher.eraseMethod
     * @see RouteMatcher.isMethodDefined
     * @see RouteMatcher.isMethodEqualTo
     */
    setMethod(method: string): this{
        if (this.isMethodEqualTo(method)){
            return this;
        }

        this.eraseMethod();
        this.method = method;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @RouteMatcher.method**. Unlike the [getMethod](RouteMatcher.getMethod) method, **the method results an undefined value if @RouteMatcher.method is not defined**.
     * @description **Gives the value of @RouteMatcher.method**. Unlike the [getMethod](RouteMatcher.getMethod) method, **the method results an undefined value if @RouteMatcher.method is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @RouteMatcher.method**. Unlike the [getMethod](RouteMatcher.getMethod) method, **the method results an undefined value if @RouteMatcher.method is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @RouteMatcher.method
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see RouteMatcher.method
     * @see RouteMatcher.setMethod
     * @see RouteMatcher.getMethod
     * @see RouteMatcher.eraseMethod
     * @see RouteMatcher.isMethodDefined
     * @see RouteMatcher.isMethodEqualTo
     */
    unsafeMethod(): string|undefined{
        return this.method;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @RouteMatcher.method**. Unlike the [unsafeMethod](RouteMatcher.unsafeMethod) method, **we will throw an [UndefinedValue](UndefinedValue) error if @RouteMatcher.method is not defined**.
     * @description **Gives the value of @RouteMatcher.method**. Unlike the [unsafeMethod](RouteMatcher.unsafeMethod) method, **we will throw an [UndefinedValue](UndefinedValue) error if @RouteMatcher.method is not defined**.
     * @return **Returns the value of @RouteMatcher.method**. Unlike the [unsafeMethod](RouteMatcher.unsafeMethod) method, **we will throw an [UndefinedValue](UndefinedValue) error if @RouteMatcher.method is not defined**.
     * @category @RouteMatcher.method
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @RouteMatcher.method isn't defined.
     * @see RouteMatcher.method
     * @see RouteMatcher.setMethod
     * @see RouteMatcher.unsafeMethod
     * @see RouteMatcher.eraseMethod
     * @see RouteMatcher.isMethodDefined
     * @see RouteMatcher.isMethodEqualTo
     */
    getMethod(): string{
        const method = this.unsafeMethod();

        if (typeof method === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getMethod");
            error.setClassname("RouteMatcher");
            error.setProperty("method");
            throw error;
        }
        return method;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @RouteMatcher.method is defined**.
     * @description **Checks if @RouteMatcher.method is defined** or not.
     * @return **Returns a boolean if @RouteMatcher.method is defined** or not.
     * @category @RouteMatcher.method
     * @category defined
     * @category field
     * @public
     * @see RouteMatcher.method
     * @see RouteMatcher.setMethod
     * @see RouteMatcher.getMethod
     * @see RouteMatcher.unsafeMethod
     * @see RouteMatcher.eraseMethod
     * @see RouteMatcher.isMethodEqualTo
     */
    isMethodDefined(): boolean{
        return typeof this.method !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @RouteMatcher.method is equal to your given value**.
     * @description **Checks if @RouteMatcher.method is equal to your given value** and if @RouteMatcher.method is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @RouteMatcher.method is equal to your given value** and if @RouteMatcher.method is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param method **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @RouteMatcher.method
     * @category equality
     * @category field
     * @public
     * @see RouteMatcher.method
     * @see RouteMatcher.setMethod
     * @see RouteMatcher.getMethod
     * @see RouteMatcher.unsafeMethod
     * @see RouteMatcher.eraseMethod
     * @see RouteMatcher.isMethodDefined
     */
    isMethodEqualTo(method?: undefined|string): boolean{
        if (typeof method === "undefined"){
            return false;
        }
        return this.unsafeMethod() === method;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @RouteMatcher.method**.
     * @description **Removes the assigned the value of @RouteMatcher.method**: *@RouteMatcher.method will be flagged as undefined*.
     * @return Return the actual [RouteMatcher](RouteMatcher) class instance.
     * @category @RouteMatcher.method
     * @category truncate
     * @category field
     * @public
     * @see RouteMatcher.method
     * @see RouteMatcher.setMethod
     * @see RouteMatcher.getMethod
     * @see RouteMatcher.unsafeMethod
     * @see RouteMatcher.isMethodDefined
     * @see RouteMatcher.isMethodEqualTo
     */
    eraseMethod(): this{
        this.method = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @RouteMatcher.pattern.
     * @description Save @RouteMatcher.pattern.
     * @category @RouteMatcher.pattern
     * @category property
     * @category field
     * @protected
     * @see RouteMatcher.setPattern
     * @see RouteMatcher.getPattern
     * @see RouteMatcher.unsafePattern
     * @see RouteMatcher.erasePattern
     * @see RouteMatcher.isPatternDefined
     * @see RouteMatcher.isPatternEqualTo
     */
    protected pattern: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @RouteMatcher.pattern**.
     * @description Allows you to update or **assign the value of @RouteMatcher.pattern**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [RouteMatcher](RouteMatcher) class instance.
     * @param pattern The new value to assign to @RouteMatcher.pattern.
     * @category @RouteMatcher.pattern
     * @category setter
     * @category field
     * @public
     * @see RouteMatcher.pattern
     * @see RouteMatcher.getPattern
     * @see RouteMatcher.unsafePattern
     * @see RouteMatcher.erasePattern
     * @see RouteMatcher.isPatternDefined
     * @see RouteMatcher.isPatternEqualTo
     */
    setPattern(pattern: string): this{
        if (this.isPatternEqualTo(pattern)){
            return this;
        }

        this.erasePattern();
        this.pattern = pattern;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @RouteMatcher.pattern**. Unlike the [getPattern](RouteMatcher.getPattern) method, **the method results an undefined value if @RouteMatcher.pattern is not defined**.
     * @description **Gives the value of @RouteMatcher.pattern**. Unlike the [getPattern](RouteMatcher.getPattern) method, **the method results an undefined value if @RouteMatcher.pattern is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @RouteMatcher.pattern**. Unlike the [getPattern](RouteMatcher.getPattern) method, **the method results an undefined value if @RouteMatcher.pattern is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @RouteMatcher.pattern
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see RouteMatcher.pattern
     * @see RouteMatcher.setPattern
     * @see RouteMatcher.getPattern
     * @see RouteMatcher.erasePattern
     * @see RouteMatcher.isPatternDefined
     * @see RouteMatcher.isPatternEqualTo
     */
    unsafePattern(): string|undefined{
        return this.pattern;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @RouteMatcher.pattern**. Unlike the [unsafePattern](RouteMatcher.unsafePattern) method, **we will throw an [UndefinedValue](UndefinedValue) error if @RouteMatcher.pattern is not defined**.
     * @description **Gives the value of @RouteMatcher.pattern**. Unlike the [unsafePattern](RouteMatcher.unsafePattern) method, **we will throw an [UndefinedValue](UndefinedValue) error if @RouteMatcher.pattern is not defined**.
     * @return **Returns the value of @RouteMatcher.pattern**. Unlike the [unsafePattern](RouteMatcher.unsafePattern) method, **we will throw an [UndefinedValue](UndefinedValue) error if @RouteMatcher.pattern is not defined**.
     * @category @RouteMatcher.pattern
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @RouteMatcher.pattern isn't defined.
     * @see RouteMatcher.pattern
     * @see RouteMatcher.setPattern
     * @see RouteMatcher.unsafePattern
     * @see RouteMatcher.erasePattern
     * @see RouteMatcher.isPatternDefined
     * @see RouteMatcher.isPatternEqualTo
     */
    getPattern(): string{
        const pattern = this.unsafePattern();

        if (typeof pattern === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getPattern");
            error.setClassname("RouteMatcher");
            error.setProperty("pattern");
            throw error;
        }
        return pattern;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @RouteMatcher.pattern is defined**.
     * @description **Checks if @RouteMatcher.pattern is defined** or not.
     * @return **Returns a boolean if @RouteMatcher.pattern is defined** or not.
     * @category @RouteMatcher.pattern
     * @category defined
     * @category field
     * @public
     * @see RouteMatcher.pattern
     * @see RouteMatcher.setPattern
     * @see RouteMatcher.getPattern
     * @see RouteMatcher.unsafePattern
     * @see RouteMatcher.erasePattern
     * @see RouteMatcher.isPatternEqualTo
     */
    isPatternDefined(): boolean{
        return typeof this.pattern !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @RouteMatcher.pattern is equal to your given value**.
     * @description **Checks if @RouteMatcher.pattern is equal to your given value** and if @RouteMatcher.pattern is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @RouteMatcher.pattern is equal to your given value** and if @RouteMatcher.pattern is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param pattern **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @RouteMatcher.pattern
     * @category equality
     * @category field
     * @public
     * @see RouteMatcher.pattern
     * @see RouteMatcher.setPattern
     * @see RouteMatcher.getPattern
     * @see RouteMatcher.unsafePattern
     * @see RouteMatcher.erasePattern
     * @see RouteMatcher.isPatternDefined
     */
    isPatternEqualTo(pattern?: undefined|string): boolean{
        if (typeof pattern === "undefined"){
            return false;
        }
        return this.unsafePattern() === pattern;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @RouteMatcher.pattern**.
     * @description **Removes the assigned the value of @RouteMatcher.pattern**: *@RouteMatcher.pattern will be flagged as undefined*.
     * @return Return the actual [RouteMatcher](RouteMatcher) class instance.
     * @category @RouteMatcher.pattern
     * @category truncate
     * @category field
     * @public
     * @see RouteMatcher.pattern
     * @see RouteMatcher.setPattern
     * @see RouteMatcher.getPattern
     * @see RouteMatcher.unsafePattern
     * @see RouteMatcher.isPatternDefined
     * @see RouteMatcher.isPatternEqualTo
     */
    erasePattern(): this{
        this.pattern = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [Route](Route) and the actual [RouteMatcher](RouteMatcher)**.
     * @description **Saves the relation between [Route](Route) class instances and the actual [RouteMatcher](RouteMatcher) class instance**.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category property
     * @category relation
     * @protected
     * @see RouteMatcher.linkMatcher
     * @see RouteMatcher.hasMatcherLinked
     * @see RouteMatcher.unsafeMatcher
     * @see RouteMatcher.getMatcher
     * @see RouteMatcher.unlinkMatcher
     * @see RouteMatcher.isLinkedWithMatcher
     */
    protected route: Route|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associates a [Route](Route) class instance**.
     * @description Allows you to **associate a [Route](Route) class instance** with your actual [RouteMatcher](RouteMatcher) class instance. *This method allows one parameter which is a [Route](Route) class instance that will be associated*. *Note that the method will also run [Route.linkMatchers](Route.linkMatchers) to associate the [Route](Route) class instance et the [RouteMatcher](RouteMatcher) class instance from the both parts*. *Note also that the method will also run [unlinkRoute](RouteMatcher.unlinkRoute) to dissociate the current [Route](Route) class instance associated*.
     * @return Returns the actual [RouteMatcher](RouteMatcher) class instance.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category association
     * @category relation
     * @public
     * @see RouteMatcher.matcher
     * @see RouteMatcher.hasMatcherLinked
     * @see RouteMatcher.unsafeMatcher
     * @see RouteMatcher.getMatcher
     * @see RouteMatcher.unlinkMatcher
     * @see RouteMatcher.isLinkedWithMatcher
     * @see Route.linkMatchers
     */
    linkRoute(route: Route): this{
        if (this.isLinkedWithRoute(route)){
            return this;
        }


        if (!route.areMatchersLinked(this)){
            route.linkMatchers(this);

            return this;
        }


        this.unlinkRoute();
        this.route = route;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if a [Route](Route) class instance is associated**.
     * @return **Gives a boolean if a [Route](Route) class instance is associated** or not to our actual [RouteMatcher](RouteMatcher) class instance.
     * @description **Check if a [Route](Route) class instance is associated** or not to our actual [RouteMatcher](RouteMatcher) class instance.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category defined
     * @public
     * @see RouteMatcher.matcher
     * @see RouteMatcher.linkMatcher
     * @see RouteMatcher.unsafeMatcher
     * @see RouteMatcher.getMatcher
     * @see RouteMatcher.unlinkMatcher
     * @see RouteMatcher.isLinkedWithMatcher
     */
    hasRouteLinked(): boolean{
        return typeof this.route !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [Route](Route) class instance**. Unlike the [unsafeRoute](RouteMatcher.unsafeRoute) method, **the method can throw an [UnlinkedValue](UnlinkedValue) error**.
     * @description **Gives the associated [Route](Route) class instance** of our actual [RouteMatcher](RouteMatcher) class instance. Unlike the [unsafeRoute](RoutesMatchers.unsafeRoute) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [RouteMatcher](RouteMatcher) class instance doesn't have [Route](Route) class instance associate**.
     * @return **Results the associated [Route](Route) class instance** of our actual [RouteMatcher](RouteMatcher) class instance. Unlike the [unsafeRoute](RoutesMatchers.unsafeRoute) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [RouteMatcher](RouteMatcher) class instance doesn't have [Route](Route) class instance associate**.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category value
     * @category getter
     * @public
     * @see RouteMatcher.matcher
     * @see RouteMatcher.linkMatcher
     * @see RouteMatcher.hasMatcherLinked
     * @see RouteMatcher.unsafeMatcher
     * @see RouteMatcher.unlinkMatcher
     * @see RouteMatcher.isLinkedWithMatcher
     */
    getRoute(): Route{
        const unsafe = this.unsafeRoute();

        if (typeof unsafe === "undefined"){
            const error = new UnlinkedValue();
            error.setMethod("getRoute");
            error.setClassname("RouteMatcher");
            error.setWantedClassname("Route");
            error.setProperty("route");
            throw error;
        }
        return unsafe;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [Route](Route) class instance**. Unlike the [getRoute](RouteMatcher.getRoute) method, **the method can result an undefined value.
     * @description **Gives the associated [Route](Route) class instance** of our actual [RouteMatcher](RouteMatcher) class instance. Unlike the [getRoute](RouteMatcher.getRoute) method, **the method results an undefined value if the [RouteMatcher](RouteMatcher) class instance doesn't have [Route](Route) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Results the associated [Route](Route) class instance** of our actual [RouteMatcher](RouteMatcher) class instance. Unlike the [getRoute](RouteMatcher.getRoute) method, **the method results an undefined value if the [RouteMatcher](RouteMatcher) class instance doesn't have [Route](Route) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category value
     * @category unsafe
     * @public
     * @see RouteMatcher.matcher
     * @see RouteMatcher.linkMatcher
     * @see RouteMatcher.hasMatcherLinked
     * @see RouteMatcher.getMatcher
     * @see RouteMatcher.unlinkMatcher
     * @see RouteMatcher.isLinkedWithMatcher
     */
    unsafeRoute(): Route|undefined{
        return this.route;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate the [Route](Route) class instance**.
     * @description **Dissociate the [Route](Route) class instance** from your actual [RouteMatcher](RouteMatcher) class instance.  *Note that the method will also run [Route.unlinkMatchers](Route.unlinkMatchers) to dissociate the [Route](Route) class instance et the [RouteMatcher](RouteMatcher) class instance from the both parts*.
     * @return Return the actual [RouteMatcher](RouteMatcher) class instance.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category value
     * @category dissociate
     * @public
     * @see RouteMatcher.matcher
     * @see RouteMatcher.linkMatcher
     * @see RouteMatcher.hasMatcherLinked
     * @see RouteMatcher.unsafeMatcher
     * @see RouteMatcher.getMatcher
     * @see RouteMatcher.isLinkedWithMatcher
     * @see Route.unlinkMatchers
     */
    unlinkRoute(): this{
        const _route = this.route;
        this.route = undefined;

        _route?.unlinkMatchers(this);

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [Route](Route) class instance** are associated.
     * @description **Checks if the [RouteMatcher](RouteMatcher) class instance is associated with all the given [Route](Route) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Return a boolean if the [RouteMatcher](RouteMatcher) class instance is associated with all the given [Route](Route) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category value
     * @category equality
     * @public
     * @see RouteMatcher.matcher
     * @see RouteMatcher.linkMatcher
     * @see RouteMatcher.hasMatcherLinked
     * @see RouteMatcher.unsafeMatcher
     * @see RouteMatcher.getMatcher
     * @see RouteMatcher.unlinkMatcher
     */
    isLinkedWithRoute(route?: undefined|Route): boolean{
        if (typeof route === "undefined"){
            return false;
        }
        return this.unsafeRoute() === route;
    }
}

export default RouteMatcher;