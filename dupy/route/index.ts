import UndefinedValue from "../error/internal/undefined-value";
import RouteMatcher from "./matcher";
import OutOfRange from "../error/internal/out-of-range";
import DeepArray from "../util/deep-array/type";
import flat from "../util/deep-array/flat";
import Project from "../project";
import UnlinkedValue from "../error/internal/unlinked-value";
import ActionConfiguration from "../action";

class Route{
    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @Route.key.
     * @description Save @Route.key.
     * @category @Route.key
     * @category property
     * @category field
     * @protected
     * @see Route.setKey
     * @see Route.getKey
     * @see Route.unsafeKey
     * @see Route.eraseKey
     * @see Route.isKeyDefined
     * @see Route.isKeyEqualTo
     */
    protected key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @Route.key**.
     * @description Allows you to update or **assign the value of @Route.key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [Route](Route) class instance.
     * @param key The new value to assign to @Route.key.
     * @category @Route.key
     * @category setter
     * @category field
     * @public
     * @see Route.key
     * @see Route.getKey
     * @see Route.unsafeKey
     * @see Route.eraseKey
     * @see Route.isKeyDefined
     * @see Route.isKeyEqualTo
     */
    setKey(key: string): this{
        if (this.isKeyEqualTo(key)){
            return this;
        }

        this.eraseKey();
        this.key = key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @Route.key**. Unlike the [getKey](Route.getKey) method, **the method results an undefined value if @Route.key is not defined**.
     * @description **Gives the value of @Route.key**. Unlike the [getKey](Route.getKey) method, **the method results an undefined value if @Route.key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @Route.key**. Unlike the [getKey](Route.getKey) method, **the method results an undefined value if @Route.key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @Route.key
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see Route.key
     * @see Route.setKey
     * @see Route.getKey
     * @see Route.eraseKey
     * @see Route.isKeyDefined
     * @see Route.isKeyEqualTo
     */
    unsafeKey(): string|undefined{
        return this.key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @Route.key**. Unlike the [unsafeKey](Route.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @Route.key is not defined**.
     * @description **Gives the value of @Route.key**. Unlike the [unsafeKey](Route.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @Route.key is not defined**.
     * @return **Returns the value of @Route.key**. Unlike the [unsafeKey](Route.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @Route.key is not defined**.
     * @category @Route.key
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @Route.key isn't defined.
     * @see Route.key
     * @see Route.setKey
     * @see Route.unsafeKey
     * @see Route.eraseKey
     * @see Route.isKeyDefined
     * @see Route.isKeyEqualTo
     */
    getKey(): string{
        const key = this.unsafeKey();

        if (typeof key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getKey");
            error.setClassname("Route");
            error.setProperty("key");
            throw error;
        }
        return key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @Route.key is defined**.
     * @description **Checks if @Route.key is defined** or not.
     * @return **Returns a boolean if @Route.key is defined** or not.
     * @category @Route.key
     * @category defined
     * @category field
     * @public
     * @see Route.key
     * @see Route.setKey
     * @see Route.getKey
     * @see Route.unsafeKey
     * @see Route.eraseKey
     * @see Route.isKeyEqualTo
     */
    isKeyDefined(): boolean{
        return typeof this.key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @Route.key is equal to your given value**.
     * @description **Checks if @Route.key is equal to your given value** and if @Route.key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @Route.key is equal to your given value** and if @Route.key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @Route.key
     * @category equality
     * @category field
     * @public
     * @see Route.key
     * @see Route.setKey
     * @see Route.getKey
     * @see Route.unsafeKey
     * @see Route.eraseKey
     * @see Route.isKeyDefined
     */
    isKeyEqualTo(key?: undefined|string): boolean{
        if (typeof key === "undefined"){
            return false;
        }
        return this.unsafeKey() === key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @Route.key**.
     * @description **Removes the assigned the value of @Route.key**: *@Route.key will be flagged as undefined*.
     * @return Return the actual [Route](Route) class instance.
     * @category @Route.key
     * @category truncate
     * @category field
     * @public
     * @see Route.key
     * @see Route.setKey
     * @see Route.getKey
     * @see Route.unsafeKey
     * @see Route.isKeyDefined
     * @see Route.isKeyEqualTo
     */
    eraseKey(): this{
        this.key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [RouteMatcher](RouteMatcher) and the actual [Route](Route)**.
     * @description **An array of saved relations between [RouteMatcher](RouteMatcher) class instances and the actual [Route](Route) class instance**.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category property
     * @protected
     * @see Route.areMatchersLinked
     * @see Route.disposeMatchers
     * @see Route.unlinkMatchers
     * @see Route.unsafeMatchers
     * @see Route.getMatchers
     * @see Route.linkMatchers
     * @see Route.getMatcher
     * @see Route.unsafeMatcher
     * @see Route.getMatchersSize
     */
    protected matchers: RouteMatcher[] = [];


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the number of the associated [RouteMatcher](RouteMatcher) class instances**.
     * @description **Gives the size of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances**.
     * @return **Returns the number of the associated [RouteMatcher](RouteMatcher) class instances**.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category length
     * @public
     * @see Route.areMatchersLinked
     * @see Route.disposeMatchers
     * @see Route.unlinkMatchers
     * @see Route.unsafeMatchers
     * @see Route.getMatchers
     * @see Route.linkMatchers
     * @see Route.getMatcher
     * @see Route.unsafeMatcher
     * @see Route.matchers
     */
    getMatchersSize(): number{
        return this.unsafeMatchers().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher)** class instances. Unlike [getMatcher](Route.getMatcher) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances**. Unlike [getMatcher](Route.getMatcher) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances**. Unlike [getMatcher](Route.getMatcher) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher), you can give `-1` for example. If you want to get the first item of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher), you can give `0`.*
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category unsafe
     * @category value
     * @public
     * @see Route.areMatchersLinked
     * @see Route.disposeMatchers
     * @see Route.unlinkMatchers
     * @see Route.unsafeMatchers
     * @see Route.getMatchers
     * @see Route.linkMatchers
     * @see Route.getMatcher
     * @see Route.getMatchersSize
     * @see Route.matchers
     */
    unsafeMatcher(position: number): RouteMatcher|undefined{
        if (position < 0){
            const new_position = this.getMatchersSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeMatcher(new_position);
        }

        return this.matchers[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances**. Unlike the [unsafeTask](#one-to-many-Route--unsafeMatcher) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances**. Unlike the [unsafeTask](#one-to-many-Route--unsafeMatcher) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances**. Unlike the [unsafeTask](#one-to-many-Route--unsafeMatcher) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher), you can give `-1` for example. If you want to get the first item of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher), you can give `0`.*
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category getter
     * @category value
     * @public
     * @see Route.areMatchersLinked
     * @see Route.disposeMatchers
     * @see Route.unlinkMatchers
     * @see Route.unsafeMatchers
     * @see Route.getMatchers
     * @see Route.linkMatchers
     * @see Route.unsafeMatcher
     * @see Route.getMatchersSize
     * @see Route.matchers
     */
    getMatcher(position: number): RouteMatcher{
        const matcher = this.unsafeMatcher(position);
        if (typeof matcher === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getMatcher");
            error.setClassname("Route");
            error.setProperty("matchers");
            error.setPosition(position);
            throw error;
        }
        return matcher;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associate one or multiple [RouteMatcher](RouteMatcher) class instances** from your actual [Route](Route) class instance.
     * @description Allows you to **associate one or multiple [RouteMatcher](RouteMatcher) class instances** from your actual [Route](Route) class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate [Route](Route) class instance** ([RouteMatcher.unlinkRoute](RouteMatcher.unlinkRoute)), and **then associate the  [RouteMatcher](RouteMatcher) class instances** to the [Route](Route) class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [RouteMatcher.linkRoute](RouteMatcher.linkRoute) to associate the [Route](Route) class instance and the [RouteMatcher](RouteMatcher) class instances from the both parts*.
     * @return Returns the actual [RouteMatcher](RouteMatcher) class instance.
     * @param matchers All the given [RouteMatcher](RouteMatcher) class instances that you to associate to your actual [Route](Route) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category associate
     * @public
     * @see Route.areMatchersLinked
     * @see Route.disposeMatchers
     * @see Route.unlinkMatchers
     * @see Route.unsafeMatchers
     * @see Route.getMatchers
     * @see Route.getMatcher
     * @see Route.unsafeMatcher
     * @see Route.getMatchersSize
     * @see Route.matchers
     * @see RouteMatcher.linkRoute
     */
    linkMatchers(... matchers: DeepArray<RouteMatcher>[]): this{
        for (const matcher of flat(matchers)){
            if (this.areMatchersLinked(matcher)){
                continue;
            }


            this.matchers.push(matcher);


            matcher.linkRoute(this);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances**.
     * @description **Gives a copy of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances**.
     * @return **Returns a copy of the [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances**.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category getter
     * @category iterable
     * @public
     * @see Route.areMatchersLinked
     * @see Route.disposeMatchers
     * @see Route.unlinkMatchers
     * @see Route.unsafeMatchers
     * @see Route.linkMatchers
     * @see Route.getMatcher
     * @see Route.unsafeMatcher
     * @see Route.getMatchersSize
     * @see Route.matchers
     */
    getMatchers(): RouteMatcher[]{
        return Array.from(this.unsafeMatchers());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives an [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances without copy**.
     * @description **Gives an [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getMatchers](Route.getMatchers) method.
     * @return **Returns an [array](Route.matchers) of the associated [RouteMatcher](RouteMatcher) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getMatchers](Route.getMatchers) method.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category unsafe
     * @category iterable
     * @public
     * @see Route.areMatchersLinked
     * @see Route.disposeMatchers
     * @see Route.unlinkMatchers
     * @see Route.getMatchers
     * @see Route.linkMatchers
     * @see Route.getMatcher
     * @see Route.unsafeMatcher
     * @see Route.getMatchersSize
     * @see Route.matchers
     */
    unsafeMatchers(): RouteMatcher[]{
        return this.matchers;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate one or multiple [RouteMatcher](RouteMatcher) class instances**.
     * @description Allows you to **dissociate one or multiple [RouteMatcher](RouteMatcher) class instances** from your actual [Route](Route) class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the [Route](Route) class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [RouteMatcher.unlinkRoute](RouteMatcher.unlinkRoute) to dissociate the [Route](Route) class instance and the given [RouteMatcher](RouteMatcher) class instances from the both parts*.
     * @return Returns the actual [RouteMatcher](RouteMatcher) class instance.
     * @param matchers All the given [RouteMatcher](RouteMatcher) class instances that you to dissociate from your actual [Route](Route) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category dissociate
     * @public
     * @see Route.areMatchersLinked
     * @see Route.disposeMatchers
     * @see Route.unsafeMatchers
     * @see Route.getMatchers
     * @see Route.linkMatchers
     * @see Route.getMatcher
     * @see Route.unsafeMatcher
     * @see Route.getMatchersSize
     * @see Route.matchers
     * @see RouteMatcher.unlinkRoute
     */
    unlinkMatchers(... matchers: DeepArray<RouteMatcher>[]): this{
        for (const matcher of flat(matchers)){
            if (!this.areMatchersLinked(matcher)){
                continue;
            }

            while (true){
                const index = this.matchers.indexOf(matcher);
                if (index < 0){
                    break;
                }

                this.matchers.splice(index, 1);
            }

            matcher.unlinkRoute();
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate all the associated [RouteMatcher](RouteMatcher) class instances**.
     * @description **Dissociate all the associated [RouteMatcher](RouteMatcher) class instances**.  *Note that all the associated [RouteMatcher](RouteMatcher) class instances will lose their [Route](Route) class instance association*.
     * @return Returns the actual [RouteMatcher](RouteMatcher) class instance.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category truncate
     * @public
     * @see Route.areMatchersLinked
     * @see Route.unlinkMatchers
     * @see Route.unsafeMatchers
     * @see Route.getMatchers
     * @see Route.linkMatchers
     * @see Route.getMatcher
     * @see Route.unsafeMatcher
     * @see Route.getMatchersSize
     * @see Route.matchers
     */
    disposeMatchers(): this{
        this.unlinkMatchers(this.unsafeMatchers());
        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [RouteMatcher](RouteMatcher) class instances are associated**.
     * @description **Checks if all the given [RouteMatcher](RouteMatcher) class instances are associated** with your actual [Route](Route) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [Route](Route) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [RouteMatcher](RouteMatcher) class instances are associated with your [Route](Route) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @return **Return a boolean if all the given [RouteMatcher](RouteMatcher) class instances are associated** with your actual [Route](Route) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [Route](Route) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [RouteMatcher](RouteMatcher) class instances are associated with your [Route](Route) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @param matchers All the given [RouteMatcher](RouteMatcher) class instances that you want to check their relation *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @RouteMatcher.route
     * @category Relation between Route and RouteMatcher
     * @category relation
     * @category defined
     * @public
     * @see Route.disposeMatchers
     * @see Route.unlinkMatchers
     * @see Route.unsafeMatchers
     * @see Route.getMatchers
     * @see Route.linkMatchers
     * @see Route.getMatcher
     * @see Route.unsafeMatcher
     * @see Route.getMatchersSize
     * @see Route.matchers
     */
    areMatchersLinked(... matchers: DeepArray<RouteMatcher|undefined>[]): boolean{
        return flat(matchers).every((matcher) => {
            if (typeof matcher === "undefined"){
                return false;
            }

            return this.unsafeMatchers().includes(matcher);
        });
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [Project](Project) and the actual [Route](Route)**.
     * @description **Saves the relation between [Project](Project) class instances and the actual [Route](Route) class instance**.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category property
     * @category relation
     * @protected
     * @see Route.linkRoute
     * @see Route.hasRouteLinked
     * @see Route.unsafeRoute
     * @see Route.getRoute
     * @see Route.unlinkRoute
     * @see Route.isLinkedWithRoute
     */
    protected project: Project|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associates a [Project](Project) class instance**.
     * @description Allows you to **associate a [Project](Project) class instance** with your actual [Route](Route) class instance. *This method allows one parameter which is a [Project](Project) class instance that will be associated*. *Note that the method will also run [Project.linkRoutes](Project.linkRoutes) to associate the [Project](Project) class instance et the [Route](Route) class instance from the both parts*. *Note also that the method will also run [unlinkProject](Route.unlinkProject) to dissociate the current [Project](Project) class instance associated*.
     * @return Returns the actual [Route](Route) class instance.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category association
     * @category relation
     * @public
     * @see Route.route
     * @see Route.hasRouteLinked
     * @see Route.unsafeRoute
     * @see Route.getRoute
     * @see Route.unlinkRoute
     * @see Route.isLinkedWithRoute
     * @see Project.linkRoutes
     */
    linkProject(project: Project): this{
        if (this.isLinkedWithProject(project)){
            return this;
        }


        if (!project.areRoutesLinked(this)){
            project.linkRoutes(this);

            return this;
        }


        this.unlinkProject();
        this.project = project;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if a [Project](Project) class instance is associated**.
     * @return **Gives a boolean if a [Project](Project) class instance is associated** or not to our actual [Route](Route) class instance.
     * @description **Check if a [Project](Project) class instance is associated** or not to our actual [Route](Route) class instance.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category defined
     * @public
     * @see Route.route
     * @see Route.linkRoute
     * @see Route.unsafeRoute
     * @see Route.getRoute
     * @see Route.unlinkRoute
     * @see Route.isLinkedWithRoute
     */
    hasProjectLinked(): boolean{
        return typeof this.project !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [Project](Project) class instance**. Unlike the [unsafeProject](Route.unsafeProject) method, **the method can throw an [UnlinkedValue](UnlinkedValue) error**.
     * @description **Gives the associated [Project](Project) class instance** of our actual [Route](Route) class instance. Unlike the [unsafeProject](Routes.unsafeProject) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [Route](Route) class instance doesn't have [Project](Project) class instance associate**.
     * @return **Results the associated [Project](Project) class instance** of our actual [Route](Route) class instance. Unlike the [unsafeProject](Routes.unsafeProject) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [Route](Route) class instance doesn't have [Project](Project) class instance associate**.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category value
     * @category getter
     * @public
     * @see Route.route
     * @see Route.linkRoute
     * @see Route.hasRouteLinked
     * @see Route.unsafeRoute
     * @see Route.unlinkRoute
     * @see Route.isLinkedWithRoute
     */
    getProject(): Project{
        const unsafe = this.unsafeProject();

        if (typeof unsafe === "undefined"){
            const error = new UnlinkedValue();
            error.setMethod("getProject");
            error.setClassname("Route");
            error.setWantedClassname("Project");
            error.setProperty("project");
            throw error;
        }
        return unsafe;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [Project](Project) class instance**. Unlike the [getProject](Route.getProject) method, **the method can result an undefined value.
     * @description **Gives the associated [Project](Project) class instance** of our actual [Route](Route) class instance. Unlike the [getProject](Route.getProject) method, **the method results an undefined value if the [Route](Route) class instance doesn't have [Project](Project) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Results the associated [Project](Project) class instance** of our actual [Route](Route) class instance. Unlike the [getProject](Route.getProject) method, **the method results an undefined value if the [Route](Route) class instance doesn't have [Project](Project) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category value
     * @category unsafe
     * @public
     * @see Route.route
     * @see Route.linkRoute
     * @see Route.hasRouteLinked
     * @see Route.getRoute
     * @see Route.unlinkRoute
     * @see Route.isLinkedWithRoute
     */
    unsafeProject(): Project|undefined{
        return this.project;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate the [Project](Project) class instance**.
     * @description **Dissociate the [Project](Project) class instance** from your actual [Route](Route) class instance.  *Note that the method will also run [Project.unlinkRoutes](Project.unlinkRoutes) to dissociate the [Project](Project) class instance et the [Route](Route) class instance from the both parts*.
     * @return Return the actual [Route](Route) class instance.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category value
     * @category dissociate
     * @public
     * @see Route.route
     * @see Route.linkRoute
     * @see Route.hasRouteLinked
     * @see Route.unsafeRoute
     * @see Route.getRoute
     * @see Route.isLinkedWithRoute
     * @see Project.unlinkRoutes
     */
    unlinkProject(): this{
        const _project = this.project;
        this.project = undefined;

        _project?.unlinkRoutes(this);

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [Project](Project) class instance** are associated.
     * @description **Checks if the [Route](Route) class instance is associated with all the given [Project](Project) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Return a boolean if the [Route](Route) class instance is associated with all the given [Project](Project) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @category @Route.project
     * @category Relation between Project and Route
     * @category relation
     * @category value
     * @category equality
     * @public
     * @see Route.route
     * @see Route.linkRoute
     * @see Route.hasRouteLinked
     * @see Route.unsafeRoute
     * @see Route.getRoute
     * @see Route.unlinkRoute
     */
    isLinkedWithProject(project?: undefined|Project): boolean{
        if (typeof project === "undefined"){
            return false;
        }
        return this.unsafeProject() === project;
    }



    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [ActionConfiguration](ActionConfiguration) and the actual [ActionConfigurationRoute](ActionConfigurationRoute)**.
     * @description **Saves the relation between [ActionConfiguration](ActionConfiguration) class instances and the actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance**.
     * @category @ActionConfigurationRoute.action
     * @category Relation between ActionConfiguration and ActionConfigurationRoute
     * @category property
     * @category relation
     * @protected
     * @see ActionConfigurationRoute.linkRoute
     * @see ActionConfigurationRoute.hasRouteLinked
     * @see ActionConfigurationRoute.unsafeRoute
     * @see ActionConfigurationRoute.getRoute
     * @see ActionConfigurationRoute.unlinkRoute
     * @see ActionConfigurationRoute.isLinkedWithRoute
     */
    protected action: ActionConfiguration|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associates a [ActionConfiguration](ActionConfiguration) class instance**.
     * @description Allows you to **associate a [ActionConfiguration](ActionConfiguration) class instance** with your actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance. *This method allows one parameter which is a [ActionConfiguration](ActionConfiguration) class instance that will be associated*. *Note that the method will also run [ActionConfiguration.linkRoutes](ActionConfiguration.linkRoutes) to associate the [ActionConfiguration](ActionConfiguration) class instance et the [ActionConfigurationRoute](ActionConfigurationRoute) class instance from the both parts*. *Note also that the method will also run [unlinkAction](ActionConfigurationRoute.unlinkAction) to dissociate the current [ActionConfiguration](ActionConfiguration) class instance associated*.
     * @return Returns the actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance.
     * @category @ActionConfigurationRoute.action
     * @category Relation between ActionConfiguration and ActionConfigurationRoute
     * @category association
     * @category relation
     * @public
     * @see ActionConfigurationRoute.route
     * @see ActionConfigurationRoute.hasRouteLinked
     * @see ActionConfigurationRoute.unsafeRoute
     * @see ActionConfigurationRoute.getRoute
     * @see ActionConfigurationRoute.unlinkRoute
     * @see ActionConfigurationRoute.isLinkedWithRoute
     * @see ActionConfiguration.linkRoutes
     */
    linkAction(action: ActionConfiguration): this{
        if (this.isLinkedWithAction(action)){
            return this;
        }


        if (!action.areRoutesLinked(this)){
            action.linkRoutes(this);

            return this;
        }


        this.unlinkAction();
        this.action = action;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if a [ActionConfiguration](ActionConfiguration) class instance is associated**.
     * @return **Gives a boolean if a [ActionConfiguration](ActionConfiguration) class instance is associated** or not to our actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance.
     * @description **Check if a [ActionConfiguration](ActionConfiguration) class instance is associated** or not to our actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance.
     * @category @ActionConfigurationRoute.action
     * @category Relation between ActionConfiguration and ActionConfigurationRoute
     * @category relation
     * @category defined
     * @public
     * @see ActionConfigurationRoute.route
     * @see ActionConfigurationRoute.linkRoute
     * @see ActionConfigurationRoute.unsafeRoute
     * @see ActionConfigurationRoute.getRoute
     * @see ActionConfigurationRoute.unlinkRoute
     * @see ActionConfigurationRoute.isLinkedWithRoute
     */
    hasActionLinked(): boolean{
        return typeof this.action !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [ActionConfiguration](ActionConfiguration) class instance**. Unlike the [unsafeAction](ActionConfigurationRoute.unsafeAction) method, **the method can throw an [UnlinkedValue](UnlinkedValue) error**.
     * @description **Gives the associated [ActionConfiguration](ActionConfiguration) class instance** of our actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance. Unlike the [unsafeAction](ActionConfigurationsRoutes.unsafeAction) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [ActionConfigurationRoute](ActionConfigurationRoute) class instance doesn't have [ActionConfiguration](ActionConfiguration) class instance associate**.
     * @return **Results the associated [ActionConfiguration](ActionConfiguration) class instance** of our actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance. Unlike the [unsafeAction](ActionConfigurationsRoutes.unsafeAction) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [ActionConfigurationRoute](ActionConfigurationRoute) class instance doesn't have [ActionConfiguration](ActionConfiguration) class instance associate**.
     * @category @ActionConfigurationRoute.action
     * @category Relation between ActionConfiguration and ActionConfigurationRoute
     * @category relation
     * @category value
     * @category getter
     * @public
     * @see ActionConfigurationRoute.route
     * @see ActionConfigurationRoute.linkRoute
     * @see ActionConfigurationRoute.hasRouteLinked
     * @see ActionConfigurationRoute.unsafeRoute
     * @see ActionConfigurationRoute.unlinkRoute
     * @see ActionConfigurationRoute.isLinkedWithRoute
     */
    getAction(): ActionConfiguration{
        const unsafe = this.unsafeAction();

        if (typeof unsafe === "undefined"){
            const error = new UnlinkedValue();
            error.setMethod("getAction");
            error.setClassname("ActionConfigurationRoute");
            error.setWantedClassname("ActionConfiguration");
            error.setProperty("action");
            throw error;
        }
        return unsafe;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [ActionConfiguration](ActionConfiguration) class instance**. Unlike the [getAction](ActionConfigurationRoute.getAction) method, **the method can result an undefined value.
     * @description **Gives the associated [ActionConfiguration](ActionConfiguration) class instance** of our actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance. Unlike the [getAction](ActionConfigurationRoute.getAction) method, **the method results an undefined value if the [ActionConfigurationRoute](ActionConfigurationRoute) class instance doesn't have [ActionConfiguration](ActionConfiguration) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Results the associated [ActionConfiguration](ActionConfiguration) class instance** of our actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance. Unlike the [getAction](ActionConfigurationRoute.getAction) method, **the method results an undefined value if the [ActionConfigurationRoute](ActionConfigurationRoute) class instance doesn't have [ActionConfiguration](ActionConfiguration) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @ActionConfigurationRoute.action
     * @category Relation between ActionConfiguration and ActionConfigurationRoute
     * @category relation
     * @category value
     * @category unsafe
     * @public
     * @see ActionConfigurationRoute.route
     * @see ActionConfigurationRoute.linkRoute
     * @see ActionConfigurationRoute.hasRouteLinked
     * @see ActionConfigurationRoute.getRoute
     * @see ActionConfigurationRoute.unlinkRoute
     * @see ActionConfigurationRoute.isLinkedWithRoute
     */
    unsafeAction(): ActionConfiguration|undefined{
        return this.action;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate the [ActionConfiguration](ActionConfiguration) class instance**.
     * @description **Dissociate the [ActionConfiguration](ActionConfiguration) class instance** from your actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance.  *Note that the method will also run [ActionConfiguration.unlinkRoutes](ActionConfiguration.unlinkRoutes) to dissociate the [ActionConfiguration](ActionConfiguration) class instance et the [ActionConfigurationRoute](ActionConfigurationRoute) class instance from the both parts*.
     * @return Return the actual [ActionConfigurationRoute](ActionConfigurationRoute) class instance.
     * @category @ActionConfigurationRoute.action
     * @category Relation between ActionConfiguration and ActionConfigurationRoute
     * @category relation
     * @category value
     * @category dissociate
     * @public
     * @see ActionConfigurationRoute.route
     * @see ActionConfigurationRoute.linkRoute
     * @see ActionConfigurationRoute.hasRouteLinked
     * @see ActionConfigurationRoute.unsafeRoute
     * @see ActionConfigurationRoute.getRoute
     * @see ActionConfigurationRoute.isLinkedWithRoute
     * @see ActionConfiguration.unlinkRoutes
     */
    unlinkAction(): this{
        const _action = this.action;
        this.action = undefined;

        _action?.unlinkRoutes(this);

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [ActionConfiguration](ActionConfiguration) class instance** are associated.
     * @description **Checks if the [ActionConfigurationRoute](ActionConfigurationRoute) class instance is associated with all the given [ActionConfiguration](ActionConfiguration) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Return a boolean if the [ActionConfigurationRoute](ActionConfigurationRoute) class instance is associated with all the given [ActionConfiguration](ActionConfiguration) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @category @ActionConfigurationRoute.action
     * @category Relation between ActionConfiguration and ActionConfigurationRoute
     * @category relation
     * @category value
     * @category equality
     * @public
     * @see ActionConfigurationRoute.route
     * @see ActionConfigurationRoute.linkRoute
     * @see ActionConfigurationRoute.hasRouteLinked
     * @see ActionConfigurationRoute.unsafeRoute
     * @see ActionConfigurationRoute.getRoute
     * @see ActionConfigurationRoute.unlinkRoute
     */
    isLinkedWithAction(action?: undefined|ActionConfiguration): boolean{
        if (typeof action === "undefined"){
            return false;
        }
        return this.unsafeAction() === action;
    }
}

export default Route;