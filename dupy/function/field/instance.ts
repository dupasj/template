import DupyObject from "../../dupy-object";

class FieldFunctionInstance extends DupyObject{
    configuration: FieldFunctionInstance;
    constructor(configuration: FieldFunctionInstance) {
        super();

        this.configuration = configuration;
    }
    getConfiguration(): FieldFunctionInstance {
        return this.configuration;
    }
    isConfigurationEqualTo(configuration?: undefined|FieldFunctionInstance): boolean{
        if (typeof configuration === "undefined"){
            return false;
        }
        return this.getConfiguration() === configuration;
    }
}


export default FieldFunctionInstance;
