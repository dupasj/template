import FieldInstance from "../../model/field/field/instance";

type FieldFunctionState<State> = {
    field: FieldInstance,
    state: State|undefined,
    getState(): State
}

export default FieldFunctionState;
