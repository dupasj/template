import DupyError from "./index";

class ActionError extends DupyError{
    isActionError(): this is ActionError{
        return true
    }
}

export default ActionError;
