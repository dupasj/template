import UndefinedValue from "./internal/undefined-value";
import BaseDupyError from "./base";
import DeepArray from "../util/deep-array/type";

class DupyError extends BaseDupyError{
    isError(): this is DupyError {
        return true;
    }

    getMessage(language: string|string[]): string {
        return DupyError.getMessage(
            DupyError.getTemplate(this.getLanguageKey(),language),
            Object.fromEntries(this.getParameters()),
            language
        )
    }

    getTemplate(language_key: string|string[]): any{
        return DupyError.getTemplate(this.getLanguageKey(),language_key)
    }

    constructor(language_key?: string,http_status?: number) {
        super();

        if (language_key){
            this.setLanguageKey(language_key);
        }

        if (http_status) {
            this.setHttpStatus(http_status);
        }
    }

    /**
     * @description
     * @protected
     * @return
     * @see setDefaultParameter
     * @see hasDefaultParameter
     * @see getDefaultParameter
     * @see removeDefaultParameter
     * @see getDefaultParameters
     * @see getDefaultParameterValues
     * @see getDefaultParameterKeys
     */
    protected default_parameter: {[key: string]: string } = {};

    /**
     * @description
     * @return The current {@link DupyError } instance
     * @param key
     * @param value
     * @see default_parameter
     * @see hasDefaultParameter
     * @see getDefaultParameter
     * @see removeDefaultParameter
     * @see getDefaultParameters
     * @see getDefaultParameterValues
     * @see getDefaultParameterKeys
     */
    setDefaultParameter(key: {[key: string]: string}|string, value?: string): this {
        if (typeof key !== "string"){
            if (typeof value !== "undefined"){
                throw new Error("Unexpected the value argument if the given value is a map or an object");
            }

            for (const _key in key){
                this.setDefaultParameter(_key, key[_key]);
            }

            return this;
        }

        if (typeof value === "undefined"){
            throw new Error("Expected a value argument value if the given value is a string");
        }

        this.removeDefaultParameter(key);
        this.default_parameter[key] = value;

        return this;
    }

    /**
     * @description
     * @return
     * @param key
     * @see default_parameter
     * @see setDefaultParameter
     * @see getDefaultParameter
     * @see removeDefaultParameter
     * @see getDefaultParameters
     * @see getDefaultParameterValues
     * @see getDefaultParameterKeys
     */
    hasDefaultParameter(... key: DeepArray<string>[]): boolean{
        return key.every((item) => {
            if (Array.isArray(item)){
                return this.hasDefaultParameter(... item);
            }

            return item in this.default_parameter;
        });
    }

    /**
     * @description
     * @return
     * @param key
     * @see default_parameter
     * @see setDefaultParameter
     * @see hasDefaultParameter
     * @see removeDefaultParameter
     * @see getDefaultParameters
     * @see getDefaultParameterValues
     * @see getDefaultParameterKeys
     */
    getDefaultParameter(key: string): string{
        if (!(key in this.default_parameter)){
            throw new Error(`Your given key "${key}" is not defined into the \`default_parameter\`'s ${this.constructor.name} instance`);
        }

        return this.default_parameter[key]!;
    }

    unsafeDefaultParameter(key: string): string|undefined{
        return this.default_parameter[key]!;
    }

    /**
     * @description
     * @return The current {@link DupyError } instance
     * @param keys
     * @see default_parameter
     * @see setDefaultParameter
     * @see hasDefaultParameter
     * @see getDefaultParameter
     * @see getDefaultParameters
     * @see getDefaultParameterValues
     * @see getDefaultParameterKeys
     */
    removeDefaultParameter(... keys: DeepArray<string>[]): this {
        for (const key of keys){
            if (Array.isArray(key)){
                this.removeDefaultParameter(... key);
                continue;
            }

            delete this.default_parameter[key];
        }

        return this;
    }

    /**
     * @description
     * @return
     * @see setDefaultParameter
     * @see hasDefaultParameter
     * @see getDefaultParameter
     * @see removeDefaultParameter
     * @see getDefaultParameters
     * @see getDefaultParameterValues
     * @see default_parameter
     */
    getDefaultParameterKeys(): string[]{
        return Object.keys(this.default_parameter);
    }

    /**
     * @description
     * @return
     * @see setDefaultParameter
     * @see hasDefaultParameter
     * @see getDefaultParameter
     * @see removeDefaultParameter
     * @see getDefaultParameters
     * @see getDefaultParameterKeys
     * @see default_parameter
     */
    getDefaultParameterValues(): string[]{
        return Object.values(this.default_parameter);
    }

    /**
     * @description
     * @return
     * @see setDefaultParameter
     * @see hasDefaultParameter
     * @see getDefaultParameter
     * @see removeDefaultParameter
     * @see default_parameter
     * @see getDefaultParameterValues
     * @see getDefaultParameterKeys
     */
    getDefaultParameters(): [string, string][]{
        return Object.entries(this.default_parameter);
    }

    /**
     * @description
     * @protected
     * @return
     * @see setParameter
     * @see hasParameter
     * @see getParameter
     * @see removeParameter
     * @see getParameters
     * @see getParameterValues
     * @see getParameterKeys
     */
    protected parameter: {[key: string]: string } = {};

    /**
     * @description
     * @return The current {@link DupyError } instance
     * @param key
     * @param value
     * @see parameter
     * @see hasParameter
     * @see getParameter
     * @see removeParameter
     * @see getParameters
     * @see getParameterValues
     * @see getParameterKeys
     */
    setParameter(key: {[key: string]: string}|string, value?: string): DupyError{
        if (typeof key !== "string"){
            if (typeof value !== "undefined"){
                throw new Error("Unexpected the value argument if the given value is a map or an object");
            }

            for (const _key in key){
                this.setParameter(_key, key[_key]);
            }

            return this;
        }

        if (typeof value === "undefined"){
            throw new Error("Expected a value argument value if the given value is a string");
        }

        this.removeParameter(key);
        this.parameter[key] = value;

        return this;
    }

    /**
     * @description
     * @return
     * @param key
     * @see parameter
     * @see setParameter
     * @see getParameter
     * @see removeParameter
     * @see getParameters
     * @see getParameterValues
     * @see getParameterKeys
     */
    hasParameter(... key: DeepArray<string>[]): boolean{
        return key.every((item) => {
            if (Array.isArray(item)){
                return this.hasParameter(... item);
            }

            return item in this.parameter;
        }) || this.hasDefaultParameter(key);
    }

    /**
     * @description
     * @return
     * @param key
     * @see parameter
     * @see setParameter
     * @see hasParameter
     * @see removeParameter
     * @see getParameters
     * @see getParameterValues
     * @see getParameterKeys
     */
    getParameter(key: string): string{
        if (!(key in this.parameter)){
            return this.getDefaultParameter(key)
        }

        return this.parameter[key]!;
    }

    unsafeParameter(key: string): string|undefined{
        return this.parameter[key] ?? this.unsafeDefaultParameter(key);
    }

    /**
     * @description
     * @return The current {@link DupyError } instance
     * @param keys
     * @see parameter
     * @see setParameter
     * @see hasParameter
     * @see getParameter
     * @see getParameters
     * @see getParameterValues
     * @see getParameterKeys
     */
    removeParameter(... keys: DeepArray<string>[]): DupyError{
        for (const key of keys){
            if (Array.isArray(key)){
                this.removeParameter(... key);
                continue;
            }

            delete this.parameter[key];
        }

        return this;
    }

    /**
     * @description
     * @return
     * @see setParameter
     * @see hasParameter
     * @see getParameter
     * @see removeParameter
     * @see getParameters
     * @see getParameterValues
     * @see parameter
     */
    getParameterKeys(): string[]{
        const results = Object.keys(this.parameter);

        for(const key in this.getDefaultParameterKeys()){
            if (!results.includes(key)){
                results.push(key);
            }
        }

        return results;
    }

    /**
     * @description
     * @return
     * @see setParameter
     * @see hasParameter
     * @see getParameter
     * @see removeParameter
     * @see getParameters
     * @see getParameterKeys
     * @see parameter
     */
    getParameterValues(): string[]{
        const keys = this.getParameterKeys();

        return keys.map(key => this.getParameter(key));
    }

    /**
     * @description
     * @return
     * @see setParameter
     * @see hasParameter
     * @see getParameter
     * @see removeParameter
     * @see parameter
     * @see getParameterValues
     * @see getParameterKeys
     */
    getParameters(): [string, string][]{
        return Object.entries({
            ... this.default_parameter,
            ... this.parameter,
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @DupyError.language_key.
     * @description Save @DupyError.language_key.
     * @category @DupyError.language_key
     * @category property
     * @category field
     * @protected
     * @see DupyError.setLanguageKey
     * @see DupyError.getLanguageKey
     * @see DupyError.unsafeLanguageKey
     * @see DupyError.eraseLanguageKey
     * @see DupyError.isLanguageKeyDefined
     * @see DupyError.isLanguageKeyEqualTo
     */
    protected language_key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @DupyError.language_key**.
     * @description Allows you to update or **assign the value of @DupyError.language_key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [DupyError](DupyError) class instance.
     * @param language_key The new value to assign to @DupyError.language_key.
     * @category @DupyError.language_key
     * @category setter
     * @category field
     * @public
     * @see DupyError.language_key
     * @see DupyError.getLanguageKey
     * @see DupyError.unsafeLanguageKey
     * @see DupyError.eraseLanguageKey
     * @see DupyError.isLanguageKeyDefined
     * @see DupyError.isLanguageKeyEqualTo
     */
    setLanguageKey(language_key: string): this{
        if (this.isLanguageKeyEqualTo(language_key)){
            return this;
        }

        this.eraseLanguageKey();
        this.language_key = language_key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @DupyError.language_key**. Unlike the [getLanguageKey](DupyError.getLanguageKey) method, **the method results an undefined value if @DupyError.language_key is not defined**.
     * @description **Gives the value of @DupyError.language_key**. Unlike the [getLanguageKey](DupyError.getLanguageKey) method, **the method results an undefined value if @DupyError.language_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @DupyError.language_key**. Unlike the [getLanguageKey](DupyError.getLanguageKey) method, **the method results an undefined value if @DupyError.language_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @DupyError.language_key
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see DupyError.language_key
     * @see DupyError.setLanguageKey
     * @see DupyError.getLanguageKey
     * @see DupyError.eraseLanguageKey
     * @see DupyError.isLanguageKeyDefined
     * @see DupyError.isLanguageKeyEqualTo
     */
    unsafeLanguageKey(): string|undefined{
        return this.language_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @DupyError.language_key**. Unlike the [unsafeLanguageKey](DupyError.unsafeLanguageKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @DupyError.language_key is not defined**.
     * @description **Gives the value of @DupyError.language_key**. Unlike the [unsafeLanguageKey](DupyError.unsafeLanguageKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @DupyError.language_key is not defined**.
     * @return **Returns the value of @DupyError.language_key**. Unlike the [unsafeLanguageKey](DupyError.unsafeLanguageKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @DupyError.language_key is not defined**.
     * @category @DupyError.language_key
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @DupyError.language_key isn't defined.
     * @see DupyError.language_key
     * @see DupyError.setLanguageKey
     * @see DupyError.unsafeLanguageKey
     * @see DupyError.eraseLanguageKey
     * @see DupyError.isLanguageKeyDefined
     * @see DupyError.isLanguageKeyEqualTo
     */
    getLanguageKey(): string{
        const language_key = this.unsafeLanguageKey();

        if (typeof language_key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getLanguageKey");
            error.setClassname("DupyError");
            error.setProperty("language_key");
            throw error;
        }
        return language_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @DupyError.language_key is defined**.
     * @description **Checks if @DupyError.language_key is defined** or not.
     * @return **Returns a boolean if @DupyError.language_key is defined** or not.
     * @category @DupyError.language_key
     * @category defined
     * @category field
     * @public
     * @see DupyError.language_key
     * @see DupyError.setLanguageKey
     * @see DupyError.getLanguageKey
     * @see DupyError.unsafeLanguageKey
     * @see DupyError.eraseLanguageKey
     * @see DupyError.isLanguageKeyEqualTo
     */
    isLanguageKeyDefined(): boolean{
        return typeof this.language_key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @DupyError.language_key is equal to your given value**.
     * @description **Checks if @DupyError.language_key is equal to your given value** and if @DupyError.language_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @DupyError.language_key is equal to your given value** and if @DupyError.language_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param language_key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @DupyError.language_key
     * @category equality
     * @category field
     * @public
     * @see DupyError.language_key
     * @see DupyError.setLanguageKey
     * @see DupyError.getLanguageKey
     * @see DupyError.unsafeLanguageKey
     * @see DupyError.eraseLanguageKey
     * @see DupyError.isLanguageKeyDefined
     */
    isLanguageKeyEqualTo(language_key?: undefined|string): boolean{
        if (typeof language_key === "undefined"){
            return false;
        }
        return this.unsafeLanguageKey() === language_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @DupyError.language_key**.
     * @description **Removes the assigned the value of @DupyError.language_key**: *@DupyError.language_key will be flagged as undefined*.
     * @return Return the actual [DupyError](DupyError) class instance.
     * @category @DupyError.language_key
     * @category truncate
     * @category field
     * @public
     * @see DupyError.language_key
     * @see DupyError.setLanguageKey
     * @see DupyError.getLanguageKey
     * @see DupyError.unsafeLanguageKey
     * @see DupyError.isLanguageKeyDefined
     * @see DupyError.isLanguageKeyEqualTo
     */
    eraseLanguageKey(): this{
        this.language_key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @DupyError.http_status.
     * @description Save @DupyError.http_status.
     * @category @DupyError.http_status
     * @category property
     * @category field
     * @protected
     * @see DupyError.setHttpStatus
     * @see DupyError.getHttpStatus
     * @see DupyError.unsafeHttpStatus
     * @see DupyError.eraseHttpStatus
     * @see DupyError.isHttpStatusDefined
     * @see DupyError.isHttpStatusEqualTo
     */
    protected http_status: number|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @DupyError.http_status**.
     * @description Allows you to update or **assign the value of @DupyError.http_status**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [DupyError](DupyError) class instance.
     * @param http_status The new value to assign to @DupyError.http_status.
     * @category @DupyError.http_status
     * @category setter
     * @category field
     * @public
     * @see DupyError.http_status
     * @see DupyError.getHttpStatus
     * @see DupyError.unsafeHttpStatus
     * @see DupyError.eraseHttpStatus
     * @see DupyError.isHttpStatusDefined
     * @see DupyError.isHttpStatusEqualTo
     */
    setHttpStatus(http_status: number): this{
        if (this.isHttpStatusEqualTo(http_status)){
            return this;
        }

        this.eraseHttpStatus();
        this.http_status = http_status;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @DupyError.http_status**. Unlike the [getHttpStatus](DupyError.getHttpStatus) method, **the method results an undefined value if @DupyError.http_status is not defined**.
     * @description **Gives the value of @DupyError.http_status**. Unlike the [getHttpStatus](DupyError.getHttpStatus) method, **the method results an undefined value if @DupyError.http_status is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @DupyError.http_status**. Unlike the [getHttpStatus](DupyError.getHttpStatus) method, **the method results an undefined value if @DupyError.http_status is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @DupyError.http_status
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see DupyError.http_status
     * @see DupyError.setHttpStatus
     * @see DupyError.getHttpStatus
     * @see DupyError.eraseHttpStatus
     * @see DupyError.isHttpStatusDefined
     * @see DupyError.isHttpStatusEqualTo
     */
    unsafeHttpStatus(): number|undefined{
        return this.http_status;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @DupyError.http_status**. Unlike the [unsafeHttpStatus](DupyError.unsafeHttpStatus) method, **we will throw an [UndefinedValue](UndefinedValue) error if @DupyError.http_status is not defined**.
     * @description **Gives the value of @DupyError.http_status**. Unlike the [unsafeHttpStatus](DupyError.unsafeHttpStatus) method, **we will throw an [UndefinedValue](UndefinedValue) error if @DupyError.http_status is not defined**.
     * @return **Returns the value of @DupyError.http_status**. Unlike the [unsafeHttpStatus](DupyError.unsafeHttpStatus) method, **we will throw an [UndefinedValue](UndefinedValue) error if @DupyError.http_status is not defined**.
     * @category @DupyError.http_status
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @DupyError.http_status isn't defined.
     * @see DupyError.http_status
     * @see DupyError.setHttpStatus
     * @see DupyError.unsafeHttpStatus
     * @see DupyError.eraseHttpStatus
     * @see DupyError.isHttpStatusDefined
     * @see DupyError.isHttpStatusEqualTo
     */
    getHttpStatus(): number{
        const http_status = this.unsafeHttpStatus();

        if (typeof http_status === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getHttpStatus");
            error.setClassname("DupyError");
            error.setProperty("http_status");
            throw error;
        }
        return http_status;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @DupyError.http_status is defined**.
     * @description **Checks if @DupyError.http_status is defined** or not.
     * @return **Returns a boolean if @DupyError.http_status is defined** or not.
     * @category @DupyError.http_status
     * @category defined
     * @category field
     * @public
     * @see DupyError.http_status
     * @see DupyError.setHttpStatus
     * @see DupyError.getHttpStatus
     * @see DupyError.unsafeHttpStatus
     * @see DupyError.eraseHttpStatus
     * @see DupyError.isHttpStatusEqualTo
     */
    isHttpStatusDefined(): boolean{
        return typeof this.http_status !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @DupyError.http_status is equal to your given value**.
     * @description **Checks if @DupyError.http_status is equal to your given value** and if @DupyError.http_status is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @DupyError.http_status is equal to your given value** and if @DupyError.http_status is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param http_status **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @DupyError.http_status
     * @category equality
     * @category field
     * @public
     * @see DupyError.http_status
     * @see DupyError.setHttpStatus
     * @see DupyError.getHttpStatus
     * @see DupyError.unsafeHttpStatus
     * @see DupyError.eraseHttpStatus
     * @see DupyError.isHttpStatusDefined
     */
    isHttpStatusEqualTo(http_status?: undefined|number): boolean{
        if (typeof http_status === "undefined"){
            return false;
        }
        return this.unsafeHttpStatus() === http_status;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @DupyError.http_status**.
     * @description **Removes the assigned the value of @DupyError.http_status**: *@DupyError.http_status will be flagged as undefined*.
     * @return Return the actual [DupyError](DupyError) class instance.
     * @category @DupyError.http_status
     * @category truncate
     * @category field
     * @public
     * @see DupyError.http_status
     * @see DupyError.setHttpStatus
     * @see DupyError.getHttpStatus
     * @see DupyError.unsafeHttpStatus
     * @see DupyError.isHttpStatusDefined
     * @see DupyError.isHttpStatusEqualTo
     */
    eraseHttpStatus(): this{
        this.http_status = undefined;

        return this;
    }
}

export default DupyError;