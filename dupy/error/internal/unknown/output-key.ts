import ModelError from "../../model";
import UndefinedValue from "../undefined-value";
import Translate from "../../../translate";
class UnknownOutputKey extends ModelError{
    constructor(key = "model.unknown.key.output",http_code = 500) {
        super(key,http_code);
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UnknownOutputKey.given.
     * @description Save @UnknownOutputKey.given.
     * @category @UnknownOutputKey.given
     * @category property
     * @category field
     * @protected
     * @see UnknownOutputKey.setGiven
     * @see UnknownOutputKey.getGiven
     * @see UnknownOutputKey.unsafeGiven
     * @see UnknownOutputKey.eraseGiven
     * @see UnknownOutputKey.isGivenDefined
     * @see UnknownOutputKey.isGivenEqualTo
     */
    protected given: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UnknownOutputKey.given**.
     * @description Allows you to update or **assign the value of @UnknownOutputKey.given**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UnknownOutputKey](UnknownOutputKey) class instance.
     * @param given The new value to assign to @UnknownOutputKey.given.
     * @category @UnknownOutputKey.given
     * @category setter
     * @category field
     * @public
     * @see UnknownOutputKey.given
     * @see UnknownOutputKey.getGiven
     * @see UnknownOutputKey.unsafeGiven
     * @see UnknownOutputKey.eraseGiven
     * @see UnknownOutputKey.isGivenDefined
     * @see UnknownOutputKey.isGivenEqualTo
     */
    setGiven(given: string): this{
        if (this.isGivenEqualTo(given)){
            return this;
        }

        this.setDefaultParameter("given",given)

        this.eraseGiven();
        this.given = given;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownOutputKey.given**. Unlike the [getGiven](UnknownOutputKey.getGiven) method, **the method results an undefined value if @UnknownOutputKey.given is not defined**.
     * @description **Gives the value of @UnknownOutputKey.given**. Unlike the [getGiven](UnknownOutputKey.getGiven) method, **the method results an undefined value if @UnknownOutputKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UnknownOutputKey.given**. Unlike the [getGiven](UnknownOutputKey.getGiven) method, **the method results an undefined value if @UnknownOutputKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UnknownOutputKey.given
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UnknownOutputKey.given
     * @see UnknownOutputKey.setGiven
     * @see UnknownOutputKey.getGiven
     * @see UnknownOutputKey.eraseGiven
     * @see UnknownOutputKey.isGivenDefined
     * @see UnknownOutputKey.isGivenEqualTo
     */
    unsafeGiven(): string|undefined{
        return this.given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownOutputKey.given**. Unlike the [unsafeGiven](UnknownOutputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownOutputKey.given is not defined**.
     * @description **Gives the value of @UnknownOutputKey.given**. Unlike the [unsafeGiven](UnknownOutputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownOutputKey.given is not defined**.
     * @return **Returns the value of @UnknownOutputKey.given**. Unlike the [unsafeGiven](UnknownOutputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownOutputKey.given is not defined**.
     * @category @UnknownOutputKey.given
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UnknownOutputKey.given isn't defined.
     * @see UnknownOutputKey.given
     * @see UnknownOutputKey.setGiven
     * @see UnknownOutputKey.unsafeGiven
     * @see UnknownOutputKey.eraseGiven
     * @see UnknownOutputKey.isGivenDefined
     * @see UnknownOutputKey.isGivenEqualTo
     */
    getGiven(): string{
        const given = this.unsafeGiven();

        if (typeof given === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getGiven");
            error.setClassname("UnknownOutputKey");
            error.setProperty("given");
            throw error;
        }
        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownOutputKey.given is defined**.
     * @description **Checks if @UnknownOutputKey.given is defined** or not.
     * @return **Returns a boolean if @UnknownOutputKey.given is defined** or not.
     * @category @UnknownOutputKey.given
     * @category defined
     * @category field
     * @public
     * @see UnknownOutputKey.given
     * @see UnknownOutputKey.setGiven
     * @see UnknownOutputKey.getGiven
     * @see UnknownOutputKey.unsafeGiven
     * @see UnknownOutputKey.eraseGiven
     * @see UnknownOutputKey.isGivenEqualTo
     */
    isGivenDefined(): boolean{
        return typeof this.given !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownOutputKey.given is equal to your given value**.
     * @description **Checks if @UnknownOutputKey.given is equal to your given value** and if @UnknownOutputKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UnknownOutputKey.given is equal to your given value** and if @UnknownOutputKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param given **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UnknownOutputKey.given
     * @category equality
     * @category field
     * @public
     * @see UnknownOutputKey.given
     * @see UnknownOutputKey.setGiven
     * @see UnknownOutputKey.getGiven
     * @see UnknownOutputKey.unsafeGiven
     * @see UnknownOutputKey.eraseGiven
     * @see UnknownOutputKey.isGivenDefined
     */
    isGivenEqualTo(given?: undefined|string): boolean{
        if (typeof given === "undefined"){
            return false;
        }
        return this.unsafeGiven() === given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UnknownOutputKey.given**.
     * @description **Removes the assigned the value of @UnknownOutputKey.given**: *@UnknownOutputKey.given will be flagged as undefined*.
     * @return Return the actual [UnknownOutputKey](UnknownOutputKey) class instance.
     * @category @UnknownOutputKey.given
     * @category truncate
     * @category field
     * @public
     * @see UnknownOutputKey.given
     * @see UnknownOutputKey.setGiven
     * @see UnknownOutputKey.getGiven
     * @see UnknownOutputKey.unsafeGiven
     * @see UnknownOutputKey.isGivenDefined
     * @see UnknownOutputKey.isGivenEqualTo
     */
    eraseGiven(): this{
        this.given = undefined;
        this.removeDefaultParameter("given")

        return this;
    }
}

Translate.setTranslate({
    'fr.model.unknown.output': 'votre clef `#given` de #model n\'est pas une valeur qui peut être visible',
    'en.model.unknown.output': 'your `#given` key of #model is not a value that can be seen',
})

export default UnknownOutputKey;