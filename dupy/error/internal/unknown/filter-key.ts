import ModelError from "../../model";
import UndefinedValue from "../undefined-value";
import Translate from "../../../translate";

class UnknownFilterKey extends ModelError{
    constructor(key = "model.unknown.key.filter",http_code = 417) {
        super(key,http_code);
    }





    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UnknownFilterKey.given.
     * @description Save @UnknownFilterKey.given.
     * @category @UnknownFilterKey.given
     * @category property
     * @category field
     * @protected
     * @see UnknownFilterKey.setGiven
     * @see UnknownFilterKey.getGiven
     * @see UnknownFilterKey.unsafeGiven
     * @see UnknownFilterKey.eraseGiven
     * @see UnknownFilterKey.isGivenDefined
     * @see UnknownFilterKey.isGivenEqualTo
     */
    protected given: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UnknownFilterKey.given**.
     * @description Allows you to update or **assign the value of @UnknownFilterKey.given**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UnknownFilterKey](UnknownFilterKey) class instance.
     * @param given The new value to assign to @UnknownFilterKey.given.
     * @category @UnknownFilterKey.given
     * @category setter
     * @category field
     * @public
     * @see UnknownFilterKey.given
     * @see UnknownFilterKey.getGiven
     * @see UnknownFilterKey.unsafeGiven
     * @see UnknownFilterKey.eraseGiven
     * @see UnknownFilterKey.isGivenDefined
     * @see UnknownFilterKey.isGivenEqualTo
     */
    setGiven(given: string): this{
        if (this.isGivenEqualTo(given)){
            return this;
        }

        this.setDefaultParameter("given",given)

        this.eraseGiven();
        this.given = given;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownFilterKey.given**. Unlike the [getGiven](UnknownFilterKey.getGiven) method, **the method results an undefined value if @UnknownFilterKey.given is not defined**.
     * @description **Gives the value of @UnknownFilterKey.given**. Unlike the [getGiven](UnknownFilterKey.getGiven) method, **the method results an undefined value if @UnknownFilterKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UnknownFilterKey.given**. Unlike the [getGiven](UnknownFilterKey.getGiven) method, **the method results an undefined value if @UnknownFilterKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UnknownFilterKey.given
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UnknownFilterKey.given
     * @see UnknownFilterKey.setGiven
     * @see UnknownFilterKey.getGiven
     * @see UnknownFilterKey.eraseGiven
     * @see UnknownFilterKey.isGivenDefined
     * @see UnknownFilterKey.isGivenEqualTo
     */
    unsafeGiven(): string|undefined{
        return this.given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownFilterKey.given**. Unlike the [unsafeGiven](UnknownFilterKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownFilterKey.given is not defined**.
     * @description **Gives the value of @UnknownFilterKey.given**. Unlike the [unsafeGiven](UnknownFilterKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownFilterKey.given is not defined**.
     * @return **Returns the value of @UnknownFilterKey.given**. Unlike the [unsafeGiven](UnknownFilterKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownFilterKey.given is not defined**.
     * @category @UnknownFilterKey.given
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UnknownFilterKey.given isn't defined.
     * @see UnknownFilterKey.given
     * @see UnknownFilterKey.setGiven
     * @see UnknownFilterKey.unsafeGiven
     * @see UnknownFilterKey.eraseGiven
     * @see UnknownFilterKey.isGivenDefined
     * @see UnknownFilterKey.isGivenEqualTo
     */
    getGiven(): string{
        const given = this.unsafeGiven();

        if (typeof given === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getGiven");
            error.setClassname("UnknownFilterKey");
            error.setProperty("given");
            throw error;
        }
        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownFilterKey.given is defined**.
     * @description **Checks if @UnknownFilterKey.given is defined** or not.
     * @return **Returns a boolean if @UnknownFilterKey.given is defined** or not.
     * @category @UnknownFilterKey.given
     * @category defined
     * @category field
     * @public
     * @see UnknownFilterKey.given
     * @see UnknownFilterKey.setGiven
     * @see UnknownFilterKey.getGiven
     * @see UnknownFilterKey.unsafeGiven
     * @see UnknownFilterKey.eraseGiven
     * @see UnknownFilterKey.isGivenEqualTo
     */
    isGivenDefined(): boolean{
        return typeof this.given !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownFilterKey.given is equal to your given value**.
     * @description **Checks if @UnknownFilterKey.given is equal to your given value** and if @UnknownFilterKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UnknownFilterKey.given is equal to your given value** and if @UnknownFilterKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param given **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UnknownFilterKey.given
     * @category equality
     * @category field
     * @public
     * @see UnknownFilterKey.given
     * @see UnknownFilterKey.setGiven
     * @see UnknownFilterKey.getGiven
     * @see UnknownFilterKey.unsafeGiven
     * @see UnknownFilterKey.eraseGiven
     * @see UnknownFilterKey.isGivenDefined
     */
    isGivenEqualTo(given?: undefined|string): boolean{
        if (typeof given === "undefined"){
            return false;
        }
        return this.unsafeGiven() === given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UnknownFilterKey.given**.
     * @description **Removes the assigned the value of @UnknownFilterKey.given**: *@UnknownFilterKey.given will be flagged as undefined*.
     * @return Return the actual [UnknownFilterKey](UnknownFilterKey) class instance.
     * @category @UnknownFilterKey.given
     * @category truncate
     * @category field
     * @public
     * @see UnknownFilterKey.given
     * @see UnknownFilterKey.setGiven
     * @see UnknownFilterKey.getGiven
     * @see UnknownFilterKey.unsafeGiven
     * @see UnknownFilterKey.isGivenDefined
     * @see UnknownFilterKey.isGivenEqualTo
     */
    eraseGiven(): this{
        this.given = undefined;
        this.removeDefaultParameter("given")

        return this;
    }
}

Translate.setTranslate({
    'fr.model.unknown.filter': 'votre clef de filtre `#given` de #model n\'est pas une valeur qui peut être selectionné ou alors utilisé en tant que filtre.',
    'en.model.unknown.filter': 'your `#given` filter key from #model is not a value that can be selected or used as a filter.',
})

export default UnknownFilterKey;