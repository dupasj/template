import ModelError from "../../model";
import UndefinedValue from "../undefined-value";
import Translate from "../../../translate";
class UnknownKey extends ModelError{
    constructor(key = "model.unknown.key",http_code = 500) {
        super(key,http_code);
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UnknownKey.given.
     * @description Save @UnknownKey.given.
     * @category @UnknownKey.given
     * @category property
     * @category field
     * @protected
     * @see UnknownKey.setGiven
     * @see UnknownKey.getGiven
     * @see UnknownKey.unsafeGiven
     * @see UnknownKey.eraseGiven
     * @see UnknownKey.isGivenDefined
     * @see UnknownKey.isGivenEqualTo
     */
    protected given: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UnknownKey.given**.
     * @description Allows you to update or **assign the value of @UnknownKey.given**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UnknownKey](UnknownKey) class instance.
     * @param given The new value to assign to @UnknownKey.given.
     * @category @UnknownKey.given
     * @category setter
     * @category field
     * @public
     * @see UnknownKey.given
     * @see UnknownKey.getGiven
     * @see UnknownKey.unsafeGiven
     * @see UnknownKey.eraseGiven
     * @see UnknownKey.isGivenDefined
     * @see UnknownKey.isGivenEqualTo
     */
    setGiven(given: string): this{
        if (this.isGivenEqualTo(given)){
            return this;
        }

        this.setDefaultParameter("given",given)

        this.eraseGiven();
        this.given = given;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownKey.given**. Unlike the [getGiven](UnknownKey.getGiven) method, **the method results an undefined value if @UnknownKey.given is not defined**.
     * @description **Gives the value of @UnknownKey.given**. Unlike the [getGiven](UnknownKey.getGiven) method, **the method results an undefined value if @UnknownKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UnknownKey.given**. Unlike the [getGiven](UnknownKey.getGiven) method, **the method results an undefined value if @UnknownKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UnknownKey.given
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UnknownKey.given
     * @see UnknownKey.setGiven
     * @see UnknownKey.getGiven
     * @see UnknownKey.eraseGiven
     * @see UnknownKey.isGivenDefined
     * @see UnknownKey.isGivenEqualTo
     */
    unsafeGiven(): string|undefined{
        return this.given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownKey.given**. Unlike the [unsafeGiven](UnknownKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownKey.given is not defined**.
     * @description **Gives the value of @UnknownKey.given**. Unlike the [unsafeGiven](UnknownKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownKey.given is not defined**.
     * @return **Returns the value of @UnknownKey.given**. Unlike the [unsafeGiven](UnknownKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownKey.given is not defined**.
     * @category @UnknownKey.given
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UnknownKey.given isn't defined.
     * @see UnknownKey.given
     * @see UnknownKey.setGiven
     * @see UnknownKey.unsafeGiven
     * @see UnknownKey.eraseGiven
     * @see UnknownKey.isGivenDefined
     * @see UnknownKey.isGivenEqualTo
     */
    getGiven(): string{
        const given = this.unsafeGiven();

        if (typeof given === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getGiven");
            error.setClassname("UnknownKey");
            error.setProperty("given");
            throw error;
        }
        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownKey.given is defined**.
     * @description **Checks if @UnknownKey.given is defined** or not.
     * @return **Returns a boolean if @UnknownKey.given is defined** or not.
     * @category @UnknownKey.given
     * @category defined
     * @category field
     * @public
     * @see UnknownKey.given
     * @see UnknownKey.setGiven
     * @see UnknownKey.getGiven
     * @see UnknownKey.unsafeGiven
     * @see UnknownKey.eraseGiven
     * @see UnknownKey.isGivenEqualTo
     */
    isGivenDefined(): boolean{
        return typeof this.given !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownKey.given is equal to your given value**.
     * @description **Checks if @UnknownKey.given is equal to your given value** and if @UnknownKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UnknownKey.given is equal to your given value** and if @UnknownKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param given **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UnknownKey.given
     * @category equality
     * @category field
     * @public
     * @see UnknownKey.given
     * @see UnknownKey.setGiven
     * @see UnknownKey.getGiven
     * @see UnknownKey.unsafeGiven
     * @see UnknownKey.eraseGiven
     * @see UnknownKey.isGivenDefined
     */
    isGivenEqualTo(given?: undefined|string): boolean{
        if (typeof given === "undefined"){
            return false;
        }
        return this.unsafeGiven() === given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UnknownKey.given**.
     * @description **Removes the assigned the value of @UnknownKey.given**: *@UnknownKey.given will be flagged as undefined*.
     * @return Return the actual [UnknownKey](UnknownKey) class instance.
     * @category @UnknownKey.given
     * @category truncate
     * @category field
     * @public
     * @see UnknownKey.given
     * @see UnknownKey.setGiven
     * @see UnknownKey.getGiven
     * @see UnknownKey.unsafeGiven
     * @see UnknownKey.isGivenDefined
     * @see UnknownKey.isGivenEqualTo
     */
    eraseGiven(): this{
        this.given = undefined;
        this.removeDefaultParameter("given")

        return this;
    }
}

Translate.setTranslate({
    'fr.model.unknown.key': 'votre clef `#given` de #model fait référence à aucunes valeurs connues.',
    'en.model.unknown.key': 'your `#given` key from #model refers to no known values.',
})

export default UnknownKey;