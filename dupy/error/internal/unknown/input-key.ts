import ModelError from "../../model";
import UndefinedValue from "../undefined-value";
import Translate from "../../../translate";
class UnknownInputKey extends ModelError{
    constructor(key = "model.unknown.key.input",http_code = 417) {
        super(key,http_code);
    }





    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UnknownInputKey.given.
     * @description Save @UnknownInputKey.given.
     * @category @UnknownInputKey.given
     * @category property
     * @category field
     * @protected
     * @see UnknownInputKey.setGiven
     * @see UnknownInputKey.getGiven
     * @see UnknownInputKey.unsafeGiven
     * @see UnknownInputKey.eraseGiven
     * @see UnknownInputKey.isGivenDefined
     * @see UnknownInputKey.isGivenEqualTo
     */
    protected given: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UnknownInputKey.given**.
     * @description Allows you to update or **assign the value of @UnknownInputKey.given**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UnknownInputKey](UnknownInputKey) class instance.
     * @param given The new value to assign to @UnknownInputKey.given.
     * @category @UnknownInputKey.given
     * @category setter
     * @category field
     * @public
     * @see UnknownInputKey.given
     * @see UnknownInputKey.getGiven
     * @see UnknownInputKey.unsafeGiven
     * @see UnknownInputKey.eraseGiven
     * @see UnknownInputKey.isGivenDefined
     * @see UnknownInputKey.isGivenEqualTo
     */
    setGiven(given: string): this{
        if (this.isGivenEqualTo(given)){
            return this;
        }

        this.setDefaultParameter("given",given)

        this.eraseGiven();
        this.given = given;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownInputKey.given**. Unlike the [getGiven](UnknownInputKey.getGiven) method, **the method results an undefined value if @UnknownInputKey.given is not defined**.
     * @description **Gives the value of @UnknownInputKey.given**. Unlike the [getGiven](UnknownInputKey.getGiven) method, **the method results an undefined value if @UnknownInputKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UnknownInputKey.given**. Unlike the [getGiven](UnknownInputKey.getGiven) method, **the method results an undefined value if @UnknownInputKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UnknownInputKey.given
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UnknownInputKey.given
     * @see UnknownInputKey.setGiven
     * @see UnknownInputKey.getGiven
     * @see UnknownInputKey.eraseGiven
     * @see UnknownInputKey.isGivenDefined
     * @see UnknownInputKey.isGivenEqualTo
     */
    unsafeGiven(): string|undefined{
        return this.given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownInputKey.given**. Unlike the [unsafeGiven](UnknownInputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownInputKey.given is not defined**.
     * @description **Gives the value of @UnknownInputKey.given**. Unlike the [unsafeGiven](UnknownInputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownInputKey.given is not defined**.
     * @return **Returns the value of @UnknownInputKey.given**. Unlike the [unsafeGiven](UnknownInputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownInputKey.given is not defined**.
     * @category @UnknownInputKey.given
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UnknownInputKey.given isn't defined.
     * @see UnknownInputKey.given
     * @see UnknownInputKey.setGiven
     * @see UnknownInputKey.unsafeGiven
     * @see UnknownInputKey.eraseGiven
     * @see UnknownInputKey.isGivenDefined
     * @see UnknownInputKey.isGivenEqualTo
     */
    getGiven(): string{
        const given = this.unsafeGiven();

        if (typeof given === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getGiven");
            error.setClassname("UnknownInputKey");
            error.setProperty("given");
            throw error;
        }
        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownInputKey.given is defined**.
     * @description **Checks if @UnknownInputKey.given is defined** or not.
     * @return **Returns a boolean if @UnknownInputKey.given is defined** or not.
     * @category @UnknownInputKey.given
     * @category defined
     * @category field
     * @public
     * @see UnknownInputKey.given
     * @see UnknownInputKey.setGiven
     * @see UnknownInputKey.getGiven
     * @see UnknownInputKey.unsafeGiven
     * @see UnknownInputKey.eraseGiven
     * @see UnknownInputKey.isGivenEqualTo
     */
    isGivenDefined(): boolean{
        return typeof this.given !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownInputKey.given is equal to your given value**.
     * @description **Checks if @UnknownInputKey.given is equal to your given value** and if @UnknownInputKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UnknownInputKey.given is equal to your given value** and if @UnknownInputKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param given **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UnknownInputKey.given
     * @category equality
     * @category field
     * @public
     * @see UnknownInputKey.given
     * @see UnknownInputKey.setGiven
     * @see UnknownInputKey.getGiven
     * @see UnknownInputKey.unsafeGiven
     * @see UnknownInputKey.eraseGiven
     * @see UnknownInputKey.isGivenDefined
     */
    isGivenEqualTo(given?: undefined|string): boolean{
        if (typeof given === "undefined"){
            return false;
        }
        return this.unsafeGiven() === given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UnknownInputKey.given**.
     * @description **Removes the assigned the value of @UnknownInputKey.given**: *@UnknownInputKey.given will be flagged as undefined*.
     * @return Return the actual [UnknownInputKey](UnknownInputKey) class instance.
     * @category @UnknownInputKey.given
     * @category truncate
     * @category field
     * @public
     * @see UnknownInputKey.given
     * @see UnknownInputKey.setGiven
     * @see UnknownInputKey.getGiven
     * @see UnknownInputKey.unsafeGiven
     * @see UnknownInputKey.isGivenDefined
     * @see UnknownInputKey.isGivenEqualTo
     */
    eraseGiven(): this{
        this.given = undefined;
        this.removeDefaultParameter("given")

        return this;
    }
}

Translate.setTranslate({
    'fr.model.unknown.input': 'votre clef de filtre `#given` de #model n\'est pas une valeur qui peut être edité.',
    'en.model.unknown.input': 'your `#given` filter key from #model is not a value that can be edited.',
})

export default UnknownInputKey;