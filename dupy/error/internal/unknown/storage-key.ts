import ModelError from "../../model";
import UndefinedValue from "../undefined-value";
import Translate from "../../../translate";

class UnknownStorageKey extends ModelError{
    constructor(key = "model.unknown.key.storage",http_code = 500) {
        super(key,http_code);
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UnknownStorageKey.given.
     * @description Save @UnknownStorageKey.given.
     * @category @UnknownStorageKey.given
     * @category property
     * @category field
     * @protected
     * @see UnknownStorageKey.setGiven
     * @see UnknownStorageKey.getGiven
     * @see UnknownStorageKey.unsafeGiven
     * @see UnknownStorageKey.eraseGiven
     * @see UnknownStorageKey.isGivenDefined
     * @see UnknownStorageKey.isGivenEqualTo
     */
    protected given: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UnknownStorageKey.given**.
     * @description Allows you to update or **assign the value of @UnknownStorageKey.given**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UnknownStorageKey](UnknownStorageKey) class instance.
     * @param given The new value to assign to @UnknownStorageKey.given.
     * @category @UnknownStorageKey.given
     * @category setter
     * @category field
     * @public
     * @see UnknownStorageKey.given
     * @see UnknownStorageKey.getGiven
     * @see UnknownStorageKey.unsafeGiven
     * @see UnknownStorageKey.eraseGiven
     * @see UnknownStorageKey.isGivenDefined
     * @see UnknownStorageKey.isGivenEqualTo
     */
    setGiven(given: string): this{
        if (this.isGivenEqualTo(given)){
            return this;
        }

        this.setDefaultParameter("given",given)

        this.eraseGiven();
        this.given = given;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownStorageKey.given**. Unlike the [getGiven](UnknownStorageKey.getGiven) method, **the method results an undefined value if @UnknownStorageKey.given is not defined**.
     * @description **Gives the value of @UnknownStorageKey.given**. Unlike the [getGiven](UnknownStorageKey.getGiven) method, **the method results an undefined value if @UnknownStorageKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UnknownStorageKey.given**. Unlike the [getGiven](UnknownStorageKey.getGiven) method, **the method results an undefined value if @UnknownStorageKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UnknownStorageKey.given
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UnknownStorageKey.given
     * @see UnknownStorageKey.setGiven
     * @see UnknownStorageKey.getGiven
     * @see UnknownStorageKey.eraseGiven
     * @see UnknownStorageKey.isGivenDefined
     * @see UnknownStorageKey.isGivenEqualTo
     */
    unsafeGiven(): string|undefined{
        return this.given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownStorageKey.given**. Unlike the [unsafeGiven](UnknownStorageKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownStorageKey.given is not defined**.
     * @description **Gives the value of @UnknownStorageKey.given**. Unlike the [unsafeGiven](UnknownStorageKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownStorageKey.given is not defined**.
     * @return **Returns the value of @UnknownStorageKey.given**. Unlike the [unsafeGiven](UnknownStorageKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownStorageKey.given is not defined**.
     * @category @UnknownStorageKey.given
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UnknownStorageKey.given isn't defined.
     * @see UnknownStorageKey.given
     * @see UnknownStorageKey.setGiven
     * @see UnknownStorageKey.unsafeGiven
     * @see UnknownStorageKey.eraseGiven
     * @see UnknownStorageKey.isGivenDefined
     * @see UnknownStorageKey.isGivenEqualTo
     */
    getGiven(): string{
        const given = this.unsafeGiven();

        if (typeof given === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getGiven");
            error.setClassname("UnknownStorageKey");
            error.setProperty("given");
            throw error;
        }
        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownStorageKey.given is defined**.
     * @description **Checks if @UnknownStorageKey.given is defined** or not.
     * @return **Returns a boolean if @UnknownStorageKey.given is defined** or not.
     * @category @UnknownStorageKey.given
     * @category defined
     * @category field
     * @public
     * @see UnknownStorageKey.given
     * @see UnknownStorageKey.setGiven
     * @see UnknownStorageKey.getGiven
     * @see UnknownStorageKey.unsafeGiven
     * @see UnknownStorageKey.eraseGiven
     * @see UnknownStorageKey.isGivenEqualTo
     */
    isGivenDefined(): boolean{
        return typeof this.given !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownStorageKey.given is equal to your given value**.
     * @description **Checks if @UnknownStorageKey.given is equal to your given value** and if @UnknownStorageKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UnknownStorageKey.given is equal to your given value** and if @UnknownStorageKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param given **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UnknownStorageKey.given
     * @category equality
     * @category field
     * @public
     * @see UnknownStorageKey.given
     * @see UnknownStorageKey.setGiven
     * @see UnknownStorageKey.getGiven
     * @see UnknownStorageKey.unsafeGiven
     * @see UnknownStorageKey.eraseGiven
     * @see UnknownStorageKey.isGivenDefined
     */
    isGivenEqualTo(given?: undefined|string): boolean{
        if (typeof given === "undefined"){
            return false;
        }
        return this.unsafeGiven() === given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UnknownStorageKey.given**.
     * @description **Removes the assigned the value of @UnknownStorageKey.given**: *@UnknownStorageKey.given will be flagged as undefined*.
     * @return Return the actual [UnknownStorageKey](UnknownStorageKey) class instance.
     * @category @UnknownStorageKey.given
     * @category truncate
     * @category field
     * @public
     * @see UnknownStorageKey.given
     * @see UnknownStorageKey.setGiven
     * @see UnknownStorageKey.getGiven
     * @see UnknownStorageKey.unsafeGiven
     * @see UnknownStorageKey.isGivenDefined
     * @see UnknownStorageKey.isGivenEqualTo
     */
    eraseGiven(): this{
        this.given = undefined;
        this.removeDefaultParameter("given")

        return this;
    }
}

Translate.setTranslate({
    'fr.model.unknown.storage': 'votre clef `#given` de #model n\'est pas une valeur qui peut être sauvegardé dans une storage de base de données',
    'en.model.unknown.storage': 'your `#given` key of #model is not a value that can be saved in a database storage',
})

export default UnknownStorageKey;