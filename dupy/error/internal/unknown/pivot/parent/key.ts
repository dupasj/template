import ModelError from "../../../../model";
import UndefinedValue from "../../../undefined-value";
import Translate from "../../../../../translate";

class UnknownPivotParentKey extends ModelError{
    constructor(key = "model.unknown.pivot.parent.key",http_code = 500) {
        super(key,http_code);
    }




    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UnknownPivotParentKey.given.
     * @description Save @UnknownPivotParentKey.given.
     * @category @UnknownPivotParentKey.given
     * @category property
     * @category field
     * @protected
     * @see UnknownPivotParentKey.setGiven
     * @see UnknownPivotParentKey.getGiven
     * @see UnknownPivotParentKey.unsafeGiven
     * @see UnknownPivotParentKey.eraseGiven
     * @see UnknownPivotParentKey.isGivenDefined
     * @see UnknownPivotParentKey.isGivenEqualTo
     */
    protected given: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UnknownPivotParentKey.given**.
     * @description Allows you to update or **assign the value of @UnknownPivotParentKey.given**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UnknownPivotParentKey](UnknownPivotParentKey) class instance.
     * @param given The new value to assign to @UnknownPivotParentKey.given.
     * @category @UnknownPivotParentKey.given
     * @category setter
     * @category field
     * @public
     * @see UnknownPivotParentKey.given
     * @see UnknownPivotParentKey.getGiven
     * @see UnknownPivotParentKey.unsafeGiven
     * @see UnknownPivotParentKey.eraseGiven
     * @see UnknownPivotParentKey.isGivenDefined
     * @see UnknownPivotParentKey.isGivenEqualTo
     */
    setGiven(given: string): this{
        if (this.isGivenEqualTo(given)){
            return this;
        }

        this.setDefaultParameter("given",given)

        this.eraseGiven();
        this.given = given;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownPivotParentKey.given**. Unlike the [getGiven](UnknownPivotParentKey.getGiven) method, **the method results an undefined value if @UnknownPivotParentKey.given is not defined**.
     * @description **Gives the value of @UnknownPivotParentKey.given**. Unlike the [getGiven](UnknownPivotParentKey.getGiven) method, **the method results an undefined value if @UnknownPivotParentKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UnknownPivotParentKey.given**. Unlike the [getGiven](UnknownPivotParentKey.getGiven) method, **the method results an undefined value if @UnknownPivotParentKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UnknownPivotParentKey.given
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UnknownPivotParentKey.given
     * @see UnknownPivotParentKey.setGiven
     * @see UnknownPivotParentKey.getGiven
     * @see UnknownPivotParentKey.eraseGiven
     * @see UnknownPivotParentKey.isGivenDefined
     * @see UnknownPivotParentKey.isGivenEqualTo
     */
    unsafeGiven(): string|undefined{
        return this.given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownPivotParentKey.given**. Unlike the [unsafeGiven](UnknownPivotParentKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownPivotParentKey.given is not defined**.
     * @description **Gives the value of @UnknownPivotParentKey.given**. Unlike the [unsafeGiven](UnknownPivotParentKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownPivotParentKey.given is not defined**.
     * @return **Returns the value of @UnknownPivotParentKey.given**. Unlike the [unsafeGiven](UnknownPivotParentKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownPivotParentKey.given is not defined**.
     * @category @UnknownPivotParentKey.given
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UnknownPivotParentKey.given isn't defined.
     * @see UnknownPivotParentKey.given
     * @see UnknownPivotParentKey.setGiven
     * @see UnknownPivotParentKey.unsafeGiven
     * @see UnknownPivotParentKey.eraseGiven
     * @see UnknownPivotParentKey.isGivenDefined
     * @see UnknownPivotParentKey.isGivenEqualTo
     */
    getGiven(): string{
        const given = this.unsafeGiven();

        if (typeof given === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getGiven");
            error.setClassname("UnknownPivotParentKey");
            error.setProperty("given");
            throw error;
        }
        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownPivotParentKey.given is defined**.
     * @description **Checks if @UnknownPivotParentKey.given is defined** or not.
     * @return **Returns a boolean if @UnknownPivotParentKey.given is defined** or not.
     * @category @UnknownPivotParentKey.given
     * @category defined
     * @category field
     * @public
     * @see UnknownPivotParentKey.given
     * @see UnknownPivotParentKey.setGiven
     * @see UnknownPivotParentKey.getGiven
     * @see UnknownPivotParentKey.unsafeGiven
     * @see UnknownPivotParentKey.eraseGiven
     * @see UnknownPivotParentKey.isGivenEqualTo
     */
    isGivenDefined(): boolean{
        return typeof this.given !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownPivotParentKey.given is equal to your given value**.
     * @description **Checks if @UnknownPivotParentKey.given is equal to your given value** and if @UnknownPivotParentKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UnknownPivotParentKey.given is equal to your given value** and if @UnknownPivotParentKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param given **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UnknownPivotParentKey.given
     * @category equality
     * @category field
     * @public
     * @see UnknownPivotParentKey.given
     * @see UnknownPivotParentKey.setGiven
     * @see UnknownPivotParentKey.getGiven
     * @see UnknownPivotParentKey.unsafeGiven
     * @see UnknownPivotParentKey.eraseGiven
     * @see UnknownPivotParentKey.isGivenDefined
     */
    isGivenEqualTo(given?: undefined|string): boolean{
        if (typeof given === "undefined"){
            return false;
        }
        return this.unsafeGiven() === given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UnknownPivotParentKey.given**.
     * @description **Removes the assigned the value of @UnknownPivotParentKey.given**: *@UnknownPivotParentKey.given will be flagged as undefined*.
     * @return Return the actual [UnknownPivotParentKey](UnknownPivotParentKey) class instance.
     * @category @UnknownPivotParentKey.given
     * @category truncate
     * @category field
     * @public
     * @see UnknownPivotParentKey.given
     * @see UnknownPivotParentKey.setGiven
     * @see UnknownPivotParentKey.getGiven
     * @see UnknownPivotParentKey.unsafeGiven
     * @see UnknownPivotParentKey.isGivenDefined
     * @see UnknownPivotParentKey.isGivenEqualTo
     */
    eraseGiven(): this{
        this.given = undefined;
        this.removeDefaultParameter("given")

        return this;
    }
}

Translate.setTranslate({
    'fr.model.unknown.pivot.parent.key': 'votre clef `#given` de #model fait référence à aucune relation connue.',
    'en.model.unknown.pivot.parent.key': 'your `#given` key from #model refers to no known relation value.',
})

export default UnknownPivotParentKey;