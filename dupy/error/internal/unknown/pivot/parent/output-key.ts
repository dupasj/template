import ModelError from "../../../../model";
import UndefinedValue from "../../../undefined-value";
import Translate from "../../../../../translate";

class UnknownPivotParentOutputKey extends ModelError{
    constructor(key = "model.unknown.pivot.parent.key.output",http_code = 500) {
        super(key,http_code);
    }




    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UnknownPivotParentOutputKey.given.
     * @description Save @UnknownPivotParentOutputKey.given.
     * @category @UnknownPivotParentOutputKey.given
     * @category property
     * @category field
     * @protected
     * @see UnknownPivotParentOutputKey.setGiven
     * @see UnknownPivotParentOutputKey.getGiven
     * @see UnknownPivotParentOutputKey.unsafeGiven
     * @see UnknownPivotParentOutputKey.eraseGiven
     * @see UnknownPivotParentOutputKey.isGivenDefined
     * @see UnknownPivotParentOutputKey.isGivenEqualTo
     */
    protected given: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UnknownPivotParentOutputKey.given**.
     * @description Allows you to update or **assign the value of @UnknownPivotParentOutputKey.given**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UnknownPivotParentOutputKey](UnknownPivotParentOutputKey) class instance.
     * @param given The new value to assign to @UnknownPivotParentOutputKey.given.
     * @category @UnknownPivotParentOutputKey.given
     * @category setter
     * @category field
     * @public
     * @see UnknownPivotParentOutputKey.given
     * @see UnknownPivotParentOutputKey.getGiven
     * @see UnknownPivotParentOutputKey.unsafeGiven
     * @see UnknownPivotParentOutputKey.eraseGiven
     * @see UnknownPivotParentOutputKey.isGivenDefined
     * @see UnknownPivotParentOutputKey.isGivenEqualTo
     */
    setGiven(given: string): this{
        if (this.isGivenEqualTo(given)){
            return this;
        }

        this.setDefaultParameter("given",given)

        this.eraseGiven();
        this.given = given;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownPivotParentOutputKey.given**. Unlike the [getGiven](UnknownPivotParentOutputKey.getGiven) method, **the method results an undefined value if @UnknownPivotParentOutputKey.given is not defined**.
     * @description **Gives the value of @UnknownPivotParentOutputKey.given**. Unlike the [getGiven](UnknownPivotParentOutputKey.getGiven) method, **the method results an undefined value if @UnknownPivotParentOutputKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UnknownPivotParentOutputKey.given**. Unlike the [getGiven](UnknownPivotParentOutputKey.getGiven) method, **the method results an undefined value if @UnknownPivotParentOutputKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UnknownPivotParentOutputKey.given
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UnknownPivotParentOutputKey.given
     * @see UnknownPivotParentOutputKey.setGiven
     * @see UnknownPivotParentOutputKey.getGiven
     * @see UnknownPivotParentOutputKey.eraseGiven
     * @see UnknownPivotParentOutputKey.isGivenDefined
     * @see UnknownPivotParentOutputKey.isGivenEqualTo
     */
    unsafeGiven(): string|undefined{
        return this.given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownPivotParentOutputKey.given**. Unlike the [unsafeGiven](UnknownPivotParentOutputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownPivotParentOutputKey.given is not defined**.
     * @description **Gives the value of @UnknownPivotParentOutputKey.given**. Unlike the [unsafeGiven](UnknownPivotParentOutputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownPivotParentOutputKey.given is not defined**.
     * @return **Returns the value of @UnknownPivotParentOutputKey.given**. Unlike the [unsafeGiven](UnknownPivotParentOutputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownPivotParentOutputKey.given is not defined**.
     * @category @UnknownPivotParentOutputKey.given
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UnknownPivotParentOutputKey.given isn't defined.
     * @see UnknownPivotParentOutputKey.given
     * @see UnknownPivotParentOutputKey.setGiven
     * @see UnknownPivotParentOutputKey.unsafeGiven
     * @see UnknownPivotParentOutputKey.eraseGiven
     * @see UnknownPivotParentOutputKey.isGivenDefined
     * @see UnknownPivotParentOutputKey.isGivenEqualTo
     */
    getGiven(): string{
        const given = this.unsafeGiven();

        if (typeof given === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getGiven");
            error.setClassname("UnknownPivotParentOutputKey");
            error.setProperty("given");
            throw error;
        }
        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownPivotParentOutputKey.given is defined**.
     * @description **Checks if @UnknownPivotParentOutputKey.given is defined** or not.
     * @return **Returns a boolean if @UnknownPivotParentOutputKey.given is defined** or not.
     * @category @UnknownPivotParentOutputKey.given
     * @category defined
     * @category field
     * @public
     * @see UnknownPivotParentOutputKey.given
     * @see UnknownPivotParentOutputKey.setGiven
     * @see UnknownPivotParentOutputKey.getGiven
     * @see UnknownPivotParentOutputKey.unsafeGiven
     * @see UnknownPivotParentOutputKey.eraseGiven
     * @see UnknownPivotParentOutputKey.isGivenEqualTo
     */
    isGivenDefined(): boolean{
        return typeof this.given !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownPivotParentOutputKey.given is equal to your given value**.
     * @description **Checks if @UnknownPivotParentOutputKey.given is equal to your given value** and if @UnknownPivotParentOutputKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UnknownPivotParentOutputKey.given is equal to your given value** and if @UnknownPivotParentOutputKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param given **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UnknownPivotParentOutputKey.given
     * @category equality
     * @category field
     * @public
     * @see UnknownPivotParentOutputKey.given
     * @see UnknownPivotParentOutputKey.setGiven
     * @see UnknownPivotParentOutputKey.getGiven
     * @see UnknownPivotParentOutputKey.unsafeGiven
     * @see UnknownPivotParentOutputKey.eraseGiven
     * @see UnknownPivotParentOutputKey.isGivenDefined
     */
    isGivenEqualTo(given?: undefined|string): boolean{
        if (typeof given === "undefined"){
            return false;
        }
        return this.unsafeGiven() === given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UnknownPivotParentOutputKey.given**.
     * @description **Removes the assigned the value of @UnknownPivotParentOutputKey.given**: *@UnknownPivotParentOutputKey.given will be flagged as undefined*.
     * @return Return the actual [UnknownPivotParentOutputKey](UnknownPivotParentOutputKey) class instance.
     * @category @UnknownPivotParentOutputKey.given
     * @category truncate
     * @category field
     * @public
     * @see UnknownPivotParentOutputKey.given
     * @see UnknownPivotParentOutputKey.setGiven
     * @see UnknownPivotParentOutputKey.getGiven
     * @see UnknownPivotParentOutputKey.unsafeGiven
     * @see UnknownPivotParentOutputKey.isGivenDefined
     * @see UnknownPivotParentOutputKey.isGivenEqualTo
     */
    eraseGiven(): this{
        this.given = undefined;
        this.removeDefaultParameter("given")

        return this;
    }
}

Translate.setTranslate({
    'fr.model.unknown.pivot.parent.key.output': 'votre clef `#given` de #model fait référence à aucune relation connue.',
    'en.model.unknown.pivot.parent.key.output': 'your `#given` key from #model refers to no known relation value.',
})

export default UnknownPivotParentOutputKey;