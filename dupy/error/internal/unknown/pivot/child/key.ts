import ModelError from "../../../../model";
import UndefinedValue from "../../../undefined-value";
import Translate from "../../../../../translate";

class UnknownPivotChildKey extends ModelError{
    constructor(key = "model.unknown.pivot.child.key",http_code = 500) {
        super(key,http_code);
    }






    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UnknownPivotChildKey.given.
     * @description Save @UnknownPivotChildKey.given.
     * @category @UnknownPivotChildKey.given
     * @category property
     * @category field
     * @protected
     * @see UnknownPivotChildKey.setGiven
     * @see UnknownPivotChildKey.getGiven
     * @see UnknownPivotChildKey.unsafeGiven
     * @see UnknownPivotChildKey.eraseGiven
     * @see UnknownPivotChildKey.isGivenDefined
     * @see UnknownPivotChildKey.isGivenEqualTo
     */
    protected given: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UnknownPivotChildKey.given**.
     * @description Allows you to update or **assign the value of @UnknownPivotChildKey.given**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UnknownPivotChildKey](UnknownPivotChildKey) class instance.
     * @param given The new value to assign to @UnknownPivotChildKey.given.
     * @category @UnknownPivotChildKey.given
     * @category setter
     * @category field
     * @public
     * @see UnknownPivotChildKey.given
     * @see UnknownPivotChildKey.getGiven
     * @see UnknownPivotChildKey.unsafeGiven
     * @see UnknownPivotChildKey.eraseGiven
     * @see UnknownPivotChildKey.isGivenDefined
     * @see UnknownPivotChildKey.isGivenEqualTo
     */
    setGiven(given: string): this{
        if (this.isGivenEqualTo(given)){
            return this;
        }

        this.setDefaultParameter("given",given)

        this.eraseGiven();
        this.given = given;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownPivotChildKey.given**. Unlike the [getGiven](UnknownPivotChildKey.getGiven) method, **the method results an undefined value if @UnknownPivotChildKey.given is not defined**.
     * @description **Gives the value of @UnknownPivotChildKey.given**. Unlike the [getGiven](UnknownPivotChildKey.getGiven) method, **the method results an undefined value if @UnknownPivotChildKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UnknownPivotChildKey.given**. Unlike the [getGiven](UnknownPivotChildKey.getGiven) method, **the method results an undefined value if @UnknownPivotChildKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UnknownPivotChildKey.given
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UnknownPivotChildKey.given
     * @see UnknownPivotChildKey.setGiven
     * @see UnknownPivotChildKey.getGiven
     * @see UnknownPivotChildKey.eraseGiven
     * @see UnknownPivotChildKey.isGivenDefined
     * @see UnknownPivotChildKey.isGivenEqualTo
     */
    unsafeGiven(): string|undefined{
        return this.given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnknownPivotChildKey.given**. Unlike the [unsafeGiven](UnknownPivotChildKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownPivotChildKey.given is not defined**.
     * @description **Gives the value of @UnknownPivotChildKey.given**. Unlike the [unsafeGiven](UnknownPivotChildKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownPivotChildKey.given is not defined**.
     * @return **Returns the value of @UnknownPivotChildKey.given**. Unlike the [unsafeGiven](UnknownPivotChildKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnknownPivotChildKey.given is not defined**.
     * @category @UnknownPivotChildKey.given
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UnknownPivotChildKey.given isn't defined.
     * @see UnknownPivotChildKey.given
     * @see UnknownPivotChildKey.setGiven
     * @see UnknownPivotChildKey.unsafeGiven
     * @see UnknownPivotChildKey.eraseGiven
     * @see UnknownPivotChildKey.isGivenDefined
     * @see UnknownPivotChildKey.isGivenEqualTo
     */
    getGiven(): string{
        const given = this.unsafeGiven();

        if (typeof given === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getGiven");
            error.setClassname("UnknownPivotChildKey");
            error.setProperty("given");
            throw error;
        }
        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownPivotChildKey.given is defined**.
     * @description **Checks if @UnknownPivotChildKey.given is defined** or not.
     * @return **Returns a boolean if @UnknownPivotChildKey.given is defined** or not.
     * @category @UnknownPivotChildKey.given
     * @category defined
     * @category field
     * @public
     * @see UnknownPivotChildKey.given
     * @see UnknownPivotChildKey.setGiven
     * @see UnknownPivotChildKey.getGiven
     * @see UnknownPivotChildKey.unsafeGiven
     * @see UnknownPivotChildKey.eraseGiven
     * @see UnknownPivotChildKey.isGivenEqualTo
     */
    isGivenDefined(): boolean{
        return typeof this.given !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnknownPivotChildKey.given is equal to your given value**.
     * @description **Checks if @UnknownPivotChildKey.given is equal to your given value** and if @UnknownPivotChildKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UnknownPivotChildKey.given is equal to your given value** and if @UnknownPivotChildKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param given **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UnknownPivotChildKey.given
     * @category equality
     * @category field
     * @public
     * @see UnknownPivotChildKey.given
     * @see UnknownPivotChildKey.setGiven
     * @see UnknownPivotChildKey.getGiven
     * @see UnknownPivotChildKey.unsafeGiven
     * @see UnknownPivotChildKey.eraseGiven
     * @see UnknownPivotChildKey.isGivenDefined
     */
    isGivenEqualTo(given?: undefined|string): boolean{
        if (typeof given === "undefined"){
            return false;
        }
        return this.unsafeGiven() === given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UnknownPivotChildKey.given**.
     * @description **Removes the assigned the value of @UnknownPivotChildKey.given**: *@UnknownPivotChildKey.given will be flagged as undefined*.
     * @return Return the actual [UnknownPivotChildKey](UnknownPivotChildKey) class instance.
     * @category @UnknownPivotChildKey.given
     * @category truncate
     * @category field
     * @public
     * @see UnknownPivotChildKey.given
     * @see UnknownPivotChildKey.setGiven
     * @see UnknownPivotChildKey.getGiven
     * @see UnknownPivotChildKey.unsafeGiven
     * @see UnknownPivotChildKey.isGivenDefined
     * @see UnknownPivotChildKey.isGivenEqualTo
     */
    eraseGiven(): this{
        this.given = undefined;
        this.removeDefaultParameter("given")

        return this;
    }
}

Translate.setTranslate({
    'fr.model.unknown.pivot.child.key': 'votre clef `#given` de #model fait référence à aucune relation connue.',
    'en.model.unknown.pivot.child.key': 'your `#given` key from #model refers to no known relation value.',
})

export default UnknownPivotChildKey;