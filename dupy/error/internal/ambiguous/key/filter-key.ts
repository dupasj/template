import ModelError from "../../../model";
import DeepArray from "../../../../util/deep-array/type";
import flat from "../../../../util/deep-array/flat";
import BaseFieldConfiguration from "../../../../model/field/base";
import BaseFieldInstance from "../../../../model/field/base/instance";
import OutOfRange from "../../out-of-range";
import Translate from "../../../../translate";
import UndefinedValue from "../../undefined-value";

class AmbiguousKeyFilterKey extends ModelError{
    constructor(key = "model.ambiguous.key.filter",http_code = 417) {
        super(key,http_code);
    }

    private updateFields(){
        this.setDefaultParameter("fields",[
            this.unsafeFields().slice(0,-1).map(field => field.getTranslate()).join(", "),
            ... this.unsafeFields().slice(-1).map(field => field.getTranslate()),
        ].join(" {and} "))
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Save @AmbiguousKeyFilterKey.fields.
     * @description Save @AmbiguousKeyFilterKey.fields.
     * @category fields
     * @category property
     * @category list
     * @protected
     * @see AmbiguousKeyFilterKey.getFields
     * @see AmbiguousKeyFilterKey.unsafeFields
     * @see AmbiguousKeyFilterKey.getField
     * @see AmbiguousKeyFilterKey.unsafeField
     * @see AmbiguousKeyFilterKey.getFieldsSize
     * @see AmbiguousKeyFilterKey.containFields
     * @see AmbiguousKeyFilterKey.addFields
     * @see AmbiguousKeyFilterKey.removeFields
     * @see AmbiguousKeyFilterKey.truncateFields
     */
    protected fields: (BaseFieldConfiguration|BaseFieldInstance)[] = [];

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the size of @AmbiguousKeyFilterKey.fields**.
     * @description **Gives the size of @AmbiguousKeyFilterKey.fields**.
     * @return **Returns the size of @AmbiguousKeyFilterKey.fields**.
     * @category @AmbiguousKeyFilterKey.fields
     * @category list
     * @category length
     * @public
     * @see AmbiguousKeyFilterKey.getFields
     * @see AmbiguousKeyFilterKey.unsafeFields
     * @see AmbiguousKeyFilterKey.getField
     * @see AmbiguousKeyFilterKey.unsafeField
     * @see AmbiguousKeyFilterKey.containFields
     * @see AmbiguousKeyFilterKey.addFields
     * @see AmbiguousKeyFilterKey.removeFields
     * @see AmbiguousKeyFilterKey.truncateFields
     * @see fields
     */
    getFieldsSize(): number{
        return this.unsafeFields().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of @AmbiguousKeyFilterKey.fields**. Unlike [getField](AmbiguousKeyFilterKey.getField) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of @AmbiguousKeyFilterKey.fields**. Unlike [getField](AmbiguousKeyFilterKey.getField) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of @AmbiguousKeyFilterKey.fields**. Unlike [getField](AmbiguousKeyFilterKey.getField) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @AmbiguousKeyFilterKey.fields, you can give `-1` for example. If you want to get the first item of @AmbiguousKeyFilterKey.fields, you can give `0`.*
     * @category @AmbiguousKeyFilterKey.fields
     * @category unsafe
     * @category list
     * @category value
     * @public
     * @see AmbiguousKeyFilterKey.getFields
     * @see AmbiguousKeyFilterKey.unsafeFields
     * @see AmbiguousKeyFilterKey.getField
     * @see AmbiguousKeyFilterKey.getFieldsSize
     * @see AmbiguousKeyFilterKey.containFields
     * @see AmbiguousKeyFilterKey.addFields
     * @see AmbiguousKeyFilterKey.removeFields
     * @see AmbiguousKeyFilterKey.truncateFields
     * @see fields
     */
    unsafeField(position: number): BaseFieldConfiguration|BaseFieldInstance|undefined{
        if (position < 0){
            const new_position = this.getFieldsSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeField(new_position);
        }

        return this.fields[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item in @AmbiguousKeyFilterKey.fields**. Unlike the [unsafeField](AmbiguousKeyFilterKey.unsafeField) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item in @AmbiguousKeyFilterKey.fields**. Unlike the [unsafeField](AmbiguousKeyFilterKey.unsafeField) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item in @AmbiguousKeyFilterKey.fields**. Unlike the [unsafeField](AmbiguousKeyFilterKey.unsafeField) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @AmbiguousKeyFilterKey.fields, you can give `-1` for example. If you want to get the first item of @AmbiguousKeyFilterKey.fields, you can give `0`.*
     * @throw OutOfRange Throws an [OutOfRange](OutOfRange) error if the given position is out of range.
     * @category @AmbiguousKeyFilterKey.fields
     * @category getter
     * @category list
     * @category value
     * @public
     * @see OutOfRange
     * @see AmbiguousKeyFilterKey.getFields
     * @see AmbiguousKeyFilterKey.unsafeFields
     * @see AmbiguousKeyFilterKey.unsafeField
     * @see AmbiguousKeyFilterKey.getFieldsSize
     * @see AmbiguousKeyFilterKey.containFields
     * @see AmbiguousKeyFilterKey.addFields
     * @see AmbiguousKeyFilterKey.removeFields
     * @see AmbiguousKeyFilterKey.truncateFields
     * @see fields
     */
    getField(position: number): BaseFieldConfiguration|BaseFieldInstance{
        const field = this.unsafeField(position);
        if (typeof field === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getField");
            error.setClassname("AmbiguousKeyFilterKey");
            error.setProperty("fields");
            error.setPosition(position);
            throw error;
        }

        return field;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Add one or multiple values** to @AmbiguousKeyFilterKey.fields.
     * @description Allows you to **add one or multiple values** to @AmbiguousKeyFilterKey.fields. Each argument of in the method will be added to @AmbiguousKeyFilterKey.fields, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in @AmbiguousKeyFilterKey.fields: the value is unique._
     * @param fields All the given values that you to add to @AmbiguousKeyFilterKey.fields *(the value can be a deep array, you should take a look to [flat](flat) function)*. *If the value is already included, the method will discard the item addition.*
     * @return Returns the actual [AmbiguousKeyFilterKey](AmbiguousKeyFilterKey) class instance.
     * @category @AmbiguousKeyFilterKey.fields
     * @category associate
     * @category list
     * @public
     * @see AmbiguousKeyFilterKey.getFields
     * @see AmbiguousKeyFilterKey.unsafeFields
     * @see AmbiguousKeyFilterKey.getField
     * @see AmbiguousKeyFilterKey.unsafeField
     * @see AmbiguousKeyFilterKey.getFieldsSize
     * @see AmbiguousKeyFilterKey.containFields
     * @see AmbiguousKeyFilterKey.removeFields
     * @see AmbiguousKeyFilterKey.truncateFields
     * @see fields
     */
    addFields(... fields: DeepArray<BaseFieldConfiguration|BaseFieldInstance>[]): this{
        for (const field of flat(fields)){
            if (this.containFields(field)){
                continue;
            }


            this.fields.push(field);
        }

        this.updateFields()

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if the given value is included** in @AmbiguousKeyFilterKey.fields.
     * @description **Checks if all the given value is included** in @AmbiguousKeyFilterKey.fields. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @AmbiguousKeyFilterKey.fields or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @AmbiguousKeyFilterKey.fields and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @return **Returns a boolean if all the given value is included** in @AmbiguousKeyFilterKey.fields. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @AmbiguousKeyFilterKey.fields or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @AmbiguousKeyFilterKey.fields and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @param fields All the values that you want to check *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @AmbiguousKeyFilterKey.fields
     * @category defined
     * @category list
     * @public
     * @see AmbiguousKeyFilterKey.getFields
     * @see AmbiguousKeyFilterKey.unsafeFields
     * @see AmbiguousKeyFilterKey.getField
     * @see AmbiguousKeyFilterKey.unsafeField
     * @see AmbiguousKeyFilterKey.getFieldsSize
     * @see AmbiguousKeyFilterKey.addFields
     * @see AmbiguousKeyFilterKey.removeFields
     * @see AmbiguousKeyFilterKey.truncateFields
     * @see fields
     */
    containFields(... fields: DeepArray<BaseFieldConfiguration|BaseFieldInstance|undefined>[]): boolean{
        return flat(fields).every((field) => {
            if (typeof field === "undefined"){
                return false;
            }

            return this.unsafeFields().includes(field);
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of @AmbiguousKeyFilterKey.fields**.
     * @description **Gives a copy of @AmbiguousKeyFilterKey.fields**.
     * @return **Returns a copy of @AmbiguousKeyFilterKey.fields**.
     * @category @AmbiguousKeyFilterKey.fields
     * @category getter
     * @category iterable
     * @category list
     * @public
     * @see AmbiguousKeyFilterKey.unsafeFields
     * @see AmbiguousKeyFilterKey.getField
     * @see AmbiguousKeyFilterKey.unsafeField
     * @see AmbiguousKeyFilterKey.getFieldsSize
     * @see AmbiguousKeyFilterKey.containFields
     * @see AmbiguousKeyFilterKey.addFields
     * @see AmbiguousKeyFilterKey.removeFields
     * @see AmbiguousKeyFilterKey.truncateFields
     * @see fields
     */
    getFields(): (BaseFieldConfiguration|BaseFieldInstance)[]{
        return Array.from(this.unsafeFields());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives @AmbiguousKeyFilterKey.fields without copy** the array.
     * @description **Gives @AmbiguousKeyFilterKey.fields without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getFields](AmbiguousKeyFilterKey.getFields) method.
     * @return **Returns @AmbiguousKeyFilterKey.fields without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getFields](AmbiguousKeyFilterKey.getFields) method.
     * @category @AmbiguousKeyFilterKey.fields
     * @category unsafe
     * @category iterable
     * @category list
     * @public
     * @see AmbiguousKeyFilterKey.getFields
     * @see AmbiguousKeyFilterKey.getField
     * @see AmbiguousKeyFilterKey.unsafeField
     * @see AmbiguousKeyFilterKey.getFieldsSize
     * @see AmbiguousKeyFilterKey.containFields
     * @see AmbiguousKeyFilterKey.addFields
     * @see AmbiguousKeyFilterKey.removeFields
     * @see AmbiguousKeyFilterKey.truncateFields
     * @see fields
     */
    unsafeFields(): (BaseFieldConfiguration|BaseFieldInstance)[]{
        return this.fields;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Allows to **remove one or multiple values** from @AmbiguousKeyFilterKey.fields.
     * @description Allows you to **remove one or multiple values** from @AmbiguousKeyFilterKey.fields. Each argument of in the method will be removed from @AmbiguousKeyFilterKey.fields, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @param fields All the given values that you to remove from @AmbiguousKeyFilterKey.fields *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @return Returns the actual [AmbiguousKeyFilterKey](AmbiguousKeyFilterKey) class instance.
     * @category @AmbiguousKeyFilterKey.fields
     * @category dissociate
     * @category list
     * @public
     * @see AmbiguousKeyFilterKey.getFields
     * @see AmbiguousKeyFilterKey.unsafeFields
     * @see AmbiguousKeyFilterKey.getField
     * @see AmbiguousKeyFilterKey.unsafeField
     * @see AmbiguousKeyFilterKey.getFieldsSize
     * @see AmbiguousKeyFilterKey.containFields
     * @see AmbiguousKeyFilterKey.addFields
     * @see AmbiguousKeyFilterKey.truncateFields
     * @see fields
     */
    removeFields(... fields: DeepArray<BaseFieldConfiguration|BaseFieldInstance>[]): this{
        for (const field of flat(fields)){
            if (!this.containFields(field)){
                continue;
            }

            while (true){
                const index = this.fields.indexOf(field);
                if (index < 0){
                    break;
                }

                this.fields.splice(index, 1);
            }
        }

        this.updateFields()

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Truncate @AmbiguousKeyFilterKey.fields**.
     * @description **Remove all the values in @AmbiguousKeyFilterKey.fields**.
     * @return Returns the actual [AmbiguousKeyFilterKey](AmbiguousKeyFilterKey) class instance.
     * @category @AmbiguousKeyFilterKey.fields
     * @category truncate
     * @category list
     * @public
     * @see AmbiguousKeyFilterKey.getFields
     * @see AmbiguousKeyFilterKey.unsafeFields
     * @see AmbiguousKeyFilterKey.getField
     * @see AmbiguousKeyFilterKey.unsafeField
     * @see AmbiguousKeyFilterKey.getFieldsSize
     * @see AmbiguousKeyFilterKey.containFields
     * @see AmbiguousKeyFilterKey.addFields
     * @see AmbiguousKeyFilterKey.removeFields
     * @see fields
     */
    truncateFields(): this{
        this.removeFields(this.unsafeFields());
        return this;
    }
















    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @AmbiguousKeyFilterKey.given.
     * @description Save @AmbiguousKeyFilterKey.given.
     * @category @AmbiguousKeyFilterKey.given
     * @category property
     * @category field
     * @protected
     * @see AmbiguousKeyFilterKey.setGiven
     * @see AmbiguousKeyFilterKey.getGiven
     * @see AmbiguousKeyFilterKey.unsafeGiven
     * @see AmbiguousKeyFilterKey.eraseGiven
     * @see AmbiguousKeyFilterKey.isGivenDefined
     * @see AmbiguousKeyFilterKey.isGivenEqualTo
     */
    protected given: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @AmbiguousKeyFilterKey.given**.
     * @description Allows you to update or **assign the value of @AmbiguousKeyFilterKey.given**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [AmbiguousKeyFilterKey](AmbiguousKeyFilterKey) class instance.
     * @param given The new value to assign to @AmbiguousKeyFilterKey.given.
     * @category @AmbiguousKeyFilterKey.given
     * @category setter
     * @category field
     * @public
     * @see AmbiguousKeyFilterKey.given
     * @see AmbiguousKeyFilterKey.getGiven
     * @see AmbiguousKeyFilterKey.unsafeGiven
     * @see AmbiguousKeyFilterKey.eraseGiven
     * @see AmbiguousKeyFilterKey.isGivenDefined
     * @see AmbiguousKeyFilterKey.isGivenEqualTo
     */
    setGiven(given: string): this{
        if (this.isGivenEqualTo(given)){
            return this;
        }

        this.setDefaultParameter("given",given)

        this.eraseGiven();
        this.given = given;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @AmbiguousKeyFilterKey.given**. Unlike the [getGiven](AmbiguousKeyFilterKey.getGiven) method, **the method results an undefined value if @AmbiguousKeyFilterKey.given is not defined**.
     * @description **Gives the value of @AmbiguousKeyFilterKey.given**. Unlike the [getGiven](AmbiguousKeyFilterKey.getGiven) method, **the method results an undefined value if @AmbiguousKeyFilterKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @AmbiguousKeyFilterKey.given**. Unlike the [getGiven](AmbiguousKeyFilterKey.getGiven) method, **the method results an undefined value if @AmbiguousKeyFilterKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @AmbiguousKeyFilterKey.given
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see AmbiguousKeyFilterKey.given
     * @see AmbiguousKeyFilterKey.setGiven
     * @see AmbiguousKeyFilterKey.getGiven
     * @see AmbiguousKeyFilterKey.eraseGiven
     * @see AmbiguousKeyFilterKey.isGivenDefined
     * @see AmbiguousKeyFilterKey.isGivenEqualTo
     */
    unsafeGiven(): string|undefined{
        return this.given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @AmbiguousKeyFilterKey.given**. Unlike the [unsafeGiven](AmbiguousKeyFilterKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @AmbiguousKeyFilterKey.given is not defined**.
     * @description **Gives the value of @AmbiguousKeyFilterKey.given**. Unlike the [unsafeGiven](AmbiguousKeyFilterKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @AmbiguousKeyFilterKey.given is not defined**.
     * @return **Returns the value of @AmbiguousKeyFilterKey.given**. Unlike the [unsafeGiven](AmbiguousKeyFilterKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @AmbiguousKeyFilterKey.given is not defined**.
     * @category @AmbiguousKeyFilterKey.given
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @AmbiguousKeyFilterKey.given isn't defined.
     * @see AmbiguousKeyFilterKey.given
     * @see AmbiguousKeyFilterKey.setGiven
     * @see AmbiguousKeyFilterKey.unsafeGiven
     * @see AmbiguousKeyFilterKey.eraseGiven
     * @see AmbiguousKeyFilterKey.isGivenDefined
     * @see AmbiguousKeyFilterKey.isGivenEqualTo
     */
    getGiven(): string{
        const given = this.unsafeGiven();

        if (typeof given === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getGiven");
            error.setClassname("AmbiguousKeyFilterKey");
            error.setProperty("given");
            throw error;
        }
        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @AmbiguousKeyFilterKey.given is defined**.
     * @description **Checks if @AmbiguousKeyFilterKey.given is defined** or not.
     * @return **Returns a boolean if @AmbiguousKeyFilterKey.given is defined** or not.
     * @category @AmbiguousKeyFilterKey.given
     * @category defined
     * @category field
     * @public
     * @see AmbiguousKeyFilterKey.given
     * @see AmbiguousKeyFilterKey.setGiven
     * @see AmbiguousKeyFilterKey.getGiven
     * @see AmbiguousKeyFilterKey.unsafeGiven
     * @see AmbiguousKeyFilterKey.eraseGiven
     * @see AmbiguousKeyFilterKey.isGivenEqualTo
     */
    isGivenDefined(): boolean{
        return typeof this.given !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @AmbiguousKeyFilterKey.given is equal to your given value**.
     * @description **Checks if @AmbiguousKeyFilterKey.given is equal to your given value** and if @AmbiguousKeyFilterKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @AmbiguousKeyFilterKey.given is equal to your given value** and if @AmbiguousKeyFilterKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param given **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @AmbiguousKeyFilterKey.given
     * @category equality
     * @category field
     * @public
     * @see AmbiguousKeyFilterKey.given
     * @see AmbiguousKeyFilterKey.setGiven
     * @see AmbiguousKeyFilterKey.getGiven
     * @see AmbiguousKeyFilterKey.unsafeGiven
     * @see AmbiguousKeyFilterKey.eraseGiven
     * @see AmbiguousKeyFilterKey.isGivenDefined
     */
    isGivenEqualTo(given?: undefined|string): boolean{
        if (typeof given === "undefined"){
            return false;
        }
        return this.unsafeGiven() === given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @AmbiguousKeyFilterKey.given**.
     * @description **Removes the assigned the value of @AmbiguousKeyFilterKey.given**: *@AmbiguousKeyFilterKey.given will be flagged as undefined*.
     * @return Return the actual [AmbiguousKeyFilterKey](AmbiguousKeyFilterKey) class instance.
     * @category @AmbiguousKeyFilterKey.given
     * @category truncate
     * @category field
     * @public
     * @see AmbiguousKeyFilterKey.given
     * @see AmbiguousKeyFilterKey.setGiven
     * @see AmbiguousKeyFilterKey.getGiven
     * @see AmbiguousKeyFilterKey.unsafeGiven
     * @see AmbiguousKeyFilterKey.isGivenDefined
     * @see AmbiguousKeyFilterKey.isGivenEqualTo
     */
    eraseGiven(): this{
        this.given = undefined;
        this.removeDefaultParameter("given")

        return this;
    }
}

Translate.setTranslate({
    'fr.model.ambiguous.key.filter': 'votre clef de filtre `#given` fait référence à un ou plusieurs valeurs. Merci de bien vouloir renseigner une clef faisant référence à une valeur présice.',
    'en.model.ambiguous.key.filter': 'your `#given` filter key refers to one or more values. Please fill in a key referring to a precise value.',

    "en.and": "and",
    "fr.and": "et",
})

export default AmbiguousKeyFilterKey;