import UndefinedValue from "../../undefined-value";
import ModelError from "../../../model";
import BaseFieldConfiguration from "../../../../model/field/base";
import BaseFieldInstance from "../../../../model/field/base/instance";
import OutOfRange from "../../out-of-range";
import DeepArray from "../../../../util/deep-array/type";
import flat from "../../../../util/deep-array/flat";
import Translate from "../../../../translate";

class AmbiguousKeyInputKey extends ModelError{
    constructor(key = "model.ambiguous.key.input",http_code = 417) {
        super(key,http_code);
    }

    private updateFields(){
        this.setDefaultParameter("fields",[
            this.unsafeFields().slice(0,-1).map(field => field.getTranslate()).join(", "),
            ... this.unsafeFields().slice(-1).map(field => field.getTranslate()),
        ].join(" {and} "))
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Save @AmbiguousKeyInputKey.fields.
     * @description Save @AmbiguousKeyInputKey.fields.
     * @category fields
     * @category property
     * @category list
     * @protected
     * @see AmbiguousKeyInputKey.getFields
     * @see AmbiguousKeyInputKey.unsafeFields
     * @see AmbiguousKeyInputKey.getField
     * @see AmbiguousKeyInputKey.unsafeField
     * @see AmbiguousKeyInputKey.getFieldsSize
     * @see AmbiguousKeyInputKey.containFields
     * @see AmbiguousKeyInputKey.addFields
     * @see AmbiguousKeyInputKey.removeFields
     * @see AmbiguousKeyInputKey.truncateFields
     */
    protected fields: (BaseFieldConfiguration|BaseFieldInstance)[] = [];

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the size of @AmbiguousKeyInputKey.fields**.
     * @description **Gives the size of @AmbiguousKeyInputKey.fields**.
     * @return **Returns the size of @AmbiguousKeyInputKey.fields**.
     * @category @AmbiguousKeyInputKey.fields
     * @category list
     * @category length
     * @public
     * @see AmbiguousKeyInputKey.getFields
     * @see AmbiguousKeyInputKey.unsafeFields
     * @see AmbiguousKeyInputKey.getField
     * @see AmbiguousKeyInputKey.unsafeField
     * @see AmbiguousKeyInputKey.containFields
     * @see AmbiguousKeyInputKey.addFields
     * @see AmbiguousKeyInputKey.removeFields
     * @see AmbiguousKeyInputKey.truncateFields
     * @see fields
     */
    getFieldsSize(): number{
        return this.unsafeFields().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of @AmbiguousKeyInputKey.fields**. Unlike [getField](AmbiguousKeyInputKey.getField) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of @AmbiguousKeyInputKey.fields**. Unlike [getField](AmbiguousKeyInputKey.getField) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of @AmbiguousKeyInputKey.fields**. Unlike [getField](AmbiguousKeyInputKey.getField) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @AmbiguousKeyInputKey.fields, you can give `-1` for example. If you want to get the first item of @AmbiguousKeyInputKey.fields, you can give `0`.*
     * @category @AmbiguousKeyInputKey.fields
     * @category unsafe
     * @category list
     * @category value
     * @public
     * @see AmbiguousKeyInputKey.getFields
     * @see AmbiguousKeyInputKey.unsafeFields
     * @see AmbiguousKeyInputKey.getField
     * @see AmbiguousKeyInputKey.getFieldsSize
     * @see AmbiguousKeyInputKey.containFields
     * @see AmbiguousKeyInputKey.addFields
     * @see AmbiguousKeyInputKey.removeFields
     * @see AmbiguousKeyInputKey.truncateFields
     * @see fields
     */
    unsafeField(position: number): BaseFieldConfiguration|BaseFieldInstance|undefined{
        if (position < 0){
            const new_position = this.getFieldsSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeField(new_position);
        }

        return this.fields[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item in @AmbiguousKeyInputKey.fields**. Unlike the [unsafeField](AmbiguousKeyInputKey.unsafeField) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item in @AmbiguousKeyInputKey.fields**. Unlike the [unsafeField](AmbiguousKeyInputKey.unsafeField) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item in @AmbiguousKeyInputKey.fields**. Unlike the [unsafeField](AmbiguousKeyInputKey.unsafeField) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @AmbiguousKeyInputKey.fields, you can give `-1` for example. If you want to get the first item of @AmbiguousKeyInputKey.fields, you can give `0`.*
     * @throw OutOfRange Throws an [OutOfRange](OutOfRange) error if the given position is out of range.
     * @category @AmbiguousKeyInputKey.fields
     * @category getter
     * @category list
     * @category value
     * @public
     * @see OutOfRange
     * @see AmbiguousKeyInputKey.getFields
     * @see AmbiguousKeyInputKey.unsafeFields
     * @see AmbiguousKeyInputKey.unsafeField
     * @see AmbiguousKeyInputKey.getFieldsSize
     * @see AmbiguousKeyInputKey.containFields
     * @see AmbiguousKeyInputKey.addFields
     * @see AmbiguousKeyInputKey.removeFields
     * @see AmbiguousKeyInputKey.truncateFields
     * @see fields
     */
    getField(position: number): BaseFieldConfiguration|BaseFieldInstance{
        const field = this.unsafeField(position);
        if (typeof field === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getField");
            error.setClassname("AmbiguousKeyInputKey");
            error.setProperty("fields");
            error.setPosition(position);
            throw error;
        }

        return field;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Add one or multiple values** to @AmbiguousKeyInputKey.fields.
     * @description Allows you to **add one or multiple values** to @AmbiguousKeyInputKey.fields. Each argument of in the method will be added to @AmbiguousKeyInputKey.fields, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in @AmbiguousKeyInputKey.fields: the value is unique._
     * @param fields All the given values that you to add to @AmbiguousKeyInputKey.fields *(the value can be a deep array, you should take a look to [flat](flat) function)*. *If the value is already included, the method will discard the item addition.*
     * @return Returns the actual [AmbiguousKeyInputKey](AmbiguousKeyInputKey) class instance.
     * @category @AmbiguousKeyInputKey.fields
     * @category associate
     * @category list
     * @public
     * @see AmbiguousKeyInputKey.getFields
     * @see AmbiguousKeyInputKey.unsafeFields
     * @see AmbiguousKeyInputKey.getField
     * @see AmbiguousKeyInputKey.unsafeField
     * @see AmbiguousKeyInputKey.getFieldsSize
     * @see AmbiguousKeyInputKey.containFields
     * @see AmbiguousKeyInputKey.removeFields
     * @see AmbiguousKeyInputKey.truncateFields
     * @see fields
     */
    addFields(... fields: DeepArray<BaseFieldConfiguration|BaseFieldInstance>[]): this{
        for (const field of flat(fields)){
            if (this.containFields(field)){
                continue;
            }


            this.fields.push(field);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if the given value is included** in @AmbiguousKeyInputKey.fields.
     * @description **Checks if all the given value is included** in @AmbiguousKeyInputKey.fields. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @AmbiguousKeyInputKey.fields or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @AmbiguousKeyInputKey.fields and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @return **Returns a boolean if all the given value is included** in @AmbiguousKeyInputKey.fields. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @AmbiguousKeyInputKey.fields or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @AmbiguousKeyInputKey.fields and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @param fields All the values that you want to check *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @AmbiguousKeyInputKey.fields
     * @category defined
     * @category list
     * @public
     * @see AmbiguousKeyInputKey.getFields
     * @see AmbiguousKeyInputKey.unsafeFields
     * @see AmbiguousKeyInputKey.getField
     * @see AmbiguousKeyInputKey.unsafeField
     * @see AmbiguousKeyInputKey.getFieldsSize
     * @see AmbiguousKeyInputKey.addFields
     * @see AmbiguousKeyInputKey.removeFields
     * @see AmbiguousKeyInputKey.truncateFields
     * @see fields
     */
    containFields(... fields: DeepArray<BaseFieldConfiguration|BaseFieldInstance|undefined>[]): boolean{
        return flat(fields).every((field) => {
            if (typeof field === "undefined"){
                return false;
            }

            return this.unsafeFields().includes(field);
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of @AmbiguousKeyInputKey.fields**.
     * @description **Gives a copy of @AmbiguousKeyInputKey.fields**.
     * @return **Returns a copy of @AmbiguousKeyInputKey.fields**.
     * @category @AmbiguousKeyInputKey.fields
     * @category getter
     * @category iterable
     * @category list
     * @public
     * @see AmbiguousKeyInputKey.unsafeFields
     * @see AmbiguousKeyInputKey.getField
     * @see AmbiguousKeyInputKey.unsafeField
     * @see AmbiguousKeyInputKey.getFieldsSize
     * @see AmbiguousKeyInputKey.containFields
     * @see AmbiguousKeyInputKey.addFields
     * @see AmbiguousKeyInputKey.removeFields
     * @see AmbiguousKeyInputKey.truncateFields
     * @see fields
     */
    getFields(): (BaseFieldConfiguration|BaseFieldInstance)[]{
        return Array.from(this.unsafeFields());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives @AmbiguousKeyInputKey.fields without copy** the array.
     * @description **Gives @AmbiguousKeyInputKey.fields without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getFields](AmbiguousKeyInputKey.getFields) method.
     * @return **Returns @AmbiguousKeyInputKey.fields without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getFields](AmbiguousKeyInputKey.getFields) method.
     * @category @AmbiguousKeyInputKey.fields
     * @category unsafe
     * @category iterable
     * @category list
     * @public
     * @see AmbiguousKeyInputKey.getFields
     * @see AmbiguousKeyInputKey.getField
     * @see AmbiguousKeyInputKey.unsafeField
     * @see AmbiguousKeyInputKey.getFieldsSize
     * @see AmbiguousKeyInputKey.containFields
     * @see AmbiguousKeyInputKey.addFields
     * @see AmbiguousKeyInputKey.removeFields
     * @see AmbiguousKeyInputKey.truncateFields
     * @see fields
     */
    unsafeFields(): (BaseFieldConfiguration|BaseFieldInstance)[]{
        return this.fields;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Allows to **remove one or multiple values** from @AmbiguousKeyInputKey.fields.
     * @description Allows you to **remove one or multiple values** from @AmbiguousKeyInputKey.fields. Each argument of in the method will be removed from @AmbiguousKeyInputKey.fields, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @param fields All the given values that you to remove from @AmbiguousKeyInputKey.fields *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @return Returns the actual [AmbiguousKeyInputKey](AmbiguousKeyInputKey) class instance.
     * @category @AmbiguousKeyInputKey.fields
     * @category dissociate
     * @category list
     * @public
     * @see AmbiguousKeyInputKey.getFields
     * @see AmbiguousKeyInputKey.unsafeFields
     * @see AmbiguousKeyInputKey.getField
     * @see AmbiguousKeyInputKey.unsafeField
     * @see AmbiguousKeyInputKey.getFieldsSize
     * @see AmbiguousKeyInputKey.containFields
     * @see AmbiguousKeyInputKey.addFields
     * @see AmbiguousKeyInputKey.truncateFields
     * @see fields
     */
    removeFields(... fields: DeepArray<BaseFieldConfiguration|BaseFieldInstance>[]): this{
        for (const field of flat(fields)){
            if (!this.containFields(field)){
                continue;
            }

            while (true){
                const index = this.fields.indexOf(field);
                if (index < 0){
                    break;
                }

                this.fields.splice(index, 1);
            }
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Truncate @AmbiguousKeyInputKey.fields**.
     * @description **Remove all the values in @AmbiguousKeyInputKey.fields**.
     * @return Returns the actual [AmbiguousKeyInputKey](AmbiguousKeyInputKey) class instance.
     * @category @AmbiguousKeyInputKey.fields
     * @category truncate
     * @category list
     * @public
     * @see AmbiguousKeyInputKey.getFields
     * @see AmbiguousKeyInputKey.unsafeFields
     * @see AmbiguousKeyInputKey.getField
     * @see AmbiguousKeyInputKey.unsafeField
     * @see AmbiguousKeyInputKey.getFieldsSize
     * @see AmbiguousKeyInputKey.containFields
     * @see AmbiguousKeyInputKey.addFields
     * @see AmbiguousKeyInputKey.removeFields
     * @see fields
     */
    truncateFields(): this{
        this.removeFields(this.unsafeFields());
        return this;
    }
















    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @AmbiguousKeyInputKey.given.
     * @description Save @AmbiguousKeyInputKey.given.
     * @category @AmbiguousKeyInputKey.given
     * @category property
     * @category field
     * @protected
     * @see AmbiguousKeyInputKey.setGiven
     * @see AmbiguousKeyInputKey.getGiven
     * @see AmbiguousKeyInputKey.unsafeGiven
     * @see AmbiguousKeyInputKey.eraseGiven
     * @see AmbiguousKeyInputKey.isGivenDefined
     * @see AmbiguousKeyInputKey.isGivenEqualTo
     */
    protected given: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @AmbiguousKeyInputKey.given**.
     * @description Allows you to update or **assign the value of @AmbiguousKeyInputKey.given**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [AmbiguousKeyInputKey](AmbiguousKeyInputKey) class instance.
     * @param given The new value to assign to @AmbiguousKeyInputKey.given.
     * @category @AmbiguousKeyInputKey.given
     * @category setter
     * @category field
     * @public
     * @see AmbiguousKeyInputKey.given
     * @see AmbiguousKeyInputKey.getGiven
     * @see AmbiguousKeyInputKey.unsafeGiven
     * @see AmbiguousKeyInputKey.eraseGiven
     * @see AmbiguousKeyInputKey.isGivenDefined
     * @see AmbiguousKeyInputKey.isGivenEqualTo
     */
    setGiven(given: string): this{
        if (this.isGivenEqualTo(given)){
            return this;
        }

        this.setDefaultParameter("given",given)

        this.eraseGiven();
        this.given = given;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @AmbiguousKeyInputKey.given**. Unlike the [getGiven](AmbiguousKeyInputKey.getGiven) method, **the method results an undefined value if @AmbiguousKeyInputKey.given is not defined**.
     * @description **Gives the value of @AmbiguousKeyInputKey.given**. Unlike the [getGiven](AmbiguousKeyInputKey.getGiven) method, **the method results an undefined value if @AmbiguousKeyInputKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @AmbiguousKeyInputKey.given**. Unlike the [getGiven](AmbiguousKeyInputKey.getGiven) method, **the method results an undefined value if @AmbiguousKeyInputKey.given is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @AmbiguousKeyInputKey.given
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see AmbiguousKeyInputKey.given
     * @see AmbiguousKeyInputKey.setGiven
     * @see AmbiguousKeyInputKey.getGiven
     * @see AmbiguousKeyInputKey.eraseGiven
     * @see AmbiguousKeyInputKey.isGivenDefined
     * @see AmbiguousKeyInputKey.isGivenEqualTo
     */
    unsafeGiven(): string|undefined{
        return this.given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @AmbiguousKeyInputKey.given**. Unlike the [unsafeGiven](AmbiguousKeyInputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @AmbiguousKeyInputKey.given is not defined**.
     * @description **Gives the value of @AmbiguousKeyInputKey.given**. Unlike the [unsafeGiven](AmbiguousKeyInputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @AmbiguousKeyInputKey.given is not defined**.
     * @return **Returns the value of @AmbiguousKeyInputKey.given**. Unlike the [unsafeGiven](AmbiguousKeyInputKey.unsafeGiven) method, **we will throw an [UndefinedValue](UndefinedValue) error if @AmbiguousKeyInputKey.given is not defined**.
     * @category @AmbiguousKeyInputKey.given
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @AmbiguousKeyInputKey.given isn't defined.
     * @see AmbiguousKeyInputKey.given
     * @see AmbiguousKeyInputKey.setGiven
     * @see AmbiguousKeyInputKey.unsafeGiven
     * @see AmbiguousKeyInputKey.eraseGiven
     * @see AmbiguousKeyInputKey.isGivenDefined
     * @see AmbiguousKeyInputKey.isGivenEqualTo
     */
    getGiven(): string{
        const given = this.unsafeGiven();

        if (typeof given === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getGiven");
            error.setClassname("AmbiguousKeyInputKey");
            error.setProperty("given");
            throw error;
        }
        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @AmbiguousKeyInputKey.given is defined**.
     * @description **Checks if @AmbiguousKeyInputKey.given is defined** or not.
     * @return **Returns a boolean if @AmbiguousKeyInputKey.given is defined** or not.
     * @category @AmbiguousKeyInputKey.given
     * @category defined
     * @category field
     * @public
     * @see AmbiguousKeyInputKey.given
     * @see AmbiguousKeyInputKey.setGiven
     * @see AmbiguousKeyInputKey.getGiven
     * @see AmbiguousKeyInputKey.unsafeGiven
     * @see AmbiguousKeyInputKey.eraseGiven
     * @see AmbiguousKeyInputKey.isGivenEqualTo
     */
    isGivenDefined(): boolean{
        return typeof this.given !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @AmbiguousKeyInputKey.given is equal to your given value**.
     * @description **Checks if @AmbiguousKeyInputKey.given is equal to your given value** and if @AmbiguousKeyInputKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @AmbiguousKeyInputKey.given is equal to your given value** and if @AmbiguousKeyInputKey.given is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param given **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @AmbiguousKeyInputKey.given
     * @category equality
     * @category field
     * @public
     * @see AmbiguousKeyInputKey.given
     * @see AmbiguousKeyInputKey.setGiven
     * @see AmbiguousKeyInputKey.getGiven
     * @see AmbiguousKeyInputKey.unsafeGiven
     * @see AmbiguousKeyInputKey.eraseGiven
     * @see AmbiguousKeyInputKey.isGivenDefined
     */
    isGivenEqualTo(given?: undefined|string): boolean{
        if (typeof given === "undefined"){
            return false;
        }
        return this.unsafeGiven() === given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @AmbiguousKeyInputKey.given**.
     * @description **Removes the assigned the value of @AmbiguousKeyInputKey.given**: *@AmbiguousKeyInputKey.given will be flagged as undefined*.
     * @return Return the actual [AmbiguousKeyInputKey](AmbiguousKeyInputKey) class instance.
     * @category @AmbiguousKeyInputKey.given
     * @category truncate
     * @category field
     * @public
     * @see AmbiguousKeyInputKey.given
     * @see AmbiguousKeyInputKey.setGiven
     * @see AmbiguousKeyInputKey.getGiven
     * @see AmbiguousKeyInputKey.unsafeGiven
     * @see AmbiguousKeyInputKey.isGivenDefined
     * @see AmbiguousKeyInputKey.isGivenEqualTo
     */
    eraseGiven(): this{
        this.given = undefined;
        this.removeDefaultParameter("given")

        return this;
    }
}

Translate.setTranslate({
    'fr.model.ambiguous.key.input': 'vos clefs #given font tous référence à #field. Merci de bien vouloir renseigner une clef faisant référence à une valeur présice.',
    'en.model.ambiguous.key.input': 'your #given keys all point to #field. Please fill in a key referring to a precise value.',

    "en.and": "and",
    "fr.and": "et",
})

export default AmbiguousKeyInputKey;