import FieldError from "../../../field";
import OutOfRange from "../../out-of-range";
import flat from "../../../../util/deep-array/flat";
import DeepArray from "../../../../util/deep-array/type";
import Translate from "../../../../translate";

class AmbiguousValueOutputKey extends FieldError{
    constructor(key = "model.ambiguous.value.output",http_code = 500) {
        super(key,http_code);
    }

    private updateGivens(){
        this.setDefaultParameter("given",[
            this.unsafeGivens().slice(0,-1).join(", "),
            ... this.unsafeGivens().slice(-1),
        ].join(" {and} "))
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Save @AmbiguousValueOutputKey.givens.
     * @description Save @AmbiguousValueOutputKey.givens.
     * @category givens
     * @category property
     * @category list
     * @protected
     * @see AmbiguousValueOutputKey.getGivens
     * @see AmbiguousValueOutputKey.unsafeGivens
     * @see AmbiguousValueOutputKey.getGiven
     * @see AmbiguousValueOutputKey.unsafeGiven
     * @see AmbiguousValueOutputKey.getGivensSize
     * @see AmbiguousValueOutputKey.containGivens
     * @see AmbiguousValueOutputKey.addGivens
     * @see AmbiguousValueOutputKey.removeGivens
     * @see AmbiguousValueOutputKey.truncateGivens
     */
    protected givens: string[] = [];

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the size of @AmbiguousValueOutputKey.givens**.
     * @description **Gives the size of @AmbiguousValueOutputKey.givens**.
     * @return **Returns the size of @AmbiguousValueOutputKey.givens**.
     * @category @AmbiguousValueOutputKey.givens
     * @category list
     * @category length
     * @public
     * @see AmbiguousValueOutputKey.getGivens
     * @see AmbiguousValueOutputKey.unsafeGivens
     * @see AmbiguousValueOutputKey.getGiven
     * @see AmbiguousValueOutputKey.unsafeGiven
     * @see AmbiguousValueOutputKey.containGivens
     * @see AmbiguousValueOutputKey.addGivens
     * @see AmbiguousValueOutputKey.removeGivens
     * @see AmbiguousValueOutputKey.truncateGivens
     * @see givens
     */
    getGivensSize(): number{
        return this.unsafeGivens().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of @AmbiguousValueOutputKey.givens**. Unlike [getGiven](AmbiguousValueOutputKey.getGiven) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of @AmbiguousValueOutputKey.givens**. Unlike [getGiven](AmbiguousValueOutputKey.getGiven) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of @AmbiguousValueOutputKey.givens**. Unlike [getGiven](AmbiguousValueOutputKey.getGiven) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @AmbiguousValueOutputKey.givens, you can give `-1` for example. If you want to get the first item of @AmbiguousValueOutputKey.givens, you can give `0`.*
     * @category @AmbiguousValueOutputKey.givens
     * @category unsafe
     * @category list
     * @category value
     * @public
     * @see AmbiguousValueOutputKey.getGivens
     * @see AmbiguousValueOutputKey.unsafeGivens
     * @see AmbiguousValueOutputKey.getGiven
     * @see AmbiguousValueOutputKey.getGivensSize
     * @see AmbiguousValueOutputKey.containGivens
     * @see AmbiguousValueOutputKey.addGivens
     * @see AmbiguousValueOutputKey.removeGivens
     * @see AmbiguousValueOutputKey.truncateGivens
     * @see givens
     */
    unsafeGiven(position: number): string|undefined{
        if (position < 0){
            const new_position = this.getGivensSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeGiven(new_position);
        }

        return this.givens[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item in @AmbiguousValueOutputKey.givens**. Unlike the [unsafeGiven](AmbiguousValueOutputKey.unsafeGiven) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item in @AmbiguousValueOutputKey.givens**. Unlike the [unsafeGiven](AmbiguousValueOutputKey.unsafeGiven) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item in @AmbiguousValueOutputKey.givens**. Unlike the [unsafeGiven](AmbiguousValueOutputKey.unsafeGiven) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @AmbiguousValueOutputKey.givens, you can give `-1` for example. If you want to get the first item of @AmbiguousValueOutputKey.givens, you can give `0`.*
     * @throw OutOfRange Throws an [OutOfRange](OutOfRange) error if the given position is out of range.
     * @category @AmbiguousValueOutputKey.givens
     * @category getter
     * @category list
     * @category value
     * @public
     * @see OutOfRange
     * @see AmbiguousValueOutputKey.getGivens
     * @see AmbiguousValueOutputKey.unsafeGivens
     * @see AmbiguousValueOutputKey.unsafeGiven
     * @see AmbiguousValueOutputKey.getGivensSize
     * @see AmbiguousValueOutputKey.containGivens
     * @see AmbiguousValueOutputKey.addGivens
     * @see AmbiguousValueOutputKey.removeGivens
     * @see AmbiguousValueOutputKey.truncateGivens
     * @see givens
     */
    getGiven(position: number): string{
        const given = this.unsafeGiven(position);
        if (typeof given === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getGiven");
            error.setClassname("AmbiguousValueOutputKey");
            error.setProperty("givens");
            error.setPosition(position);
            throw error;
        }

        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Add one or multiple values** to @AmbiguousValueOutputKey.givens.
     * @description Allows you to **add one or multiple values** to @AmbiguousValueOutputKey.givens. Each argument of in the method will be added to @AmbiguousValueOutputKey.givens, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in @AmbiguousValueOutputKey.givens: the value is unique._
     * @param givens All the given values that you to add to @AmbiguousValueOutputKey.givens *(the value can be a deep array, you should take a look to [flat](flat) function)*. *If the value is already included, the method will discard the item addition.*
     * @return Returns the actual [AmbiguousValueOutputKey](AmbiguousValueOutputKey) class instance.
     * @category @AmbiguousValueOutputKey.givens
     * @category associate
     * @category list
     * @public
     * @see AmbiguousValueOutputKey.getGivens
     * @see AmbiguousValueOutputKey.unsafeGivens
     * @see AmbiguousValueOutputKey.getGiven
     * @see AmbiguousValueOutputKey.unsafeGiven
     * @see AmbiguousValueOutputKey.getGivensSize
     * @see AmbiguousValueOutputKey.containGivens
     * @see AmbiguousValueOutputKey.removeGivens
     * @see AmbiguousValueOutputKey.truncateGivens
     * @see givens
     */
    addGivens(... givens: DeepArray<string>[]): this{
        for (const given of flat(givens)){
            if (this.containGivens(given)){
                continue;
            }


            this.givens.push(given);
        }

        this.updateGivens();

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if the given value is included** in @AmbiguousValueOutputKey.givens.
     * @description **Checks if all the given value is included** in @AmbiguousValueOutputKey.givens. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @AmbiguousValueOutputKey.givens or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @AmbiguousValueOutputKey.givens and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @return **Returns a boolean if all the given value is included** in @AmbiguousValueOutputKey.givens. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @AmbiguousValueOutputKey.givens or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @AmbiguousValueOutputKey.givens and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @param givens All the values that you want to check *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @AmbiguousValueOutputKey.givens
     * @category defined
     * @category list
     * @public
     * @see AmbiguousValueOutputKey.getGivens
     * @see AmbiguousValueOutputKey.unsafeGivens
     * @see AmbiguousValueOutputKey.getGiven
     * @see AmbiguousValueOutputKey.unsafeGiven
     * @see AmbiguousValueOutputKey.getGivensSize
     * @see AmbiguousValueOutputKey.addGivens
     * @see AmbiguousValueOutputKey.removeGivens
     * @see AmbiguousValueOutputKey.truncateGivens
     * @see givens
     */
    containGivens(... givens: DeepArray<string|undefined>[]): boolean{
        return flat(givens).every((given) => {
            if (typeof given === "undefined"){
                return false;
            }

            return this.unsafeGivens().includes(given);
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of @AmbiguousValueOutputKey.givens**.
     * @description **Gives a copy of @AmbiguousValueOutputKey.givens**.
     * @return **Returns a copy of @AmbiguousValueOutputKey.givens**.
     * @category @AmbiguousValueOutputKey.givens
     * @category getter
     * @category iterable
     * @category list
     * @public
     * @see AmbiguousValueOutputKey.unsafeGivens
     * @see AmbiguousValueOutputKey.getGiven
     * @see AmbiguousValueOutputKey.unsafeGiven
     * @see AmbiguousValueOutputKey.getGivensSize
     * @see AmbiguousValueOutputKey.containGivens
     * @see AmbiguousValueOutputKey.addGivens
     * @see AmbiguousValueOutputKey.removeGivens
     * @see AmbiguousValueOutputKey.truncateGivens
     * @see givens
     */
    getGivens(): string[]{
        return Array.from(this.unsafeGivens());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives @AmbiguousValueOutputKey.givens without copy** the array.
     * @description **Gives @AmbiguousValueOutputKey.givens without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getGivens](AmbiguousValueOutputKey.getGivens) method.
     * @return **Returns @AmbiguousValueOutputKey.givens without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getGivens](AmbiguousValueOutputKey.getGivens) method.
     * @category @AmbiguousValueOutputKey.givens
     * @category unsafe
     * @category iterable
     * @category list
     * @public
     * @see AmbiguousValueOutputKey.getGivens
     * @see AmbiguousValueOutputKey.getGiven
     * @see AmbiguousValueOutputKey.unsafeGiven
     * @see AmbiguousValueOutputKey.getGivensSize
     * @see AmbiguousValueOutputKey.containGivens
     * @see AmbiguousValueOutputKey.addGivens
     * @see AmbiguousValueOutputKey.removeGivens
     * @see AmbiguousValueOutputKey.truncateGivens
     * @see givens
     */
    unsafeGivens(): string[]{
        return this.givens;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Allows to **remove one or multiple values** from @AmbiguousValueOutputKey.givens.
     * @description Allows you to **remove one or multiple values** from @AmbiguousValueOutputKey.givens. Each argument of in the method will be removed from @AmbiguousValueOutputKey.givens, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @param givens All the given values that you to remove from @AmbiguousValueOutputKey.givens *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @return Returns the actual [AmbiguousValueOutputKey](AmbiguousValueOutputKey) class instance.
     * @category @AmbiguousValueOutputKey.givens
     * @category dissociate
     * @category list
     * @public
     * @see AmbiguousValueOutputKey.getGivens
     * @see AmbiguousValueOutputKey.unsafeGivens
     * @see AmbiguousValueOutputKey.getGiven
     * @see AmbiguousValueOutputKey.unsafeGiven
     * @see AmbiguousValueOutputKey.getGivensSize
     * @see AmbiguousValueOutputKey.containGivens
     * @see AmbiguousValueOutputKey.addGivens
     * @see AmbiguousValueOutputKey.truncateGivens
     * @see givens
     */
    removeGivens(... givens: DeepArray<string>[]): this{
        for (const given of flat(givens)){
            if (!this.containGivens(given)){
                continue;
            }

            while (true){
                const index = this.givens.indexOf(given);
                if (index < 0){
                    break;
                }

                this.givens.splice(index, 1);
            }
        }

        this.updateGivens();

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Truncate @AmbiguousValueOutputKey.givens**.
     * @description **Remove all the values in @AmbiguousValueOutputKey.givens**.
     * @return Returns the actual [AmbiguousValueOutputKey](AmbiguousValueOutputKey) class instance.
     * @category @AmbiguousValueOutputKey.givens
     * @category truncate
     * @category list
     * @public
     * @see AmbiguousValueOutputKey.getGivens
     * @see AmbiguousValueOutputKey.unsafeGivens
     * @see AmbiguousValueOutputKey.getGiven
     * @see AmbiguousValueOutputKey.unsafeGiven
     * @see AmbiguousValueOutputKey.getGivensSize
     * @see AmbiguousValueOutputKey.containGivens
     * @see AmbiguousValueOutputKey.addGivens
     * @see AmbiguousValueOutputKey.removeGivens
     * @see givens
     */
    truncateGivens(): this{
        this.removeGivens(this.unsafeGivens());
        return this;
    }
}

Translate.setTranslate({
    'fr.model.ambiguous.value.output': 'vos clefs #given font tous référence à #field. Merci de bien vouloir renseigner une clef faisant référence à une valeur présice.',
    'en.model.ambiguous.value.output': 'your #given keys all point to #field. Please fill in a key referring to a precise value.',

    "en.and": "and",
    "fr.and": "et",
})

export default AmbiguousValueOutputKey;