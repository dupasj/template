import FieldError from "../../../field";
import OutOfRange from "../../out-of-range";
import flat from "../../../../util/deep-array/flat";
import DeepArray from "../../../../util/deep-array/type";
import Translate from "../../../../translate";

class AmbiguousValueFilterKey extends FieldError{
    constructor(key = "model.ambiguous.value.filter",http_code = 417) {
        super(key,http_code);
    }

    private updateGivens(){
        this.setDefaultParameter("given",[
            this.unsafeGivens().slice(0,-1).join(", "),
            ... this.unsafeGivens().slice(-1),
        ].join(" {and} "))
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Save @AmbiguousValueFilterKey.givens.
     * @description Save @AmbiguousValueFilterKey.givens.
     * @category givens
     * @category property
     * @category list
     * @protected
     * @see AmbiguousValueFilterKey.getGivens
     * @see AmbiguousValueFilterKey.unsafeGivens
     * @see AmbiguousValueFilterKey.getGiven
     * @see AmbiguousValueFilterKey.unsafeGiven
     * @see AmbiguousValueFilterKey.getGivensSize
     * @see AmbiguousValueFilterKey.containGivens
     * @see AmbiguousValueFilterKey.addGivens
     * @see AmbiguousValueFilterKey.removeGivens
     * @see AmbiguousValueFilterKey.truncateGivens
     */
    protected givens: string[] = [];

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the size of @AmbiguousValueFilterKey.givens**.
     * @description **Gives the size of @AmbiguousValueFilterKey.givens**.
     * @return **Returns the size of @AmbiguousValueFilterKey.givens**.
     * @category @AmbiguousValueFilterKey.givens
     * @category list
     * @category length
     * @public
     * @see AmbiguousValueFilterKey.getGivens
     * @see AmbiguousValueFilterKey.unsafeGivens
     * @see AmbiguousValueFilterKey.getGiven
     * @see AmbiguousValueFilterKey.unsafeGiven
     * @see AmbiguousValueFilterKey.containGivens
     * @see AmbiguousValueFilterKey.addGivens
     * @see AmbiguousValueFilterKey.removeGivens
     * @see AmbiguousValueFilterKey.truncateGivens
     * @see givens
     */
    getGivensSize(): number{
        return this.unsafeGivens().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of @AmbiguousValueFilterKey.givens**. Unlike [getGiven](AmbiguousValueFilterKey.getGiven) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of @AmbiguousValueFilterKey.givens**. Unlike [getGiven](AmbiguousValueFilterKey.getGiven) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of @AmbiguousValueFilterKey.givens**. Unlike [getGiven](AmbiguousValueFilterKey.getGiven) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @AmbiguousValueFilterKey.givens, you can give `-1` for example. If you want to get the first item of @AmbiguousValueFilterKey.givens, you can give `0`.*
     * @category @AmbiguousValueFilterKey.givens
     * @category unsafe
     * @category list
     * @category value
     * @public
     * @see AmbiguousValueFilterKey.getGivens
     * @see AmbiguousValueFilterKey.unsafeGivens
     * @see AmbiguousValueFilterKey.getGiven
     * @see AmbiguousValueFilterKey.getGivensSize
     * @see AmbiguousValueFilterKey.containGivens
     * @see AmbiguousValueFilterKey.addGivens
     * @see AmbiguousValueFilterKey.removeGivens
     * @see AmbiguousValueFilterKey.truncateGivens
     * @see givens
     */
    unsafeGiven(position: number): string|undefined{
        if (position < 0){
            const new_position = this.getGivensSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeGiven(new_position);
        }

        return this.givens[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item in @AmbiguousValueFilterKey.givens**. Unlike the [unsafeGiven](AmbiguousValueFilterKey.unsafeGiven) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item in @AmbiguousValueFilterKey.givens**. Unlike the [unsafeGiven](AmbiguousValueFilterKey.unsafeGiven) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item in @AmbiguousValueFilterKey.givens**. Unlike the [unsafeGiven](AmbiguousValueFilterKey.unsafeGiven) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @AmbiguousValueFilterKey.givens, you can give `-1` for example. If you want to get the first item of @AmbiguousValueFilterKey.givens, you can give `0`.*
     * @throw OutOfRange Throws an [OutOfRange](OutOfRange) error if the given position is out of range.
     * @category @AmbiguousValueFilterKey.givens
     * @category getter
     * @category list
     * @category value
     * @public
     * @see OutOfRange
     * @see AmbiguousValueFilterKey.getGivens
     * @see AmbiguousValueFilterKey.unsafeGivens
     * @see AmbiguousValueFilterKey.unsafeGiven
     * @see AmbiguousValueFilterKey.getGivensSize
     * @see AmbiguousValueFilterKey.containGivens
     * @see AmbiguousValueFilterKey.addGivens
     * @see AmbiguousValueFilterKey.removeGivens
     * @see AmbiguousValueFilterKey.truncateGivens
     * @see givens
     */
    getGiven(position: number): string{
        const given = this.unsafeGiven(position);
        if (typeof given === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getGiven");
            error.setClassname("AmbiguousValueFilterKey");
            error.setProperty("givens");
            error.setPosition(position);
            throw error;
        }

        return given;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Add one or multiple values** to @AmbiguousValueFilterKey.givens.
     * @description Allows you to **add one or multiple values** to @AmbiguousValueFilterKey.givens. Each argument of in the method will be added to @AmbiguousValueFilterKey.givens, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in @AmbiguousValueFilterKey.givens: the value is unique._
     * @param givens All the given values that you to add to @AmbiguousValueFilterKey.givens *(the value can be a deep array, you should take a look to [flat](flat) function)*. *If the value is already included, the method will discard the item addition.*
     * @return Returns the actual [AmbiguousValueFilterKey](AmbiguousValueFilterKey) class instance.
     * @category @AmbiguousValueFilterKey.givens
     * @category associate
     * @category list
     * @public
     * @see AmbiguousValueFilterKey.getGivens
     * @see AmbiguousValueFilterKey.unsafeGivens
     * @see AmbiguousValueFilterKey.getGiven
     * @see AmbiguousValueFilterKey.unsafeGiven
     * @see AmbiguousValueFilterKey.getGivensSize
     * @see AmbiguousValueFilterKey.containGivens
     * @see AmbiguousValueFilterKey.removeGivens
     * @see AmbiguousValueFilterKey.truncateGivens
     * @see givens
     */
    addGivens(... givens: DeepArray<string>[]): this{
        for (const given of flat(givens)){
            if (this.containGivens(given)){
                continue;
            }


            this.givens.push(given);
        }

        this.updateGivens();

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if the given value is included** in @AmbiguousValueFilterKey.givens.
     * @description **Checks if all the given value is included** in @AmbiguousValueFilterKey.givens. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @AmbiguousValueFilterKey.givens or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @AmbiguousValueFilterKey.givens and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @return **Returns a boolean if all the given value is included** in @AmbiguousValueFilterKey.givens. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @AmbiguousValueFilterKey.givens or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @AmbiguousValueFilterKey.givens and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @param givens All the values that you want to check *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @AmbiguousValueFilterKey.givens
     * @category defined
     * @category list
     * @public
     * @see AmbiguousValueFilterKey.getGivens
     * @see AmbiguousValueFilterKey.unsafeGivens
     * @see AmbiguousValueFilterKey.getGiven
     * @see AmbiguousValueFilterKey.unsafeGiven
     * @see AmbiguousValueFilterKey.getGivensSize
     * @see AmbiguousValueFilterKey.addGivens
     * @see AmbiguousValueFilterKey.removeGivens
     * @see AmbiguousValueFilterKey.truncateGivens
     * @see givens
     */
    containGivens(... givens: DeepArray<string|undefined>[]): boolean{
        return flat(givens).every((given) => {
            if (typeof given === "undefined"){
                return false;
            }

            return this.unsafeGivens().includes(given);
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of @AmbiguousValueFilterKey.givens**.
     * @description **Gives a copy of @AmbiguousValueFilterKey.givens**.
     * @return **Returns a copy of @AmbiguousValueFilterKey.givens**.
     * @category @AmbiguousValueFilterKey.givens
     * @category getter
     * @category iterable
     * @category list
     * @public
     * @see AmbiguousValueFilterKey.unsafeGivens
     * @see AmbiguousValueFilterKey.getGiven
     * @see AmbiguousValueFilterKey.unsafeGiven
     * @see AmbiguousValueFilterKey.getGivensSize
     * @see AmbiguousValueFilterKey.containGivens
     * @see AmbiguousValueFilterKey.addGivens
     * @see AmbiguousValueFilterKey.removeGivens
     * @see AmbiguousValueFilterKey.truncateGivens
     * @see givens
     */
    getGivens(): string[]{
        return Array.from(this.unsafeGivens());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives @AmbiguousValueFilterKey.givens without copy** the array.
     * @description **Gives @AmbiguousValueFilterKey.givens without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getGivens](AmbiguousValueFilterKey.getGivens) method.
     * @return **Returns @AmbiguousValueFilterKey.givens without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getGivens](AmbiguousValueFilterKey.getGivens) method.
     * @category @AmbiguousValueFilterKey.givens
     * @category unsafe
     * @category iterable
     * @category list
     * @public
     * @see AmbiguousValueFilterKey.getGivens
     * @see AmbiguousValueFilterKey.getGiven
     * @see AmbiguousValueFilterKey.unsafeGiven
     * @see AmbiguousValueFilterKey.getGivensSize
     * @see AmbiguousValueFilterKey.containGivens
     * @see AmbiguousValueFilterKey.addGivens
     * @see AmbiguousValueFilterKey.removeGivens
     * @see AmbiguousValueFilterKey.truncateGivens
     * @see givens
     */
    unsafeGivens(): string[]{
        return this.givens;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Allows to **remove one or multiple values** from @AmbiguousValueFilterKey.givens.
     * @description Allows you to **remove one or multiple values** from @AmbiguousValueFilterKey.givens. Each argument of in the method will be removed from @AmbiguousValueFilterKey.givens, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @param givens All the given values that you to remove from @AmbiguousValueFilterKey.givens *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @return Returns the actual [AmbiguousValueFilterKey](AmbiguousValueFilterKey) class instance.
     * @category @AmbiguousValueFilterKey.givens
     * @category dissociate
     * @category list
     * @public
     * @see AmbiguousValueFilterKey.getGivens
     * @see AmbiguousValueFilterKey.unsafeGivens
     * @see AmbiguousValueFilterKey.getGiven
     * @see AmbiguousValueFilterKey.unsafeGiven
     * @see AmbiguousValueFilterKey.getGivensSize
     * @see AmbiguousValueFilterKey.containGivens
     * @see AmbiguousValueFilterKey.addGivens
     * @see AmbiguousValueFilterKey.truncateGivens
     * @see givens
     */
    removeGivens(... givens: DeepArray<string>[]): this{
        for (const given of flat(givens)){
            if (!this.containGivens(given)){
                continue;
            }

            while (true){
                const index = this.givens.indexOf(given);
                if (index < 0){
                    break;
                }

                this.givens.splice(index, 1);
            }
        }

        this.updateGivens();

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Truncate @AmbiguousValueFilterKey.givens**.
     * @description **Remove all the values in @AmbiguousValueFilterKey.givens**.
     * @return Returns the actual [AmbiguousValueFilterKey](AmbiguousValueFilterKey) class instance.
     * @category @AmbiguousValueFilterKey.givens
     * @category truncate
     * @category list
     * @public
     * @see AmbiguousValueFilterKey.getGivens
     * @see AmbiguousValueFilterKey.unsafeGivens
     * @see AmbiguousValueFilterKey.getGiven
     * @see AmbiguousValueFilterKey.unsafeGiven
     * @see AmbiguousValueFilterKey.getGivensSize
     * @see AmbiguousValueFilterKey.containGivens
     * @see AmbiguousValueFilterKey.addGivens
     * @see AmbiguousValueFilterKey.removeGivens
     * @see givens
     */
    truncateGivens(): this{
        this.removeGivens(this.unsafeGivens());
        return this;
    }
}

Translate.setTranslate({
    'fr.model.ambiguous.value.filter': 'vos clefs #given font tous référence à #field. Merci de bien vouloir renseigner une clef faisant référence à une valeur présice.',
    'en.model.ambiguous.value.filter': 'your #given keys all point to #field. Please fill in a key referring to a precise value.',

    "en.and": "and",
    "fr.and": "et",
})

export default AmbiguousValueFilterKey;