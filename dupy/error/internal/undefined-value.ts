import DupyError from "../index";

class UndefinedValue extends DupyError {
    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UndefinedValue.property.
     * @description Save @UndefinedValue.property.
     * @category @UndefinedValue.property
     * @category property
     * @category field
     * @protected
     * @see UndefinedValue.setProperty
     * @see UndefinedValue.getProperty
     * @see UndefinedValue.unsafeProperty
     * @see UndefinedValue.eraseProperty
     * @see UndefinedValue.isPropertyDefined
     * @see UndefinedValue.isPropertyEqualTo
     */
    protected property: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UndefinedValue.property**.
     * @description Allows you to update or **assign the value of @UndefinedValue.property**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UndefinedValue](UndefinedValue) class instance.
     * @param property The new value to assign to @UndefinedValue.property.
     * @category @UndefinedValue.property
     * @category setter
     * @category field
     * @public
     * @see UndefinedValue.property
     * @see UndefinedValue.getProperty
     * @see UndefinedValue.unsafeProperty
     * @see UndefinedValue.eraseProperty
     * @see UndefinedValue.isPropertyDefined
     * @see UndefinedValue.isPropertyEqualTo
     */
    setProperty(property: string): this{
        if (this.isPropertyEqualTo(property)){
            return this;
        }

        this.eraseProperty();
        this.property = property;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UndefinedValue.property**. Unlike the [getProperty](UndefinedValue.getProperty) method, **the method results an undefined value if @UndefinedValue.property is not defined**.
     * @description **Gives the value of @UndefinedValue.property**. Unlike the [getProperty](UndefinedValue.getProperty) method, **the method results an undefined value if @UndefinedValue.property is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UndefinedValue.property**. Unlike the [getProperty](UndefinedValue.getProperty) method, **the method results an undefined value if @UndefinedValue.property is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UndefinedValue.property
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UndefinedValue.property
     * @see UndefinedValue.setProperty
     * @see UndefinedValue.getProperty
     * @see UndefinedValue.eraseProperty
     * @see UndefinedValue.isPropertyDefined
     * @see UndefinedValue.isPropertyEqualTo
     */
    unsafeProperty(): string|undefined{
        return this.property;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UndefinedValue.property**. Unlike the [unsafeProperty](UndefinedValue.unsafeProperty) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UndefinedValue.property is not defined**.
     * @description **Gives the value of @UndefinedValue.property**. Unlike the [unsafeProperty](UndefinedValue.unsafeProperty) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UndefinedValue.property is not defined**.
     * @return **Returns the value of @UndefinedValue.property**. Unlike the [unsafeProperty](UndefinedValue.unsafeProperty) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UndefinedValue.property is not defined**.
     * @category @UndefinedValue.property
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UndefinedValue.property isn't defined.
     * @see UndefinedValue.property
     * @see UndefinedValue.setProperty
     * @see UndefinedValue.unsafeProperty
     * @see UndefinedValue.eraseProperty
     * @see UndefinedValue.isPropertyDefined
     * @see UndefinedValue.isPropertyEqualTo
     */
    getProperty(): string{
        const property = this.unsafeProperty();

        if (typeof property === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getProperty");
            error.setClassname("UndefinedValue");
            error.setProperty("property");
            throw error;
        }
        return property;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UndefinedValue.property is defined**.
     * @description **Checks if @UndefinedValue.property is defined** or not.
     * @return **Returns a boolean if @UndefinedValue.property is defined** or not.
     * @category @UndefinedValue.property
     * @category defined
     * @category field
     * @public
     * @see UndefinedValue.property
     * @see UndefinedValue.setProperty
     * @see UndefinedValue.getProperty
     * @see UndefinedValue.unsafeProperty
     * @see UndefinedValue.eraseProperty
     * @see UndefinedValue.isPropertyEqualTo
     */
    isPropertyDefined(): boolean{
        return typeof this.property !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UndefinedValue.property is equal to your given value**.
     * @description **Checks if @UndefinedValue.property is equal to your given value** and if @UndefinedValue.property is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UndefinedValue.property is equal to your given value** and if @UndefinedValue.property is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param property **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UndefinedValue.property
     * @category equality
     * @category field
     * @public
     * @see UndefinedValue.property
     * @see UndefinedValue.setProperty
     * @see UndefinedValue.getProperty
     * @see UndefinedValue.unsafeProperty
     * @see UndefinedValue.eraseProperty
     * @see UndefinedValue.isPropertyDefined
     */
    isPropertyEqualTo(property?: undefined|string): boolean{
        if (typeof property === "undefined"){
            return false;
        }
        return this.unsafeProperty() === property;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UndefinedValue.property**.
     * @description **Removes the assigned the value of @UndefinedValue.property**: *@UndefinedValue.property will be flagged as undefined*.
     * @return Return the actual [UndefinedValue](UndefinedValue) class instance.
     * @category @UndefinedValue.property
     * @category truncate
     * @category field
     * @public
     * @see UndefinedValue.property
     * @see UndefinedValue.setProperty
     * @see UndefinedValue.getProperty
     * @see UndefinedValue.unsafeProperty
     * @see UndefinedValue.isPropertyDefined
     * @see UndefinedValue.isPropertyEqualTo
     */
    eraseProperty(): this{
        this.property = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UndefinedValue.classname.
     * @description Save @UndefinedValue.classname.
     * @category @UndefinedValue.classname
     * @category property
     * @category field
     * @protected
     * @see UndefinedValue.setClassname
     * @see UndefinedValue.getClassname
     * @see UndefinedValue.unsafeClassname
     * @see UndefinedValue.eraseClassname
     * @see UndefinedValue.isClassnameDefined
     * @see UndefinedValue.isClassnameEqualTo
     */
    protected classname: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UndefinedValue.classname**.
     * @description Allows you to update or **assign the value of @UndefinedValue.classname**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UndefinedValue](UndefinedValue) class instance.
     * @param classname The new value to assign to @UndefinedValue.classname.
     * @category @UndefinedValue.classname
     * @category setter
     * @category field
     * @public
     * @see UndefinedValue.classname
     * @see UndefinedValue.getClassname
     * @see UndefinedValue.unsafeClassname
     * @see UndefinedValue.eraseClassname
     * @see UndefinedValue.isClassnameDefined
     * @see UndefinedValue.isClassnameEqualTo
     */
    setClassname(classname: string): this{
        if (this.isClassnameEqualTo(classname)){
            return this;
        }

        this.eraseClassname();
        this.classname = classname;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UndefinedValue.classname**. Unlike the [getClassname](UndefinedValue.getClassname) method, **the method results an undefined value if @UndefinedValue.classname is not defined**.
     * @description **Gives the value of @UndefinedValue.classname**. Unlike the [getClassname](UndefinedValue.getClassname) method, **the method results an undefined value if @UndefinedValue.classname is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UndefinedValue.classname**. Unlike the [getClassname](UndefinedValue.getClassname) method, **the method results an undefined value if @UndefinedValue.classname is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UndefinedValue.classname
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UndefinedValue.classname
     * @see UndefinedValue.setClassname
     * @see UndefinedValue.getClassname
     * @see UndefinedValue.eraseClassname
     * @see UndefinedValue.isClassnameDefined
     * @see UndefinedValue.isClassnameEqualTo
     */
    unsafeClassname(): string|undefined{
        return this.classname;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UndefinedValue.classname**. Unlike the [unsafeClassname](UndefinedValue.unsafeClassname) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UndefinedValue.classname is not defined**.
     * @description **Gives the value of @UndefinedValue.classname**. Unlike the [unsafeClassname](UndefinedValue.unsafeClassname) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UndefinedValue.classname is not defined**.
     * @return **Returns the value of @UndefinedValue.classname**. Unlike the [unsafeClassname](UndefinedValue.unsafeClassname) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UndefinedValue.classname is not defined**.
     * @category @UndefinedValue.classname
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UndefinedValue.classname isn't defined.
     * @see UndefinedValue.classname
     * @see UndefinedValue.setClassname
     * @see UndefinedValue.unsafeClassname
     * @see UndefinedValue.eraseClassname
     * @see UndefinedValue.isClassnameDefined
     * @see UndefinedValue.isClassnameEqualTo
     */
    getClassname(): string{
        const classname = this.unsafeClassname();

        if (typeof classname === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getClassname");
            error.setClassname("UndefinedValue");
            error.setProperty("classname");
            throw error;
        }
        return classname;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UndefinedValue.classname is defined**.
     * @description **Checks if @UndefinedValue.classname is defined** or not.
     * @return **Returns a boolean if @UndefinedValue.classname is defined** or not.
     * @category @UndefinedValue.classname
     * @category defined
     * @category field
     * @public
     * @see UndefinedValue.classname
     * @see UndefinedValue.setClassname
     * @see UndefinedValue.getClassname
     * @see UndefinedValue.unsafeClassname
     * @see UndefinedValue.eraseClassname
     * @see UndefinedValue.isClassnameEqualTo
     */
    isClassnameDefined(): boolean{
        return typeof this.classname !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UndefinedValue.classname is equal to your given value**.
     * @description **Checks if @UndefinedValue.classname is equal to your given value** and if @UndefinedValue.classname is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UndefinedValue.classname is equal to your given value** and if @UndefinedValue.classname is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param classname **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UndefinedValue.classname
     * @category equality
     * @category field
     * @public
     * @see UndefinedValue.classname
     * @see UndefinedValue.setClassname
     * @see UndefinedValue.getClassname
     * @see UndefinedValue.unsafeClassname
     * @see UndefinedValue.eraseClassname
     * @see UndefinedValue.isClassnameDefined
     */
    isClassnameEqualTo(classname?: undefined|string): boolean{
        if (typeof classname === "undefined"){
            return false;
        }
        return this.unsafeClassname() === classname;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UndefinedValue.classname**.
     * @description **Removes the assigned the value of @UndefinedValue.classname**: *@UndefinedValue.classname will be flagged as undefined*.
     * @return Return the actual [UndefinedValue](UndefinedValue) class instance.
     * @category @UndefinedValue.classname
     * @category truncate
     * @category field
     * @public
     * @see UndefinedValue.classname
     * @see UndefinedValue.setClassname
     * @see UndefinedValue.getClassname
     * @see UndefinedValue.unsafeClassname
     * @see UndefinedValue.isClassnameDefined
     * @see UndefinedValue.isClassnameEqualTo
     */
    eraseClassname(): this{
        this.classname = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UndefinedValue.method.
     * @description Save @UndefinedValue.method.
     * @category @UndefinedValue.method
     * @category property
     * @category field
     * @protected
     * @see UndefinedValue.setMethod
     * @see UndefinedValue.getMethod
     * @see UndefinedValue.unsafeMethod
     * @see UndefinedValue.eraseMethod
     * @see UndefinedValue.isMethodDefined
     * @see UndefinedValue.isMethodEqualTo
     */
    protected method: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UndefinedValue.method**.
     * @description Allows you to update or **assign the value of @UndefinedValue.method**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UndefinedValue](UndefinedValue) class instance.
     * @param method The new value to assign to @UndefinedValue.method.
     * @category @UndefinedValue.method
     * @category setter
     * @category field
     * @public
     * @see UndefinedValue.method
     * @see UndefinedValue.getMethod
     * @see UndefinedValue.unsafeMethod
     * @see UndefinedValue.eraseMethod
     * @see UndefinedValue.isMethodDefined
     * @see UndefinedValue.isMethodEqualTo
     */
    setMethod(method: string): this{
        if (this.isMethodEqualTo(method)){
            return this;
        }

        this.eraseMethod();
        this.method = method;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UndefinedValue.method**. Unlike the [getMethod](UndefinedValue.getMethod) method, **the method results an undefined value if @UndefinedValue.method is not defined**.
     * @description **Gives the value of @UndefinedValue.method**. Unlike the [getMethod](UndefinedValue.getMethod) method, **the method results an undefined value if @UndefinedValue.method is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UndefinedValue.method**. Unlike the [getMethod](UndefinedValue.getMethod) method, **the method results an undefined value if @UndefinedValue.method is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UndefinedValue.method
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UndefinedValue.method
     * @see UndefinedValue.setMethod
     * @see UndefinedValue.getMethod
     * @see UndefinedValue.eraseMethod
     * @see UndefinedValue.isMethodDefined
     * @see UndefinedValue.isMethodEqualTo
     */
    unsafeMethod(): string|undefined{
        return this.method;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UndefinedValue.method**. Unlike the [unsafeMethod](UndefinedValue.unsafeMethod) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UndefinedValue.method is not defined**.
     * @description **Gives the value of @UndefinedValue.method**. Unlike the [unsafeMethod](UndefinedValue.unsafeMethod) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UndefinedValue.method is not defined**.
     * @return **Returns the value of @UndefinedValue.method**. Unlike the [unsafeMethod](UndefinedValue.unsafeMethod) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UndefinedValue.method is not defined**.
     * @category @UndefinedValue.method
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UndefinedValue.method isn't defined.
     * @see UndefinedValue.method
     * @see UndefinedValue.setMethod
     * @see UndefinedValue.unsafeMethod
     * @see UndefinedValue.eraseMethod
     * @see UndefinedValue.isMethodDefined
     * @see UndefinedValue.isMethodEqualTo
     */
    getMethod(): string{
        const method = this.unsafeMethod();

        if (typeof method === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getMethod");
            error.setClassname("UndefinedValue");
            error.setProperty("method");
            throw error;
        }
        return method;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UndefinedValue.method is defined**.
     * @description **Checks if @UndefinedValue.method is defined** or not.
     * @return **Returns a boolean if @UndefinedValue.method is defined** or not.
     * @category @UndefinedValue.method
     * @category defined
     * @category field
     * @public
     * @see UndefinedValue.method
     * @see UndefinedValue.setMethod
     * @see UndefinedValue.getMethod
     * @see UndefinedValue.unsafeMethod
     * @see UndefinedValue.eraseMethod
     * @see UndefinedValue.isMethodEqualTo
     */
    isMethodDefined(): boolean{
        return typeof this.method !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UndefinedValue.method is equal to your given value**.
     * @description **Checks if @UndefinedValue.method is equal to your given value** and if @UndefinedValue.method is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UndefinedValue.method is equal to your given value** and if @UndefinedValue.method is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param method **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UndefinedValue.method
     * @category equality
     * @category field
     * @public
     * @see UndefinedValue.method
     * @see UndefinedValue.setMethod
     * @see UndefinedValue.getMethod
     * @see UndefinedValue.unsafeMethod
     * @see UndefinedValue.eraseMethod
     * @see UndefinedValue.isMethodDefined
     */
    isMethodEqualTo(method?: undefined|string): boolean{
        if (typeof method === "undefined"){
            return false;
        }
        return this.unsafeMethod() === method;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UndefinedValue.method**.
     * @description **Removes the assigned the value of @UndefinedValue.method**: *@UndefinedValue.method will be flagged as undefined*.
     * @return Return the actual [UndefinedValue](UndefinedValue) class instance.
     * @category @UndefinedValue.method
     * @category truncate
     * @category field
     * @public
     * @see UndefinedValue.method
     * @see UndefinedValue.setMethod
     * @see UndefinedValue.getMethod
     * @see UndefinedValue.unsafeMethod
     * @see UndefinedValue.isMethodDefined
     * @see UndefinedValue.isMethodEqualTo
     */
    eraseMethod(): this{
        this.method = undefined;

        return this;
    }
}

export default UndefinedValue;