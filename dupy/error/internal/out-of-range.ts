import UndefinedValue from "./undefined-value";

class OutOfRange extends UndefinedValue {
    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @OutOfRange.position.
     * @description Save @OutOfRange.position.
     * @category @OutOfRange.position
     * @category property
     * @category field
     * @protected
     * @see OutOfRange.setPosition
     * @see OutOfRange.getPosition
     * @see OutOfRange.unsafePosition
     * @see OutOfRange.erasePosition
     * @see OutOfRange.isPositionDefined
     * @see OutOfRange.isPositionEqualTo
     */
    protected position: number|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @OutOfRange.position**.
     * @description Allows you to update or **assign the value of @OutOfRange.position**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [OutOfRange](OutOfRange) class instance.
     * @param position The new value to assign to @OutOfRange.position.
     * @category @OutOfRange.position
     * @category setter
     * @category field
     * @public
     * @see OutOfRange.position
     * @see OutOfRange.getPosition
     * @see OutOfRange.unsafePosition
     * @see OutOfRange.erasePosition
     * @see OutOfRange.isPositionDefined
     * @see OutOfRange.isPositionEqualTo
     */
    setPosition(position: number): this{
        if (this.isPositionEqualTo(position)){
            return this;
        }

        this.erasePosition();
        this.position = position;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @OutOfRange.position**. Unlike the [getPosition](OutOfRange.getPosition) method, **the method results an undefined value if @OutOfRange.position is not defined**.
     * @description **Gives the value of @OutOfRange.position**. Unlike the [getPosition](OutOfRange.getPosition) method, **the method results an undefined value if @OutOfRange.position is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @OutOfRange.position**. Unlike the [getPosition](OutOfRange.getPosition) method, **the method results an undefined value if @OutOfRange.position is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @OutOfRange.position
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see OutOfRange.position
     * @see OutOfRange.setPosition
     * @see OutOfRange.getPosition
     * @see OutOfRange.erasePosition
     * @see OutOfRange.isPositionDefined
     * @see OutOfRange.isPositionEqualTo
     */
    unsafePosition(): number|undefined{
        return this.position;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @OutOfRange.position**. Unlike the [unsafePosition](OutOfRange.unsafePosition) method, **we will throw an [UndefinedValue](UndefinedValue) error if @OutOfRange.position is not defined**.
     * @description **Gives the value of @OutOfRange.position**. Unlike the [unsafePosition](OutOfRange.unsafePosition) method, **we will throw an [UndefinedValue](UndefinedValue) error if @OutOfRange.position is not defined**.
     * @return **Returns the value of @OutOfRange.position**. Unlike the [unsafePosition](OutOfRange.unsafePosition) method, **we will throw an [UndefinedValue](UndefinedValue) error if @OutOfRange.position is not defined**.
     * @category @OutOfRange.position
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @OutOfRange.position isn't defined.
     * @see OutOfRange.position
     * @see OutOfRange.setPosition
     * @see OutOfRange.unsafePosition
     * @see OutOfRange.erasePosition
     * @see OutOfRange.isPositionDefined
     * @see OutOfRange.isPositionEqualTo
     */
    getPosition(): number{
        const position = this.unsafePosition();

        if (typeof position === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getPosition");
            error.setClassname("OutOfRange");
            error.setProperty("position");
            throw error;
        }
        return position;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @OutOfRange.position is defined**.
     * @description **Checks if @OutOfRange.position is defined** or not.
     * @return **Returns a boolean if @OutOfRange.position is defined** or not.
     * @category @OutOfRange.position
     * @category defined
     * @category field
     * @public
     * @see OutOfRange.position
     * @see OutOfRange.setPosition
     * @see OutOfRange.getPosition
     * @see OutOfRange.unsafePosition
     * @see OutOfRange.erasePosition
     * @see OutOfRange.isPositionEqualTo
     */
    isPositionDefined(): boolean{
        return typeof this.position !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @OutOfRange.position is equal to your given value**.
     * @description **Checks if @OutOfRange.position is equal to your given value** and if @OutOfRange.position is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @OutOfRange.position is equal to your given value** and if @OutOfRange.position is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param position **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @OutOfRange.position
     * @category equality
     * @category field
     * @public
     * @see OutOfRange.position
     * @see OutOfRange.setPosition
     * @see OutOfRange.getPosition
     * @see OutOfRange.unsafePosition
     * @see OutOfRange.erasePosition
     * @see OutOfRange.isPositionDefined
     */
    isPositionEqualTo(position?: undefined|number): boolean{
        if (typeof position === "undefined"){
            return false;
        }
        return this.unsafePosition() === position;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @OutOfRange.position**.
     * @description **Removes the assigned the value of @OutOfRange.position**: *@OutOfRange.position will be flagged as undefined*.
     * @return Return the actual [OutOfRange](OutOfRange) class instance.
     * @category @OutOfRange.position
     * @category truncate
     * @category field
     * @public
     * @see OutOfRange.position
     * @see OutOfRange.setPosition
     * @see OutOfRange.getPosition
     * @see OutOfRange.unsafePosition
     * @see OutOfRange.isPositionDefined
     * @see OutOfRange.isPositionEqualTo
     */
    erasePosition(): this{
        this.position = undefined;

        return this;
    }
}

export default OutOfRange;