import UndefinedValue from "./undefined-value";

class UnlinkedValue extends UndefinedValue{
    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @UnlinkedValue.wanted_classname.
     * @description Save @UnlinkedValue.wanted_classname.
     * @category @UnlinkedValue.wanted_classname
     * @category property
     * @category field
     * @protected
     * @see UnlinkedValue.setWantedClassname
     * @see UnlinkedValue.getWantedClassname
     * @see UnlinkedValue.unsafeWantedClassname
     * @see UnlinkedValue.eraseWantedClassname
     * @see UnlinkedValue.isWantedClassnameDefined
     * @see UnlinkedValue.isWantedClassnameEqualTo
     */
    protected wanted_classname: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @UnlinkedValue.wanted_classname**.
     * @description Allows you to update or **assign the value of @UnlinkedValue.wanted_classname**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [UnlinkedValue](UnlinkedValue) class instance.
     * @param wanted_classname The new value to assign to @UnlinkedValue.wanted_classname.
     * @category @UnlinkedValue.wanted_classname
     * @category setter
     * @category field
     * @public
     * @see UnlinkedValue.wanted_classname
     * @see UnlinkedValue.getWantedClassname
     * @see UnlinkedValue.unsafeWantedClassname
     * @see UnlinkedValue.eraseWantedClassname
     * @see UnlinkedValue.isWantedClassnameDefined
     * @see UnlinkedValue.isWantedClassnameEqualTo
     */
    setWantedClassname(wanted_classname: string): this{
        if (this.isWantedClassnameEqualTo(wanted_classname)){
            return this;
        }

        this.eraseWantedClassname();
        this.wanted_classname = wanted_classname;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnlinkedValue.wanted_classname**. Unlike the [getWantedClassname](UnlinkedValue.getWantedClassname) method, **the method results an undefined value if @UnlinkedValue.wanted_classname is not defined**.
     * @description **Gives the value of @UnlinkedValue.wanted_classname**. Unlike the [getWantedClassname](UnlinkedValue.getWantedClassname) method, **the method results an undefined value if @UnlinkedValue.wanted_classname is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @UnlinkedValue.wanted_classname**. Unlike the [getWantedClassname](UnlinkedValue.getWantedClassname) method, **the method results an undefined value if @UnlinkedValue.wanted_classname is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @UnlinkedValue.wanted_classname
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see UnlinkedValue.wanted_classname
     * @see UnlinkedValue.setWantedClassname
     * @see UnlinkedValue.getWantedClassname
     * @see UnlinkedValue.eraseWantedClassname
     * @see UnlinkedValue.isWantedClassnameDefined
     * @see UnlinkedValue.isWantedClassnameEqualTo
     */
    unsafeWantedClassname(): string|undefined{
        return this.wanted_classname;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @UnlinkedValue.wanted_classname**. Unlike the [unsafeWantedClassname](UnlinkedValue.unsafeWantedClassname) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnlinkedValue.wanted_classname is not defined**.
     * @description **Gives the value of @UnlinkedValue.wanted_classname**. Unlike the [unsafeWantedClassname](UnlinkedValue.unsafeWantedClassname) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnlinkedValue.wanted_classname is not defined**.
     * @return **Returns the value of @UnlinkedValue.wanted_classname**. Unlike the [unsafeWantedClassname](UnlinkedValue.unsafeWantedClassname) method, **we will throw an [UndefinedValue](UndefinedValue) error if @UnlinkedValue.wanted_classname is not defined**.
     * @category @UnlinkedValue.wanted_classname
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @UnlinkedValue.wanted_classname isn't defined.
     * @see UnlinkedValue.wanted_classname
     * @see UnlinkedValue.setWantedClassname
     * @see UnlinkedValue.unsafeWantedClassname
     * @see UnlinkedValue.eraseWantedClassname
     * @see UnlinkedValue.isWantedClassnameDefined
     * @see UnlinkedValue.isWantedClassnameEqualTo
     */
    getWantedClassname(): string{
        const wanted_classname = this.unsafeWantedClassname();

        if (typeof wanted_classname === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getWantedClassname");
            error.setClassname("UnlinkedValue");
            error.setProperty("wanted_classname");
            throw error;
        }
        return wanted_classname;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnlinkedValue.wanted_classname is defined**.
     * @description **Checks if @UnlinkedValue.wanted_classname is defined** or not.
     * @return **Returns a boolean if @UnlinkedValue.wanted_classname is defined** or not.
     * @category @UnlinkedValue.wanted_classname
     * @category defined
     * @category field
     * @public
     * @see UnlinkedValue.wanted_classname
     * @see UnlinkedValue.setWantedClassname
     * @see UnlinkedValue.getWantedClassname
     * @see UnlinkedValue.unsafeWantedClassname
     * @see UnlinkedValue.eraseWantedClassname
     * @see UnlinkedValue.isWantedClassnameEqualTo
     */
    isWantedClassnameDefined(): boolean{
        return typeof this.wanted_classname !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @UnlinkedValue.wanted_classname is equal to your given value**.
     * @description **Checks if @UnlinkedValue.wanted_classname is equal to your given value** and if @UnlinkedValue.wanted_classname is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @UnlinkedValue.wanted_classname is equal to your given value** and if @UnlinkedValue.wanted_classname is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param wanted_classname **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @UnlinkedValue.wanted_classname
     * @category equality
     * @category field
     * @public
     * @see UnlinkedValue.wanted_classname
     * @see UnlinkedValue.setWantedClassname
     * @see UnlinkedValue.getWantedClassname
     * @see UnlinkedValue.unsafeWantedClassname
     * @see UnlinkedValue.eraseWantedClassname
     * @see UnlinkedValue.isWantedClassnameDefined
     */
    isWantedClassnameEqualTo(wanted_classname?: undefined|string): boolean{
        if (typeof wanted_classname === "undefined"){
            return false;
        }
        return this.unsafeWantedClassname() === wanted_classname;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @UnlinkedValue.wanted_classname**.
     * @description **Removes the assigned the value of @UnlinkedValue.wanted_classname**: *@UnlinkedValue.wanted_classname will be flagged as undefined*.
     * @return Return the actual [UnlinkedValue](UnlinkedValue) class instance.
     * @category @UnlinkedValue.wanted_classname
     * @category truncate
     * @category field
     * @public
     * @see UnlinkedValue.wanted_classname
     * @see UnlinkedValue.setWantedClassname
     * @see UnlinkedValue.getWantedClassname
     * @see UnlinkedValue.unsafeWantedClassname
     * @see UnlinkedValue.isWantedClassnameDefined
     * @see UnlinkedValue.isWantedClassnameEqualTo
     */
    eraseWantedClassname(): this{
        this.wanted_classname = undefined;

        return this;
    }
}

export default UnlinkedValue;