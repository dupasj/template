import ModelError from "./model";
import FieldConfiguration from "../model/field/field";
import FieldInstance from "../model/field/field/instance";
import UndefinedValue from "./internal/undefined-value";

class FieldError extends ModelError{
    isFieldError(): this is FieldError{
        return true
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @FieldError.field.
     * @description Save @FieldError.field.
     * @category @FieldError.field
     * @category property
     * @category field
     * @protected
     * @see FieldError.setField
     * @see FieldError.getField
     * @see FieldError.unsafeField
     * @see FieldError.eraseField
     * @see FieldError.isFieldDefined
     * @see FieldError.isFieldEqualTo
     */
    protected field: (FieldInstance|FieldConfiguration)|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @FieldError.field**.
     * @description Allows you to update or **assign the value of @FieldError.field**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [FieldError](FieldError) class instance.
     * @param field The new value to assign to @FieldError.field.
     * @category @FieldError.field
     * @category setter
     * @category field
     * @public
     * @see FieldError.field
     * @see FieldError.getField
     * @see FieldError.unsafeField
     * @see FieldError.eraseField
     * @see FieldError.isFieldDefined
     * @see FieldError.isFieldEqualTo
     */
    setField(field: (FieldInstance|FieldConfiguration)): this{
        if (this.isFieldEqualTo(field)){
            return this;
        }

        this.eraseField();
        this.field = field;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @FieldError.field**. Unlike the [getField](FieldError.getField) method, **the method results an undefined value if @FieldError.field is not defined**.
     * @description **Gives the value of @FieldError.field**. Unlike the [getField](FieldError.getField) method, **the method results an undefined value if @FieldError.field is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @FieldError.field**. Unlike the [getField](FieldError.getField) method, **the method results an undefined value if @FieldError.field is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @FieldError.field
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see FieldError.field
     * @see FieldError.setField
     * @see FieldError.getField
     * @see FieldError.eraseField
     * @see FieldError.isFieldDefined
     * @see FieldError.isFieldEqualTo
     */
    unsafeField(): (FieldInstance|FieldConfiguration)|undefined{
        return this.field;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @FieldError.field**. Unlike the [unsafeField](FieldError.unsafeField) method, **we will throw an [UndefinedValue](UndefinedValue) error if @FieldError.field is not defined**.
     * @description **Gives the value of @FieldError.field**. Unlike the [unsafeField](FieldError.unsafeField) method, **we will throw an [UndefinedValue](UndefinedValue) error if @FieldError.field is not defined**.
     * @return **Returns the value of @FieldError.field**. Unlike the [unsafeField](FieldError.unsafeField) method, **we will throw an [UndefinedValue](UndefinedValue) error if @FieldError.field is not defined**.
     * @category @FieldError.field
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @FieldError.field isn't defined.
     * @see FieldError.field
     * @see FieldError.setField
     * @see FieldError.unsafeField
     * @see FieldError.eraseField
     * @see FieldError.isFieldDefined
     * @see FieldError.isFieldEqualTo
     */
    getField(): (FieldInstance|FieldConfiguration){
        const field = this.unsafeField();

        if (typeof field === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getField");
            error.setClassname("FieldError");
            error.setProperty("field");
            throw error;
        }
        return field;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @FieldError.field is defined**.
     * @description **Checks if @FieldError.field is defined** or not.
     * @return **Returns a boolean if @FieldError.field is defined** or not.
     * @category @FieldError.field
     * @category defined
     * @category field
     * @public
     * @see FieldError.field
     * @see FieldError.setField
     * @see FieldError.getField
     * @see FieldError.unsafeField
     * @see FieldError.eraseField
     * @see FieldError.isFieldEqualTo
     */
    isFieldDefined(): boolean{
        return typeof this.field !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @FieldError.field is equal to your given value**.
     * @description **Checks if @FieldError.field is equal to your given value** and if @FieldError.field is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @FieldError.field is equal to your given value** and if @FieldError.field is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param field **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @FieldError.field
     * @category equality
     * @category field
     * @public
     * @see FieldError.field
     * @see FieldError.setField
     * @see FieldError.getField
     * @see FieldError.unsafeField
     * @see FieldError.eraseField
     * @see FieldError.isFieldDefined
     */
    isFieldEqualTo(field?: undefined|(FieldInstance|FieldConfiguration)): boolean{
        if (typeof field === "undefined"){
            return false;
        }
        return this.unsafeField() === field;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @FieldError.field**.
     * @description **Removes the assigned the value of @FieldError.field**: *@FieldError.field will be flagged as undefined*.
     * @return Return the actual [FieldError](FieldError) class instance.
     * @category @FieldError.field
     * @category truncate
     * @category field
     * @public
     * @see FieldError.field
     * @see FieldError.setField
     * @see FieldError.getField
     * @see FieldError.unsafeField
     * @see FieldError.isFieldDefined
     * @see FieldError.isFieldEqualTo
     */
    eraseField(): this{
        this.field = undefined;

        return this;
    }
}

export default FieldError;
