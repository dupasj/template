import BaseDupyError from "./base";
import DeepArray from "../util/deep-array/type";
import flat from "../util/deep-array/flat";
import OutOfRange from "./internal/out-of-range";

class BundleError extends BaseDupyError{
    isBundleError(): this is BundleError {
        return true;
    }

    toMessageArray(language: string|string[]): {[key: string]: string[]}{
        const result: {[key: string]: string[]} = {};

        for(const child of this.getChildren()){
            if (child.isBundleError()){
                const child_result = child.toMessageArray(language);

                for(const context in child_result){
                    if (!(context in result)){
                        result[context] = child_result[context];
                    }else{
                        result[context].push(... child_result[context]);
                    }
                }
            }else if(child.isError()){
                const context = child.getContext();

                if (!(context in result)){
                    result[context] = [child.getMessage(language)];
                }else{
                    result[context].push(child.getMessage(language));
                }
            }
        }

        return result;
    }

    getHttpStatusList(): number[] {
        const result: number[] = [];

        for(const error of this.unsafeChildren()){
            if (error.isBundleError()){
                for(const item of error.getHttpStatusList()){
                    if (result.includes(item)){
                        continue;
                    }
                    result.push(item);
                }
            }else if (error.isError() && error.isHttpStatusDefined()){
                const item = error.getHttpStatus();
                if (result.includes(item)){
                    continue;
                }
                result.push(item);
            }
        }

        return result;
    }

    isEmpty(){
        for(const child of this.unsafeChildren()){
            if (child.isBundleError()){
                if (!child.isEmpty()){
                    return false;
                }
            }else{
                return false;
            }
        }

        return true;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [BaseDupyError](BaseDupyError) and the actual [BundleError](BundleError)**.
     * @description **An array of saved relations between [BaseDupyError](BaseDupyError) class instances and the actual [BundleError](BundleError) class instance**.
     * @category @BaseDupyError.bundle_error
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category property
     * @protected
     * @see BundleError.areChildrenLinked
     * @see BundleError.disposeBaseDupyErrors
     * @see BundleError.unlinkChildren
     * @see BundleError.unsafeChildren
     * @see BundleError.getChildren
     * @see BundleError.linkChildren
     * @see BundleError.getChild
     * @see BundleError.unsafeChild
     * @see BundleError.getChildrenSize
     */
    protected children: BaseDupyError[] = [];


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the number of the associated [BaseDupyError](BaseDupyError) class instances**.
     * @description **Gives the size of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances**.
     * @return **Returns the number of the associated [BaseDupyError](BaseDupyError) class instances**.
     * @category @BaseDupyError.bundle_error
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category length
     * @public
     * @see BundleError.areChildrenLinked
     * @see BundleError.disposeBaseDupyErrors
     * @see BundleError.unlinkChildren
     * @see BundleError.unsafeChildren
     * @see BundleError.getChildren
     * @see BundleError.linkChildren
     * @see BundleError.getChild
     * @see BundleError.unsafeChild
     * @see BundleError.children
     */
    getChildrenSize(): number{
        return this.unsafeChildren().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError)** class instances. Unlike [getChild](BundleError.getChild) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances**. Unlike [getChild](BundleError.getChild) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances**. Unlike [getChild](BundleError.getChild) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError), you can give `-1` for example. If you want to get the first item of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError), you can give `0`.*
     * @category @BaseDupyError.bundle_error
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category unsafe
     * @category value
     * @public
     * @see BundleError.areChildrenLinked
     * @see BundleError.disposeBaseDupyErrors
     * @see BundleError.unlinkChildren
     * @see BundleError.unsafeChildren
     * @see BundleError.getChildren
     * @see BundleError.linkChildren
     * @see BundleError.getChild
     * @see BundleError.getChildrenSize
     * @see BundleError.children
     */
    unsafeChild(position: number): BaseDupyError|undefined{
        if (position < 0){
            const new_position = this.getChildrenSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeChild(new_position);
        }

        return this.children[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances**. Unlike the [unsafeTask](#one-to-many-BundleError--unsafeChild) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances**. Unlike the [unsafeTask](#one-to-many-BundleError--unsafeChild) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances**. Unlike the [unsafeTask](#one-to-many-BundleError--unsafeChild) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError), you can give `-1` for example. If you want to get the first item of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError), you can give `0`.*
     * @category @BaseDupyError.bundle_error
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category getter
     * @category value
     * @public
     * @see BundleError.areChildrenLinked
     * @see BundleError.disposeBaseDupyErrors
     * @see BundleError.unlinkChildren
     * @see BundleError.unsafeChildren
     * @see BundleError.getChildren
     * @see BundleError.linkChildren
     * @see BundleError.unsafeChild
     * @see BundleError.getChildrenSize
     * @see BundleError.children
     */
    getChild(position: number): BaseDupyError{
        const child = this.unsafeChild(position);
        if (typeof child === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getChild");
            error.setClassname("BundleError");
            error.setProperty("children");
            error.setPosition(position);
            throw error;
        }
        return child;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associate one or multiple [BaseDupyError](BaseDupyError) class instances** from your actual [BundleError](BundleError) class instance.
     * @description Allows you to **associate one or multiple [BaseDupyError](BaseDupyError) class instances** from your actual [BundleError](BundleError) class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate [BundleError](BundleError) class instance** ([BaseDupyError.unlinkParent](BaseDupyError.unlinkParent)), and **then associate the  [BaseDupyError](BaseDupyError) class instances** to the [BundleError](BundleError) class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [BaseDupyError.linkParent](BaseDupyError.linkParent) to associate the [BundleError](BundleError) class instance and the [BaseDupyError](BaseDupyError) class instances from the both parts*.
     * @return Returns the actual [BaseDupyError](BaseDupyError) class instance.
     * @param children All the given [BaseDupyError](BaseDupyError) class instances that you to associate to your actual [BundleError](BundleError) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseDupyError.bundle_error
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category associate
     * @public
     * @see BundleError.areChildrenLinked
     * @see BundleError.disposeBaseDupyErrors
     * @see BundleError.unlinkChildren
     * @see BundleError.unsafeChildren
     * @see BundleError.getChildren
     * @see BundleError.getChild
     * @see BundleError.unsafeChild
     * @see BundleError.getChildrenSize
     * @see BundleError.children
     * @see BaseDupyError.linkParent
     */
    linkChildren(... children: DeepArray<BaseDupyError>[]): this{
        for (const child of flat(children)){
            if (this.areChildrenLinked(child)){
                continue;
            }


            this.children.push(child);


            child.linkParent(this);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances**.
     * @description **Gives a copy of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances**.
     * @return **Returns a copy of the [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances**.
     * @category @BaseDupyError.bundle_error
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category getter
     * @category iterable
     * @public
     * @see BundleError.areChildrenLinked
     * @see BundleError.disposeBaseDupyErrors
     * @see BundleError.unlinkChildren
     * @see BundleError.unsafeChildren
     * @see BundleError.linkChildren
     * @see BundleError.getChild
     * @see BundleError.unsafeChild
     * @see BundleError.getChildrenSize
     * @see BundleError.children
     */
    getChildren(): BaseDupyError[]{
        return Array.from(this.unsafeChildren());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives an [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances without copy**.
     * @description **Gives an [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getChildren](BundleError.getChildren) method.
     * @return **Returns an [array](BundleError.children) of the associated [BaseDupyError](BaseDupyError) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getChildren](BundleError.getChildren) method.
     * @category @BaseDupyError.bundle_error
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category unsafe
     * @category iterable
     * @public
     * @see BundleError.areChildrenLinked
     * @see BundleError.disposeBaseDupyErrors
     * @see BundleError.unlinkChildren
     * @see BundleError.getChildren
     * @see BundleError.linkChildren
     * @see BundleError.getChild
     * @see BundleError.unsafeChild
     * @see BundleError.getChildrenSize
     * @see BundleError.children
     */
    unsafeChildren(): BaseDupyError[]{
        return this.children;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate one or multiple [BaseDupyError](BaseDupyError) class instances**.
     * @description Allows you to **dissociate one or multiple [BaseDupyError](BaseDupyError) class instances** from your actual [BundleError](BundleError) class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the [BundleError](BundleError) class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [BaseDupyError.unlinkParent](BaseDupyError.unlinkParent) to dissociate the [BundleError](BundleError) class instance and the given [BaseDupyError](BaseDupyError) class instances from the both parts*.
     * @return Returns the actual [BaseDupyError](BaseDupyError) class instance.
     * @param children All the given [BaseDupyError](BaseDupyError) class instances that you to dissociate from your actual [BundleError](BundleError) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseDupyError.bundle_error
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category dissociate
     * @public
     * @see BundleError.areChildrenLinked
     * @see BundleError.disposeBaseDupyErrors
     * @see BundleError.unsafeChildren
     * @see BundleError.getChildren
     * @see BundleError.linkChildren
     * @see BundleError.getChild
     * @see BundleError.unsafeChild
     * @see BundleError.getChildrenSize
     * @see BundleError.children
     * @see BaseDupyError.unlinkParent
     */
    unlinkChildren(... children: DeepArray<BaseDupyError>[]): this{
        for (const child of flat(children)){
            if (!this.areChildrenLinked(child)){
                continue;
            }

            while (true){
                const index = this.children.indexOf(child);
                if (index < 0){
                    break;
                }

                this.children.splice(index, 1);
            }

            child.unlinkParent();
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate all the associated [BaseDupyError](BaseDupyError) class instances**.
     * @description **Dissociate all the associated [BaseDupyError](BaseDupyError) class instances**.  *Note that all the associated [BaseDupyError](BaseDupyError) class instances will lose their [BundleError](BundleError) class instance association*.
     * @return Returns the actual [BaseDupyError](BaseDupyError) class instance.
     * @category @BaseDupyError.bundle_error
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category truncate
     * @public
     * @see BundleError.areChildrenLinked
     * @see BundleError.unlinkChildren
     * @see BundleError.unsafeChildren
     * @see BundleError.getChildren
     * @see BundleError.linkChildren
     * @see BundleError.getChild
     * @see BundleError.unsafeChild
     * @see BundleError.getChildrenSize
     * @see BundleError.children
     */
    disposeBaseDupyErrors(): this{
        this.unlinkChildren(this.unsafeChildren());
        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [BaseDupyError](BaseDupyError) class instances are associated**.
     * @description **Checks if all the given [BaseDupyError](BaseDupyError) class instances are associated** with your actual [BundleError](BundleError) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [BundleError](BundleError) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [BaseDupyError](BaseDupyError) class instances are associated with your [BundleError](BundleError) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @return **Return a boolean if all the given [BaseDupyError](BaseDupyError) class instances are associated** with your actual [BundleError](BundleError) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [BundleError](BundleError) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [BaseDupyError](BaseDupyError) class instances are associated with your [BundleError](BundleError) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @param children All the given [BaseDupyError](BaseDupyError) class instances that you want to check their relation *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseDupyError.bundle_error
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category defined
     * @public
     * @see BundleError.disposeBaseDupyErrors
     * @see BundleError.unlinkChildren
     * @see BundleError.unsafeChildren
     * @see BundleError.getChildren
     * @see BundleError.linkChildren
     * @see BundleError.getChild
     * @see BundleError.unsafeChild
     * @see BundleError.getChildrenSize
     * @see BundleError.children
     */
    areChildrenLinked(... children: DeepArray<BaseDupyError|undefined>[]): boolean{
        return flat(children).every((child) => {
            if (typeof child === "undefined"){
                return false;
            }

            return this.unsafeChildren().includes(child);
        });
    }
}

export default BundleError;