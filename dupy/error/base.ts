import DupyObject from "../dupy-object";
import BundleError from "./bundle";
import UnlinkedValue from "./internal/unlinked-value";
import querystring from "querystring";
import UndefinedValue from "./internal/undefined-value";

class BaseDupyError extends DupyObject {
    static getMessage(template: string,parameters: {[key: string]: string},language_key: string|string[]): string {
        for(const [key,value] of Object.entries(parameters)){
            template = template.split("#"+key).join(value);
        }

        const regex = new RegExp("((?:{)[a-z.]*(?:}))");
        while(true){
            const matches = template.match(regex);
            if (!matches){
                break;
            }
            for(const match of matches){
                template = template.split(match).join(
                    this.getMessage(
                        this.getTemplate(
                            match.slice(1,-1),
                            language_key
                        ),
                        parameters,
                        language_key
                    ),
                );
            }
        }

        if (Object.keys(parameters).length > 0){
            return template+"?"+querystring.stringify(parameters)
        }

        return template;
    }
    static getTemplate(tag: string,language_key: string|string[]): string{
        /*
        if (Array.isArray(language_key)){
            for(const key of language_key){
                const split = key.split('-');
                for(let i=split.length - 1;i>=0;i--){
                    const translation_key = [split.slice(0,i+1).join('-'),tag].join(".");

                    if (Translate.hasTranslate(translation_key)){
                        return Translate.getTranslate(translation_key);
                    }
                }
            }

            return tag;
        }

        const split = language_key.split('-');
        for(let i=split.length - 1;i>=0;i--){
            const translation_key = [split.slice(0,i+1).join('-'),tag].join(".");

            if (Translate.hasTranslate(translation_key)){
                return Translate.getTranslate(translation_key);
            }
        }
         */

        return tag;
    }

    isBaseError(): this is BaseDupyError {
        return true;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [BundleError](BundleError) and the actual [BaseDupyError](BaseDupyError)**.
     * @description **Saves the relation between [BundleError](BundleError) class instances and the actual [BaseDupyError](BaseDupyError) class instance**.
     * @category @BaseDupyError.parent
     * @category Relation between BundleError and BaseDupyError
     * @category property
     * @category relation
     * @protected
     * @see BaseDupyError.linkBaseDupyError
     * @see BaseDupyError.hasBaseDupyErrorLinked
     * @see BaseDupyError.unsafeBaseDupyError
     * @see BaseDupyError.getBaseDupyError
     * @see BaseDupyError.unlinkBaseDupyError
     * @see BaseDupyError.isLinkedWithBaseDupyError
     */
    protected parent: BundleError|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associates a [BundleError](BundleError) class instance**.
     * @description Allows you to **associate a [BundleError](BundleError) class instance** with your actual [BaseDupyError](BaseDupyError) class instance. *This method allows one parameter which is a [BundleError](BundleError) class instance that will be associated*. *Note that the method will also run [BundleError.linkChildren](BundleError.linkChildren) to associate the [BundleError](BundleError) class instance et the [BaseDupyError](BaseDupyError) class instance from the both parts*. *Note also that the method will also run [unlinkParent](BaseDupyError.unlinkParent) to dissociate the current [BundleError](BundleError) class instance associated*.
     * @return Returns the actual [BaseDupyError](BaseDupyError) class instance.
     * @category @BaseDupyError.parent
     * @category Relation between BundleError and BaseDupyError
     * @category association
     * @category relation
     * @public
     * @see BaseDupyError.base_dupy_error
     * @see BaseDupyError.hasBaseDupyErrorLinked
     * @see BaseDupyError.unsafeBaseDupyError
     * @see BaseDupyError.getBaseDupyError
     * @see BaseDupyError.unlinkBaseDupyError
     * @see BaseDupyError.isLinkedWithBaseDupyError
     * @see BundleError.linkChildren
     */
    linkParent(parent: BundleError): this{
        if (this.isLinkedWithParent(parent)){
            return this;
        }


        if (!parent.areChildrenLinked(this)){
            parent.linkChildren(this);

            return this;
        }


        this.unlinkParent();
        this.parent = parent;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if a [BundleError](BundleError) class instance is associated**.
     * @return **Gives a boolean if a [BundleError](BundleError) class instance is associated** or not to our actual [BaseDupyError](BaseDupyError) class instance.
     * @description **Check if a [BundleError](BundleError) class instance is associated** or not to our actual [BaseDupyError](BaseDupyError) class instance.
     * @category @BaseDupyError.parent
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category defined
     * @public
     * @see BaseDupyError.base_dupy_error
     * @see BaseDupyError.linkBaseDupyError
     * @see BaseDupyError.unsafeBaseDupyError
     * @see BaseDupyError.getBaseDupyError
     * @see BaseDupyError.unlinkBaseDupyError
     * @see BaseDupyError.isLinkedWithBaseDupyError
     */
    hasBundleErrorLinked(): boolean{
        return typeof this.parent !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [BundleError](BundleError) class instance**. Unlike the [unsafeParent](BaseDupyError.unsafeParent) method, **the method can throw an [UnlinkedValue](UnlinkedValue) error**.
     * @description **Gives the associated [BundleError](BundleError) class instance** of our actual [BaseDupyError](BaseDupyError) class instance. Unlike the [unsafeParent](BaseDupyError.unsafeParent) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [BaseDupyError](BaseDupyError) class instance doesn't have [BundleError](BundleError) class instance associate**.
     * @return **Results the associated [BundleError](BundleError) class instance** of our actual [BaseDupyError](BaseDupyError) class instance. Unlike the [unsafeParent](BaseDupyError.unsafeParent) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [BaseDupyError](BaseDupyError) class instance doesn't have [BundleError](BundleError) class instance associate**.
     * @category @BaseDupyError.parent
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category value
     * @category getter
     * @public
     * @see BaseDupyError.base_dupy_error
     * @see BaseDupyError.linkBaseDupyError
     * @see BaseDupyError.hasBaseDupyErrorLinked
     * @see BaseDupyError.unsafeBaseDupyError
     * @see BaseDupyError.unlinkBaseDupyError
     * @see BaseDupyError.isLinkedWithBaseDupyError
     */
    getParent(): BundleError{
        const unsafe = this.unsafeParent();

        if (typeof unsafe === "undefined"){
            const error = new UnlinkedValue();
            error.setMethod("getParent");
            error.setClassname("BaseDupyError");
            error.setWantedClassname("BundleError");
            error.setProperty("parent");
            throw error;
        }
        return unsafe;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [BundleError](BundleError) class instance**. Unlike the [getParent](BaseDupyError.getParent) method, **the method can result an undefined value.
     * @description **Gives the associated [BundleError](BundleError) class instance** of our actual [BaseDupyError](BaseDupyError) class instance. Unlike the [getParent](BaseDupyError.getParent) method, **the method results an undefined value if the [BaseDupyError](BaseDupyError) class instance doesn't have [BundleError](BundleError) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Results the associated [BundleError](BundleError) class instance** of our actual [BaseDupyError](BaseDupyError) class instance. Unlike the [getParent](BaseDupyError.getParent) method, **the method results an undefined value if the [BaseDupyError](BaseDupyError) class instance doesn't have [BundleError](BundleError) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @BaseDupyError.parent
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category value
     * @category unsafe
     * @public
     * @see BaseDupyError.base_dupy_error
     * @see BaseDupyError.linkBaseDupyError
     * @see BaseDupyError.hasBaseDupyErrorLinked
     * @see BaseDupyError.getBaseDupyError
     * @see BaseDupyError.unlinkBaseDupyError
     * @see BaseDupyError.isLinkedWithBaseDupyError
     */
    unsafeParent(): BundleError|undefined{
        return this.parent;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate the [BundleError](BundleError) class instance**.
     * @description **Dissociate the [BundleError](BundleError) class instance** from your actual [BaseDupyError](BaseDupyError) class instance.  *Note that the method will also run [BundleError.unlinkChildren](BundleError.unlinkChildren) to dissociate the [BundleError](BundleError) class instance et the [BaseDupyError](BaseDupyError) class instance from the both parts*.
     * @return Return the actual [BaseDupyError](BaseDupyError) class instance.
     * @category @BaseDupyError.parent
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category value
     * @category dissociate
     * @public
     * @see BaseDupyError.base_dupy_error
     * @see BaseDupyError.linkBaseDupyError
     * @see BaseDupyError.hasBaseDupyErrorLinked
     * @see BaseDupyError.unsafeBaseDupyError
     * @see BaseDupyError.getBaseDupyError
     * @see BaseDupyError.isLinkedWithBaseDupyError
     * @see BundleError.unlinkChildren
     */
    unlinkParent(): this{
        const _parent = this.parent;
        this.parent = undefined;

        _parent?.unlinkChildren(this);

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [BundleError](BundleError) class instance** are associated.
     * @description **Checks if the [BaseDupyError](BaseDupyError) class instance is associated with all the given [BundleError](BundleError) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Return a boolean if the [BaseDupyError](BaseDupyError) class instance is associated with all the given [BundleError](BundleError) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @category @BaseDupyError.parent
     * @category Relation between BundleError and BaseDupyError
     * @category relation
     * @category value
     * @category equality
     * @public
     * @see BaseDupyError.base_dupy_error
     * @see BaseDupyError.linkBaseDupyError
     * @see BaseDupyError.hasBaseDupyErrorLinked
     * @see BaseDupyError.unsafeBaseDupyError
     * @see BaseDupyError.getBaseDupyError
     * @see BaseDupyError.unlinkBaseDupyError
     */
    isLinkedWithParent(parent?: undefined|BundleError): boolean{
        if (typeof parent === "undefined"){
            return false;
        }
        return this.unsafeParent() === parent;
    }
    
    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @BaseDupyError.context.
     * @description Save @BaseDupyError.context.
     * @category @BaseDupyError.context
     * @category property
     * @category field
     * @protected
     * @see BaseDupyError.setContext
     * @see BaseDupyError.getContext
     * @see BaseDupyError.unsafeContext
     * @see BaseDupyError.eraseContext
     * @see BaseDupyError.isContextDefined
     * @see BaseDupyError.isContextEqualTo
     */
    protected context: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @BaseDupyError.context**.
     * @description Allows you to update or **assign the value of @BaseDupyError.context**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [BaseDupyError](BaseDupyError) class instance.
     * @param context The new value to assign to @BaseDupyError.context.
     * @category @BaseDupyError.context
     * @category setter
     * @category field
     * @public
     * @see BaseDupyError.context
     * @see BaseDupyError.getContext
     * @see BaseDupyError.unsafeContext
     * @see BaseDupyError.eraseContext
     * @see BaseDupyError.isContextDefined
     * @see BaseDupyError.isContextEqualTo
     */
    setContext(context: string): this{
        if (this.isContextEqualTo(context)){
            return this;
        }

        this.eraseContext();
        this.context = context;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseDupyError.context**. Unlike the [getContext](BaseDupyError.getContext) method, **the method results an undefined value if @BaseDupyError.context is not defined**.
     * @description **Gives the value of @BaseDupyError.context**. Unlike the [getContext](BaseDupyError.getContext) method, **the method results an undefined value if @BaseDupyError.context is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @BaseDupyError.context**. Unlike the [getContext](BaseDupyError.getContext) method, **the method results an undefined value if @BaseDupyError.context is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @BaseDupyError.context
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see BaseDupyError.context
     * @see BaseDupyError.setContext
     * @see BaseDupyError.getContext
     * @see BaseDupyError.eraseContext
     * @see BaseDupyError.isContextDefined
     * @see BaseDupyError.isContextEqualTo
     */
    unsafeContext(): string|undefined{
        return this.context;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseDupyError.context**. Unlike the [unsafeContext](BaseDupyError.unsafeContext) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseDupyError.context is not defined**.
     * @description **Gives the value of @BaseDupyError.context**. Unlike the [unsafeContext](BaseDupyError.unsafeContext) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseDupyError.context is not defined**.
     * @return **Returns the value of @BaseDupyError.context**. Unlike the [unsafeContext](BaseDupyError.unsafeContext) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseDupyError.context is not defined**.
     * @category @BaseDupyError.context
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @BaseDupyError.context isn't defined.
     * @see BaseDupyError.context
     * @see BaseDupyError.setContext
     * @see BaseDupyError.unsafeContext
     * @see BaseDupyError.eraseContext
     * @see BaseDupyError.isContextDefined
     * @see BaseDupyError.isContextEqualTo
     */
    getContext(): string{
        const context = this.unsafeContext();

        if (typeof context === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getContext");
            error.setClassname("BaseDupyError");
            error.setProperty("context");
            throw error;
        }
        return context;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseDupyError.context is defined**.
     * @description **Checks if @BaseDupyError.context is defined** or not.
     * @return **Returns a boolean if @BaseDupyError.context is defined** or not.
     * @category @BaseDupyError.context
     * @category defined
     * @category field
     * @public
     * @see BaseDupyError.context
     * @see BaseDupyError.setContext
     * @see BaseDupyError.getContext
     * @see BaseDupyError.unsafeContext
     * @see BaseDupyError.eraseContext
     * @see BaseDupyError.isContextEqualTo
     */
    isContextDefined(): boolean{
        return typeof this.context !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseDupyError.context is equal to your given value**.
     * @description **Checks if @BaseDupyError.context is equal to your given value** and if @BaseDupyError.context is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @BaseDupyError.context is equal to your given value** and if @BaseDupyError.context is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param context **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @BaseDupyError.context
     * @category equality
     * @category field
     * @public
     * @see BaseDupyError.context
     * @see BaseDupyError.setContext
     * @see BaseDupyError.getContext
     * @see BaseDupyError.unsafeContext
     * @see BaseDupyError.eraseContext
     * @see BaseDupyError.isContextDefined
     */
    isContextEqualTo(context?: undefined|string): boolean{
        if (typeof context === "undefined"){
            return false;
        }
        return this.unsafeContext() === context;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @BaseDupyError.context**.
     * @description **Removes the assigned the value of @BaseDupyError.context**: *@BaseDupyError.context will be flagged as undefined*.
     * @return Return the actual [BaseDupyError](BaseDupyError) class instance.
     * @category @BaseDupyError.context
     * @category truncate
     * @category field
     * @public
     * @see BaseDupyError.context
     * @see BaseDupyError.setContext
     * @see BaseDupyError.getContext
     * @see BaseDupyError.unsafeContext
     * @see BaseDupyError.isContextDefined
     * @see BaseDupyError.isContextEqualTo
     */
    eraseContext(): this{
        this.context = undefined;

        return this;
    }
}

export default BaseDupyError;