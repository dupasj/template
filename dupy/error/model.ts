import DupyError from "./index";
import ModelInstance from "../model/instance";
import ModelConfiguration from "../model";
import UndefinedValue from "./internal/undefined-value";

class ModelError extends DupyError{
    isModelError(): this is ModelError{
        return true
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @ModelError.model.
     * @description Save @ModelError.model.
     * @category @ModelError.model
     * @category property
     * @category field
     * @protected
     * @see ModelError.setModel
     * @see ModelError.getModel
     * @see ModelError.unsafeModel
     * @see ModelError.eraseModel
     * @see ModelError.isModelDefined
     * @see ModelError.isModelEqualTo
     */
    protected model: (ModelInstance|ModelConfiguration)|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @ModelError.model**.
     * @description Allows you to update or **assign the value of @ModelError.model**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [ModelError](ModelError) class instance.
     * @param model The new value to assign to @ModelError.model.
     * @category @ModelError.model
     * @category setter
     * @category field
     * @public
     * @see ModelError.model
     * @see ModelError.getModel
     * @see ModelError.unsafeModel
     * @see ModelError.eraseModel
     * @see ModelError.isModelDefined
     * @see ModelError.isModelEqualTo
     */
    setModel(model: (ModelInstance|ModelConfiguration)): this{
        if (this.isModelEqualTo(model)){
            return this;
        }

        this.eraseModel();
        this.model = model;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @ModelError.model**. Unlike the [getModel](ModelError.getModel) method, **the method results an undefined value if @ModelError.model is not defined**.
     * @description **Gives the value of @ModelError.model**. Unlike the [getModel](ModelError.getModel) method, **the method results an undefined value if @ModelError.model is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @ModelError.model**. Unlike the [getModel](ModelError.getModel) method, **the method results an undefined value if @ModelError.model is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @ModelError.model
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see ModelError.model
     * @see ModelError.setModel
     * @see ModelError.getModel
     * @see ModelError.eraseModel
     * @see ModelError.isModelDefined
     * @see ModelError.isModelEqualTo
     */
    unsafeModel(): (ModelInstance|ModelConfiguration)|undefined{
        return this.model;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @ModelError.model**. Unlike the [unsafeModel](ModelError.unsafeModel) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ModelError.model is not defined**.
     * @description **Gives the value of @ModelError.model**. Unlike the [unsafeModel](ModelError.unsafeModel) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ModelError.model is not defined**.
     * @return **Returns the value of @ModelError.model**. Unlike the [unsafeModel](ModelError.unsafeModel) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ModelError.model is not defined**.
     * @category @ModelError.model
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @ModelError.model isn't defined.
     * @see ModelError.model
     * @see ModelError.setModel
     * @see ModelError.unsafeModel
     * @see ModelError.eraseModel
     * @see ModelError.isModelDefined
     * @see ModelError.isModelEqualTo
     */
    getModel(): (ModelInstance|ModelConfiguration){
        const model = this.unsafeModel();

        if (typeof model === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getModel");
            error.setClassname("ModelError");
            error.setProperty("model");
            throw error;
        }
        return model;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @ModelError.model is defined**.
     * @description **Checks if @ModelError.model is defined** or not.
     * @return **Returns a boolean if @ModelError.model is defined** or not.
     * @category @ModelError.model
     * @category defined
     * @category field
     * @public
     * @see ModelError.model
     * @see ModelError.setModel
     * @see ModelError.getModel
     * @see ModelError.unsafeModel
     * @see ModelError.eraseModel
     * @see ModelError.isModelEqualTo
     */
    isModelDefined(): boolean{
        return typeof this.model !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @ModelError.model is equal to your given value**.
     * @description **Checks if @ModelError.model is equal to your given value** and if @ModelError.model is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @ModelError.model is equal to your given value** and if @ModelError.model is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param model **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @ModelError.model
     * @category equality
     * @category field
     * @public
     * @see ModelError.model
     * @see ModelError.setModel
     * @see ModelError.getModel
     * @see ModelError.unsafeModel
     * @see ModelError.eraseModel
     * @see ModelError.isModelDefined
     */
    isModelEqualTo(model?: undefined|(ModelInstance|ModelConfiguration)): boolean{
        if (typeof model === "undefined"){
            return false;
        }
        return this.unsafeModel() === model;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @ModelError.model**.
     * @description **Removes the assigned the value of @ModelError.model**: *@ModelError.model will be flagged as undefined*.
     * @return Return the actual [ModelError](ModelError) class instance.
     * @category @ModelError.model
     * @category truncate
     * @category field
     * @public
     * @see ModelError.model
     * @see ModelError.setModel
     * @see ModelError.getModel
     * @see ModelError.unsafeModel
     * @see ModelError.isModelDefined
     * @see ModelError.isModelEqualTo
     */
    eraseModel(): this{
        this.model = undefined;

        return this;
    }
}

export default ModelError;
