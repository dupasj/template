import BaseFieldInstance from "./field/base/instance";
import DeepArray from "../util/deep-array/type";
import flat from "../util/deep-array/flat";
import OutOfRange from "../error/internal/out-of-range";
import DupyObject from "../dupy-object";
import ModelConfiguration from "./index";

class ModelInstance<Configuration extends ModelConfiguration = ModelConfiguration> extends DupyObject{


    isModel(): this is ModelInstance | ModelConfiguration {
        return true;
    }
    isModelInstance(): this is ModelInstance {
        return true;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [BaseFieldInstance](BaseFieldInstance) and the actual [ModelInstance](ModelInstance)**.
     * @description **An array of saved relations between [BaseFieldInstance](BaseFieldInstance) class instances and the actual [ModelInstance](ModelInstance) class instance**.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category property
     * @protected
     * @see ModelInstance.areFieldsLinked
     * @see ModelInstance.disposeFields
     * @see ModelInstance.unlinkFields
     * @see ModelInstance.unsafeFields
     * @see ModelInstance.getFields
     * @see ModelInstance.linkFields
     * @see ModelInstance.getField
     * @see ModelInstance.unsafeField
     * @see ModelInstance.getFieldsSize
     */
    protected fields: BaseFieldInstance[] = [];


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the number of the associated [BaseFieldInstance](BaseFieldInstance) class instances**.
     * @description **Gives the size of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances**.
     * @return **Returns the number of the associated [BaseFieldInstance](BaseFieldInstance) class instances**.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category length
     * @public
     * @see ModelInstance.areFieldsLinked
     * @see ModelInstance.disposeFields
     * @see ModelInstance.unlinkFields
     * @see ModelInstance.unsafeFields
     * @see ModelInstance.getFields
     * @see ModelInstance.linkFields
     * @see ModelInstance.getField
     * @see ModelInstance.unsafeField
     * @see ModelInstance.fields
     */
    getFieldsSize(): number{
        return this.unsafeFields().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance)** class instances. Unlike [getField](ModelInstance.getField) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances**. Unlike [getField](ModelInstance.getField) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances**. Unlike [getField](ModelInstance.getField) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance), you can give `-1` for example. If you want to get the first item of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance), you can give `0`.*
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category unsafe
     * @category value
     * @public
     * @see ModelInstance.areFieldsLinked
     * @see ModelInstance.disposeFields
     * @see ModelInstance.unlinkFields
     * @see ModelInstance.unsafeFields
     * @see ModelInstance.getFields
     * @see ModelInstance.linkFields
     * @see ModelInstance.getField
     * @see ModelInstance.getFieldsSize
     * @see ModelInstance.fields
     */
    unsafeField(position: number): BaseFieldInstance|undefined{
        if (position < 0){
            const new_position = this.getFieldsSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeField(new_position);
        }

        return this.fields[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances**. Unlike the [unsafeTask](#one-to-many-ModelInstance--unsafeField) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances**. Unlike the [unsafeTask](#one-to-many-ModelInstance--unsafeField) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances**. Unlike the [unsafeTask](#one-to-many-ModelInstance--unsafeField) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance), you can give `-1` for example. If you want to get the first item of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance), you can give `0`.*
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category getter
     * @category value
     * @public
     * @see ModelInstance.areFieldsLinked
     * @see ModelInstance.disposeFields
     * @see ModelInstance.unlinkFields
     * @see ModelInstance.unsafeFields
     * @see ModelInstance.getFields
     * @see ModelInstance.linkFields
     * @see ModelInstance.unsafeField
     * @see ModelInstance.getFieldsSize
     * @see ModelInstance.fields
     */
    getField(position: number): BaseFieldInstance{
        const field = this.unsafeField(position);
        if (typeof field === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getField");
            error.setClassname("ModelInstance");
            error.setProperty("fields");
            error.setPosition(position);
            throw error;
        }
        return field;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associate one or multiple [BaseFieldInstance](BaseFieldInstance) class instances** from your actual [ModelInstance](ModelInstance) class instance.
     * @description Allows you to **associate one or multiple [BaseFieldInstance](BaseFieldInstance) class instances** from your actual [ModelInstance](ModelInstance) class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate [ModelInstance](ModelInstance) class instance** ([BaseFieldInstance.unlinkModelInstance](BaseFieldInstance.unlinkModelInstance)), and **then associate the  [BaseFieldInstance](BaseFieldInstance) class instances** to the [ModelInstance](ModelInstance) class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [BaseFieldInstance.linkModelInstance](BaseFieldInstance.linkModelInstance) to associate the [ModelInstance](ModelInstance) class instance and the [BaseFieldInstance](BaseFieldInstance) class instances from the both parts*.
     * @return Returns the actual [BaseFieldInstance](BaseFieldInstance) class instance.
     * @param fields All the given [BaseFieldInstance](BaseFieldInstance) class instances that you to associate to your actual [ModelInstance](ModelInstance) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category associate
     * @public
     * @see ModelInstance.areFieldsLinked
     * @see ModelInstance.disposeFields
     * @see ModelInstance.unlinkFields
     * @see ModelInstance.unsafeFields
     * @see ModelInstance.getFields
     * @see ModelInstance.getField
     * @see ModelInstance.unsafeField
     * @see ModelInstance.getFieldsSize
     * @see ModelInstance.fields
     * @see BaseFieldInstance.linkModelInstance
     */
    linkFields(... fields: DeepArray<BaseFieldInstance>[]): this{
        for (const field of flat(fields)){
            if (this.areFieldsLinked(field)){
                continue;
            }


            this.fields.push(field);


            field.linkModelInstance(this);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances**.
     * @description **Gives a copy of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances**.
     * @return **Returns a copy of the [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances**.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category getter
     * @category iterable
     * @public
     * @see ModelInstance.areFieldsLinked
     * @see ModelInstance.disposeFields
     * @see ModelInstance.unlinkFields
     * @see ModelInstance.unsafeFields
     * @see ModelInstance.linkFields
     * @see ModelInstance.getField
     * @see ModelInstance.unsafeField
     * @see ModelInstance.getFieldsSize
     * @see ModelInstance.fields
     */
    getFields(): BaseFieldInstance[]{
        return Array.from(this.unsafeFields());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives an [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances without copy**.
     * @description **Gives an [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getFields](ModelInstance.getFields) method.
     * @return **Returns an [array](ModelInstance.fields) of the associated [BaseFieldInstance](BaseFieldInstance) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getFields](ModelInstance.getFields) method.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category unsafe
     * @category iterable
     * @public
     * @see ModelInstance.areFieldsLinked
     * @see ModelInstance.disposeFields
     * @see ModelInstance.unlinkFields
     * @see ModelInstance.getFields
     * @see ModelInstance.linkFields
     * @see ModelInstance.getField
     * @see ModelInstance.unsafeField
     * @see ModelInstance.getFieldsSize
     * @see ModelInstance.fields
     */
    unsafeFields(): BaseFieldInstance[]{
        return this.fields;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate one or multiple [BaseFieldInstance](BaseFieldInstance) class instances**.
     * @description Allows you to **dissociate one or multiple [BaseFieldInstance](BaseFieldInstance) class instances** from your actual [ModelInstance](ModelInstance) class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the [ModelInstance](ModelInstance) class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [BaseFieldInstance.unlinkModelInstance](BaseFieldInstance.unlinkModelInstance) to dissociate the [ModelInstance](ModelInstance) class instance and the given [BaseFieldInstance](BaseFieldInstance) class instances from the both parts*.
     * @return Returns the actual [BaseFieldInstance](BaseFieldInstance) class instance.
     * @param fields All the given [BaseFieldInstance](BaseFieldInstance) class instances that you to dissociate from your actual [ModelInstance](ModelInstance) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category dissociate
     * @public
     * @see ModelInstance.areFieldsLinked
     * @see ModelInstance.disposeFields
     * @see ModelInstance.unsafeFields
     * @see ModelInstance.getFields
     * @see ModelInstance.linkFields
     * @see ModelInstance.getField
     * @see ModelInstance.unsafeField
     * @see ModelInstance.getFieldsSize
     * @see ModelInstance.fields
     * @see BaseFieldInstance.unlinkModelInstance
     */
    unlinkFields(... fields: DeepArray<BaseFieldInstance>[]): this{
        for (const field of flat(fields)){
            if (!this.areFieldsLinked(field)){
                continue;
            }

            while (true){
                const index = this.fields.indexOf(field);
                if (index < 0){
                    break;
                }

                this.fields.splice(index, 1);
            }

            field.unlinkModelInstance();
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate all the associated [BaseFieldInstance](BaseFieldInstance) class instances**.
     * @description **Dissociate all the associated [BaseFieldInstance](BaseFieldInstance) class instances**.  *Note that all the associated [BaseFieldInstance](BaseFieldInstance) class instances will lose their [ModelInstance](ModelInstance) class instance association*.
     * @return Returns the actual [BaseFieldInstance](BaseFieldInstance) class instance.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category truncate
     * @public
     * @see ModelInstance.areFieldsLinked
     * @see ModelInstance.unlinkFields
     * @see ModelInstance.unsafeFields
     * @see ModelInstance.getFields
     * @see ModelInstance.linkFields
     * @see ModelInstance.getField
     * @see ModelInstance.unsafeField
     * @see ModelInstance.getFieldsSize
     * @see ModelInstance.fields
     */
    disposeFields(): this{
        this.unlinkFields(this.unsafeFields());
        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [BaseFieldInstance](BaseFieldInstance) class instances are associated**.
     * @description **Checks if all the given [BaseFieldInstance](BaseFieldInstance) class instances are associated** with your actual [ModelInstance](ModelInstance) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [ModelInstance](ModelInstance) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [BaseFieldInstance](BaseFieldInstance) class instances are associated with your [ModelInstance](ModelInstance) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @return **Return a boolean if all the given [BaseFieldInstance](BaseFieldInstance) class instances are associated** with your actual [ModelInstance](ModelInstance) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [ModelInstance](ModelInstance) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [BaseFieldInstance](BaseFieldInstance) class instances are associated with your [ModelInstance](ModelInstance) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @param fields All the given [BaseFieldInstance](BaseFieldInstance) class instances that you want to check their relation *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category defined
     * @public
     * @see ModelInstance.disposeFields
     * @see ModelInstance.unlinkFields
     * @see ModelInstance.unsafeFields
     * @see ModelInstance.getFields
     * @see ModelInstance.linkFields
     * @see ModelInstance.getField
     * @see ModelInstance.unsafeField
     * @see ModelInstance.getFieldsSize
     * @see ModelInstance.fields
     */
    areFieldsLinked(... fields: DeepArray<BaseFieldInstance|undefined>[]): boolean{
        return flat(fields).every((field) => {
            if (typeof field === "undefined"){
                return false;
            }

            return this.unsafeFields().includes(field);
        });
    }
}

export default ModelInstance
