import UndefinedValue from "../error/internal/undefined-value";
import BaseFieldConfiguration from "./field/base";
import OutOfRange from "../error/internal/out-of-range";
import DeepArray from "../util/deep-array/type";
import flat from "../util/deep-array/flat";
import Project from "../project";
import UnlinkedValue from "../error/internal/unlinked-value";
import ItemOrArray from "../util/item-or-array";
import PivotConfiguration from "./field/pivot";
import DupyObject from "../dupy-object";
import ModelInstance from "./instance";
import UnknownPivotParentKey from "../error/internal/unknown/pivot/parent/key";
import UnknownPivotParentOutputKey from "../error/internal/unknown/pivot/parent/output-key";
import UnknownPivotChildKey from "../error/internal/unknown/pivot/child/key";
import UnknownPivotChildOutputKey from "../error/internal/unknown/pivot/child/output-key";
import UnknownStorageKey from "../error/internal/unknown/storage-key";
import UnknownOutputKey from "../error/internal/unknown/output-key";
import Match from "../util/match/type";

class ModelConfiguration extends DupyObject{

    isModel(): this is ModelInstance | ModelConfiguration {
        return true;
    }
    isModelConfiguration(): this is ModelConfiguration {
        return true;
    }

    getParentPivots(): PivotConfiguration[] {
        return this.unsafeFields().filter(field => field.isPivotConfiguration()) as PivotConfiguration[];
    }
    getParentFields(): BaseFieldConfiguration[] {
        return this.getParentPivots().map(field => field.unsafeBase()).filter(base => typeof base !== "undefined") as BaseFieldConfiguration[];
    }
    getParents(): ModelConfiguration[] {
        return this.getParentFields().map(field => field.unsafeModel()).filter(model => typeof model !== "undefined") as ModelConfiguration[];
    }

    hasParentPivotFromKey(key?: undefined|string): boolean {
        return !this.getParentPivots().every(field => !field.isParentKeyEqualTo(key));
    }
    hasParentFieldFromKey(key?: undefined|string): boolean {
        return !this.getParentPivots().every(field => !(field.isParentKeyEqualTo(key) && field.hasBaseLinked()));
    }
    hasParentFromKey(key?: undefined|string): boolean {
        return !this.getParentPivots().every(field => !(field.isParentKeyEqualTo(key) && field.unsafeBase()?.hasModelLinked()));
    }
    unsafeParentPivotFromKey(key?: undefined|string): PivotConfiguration|undefined {
        return this.getParentPivots().find(field => field.isParentKeyEqualTo(key));
    }
    unsafeParentFieldFromKey(key?: undefined|string): BaseFieldConfiguration|undefined {
        return this.unsafeParentPivotFromKey(key)?.unsafeBase();
    }
    unsafeParentFromKey(key?: undefined|string): ModelConfiguration|undefined {
        return this.unsafeParentFieldFromKey(key)?.unsafeModel();
    }
    getParentPivotFromKey(key: string): PivotConfiguration {
        const parent = this.getParentPivots().find(field => field.isParentKeyEqualTo(key));

        if (typeof parent === "undefined"){
            const error = new UnknownPivotParentKey();
            error.setGiven(key);
            error.setModel(this);
            throw error;
        }

        return parent;
    }
    getParentFieldFromKey(key: string): BaseFieldConfiguration {
        return this.getParentPivotFromKey(key).getBase();
    }
    getParentFromKey(key: string): ModelConfiguration {
        return this.getParentFieldFromKey(key).getModel();
    }
    hasParentPivotFromOutputKey(key?: undefined|string): boolean {
        return !this.getParentPivots().every(field => !field.isParentOutputKeyEqualTo(key));
    }
    hasParentFieldFromOutputKey(key?: undefined|string): boolean {
        return !this.getParentPivots().every(field => !(field.isParentOutputKeyEqualTo(key) && field.hasBaseLinked()));
    }
    hasParentFromOutputKey(key?: undefined|string): boolean {
        return !this.getParentPivots().every(field => !(field.isParentOutputKeyEqualTo(key) && field.unsafeBase()?.hasModelLinked()));
    }
    unsafeParentPivotFromOutputKey(key?: undefined|string): PivotConfiguration|undefined {
        return this.getParentPivots().find(field => field.isParentOutputKeyEqualTo(key));
    }
    unsafeParentFieldFromOutputKey(key?: undefined|string): BaseFieldConfiguration|undefined {
        return this.unsafeParentPivotFromOutputKey(key)?.unsafeBase();
    }
    unsafeParentFromOutputKey(key?: undefined|string): ModelConfiguration|undefined {
        return this.unsafeParentFieldFromOutputKey(key)?.unsafeModel();
    }
    getParentPivotFromOutputKey(key: string): PivotConfiguration {
        const parent = this.getParentPivots().find(field => field.isParentOutputKeyEqualTo(key));

        if (typeof parent === "undefined"){
            const error = new UnknownPivotParentOutputKey();
            error.setGiven(key);
            error.setModel(this);
            throw error;
        }

        return parent;
    }
    getParentFieldFromOutputKey(key: string): BaseFieldConfiguration {
        return this.getParentPivotFromOutputKey(key).getBase();
    }
    getParentFromOutputKey(key: string): ModelConfiguration {
        return this.getParentFieldFromOutputKey(key).getModel();
    }
    fetchParentsPivotsFromInputKey(key?: Match): PivotConfiguration[] {
        return this.getParentPivots().filter(field => field.matchingWithParentInputKeys(key));
    }
    fetchParentsFieldsFromInputKey(key?: Match): BaseFieldConfiguration[] {
        return this.fetchParentsPivotsFromInputKey(key).map(field => field.getBase());
    }
    fetchParentsFromInputKey(key?: Match): ModelConfiguration[] {
        return this.fetchParentsFieldsFromInputKey(key).map(field => field.getModel());
    }
    fetchParentsPivotsFromFilterKey(key?: Match): PivotConfiguration[] {
        return this.getParentPivots().filter(field => field.matchingWithParentFilterKeys(key));
    }
    fetchParentsFieldsFromFilterKey(key?: Match): BaseFieldConfiguration[] {
        return this.fetchParentsPivotsFromFilterKey(key).map(field => field.getBase());
    }
    fetchParentsFromFilterKey(key?: Match): ModelConfiguration[] {
        return this.fetchParentsFieldsFromFilterKey(key).map(field => field.getModel());
    }
    getChildrenPivots(): PivotConfiguration[] {
        const result: PivotConfiguration[] = [];

        for(const field of this.getFields()){
            for(const child of field.unsafePivots()){
                if (!result.includes(child)){
                    result.push(child);
                }
            }
        }

        return result;
    }
    getChildrenFields(): PivotConfiguration[] {
        return this.getChildrenPivots();
    }
    getChildren(): ModelConfiguration[] {
        return this.getChildrenFields().map(field => field.unsafeModel()).filter(model => typeof model !== "undefined") as ModelConfiguration[];
    }
    hasChildPivotFromKey(key?: undefined|string): boolean {
        return !this.getChildrenPivots().every(field => !field.isChildKeyEqualTo(key));
    }
    hasChildFieldFromKey(key?: undefined|string): boolean {
        return !this.hasChildPivotFromKey(key);
    }
    hasChildFromKey(key?: undefined|string): boolean {
        return !this.getChildrenPivots().every(field => !(field.isChildKeyEqualTo(key) && field.hasModelLinked()));
    }
    unsafeChildPivotFromKey(key?: undefined|string): PivotConfiguration|undefined {
        return this.getChildrenPivots().find(field => field.isChildKeyEqualTo(key));
    }
    unsafeChildFieldFromKey(key?: undefined|string): BaseFieldConfiguration|undefined {
        return this.unsafeChildPivotFromKey(key);
    }
    unsafeChildFromKey(key?: undefined|string): ModelConfiguration|undefined {
        return this.unsafeChildPivotFromKey(key)?.unsafeModel();
    }
    getChildPivotFromKey(key: string): PivotConfiguration {
        const child = this.getChildrenPivots().find(field => field.isChildKeyEqualTo(key));

        if (typeof child === "undefined"){
            const error = new UnknownPivotChildKey();
            error.setGiven(key);
            error.setModel(this);
            throw error;
        }

        return child;
    }
    getChildFieldFromKey(key: string): BaseFieldConfiguration {
        return this.getChildPivotFromKey(key);
    }
    getChildFromKey(key: string): ModelConfiguration {
        return this.getChildPivotFromKey(key).getModel();
    }
    hasChildPivotFromOutputKey(key?: undefined|string): boolean {
        return !this.getChildrenPivots().every(field => !field.isChildOutputKeyEqualTo(key));
    }
    hasChildFieldFromOutputKey(key?: undefined|string): boolean {
        return !this.hasChildPivotFromOutputKey(key);
    }
    hasChildFromOutputKey(key?: undefined|string): boolean {
        return !this.getChildrenPivots().every(field => !(field.isChildOutputKeyEqualTo(key) && field.hasModelLinked()));
    }
    unsafeChildPivotFromOutputKey(key?: undefined|string): PivotConfiguration|undefined {
        return this.getChildrenPivots().find(field => field.isChildOutputKeyEqualTo(key));
    }
    unsafeChildFieldFromOutputKey(key?: undefined|string): BaseFieldConfiguration|undefined {
        return this.unsafeChildPivotFromOutputKey(key);
    }
    unsafeChildFromOutputKey(key?: undefined|string): ModelConfiguration|undefined {
        return this.unsafeChildPivotFromOutputKey(key)?.unsafeModel();
    }
    getChildPivotFromOutputKey(key: string): PivotConfiguration {
        const child = this.getChildrenPivots().find(field => field.isChildOutputKeyEqualTo(key));

        if (typeof child === "undefined"){
            const error = new UnknownPivotChildOutputKey();
            error.setGiven(key);
            error.setModel(this);
            throw error;
        }

        return child;
    }
    getChildFieldFromOutputKey(key: string): BaseFieldConfiguration {
        return this.getChildPivotFromOutputKey(key);
    }
    getChildFromOutputKey(key: string): ModelConfiguration {
        return this.getChildPivotFromOutputKey(key).getModel();
    }
    fetchChildrenPivotsFromInputKey(key?: Match): PivotConfiguration[] {
        return this.getChildrenPivots().filter(field => field.matchingWithChildInputKeys(key));
    }
    fetchChildrenFieldsFromInputKey(key?: Match): BaseFieldConfiguration[] {
        return this.fetchChildrenPivotsFromInputKey(key);
    }
    fetchChildrenFromInputKey(key?: Match): ModelConfiguration[] {
        return this.fetchChildrenFieldsFromInputKey(key).map(field => field.getModel());
    }
    fetchChildrenPivotsFromFilterKey(key?: Match): PivotConfiguration[] {
        return this.getChildrenPivots().filter(field => field.matchingWithChildFilterKeys(key));
    }
    fetchChildrenFieldsFromFilterKey(key?: Match): BaseFieldConfiguration[] {
        return this.fetchChildrenPivotsFromFilterKey(key);
    }
    fetchChildrenFromFilterKey(key?: Match): ModelConfiguration[] {
        return this.fetchChildrenFieldsFromFilterKey(key).map(field => field.getModel());
    }
    hasAutoIncrementField(): boolean {
        return !this.unsafeFields().every(field => !field.isAutoIncrement());
    }
    unsafeAutoIncrementField(): BaseFieldConfiguration|undefined{
        return this.unsafeFields().find(field => field.isAutoIncrement());
    }
    getAutoIncrementField(): BaseFieldConfiguration{
        const field = this.unsafeAutoIncrementField();

        if (typeof field === "undefined"){
            throw new Error("TODO")
        }

        return field;
    }
    hasPrimaryField(): boolean {
        return !this.unsafeFields().every(field => !field.isPrimary());
    }
    unsafePrimaryField(): BaseFieldConfiguration|undefined{
        return this.unsafeFields().find(field => field.isPrimary());
    }
    getPrimaryField(): BaseFieldConfiguration{
        const field = this.unsafePrimaryField();

        if (typeof field === "undefined"){
            throw new Error("TODO")
        }

        return field;
    }
    hasFieldFromKey(key?: undefined|string): boolean {
        return !this.unsafeFields().every(field => !field.isKeyEqualTo(key));
    }
    unsafeFieldFromKey(key?: undefined|string): BaseFieldConfiguration|undefined{
        return this.unsafeFields().find(field => field.isKeyEqualTo(key));
    }
    getFieldFromKey(key: string): BaseFieldConfiguration{
        const field = this.unsafeFieldFromKey(key);

        if (typeof field === "undefined"){
            throw new Error("TODO")
        }

        return field;
    }
    hasFieldFromStorageKey(key?: undefined|string): boolean {
        return !this.unsafeFields().every(field => !field.isStorageEqualTo(key));
    }
    unsafeFieldFromStorageKey(key?: undefined|string): BaseFieldConfiguration|undefined{
        return this.unsafeFields().find(field => field.isStorageEqualTo(key));
    }
    getFieldFromStorageKey(key: string): BaseFieldConfiguration{
        const field = this.unsafeFieldFromStorageKey(key);

        if (typeof field === "undefined"){
            const error = new UnknownStorageKey();
            error.setGiven(key);
            error.setModel(this);
            throw error;
        }

        return field;
    }
    hasFieldFromOutputKey(key?: undefined|string): boolean {
        return !this.unsafeFields().every(field => !field.isOutputKeyEqualTo(key));
    }
    unsafeFieldFromOutputKey(key?: undefined|string): BaseFieldConfiguration|undefined{
        return this.unsafeFields().find(field => field.isOutputKeyEqualTo(key));
    }
    getFieldFromOutputKey(key: string): BaseFieldConfiguration{
        const field = this.unsafeFieldFromOutputKey(key);

        if (typeof field === "undefined"){
            const error = new UnknownOutputKey();
            error.setGiven(key);
            error.setModel(this);
            throw error;
        }

        return field;
    }
    fetchFieldFromFilterKey(key?: undefined|Match): BaseFieldConfiguration[]{
        return this.unsafeFields().filter(field => field.matchingWithFilterKeys(key));
    }
    fetchFieldFromInputKey(key?: undefined|Match): BaseFieldConfiguration[]{
        return this.unsafeFields().filter(field => field.matchingWithInputKeys(key));
    }


    uniques: BaseFieldConfiguration[][] = [];
    hasUnique(uniques: ItemOrArray<BaseFieldConfiguration>): boolean {
        if (!Array.isArray(uniques)){
            return this.hasUnique([uniques]);
        }

        if (typeof this.uniques === "undefined"){
            return false;
        }

        const unify = uniques.filter((value, index, self) => {
            return self.indexOf(value) === index;
        });

        for(const value of this.uniques){
            if (unify.length !== value.length) {
                continue;
            }

            let ok = true;
            for(const unique of unify){
                if (!value.includes(unique)){
                    ok = false;
                    break;
                }
            }

            if (ok){
                return true;
            }
        }

        return false;
    }
    removeUnique(unique: ItemOrArray<BaseFieldConfiguration>): this {
        if (!Array.isArray(unique)){
            return this.removeUnique([unique]);
        }

        if (typeof this.uniques === "undefined"){
            return this;
        }

        for(let i=this.uniques.length - 1;i>=0;i--){
            if (this.uniques[i].length !== unique.length) {
                continue;
            }

            let ok = true;
            for(const _unique of this.uniques[i]){
                if (!unique.includes(_unique)){
                    ok = false;
                    break;
                }
            }

            if (ok){
                this.uniques.splice(i,1);
            }
        }

        return this;
    }
    addUnique(uniques: ItemOrArray<BaseFieldConfiguration>): this  {
        if (!Array.isArray(uniques)){
            return this.addUnique([uniques]);
        }

        if (uniques.length <= 0){
            return this;
        }

        const unify = uniques.filter((value, index, self) => {
            return self.indexOf(value) === index;
        });

        if (this.hasUnique(unify)){
            return this;
        }

        for(const unique of unify){
            if (!unique.isStorageDefined()){
                throw new Error("You cannot flag your given field as unique field because the field doesn't have storage key needed for the unique feature");
            }
            if (!this.areFieldsLinked(unique)){
                throw new Error("You cannot flag your given field as unique field because the field isn't linked to your model configuration");
            }
        }

        this.uniques.push(unify);

        return this;
    }
    getUniques(): BaseFieldConfiguration[][] {
        const results: BaseFieldConfiguration[][] = Array.from(this.uniques);

        const auto = this.unsafeAutoIncrementField();
        const primary = this.unsafePrimaryField();

        if (auto && !this.hasUnique(auto)){
            results.push([auto]);
        }

        if (primary !== auto && primary && !this.hasUnique(primary)){
            results.push([primary]);
        }

        return results;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @ModelConfiguration.key.
     * @description Save @ModelConfiguration.key.
     * @category @ModelConfiguration.key
     * @category property
     * @category field
     * @protected
     * @see ModelConfiguration.setKey
     * @see ModelConfiguration.getKey
     * @see ModelConfiguration.unsafeKey
     * @see ModelConfiguration.eraseKey
     * @see ModelConfiguration.isKeyDefined
     * @see ModelConfiguration.isKeyEqualTo
     */
    protected key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @ModelConfiguration.key**.
     * @description Allows you to update or **assign the value of @ModelConfiguration.key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [ModelConfiguration](ModelConfiguration) class instance.
     * @param key The new value to assign to @ModelConfiguration.key.
     * @category @ModelConfiguration.key
     * @category setter
     * @category field
     * @public
     * @see ModelConfiguration.key
     * @see ModelConfiguration.getKey
     * @see ModelConfiguration.unsafeKey
     * @see ModelConfiguration.eraseKey
     * @see ModelConfiguration.isKeyDefined
     * @see ModelConfiguration.isKeyEqualTo
     */
    setKey(key: string): this{
        if (this.isKeyEqualTo(key)){
            return this;
        }

        this.eraseKey();
        this.key = key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @ModelConfiguration.key**. Unlike the [getKey](ModelConfiguration.getKey) method, **the method results an undefined value if @ModelConfiguration.key is not defined**.
     * @description **Gives the value of @ModelConfiguration.key**. Unlike the [getKey](ModelConfiguration.getKey) method, **the method results an undefined value if @ModelConfiguration.key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @ModelConfiguration.key**. Unlike the [getKey](ModelConfiguration.getKey) method, **the method results an undefined value if @ModelConfiguration.key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @ModelConfiguration.key
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see ModelConfiguration.key
     * @see ModelConfiguration.setKey
     * @see ModelConfiguration.getKey
     * @see ModelConfiguration.eraseKey
     * @see ModelConfiguration.isKeyDefined
     * @see ModelConfiguration.isKeyEqualTo
     */
    unsafeKey(): string|undefined{
        return this.key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @ModelConfiguration.key**. Unlike the [unsafeKey](ModelConfiguration.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ModelConfiguration.key is not defined**.
     * @description **Gives the value of @ModelConfiguration.key**. Unlike the [unsafeKey](ModelConfiguration.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ModelConfiguration.key is not defined**.
     * @return **Returns the value of @ModelConfiguration.key**. Unlike the [unsafeKey](ModelConfiguration.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ModelConfiguration.key is not defined**.
     * @category @ModelConfiguration.key
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @ModelConfiguration.key isn't defined.
     * @see ModelConfiguration.key
     * @see ModelConfiguration.setKey
     * @see ModelConfiguration.unsafeKey
     * @see ModelConfiguration.eraseKey
     * @see ModelConfiguration.isKeyDefined
     * @see ModelConfiguration.isKeyEqualTo
     */
    getKey(): string{
        const key = this.unsafeKey();

        if (typeof key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getKey");
            error.setClassname("ModelConfiguration");
            error.setProperty("key");
            throw error;
        }
        return key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @ModelConfiguration.key is defined**.
     * @description **Checks if @ModelConfiguration.key is defined** or not.
     * @return **Returns a boolean if @ModelConfiguration.key is defined** or not.
     * @category @ModelConfiguration.key
     * @category defined
     * @category field
     * @public
     * @see ModelConfiguration.key
     * @see ModelConfiguration.setKey
     * @see ModelConfiguration.getKey
     * @see ModelConfiguration.unsafeKey
     * @see ModelConfiguration.eraseKey
     * @see ModelConfiguration.isKeyEqualTo
     */
    isKeyDefined(): boolean{
        return typeof this.key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @ModelConfiguration.key is equal to your given value**.
     * @description **Checks if @ModelConfiguration.key is equal to your given value** and if @ModelConfiguration.key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @ModelConfiguration.key is equal to your given value** and if @ModelConfiguration.key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @ModelConfiguration.key
     * @category equality
     * @category field
     * @public
     * @see ModelConfiguration.key
     * @see ModelConfiguration.setKey
     * @see ModelConfiguration.getKey
     * @see ModelConfiguration.unsafeKey
     * @see ModelConfiguration.eraseKey
     * @see ModelConfiguration.isKeyDefined
     */
    isKeyEqualTo(key?: undefined|string): boolean{
        if (typeof key === "undefined"){
            return false;
        }
        return this.unsafeKey() === key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @ModelConfiguration.key**.
     * @description **Removes the assigned the value of @ModelConfiguration.key**: *@ModelConfiguration.key will be flagged as undefined*.
     * @return Return the actual [ModelConfiguration](ModelConfiguration) class instance.
     * @category @ModelConfiguration.key
     * @category truncate
     * @category field
     * @public
     * @see ModelConfiguration.key
     * @see ModelConfiguration.setKey
     * @see ModelConfiguration.getKey
     * @see ModelConfiguration.unsafeKey
     * @see ModelConfiguration.isKeyDefined
     * @see ModelConfiguration.isKeyEqualTo
     */
    eraseKey(): this{
        this.key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @ModelConfiguration.storage_key.
     * @description Save @ModelConfiguration.storage_key.
     * @category @ModelConfiguration.storage_key
     * @category property
     * @category field
     * @protected
     * @see ModelConfiguration.setStorageKey
     * @see ModelConfiguration.getStorageKey
     * @see ModelConfiguration.unsafeStorageKey
     * @see ModelConfiguration.eraseStorageKey
     * @see ModelConfiguration.isStorageKeyDefined
     * @see ModelConfiguration.isStorageKeyEqualTo
     */
    protected storage_key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @ModelConfiguration.storage_key**.
     * @description Allows you to update or **assign the value of @ModelConfiguration.storage_key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [ModelConfiguration](ModelConfiguration) class instance.
     * @param storage_key The new value to assign to @ModelConfiguration.storage_key.
     * @category @ModelConfiguration.storage_key
     * @category setter
     * @category field
     * @public
     * @see ModelConfiguration.storage_key
     * @see ModelConfiguration.getStorageKey
     * @see ModelConfiguration.unsafeStorageKey
     * @see ModelConfiguration.eraseStorageKey
     * @see ModelConfiguration.isStorageKeyDefined
     * @see ModelConfiguration.isStorageKeyEqualTo
     */
    setStorageKey(storage_key: string): this{
        if (this.isStorageKeyEqualTo(storage_key)){
            return this;
        }

        this.eraseStorageKey();
        this.storage_key = storage_key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @ModelConfiguration.storage_key**. Unlike the [getStorageKey](ModelConfiguration.getStorageKey) method, **the method results an undefined value if @ModelConfiguration.storage_key is not defined**.
     * @description **Gives the value of @ModelConfiguration.storage_key**. Unlike the [getStorageKey](ModelConfiguration.getStorageKey) method, **the method results an undefined value if @ModelConfiguration.storage_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @ModelConfiguration.storage_key**. Unlike the [getStorageKey](ModelConfiguration.getStorageKey) method, **the method results an undefined value if @ModelConfiguration.storage_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @ModelConfiguration.storage_key
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see ModelConfiguration.storage_key
     * @see ModelConfiguration.setStorageKey
     * @see ModelConfiguration.getStorageKey
     * @see ModelConfiguration.eraseStorageKey
     * @see ModelConfiguration.isStorageKeyDefined
     * @see ModelConfiguration.isStorageKeyEqualTo
     */
    unsafeStorageKey(): string|undefined{
        return this.storage_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @ModelConfiguration.storage_key**. Unlike the [unsafeStorageKey](ModelConfiguration.unsafeStorageKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ModelConfiguration.storage_key is not defined**.
     * @description **Gives the value of @ModelConfiguration.storage_key**. Unlike the [unsafeStorageKey](ModelConfiguration.unsafeStorageKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ModelConfiguration.storage_key is not defined**.
     * @return **Returns the value of @ModelConfiguration.storage_key**. Unlike the [unsafeStorageKey](ModelConfiguration.unsafeStorageKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ModelConfiguration.storage_key is not defined**.
     * @category @ModelConfiguration.storage_key
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @ModelConfiguration.storage_key isn't defined.
     * @see ModelConfiguration.storage_key
     * @see ModelConfiguration.setStorageKey
     * @see ModelConfiguration.unsafeStorageKey
     * @see ModelConfiguration.eraseStorageKey
     * @see ModelConfiguration.isStorageKeyDefined
     * @see ModelConfiguration.isStorageKeyEqualTo
     */
    getStorageKey(): string{
        const storage_key = this.unsafeStorageKey();

        if (typeof storage_key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getStorageKey");
            error.setClassname("ModelConfiguration");
            error.setProperty("storage_key");
            throw error;
        }
        return storage_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @ModelConfiguration.storage_key is defined**.
     * @description **Checks if @ModelConfiguration.storage_key is defined** or not.
     * @return **Returns a boolean if @ModelConfiguration.storage_key is defined** or not.
     * @category @ModelConfiguration.storage_key
     * @category defined
     * @category field
     * @public
     * @see ModelConfiguration.storage_key
     * @see ModelConfiguration.setStorageKey
     * @see ModelConfiguration.getStorageKey
     * @see ModelConfiguration.unsafeStorageKey
     * @see ModelConfiguration.eraseStorageKey
     * @see ModelConfiguration.isStorageKeyEqualTo
     */
    isStorageKeyDefined(): boolean{
        return typeof this.storage_key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @ModelConfiguration.storage_key is equal to your given value**.
     * @description **Checks if @ModelConfiguration.storage_key is equal to your given value** and if @ModelConfiguration.storage_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @ModelConfiguration.storage_key is equal to your given value** and if @ModelConfiguration.storage_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param storage_key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @ModelConfiguration.storage_key
     * @category equality
     * @category field
     * @public
     * @see ModelConfiguration.storage_key
     * @see ModelConfiguration.setStorageKey
     * @see ModelConfiguration.getStorageKey
     * @see ModelConfiguration.unsafeStorageKey
     * @see ModelConfiguration.eraseStorageKey
     * @see ModelConfiguration.isStorageKeyDefined
     */
    isStorageKeyEqualTo(storage_key?: undefined|string): boolean{
        if (typeof storage_key === "undefined"){
            return false;
        }
        return this.unsafeStorageKey() === storage_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @ModelConfiguration.storage_key**.
     * @description **Removes the assigned the value of @ModelConfiguration.storage_key**: *@ModelConfiguration.storage_key will be flagged as undefined*.
     * @return Return the actual [ModelConfiguration](ModelConfiguration) class instance.
     * @category @ModelConfiguration.storage_key
     * @category truncate
     * @category field
     * @public
     * @see ModelConfiguration.storage_key
     * @see ModelConfiguration.setStorageKey
     * @see ModelConfiguration.getStorageKey
     * @see ModelConfiguration.unsafeStorageKey
     * @see ModelConfiguration.isStorageKeyDefined
     * @see ModelConfiguration.isStorageKeyEqualTo
     */
    eraseStorageKey(): this{
        this.storage_key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [BaseFieldConfiguration](BaseFieldConfiguration) and the actual [ModelConfiguration](ModelConfiguration)**.
     * @description **An array of saved relations between [BaseFieldConfiguration](BaseFieldConfiguration) class instances and the actual [ModelConfiguration](ModelConfiguration) class instance**.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category property
     * @protected
     * @see ModelConfiguration.areFieldsLinked
     * @see ModelConfiguration.disposeFields
     * @see ModelConfiguration.unlinkFields
     * @see ModelConfiguration.unsafeFields
     * @see ModelConfiguration.getFields
     * @see ModelConfiguration.linkFields
     * @see ModelConfiguration.getField
     * @see ModelConfiguration.unsafeField
     * @see ModelConfiguration.getFieldsSize
     */
    protected fields: BaseFieldConfiguration[] = [];


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the number of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**.
     * @description **Gives the size of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**.
     * @return **Returns the number of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category length
     * @public
     * @see ModelConfiguration.areFieldsLinked
     * @see ModelConfiguration.disposeFields
     * @see ModelConfiguration.unlinkFields
     * @see ModelConfiguration.unsafeFields
     * @see ModelConfiguration.getFields
     * @see ModelConfiguration.linkFields
     * @see ModelConfiguration.getField
     * @see ModelConfiguration.unsafeField
     * @see ModelConfiguration.fields
     */
    getFieldsSize(): number{
        return this.unsafeFields().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration)** class instances. Unlike [getField](ModelConfiguration.getField) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**. Unlike [getField](ModelConfiguration.getField) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**. Unlike [getField](ModelConfiguration.getField) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration), you can give `-1` for example. If you want to get the first item of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration), you can give `0`.*
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category unsafe
     * @category value
     * @public
     * @see ModelConfiguration.areFieldsLinked
     * @see ModelConfiguration.disposeFields
     * @see ModelConfiguration.unlinkFields
     * @see ModelConfiguration.unsafeFields
     * @see ModelConfiguration.getFields
     * @see ModelConfiguration.linkFields
     * @see ModelConfiguration.getField
     * @see ModelConfiguration.getFieldsSize
     * @see ModelConfiguration.fields
     */
    unsafeField(position: number): BaseFieldConfiguration|undefined{
        if (position < 0){
            const new_position = this.getFieldsSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeField(new_position);
        }

        return this.fields[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Model--unsafeField) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Model--unsafeField) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Model--unsafeField) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration), you can give `-1` for example. If you want to get the first item of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration), you can give `0`.*
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category getter
     * @category value
     * @public
     * @see ModelConfiguration.areFieldsLinked
     * @see ModelConfiguration.disposeFields
     * @see ModelConfiguration.unlinkFields
     * @see ModelConfiguration.unsafeFields
     * @see ModelConfiguration.getFields
     * @see ModelConfiguration.linkFields
     * @see ModelConfiguration.unsafeField
     * @see ModelConfiguration.getFieldsSize
     * @see ModelConfiguration.fields
     */
    getField(position: number): BaseFieldConfiguration{
        const field = this.unsafeField(position);
        if (typeof field === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getField");
            error.setClassname("ModelConfiguration");
            error.setProperty("fields");
            error.setPosition(position);
            throw error;
        }
        return field;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associate one or multiple [BaseFieldConfiguration](BaseFieldConfiguration) class instances** from your actual [ModelConfiguration](ModelConfiguration) class instance.
     * @description Allows you to **associate one or multiple [BaseFieldConfiguration](BaseFieldConfiguration) class instances** from your actual [ModelConfiguration](ModelConfiguration) class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate [ModelConfiguration](ModelConfiguration) class instance** ([BaseFieldConfiguration.unlinkModel](BaseFieldConfiguration.unlinkModel)), and **then associate the  [BaseFieldConfiguration](BaseFieldConfiguration) class instances** to the [ModelConfiguration](ModelConfiguration) class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [BaseFieldConfiguration.linkModel](BaseFieldConfiguration.linkModel) to associate the [ModelConfiguration](ModelConfiguration) class instance and the [BaseFieldConfiguration](BaseFieldConfiguration) class instances from the both parts*.
     * @return Returns the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @param fields All the given [BaseFieldConfiguration](BaseFieldConfiguration) class instances that you to associate to your actual [ModelConfiguration](ModelConfiguration) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category associate
     * @public
     * @see ModelConfiguration.areFieldsLinked
     * @see ModelConfiguration.disposeFields
     * @see ModelConfiguration.unlinkFields
     * @see ModelConfiguration.unsafeFields
     * @see ModelConfiguration.getFields
     * @see ModelConfiguration.getField
     * @see ModelConfiguration.unsafeField
     * @see ModelConfiguration.getFieldsSize
     * @see ModelConfiguration.fields
     * @see BaseFieldConfiguration.linkModel
     */
    linkFields(... fields: DeepArray<BaseFieldConfiguration>[]): this{
        for (const field of flat(fields)){
            if (this.areFieldsLinked(field)){
                continue;
            }


            this.fields.push(field);


            field.linkModel(this);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**.
     * @description **Gives a copy of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**.
     * @return **Returns a copy of the [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category getter
     * @category iterable
     * @public
     * @see ModelConfiguration.areFieldsLinked
     * @see ModelConfiguration.disposeFields
     * @see ModelConfiguration.unlinkFields
     * @see ModelConfiguration.unsafeFields
     * @see ModelConfiguration.linkFields
     * @see ModelConfiguration.getField
     * @see ModelConfiguration.unsafeField
     * @see ModelConfiguration.getFieldsSize
     * @see ModelConfiguration.fields
     */
    getFields(): BaseFieldConfiguration[]{
        return Array.from(this.unsafeFields());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives an [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances without copy**.
     * @description **Gives an [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getFields](ModelConfiguration.getFields) method.
     * @return **Returns an [array](ModelConfiguration.fields) of the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getFields](ModelConfiguration.getFields) method.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category unsafe
     * @category iterable
     * @public
     * @see ModelConfiguration.areFieldsLinked
     * @see ModelConfiguration.disposeFields
     * @see ModelConfiguration.unlinkFields
     * @see ModelConfiguration.getFields
     * @see ModelConfiguration.linkFields
     * @see ModelConfiguration.getField
     * @see ModelConfiguration.unsafeField
     * @see ModelConfiguration.getFieldsSize
     * @see ModelConfiguration.fields
     */
    unsafeFields(): BaseFieldConfiguration[]{
        return this.fields;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate one or multiple [BaseFieldConfiguration](BaseFieldConfiguration) class instances**.
     * @description Allows you to **dissociate one or multiple [BaseFieldConfiguration](BaseFieldConfiguration) class instances** from your actual [ModelConfiguration](ModelConfiguration) class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the [ModelConfiguration](ModelConfiguration) class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [BaseFieldConfiguration.unlinkModel](BaseFieldConfiguration.unlinkModel) to dissociate the [ModelConfiguration](ModelConfiguration) class instance and the given [BaseFieldConfiguration](BaseFieldConfiguration) class instances from the both parts*.
     * @return Returns the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @param fields All the given [BaseFieldConfiguration](BaseFieldConfiguration) class instances that you to dissociate from your actual [ModelConfiguration](ModelConfiguration) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category dissociate
     * @public
     * @see ModelConfiguration.areFieldsLinked
     * @see ModelConfiguration.disposeFields
     * @see ModelConfiguration.unsafeFields
     * @see ModelConfiguration.getFields
     * @see ModelConfiguration.linkFields
     * @see ModelConfiguration.getField
     * @see ModelConfiguration.unsafeField
     * @see ModelConfiguration.getFieldsSize
     * @see ModelConfiguration.fields
     * @see BaseFieldConfiguration.unlinkModel
     */
    unlinkFields(... fields: DeepArray<BaseFieldConfiguration>[]): this{
        for (const field of flat(fields)){
            if (!this.areFieldsLinked(field)){
                continue;
            }

            while (true){
                const index = this.fields.indexOf(field);
                if (index < 0){
                    break;
                }

                this.fields.splice(index, 1);
            }

            field.unlinkModel();
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate all the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**.
     * @description **Dissociate all the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances**.  *Note that all the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instances will lose their [ModelConfiguration](ModelConfiguration) class instance association*.
     * @return Returns the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category truncate
     * @public
     * @see ModelConfiguration.areFieldsLinked
     * @see ModelConfiguration.unlinkFields
     * @see ModelConfiguration.unsafeFields
     * @see ModelConfiguration.getFields
     * @see ModelConfiguration.linkFields
     * @see ModelConfiguration.getField
     * @see ModelConfiguration.unsafeField
     * @see ModelConfiguration.getFieldsSize
     * @see ModelConfiguration.fields
     */
    disposeFields(): this{
        this.unlinkFields(this.unsafeFields());
        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [BaseFieldConfiguration](BaseFieldConfiguration) class instances are associated**.
     * @description **Checks if all the given [BaseFieldConfiguration](BaseFieldConfiguration) class instances are associated** with your actual [ModelConfiguration](ModelConfiguration) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [ModelConfiguration](ModelConfiguration) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [BaseFieldConfiguration](BaseFieldConfiguration) class instances are associated with your [ModelConfiguration](ModelConfiguration) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @return **Return a boolean if all the given [BaseFieldConfiguration](BaseFieldConfiguration) class instances are associated** with your actual [ModelConfiguration](ModelConfiguration) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [ModelConfiguration](ModelConfiguration) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [BaseFieldConfiguration](BaseFieldConfiguration) class instances are associated with your [ModelConfiguration](ModelConfiguration) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @param fields All the given [BaseFieldConfiguration](BaseFieldConfiguration) class instances that you want to check their relation *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category defined
     * @public
     * @see ModelConfiguration.disposeFields
     * @see ModelConfiguration.unlinkFields
     * @see ModelConfiguration.unsafeFields
     * @see ModelConfiguration.getFields
     * @see ModelConfiguration.linkFields
     * @see ModelConfiguration.getField
     * @see ModelConfiguration.unsafeField
     * @see ModelConfiguration.getFieldsSize
     * @see ModelConfiguration.fields
     */
    areFieldsLinked(... fields: DeepArray<BaseFieldConfiguration|undefined>[]): boolean{
        return flat(fields).every((field) => {
            if (typeof field === "undefined"){
                return false;
            }

            return this.unsafeFields().includes(field);
        });
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [Project](Project) and the actual [ModelConfiguration](ModelConfiguration)**.
     * @description **Saves the relation between [Project](Project) class instances and the actual [ModelConfiguration](ModelConfiguration) class instance**.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category property
     * @category relation
     * @protected
     * @see ModelConfiguration.linkModel
     * @see ModelConfiguration.hasModelLinked
     * @see ModelConfiguration.unsafeModel
     * @see ModelConfiguration.getModel
     * @see ModelConfiguration.unlinkModel
     * @see ModelConfiguration.isLinkedWithModel
     */
    protected project: Project|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associates a [Project](Project) class instance**.
     * @description Allows you to **associate a [Project](Project) class instance** with your actual [ModelConfiguration](ModelConfiguration) class instance. *This method allows one parameter which is a [Project](Project) class instance that will be associated*. *Note that the method will also run [Project.linkModels](Project.linkModels) to associate the [Project](Project) class instance et the [ModelConfiguration](ModelConfiguration) class instance from the both parts*. *Note also that the method will also run [unlinkProject](ModelConfiguration.unlinkProject) to dissociate the current [Project](Project) class instance associated*.
     * @return Returns the actual [ModelConfiguration](ModelConfiguration) class instance.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category association
     * @category relation
     * @public
     * @see ModelConfiguration.model
     * @see ModelConfiguration.hasModelLinked
     * @see ModelConfiguration.unsafeModel
     * @see ModelConfiguration.getModel
     * @see ModelConfiguration.unlinkModel
     * @see ModelConfiguration.isLinkedWithModel
     * @see Project.linkModels
     */
    linkProject(project: Project): this{
        if (this.isLinkedWithProject(project)){
            return this;
        }


        if (!project.areModelsLinked(this)){
            project.linkModels(this);

            return this;
        }


        this.unlinkProject();
        this.project = project;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if a [Project](Project) class instance is associated**.
     * @return **Gives a boolean if a [Project](Project) class instance is associated** or not to our actual [ModelConfiguration](ModelConfiguration) class instance.
     * @description **Check if a [Project](Project) class instance is associated** or not to our actual [ModelConfiguration](ModelConfiguration) class instance.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category defined
     * @public
     * @see ModelConfiguration.model
     * @see ModelConfiguration.linkModel
     * @see ModelConfiguration.unsafeModel
     * @see ModelConfiguration.getModel
     * @see ModelConfiguration.unlinkModel
     * @see ModelConfiguration.isLinkedWithModel
     */
    hasProjectLinked(): boolean{
        return typeof this.project !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [Project](Project) class instance**. Unlike the [unsafeProject](ModelConfiguration.unsafeProject) method, **the method can throw an [UnlinkedValue](UnlinkedValue) error**.
     * @description **Gives the associated [Project](Project) class instance** of our actual [ModelConfiguration](ModelConfiguration) class instance. Unlike the [unsafeProject](ModelConfigurations.unsafeProject) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [ModelConfiguration](ModelConfiguration) class instance doesn't have [Project](Project) class instance associate**.
     * @return **Results the associated [Project](Project) class instance** of our actual [ModelConfiguration](ModelConfiguration) class instance. Unlike the [unsafeProject](ModelConfigurations.unsafeProject) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [ModelConfiguration](ModelConfiguration) class instance doesn't have [Project](Project) class instance associate**.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category value
     * @category getter
     * @public
     * @see ModelConfiguration.model
     * @see ModelConfiguration.linkModel
     * @see ModelConfiguration.hasModelLinked
     * @see ModelConfiguration.unsafeModel
     * @see ModelConfiguration.unlinkModel
     * @see ModelConfiguration.isLinkedWithModel
     */
    getProject(): Project{
        const unsafe = this.unsafeProject();

        if (typeof unsafe === "undefined"){
            const error = new UnlinkedValue();
            error.setMethod("getProject");
            error.setClassname("ModelConfiguration");
            error.setWantedClassname("Project");
            error.setProperty("project");
            throw error;
        }
        return unsafe;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [Project](Project) class instance**. Unlike the [getProject](ModelConfiguration.getProject) method, **the method can result an undefined value.
     * @description **Gives the associated [Project](Project) class instance** of our actual [ModelConfiguration](ModelConfiguration) class instance. Unlike the [getProject](ModelConfiguration.getProject) method, **the method results an undefined value if the [ModelConfiguration](ModelConfiguration) class instance doesn't have [Project](Project) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Results the associated [Project](Project) class instance** of our actual [ModelConfiguration](ModelConfiguration) class instance. Unlike the [getProject](ModelConfiguration.getProject) method, **the method results an undefined value if the [ModelConfiguration](ModelConfiguration) class instance doesn't have [Project](Project) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category value
     * @category unsafe
     * @public
     * @see ModelConfiguration.model
     * @see ModelConfiguration.linkModel
     * @see ModelConfiguration.hasModelLinked
     * @see ModelConfiguration.getModel
     * @see ModelConfiguration.unlinkModel
     * @see ModelConfiguration.isLinkedWithModel
     */
    unsafeProject(): Project|undefined{
        return this.project;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate the [Project](Project) class instance**.
     * @description **Dissociate the [Project](Project) class instance** from your actual [ModelConfiguration](ModelConfiguration) class instance.  *Note that the method will also run [Project.unlinkModels](Project.unlinkModels) to dissociate the [Project](Project) class instance et the [ModelConfiguration](ModelConfiguration) class instance from the both parts*.
     * @return Return the actual [ModelConfiguration](ModelConfiguration) class instance.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category value
     * @category dissociate
     * @public
     * @see ModelConfiguration.model
     * @see ModelConfiguration.linkModel
     * @see ModelConfiguration.hasModelLinked
     * @see ModelConfiguration.unsafeModel
     * @see ModelConfiguration.getModel
     * @see ModelConfiguration.isLinkedWithModel
     * @see Project.unlinkModels
     */
    unlinkProject(): this{
        const _project = this.project;
        this.project = undefined;

        _project?.unlinkModels(this);

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [Project](Project) class instance** are associated.
     * @description **Checks if the [ModelConfiguration](ModelConfiguration) class instance is associated with all the given [Project](Project) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Return a boolean if the [ModelConfiguration](ModelConfiguration) class instance is associated with all the given [Project](Project) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @category @ModelConfiguration.project
     * @category Relation between Project and ModelConfiguration
     * @category relation
     * @category value
     * @category equality
     * @public
     * @see ModelConfiguration.model
     * @see ModelConfiguration.linkModel
     * @see ModelConfiguration.hasModelLinked
     * @see ModelConfiguration.unsafeModel
     * @see ModelConfiguration.getModel
     * @see ModelConfiguration.unlinkModel
     */
    isLinkedWithProject(project?: undefined|Project): boolean{
        if (typeof project === "undefined"){
            return false;
        }
        return this.unsafeProject() === project;
    }
}

export default ModelConfiguration;