import BaseFieldConfiguration from "../base";
import OutOfRange from "../../../error/internal/out-of-range";
import DeepArray from "../../../util/deep-array/type";
import flat from "../../../util/deep-array/flat";
import UndefinedValue from "../../../error/internal/undefined-value";
import Match from "../../../util/match/type";
import UnlinkedValue from "../../../error/internal/unlinked-value";
import PivotInstance from "./instance";
import matcher from "../../../util/match/matcher";

class PivotConfiguration extends BaseFieldConfiguration {
    matchingWithParentInputKeys(key?: undefined|Match){
        return matcher(this.unsafeParentInputKeys(),key);
    }

    matchingWithChildInputKeys(key?: undefined|Match){
        return matcher(this.unsafeChildInputKeys(),key);
    }

    matchingWithParentFilterKeys(key?: undefined|Match){
        return matcher(this.unsafeParentFilterKeys(),key);
    }

    matchingWithChildFilterKeys(key?: undefined|Match){
        return matcher(this.unsafeChildFilterKeys(),key);
    }

    unsafeContext() {
        return this.getBase().unsafeContext();
    }
    unsafeTranslate() {
        return this.getBase().unsafeTranslate();
    }

    isPivot(): this is PivotInstance | PivotConfiguration {
        return true;
    }
    isPivotConfiguration(): this is PivotConfiguration {
        return true;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Save @PivotConfiguration.child_input_keys.
     * @description Save @PivotConfiguration.child_input_keys.
     * @category child_input_keys
     * @category property
     * @category list
     * @protected
     * @see PivotConfiguration.getChildInputKeys
     * @see PivotConfiguration.unsafeChildInputKeys
     * @see PivotConfiguration.getChildInputKey
     * @see PivotConfiguration.unsafeChildInputKey
     * @see PivotConfiguration.getChildInputKeysSize
     * @see PivotConfiguration.containChildInputKeys
     * @see PivotConfiguration.addChildInputKeys
     * @see PivotConfiguration.removeChildInputKeys
     * @see PivotConfiguration.truncateChildInputKeys
     */
    protected child_input_keys: Match[][] = [];

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the size of @PivotConfiguration.child_input_keys**.
     * @description **Gives the size of @PivotConfiguration.child_input_keys**.
     * @return **Returns the size of @PivotConfiguration.child_input_keys**.
     * @category @PivotConfiguration.child_input_keys
     * @category list
     * @category length
     * @public
     * @see PivotConfiguration.getChildInputKeys
     * @see PivotConfiguration.unsafeChildInputKeys
     * @see PivotConfiguration.getChildInputKey
     * @see PivotConfiguration.unsafeChildInputKey
     * @see PivotConfiguration.containChildInputKeys
     * @see PivotConfiguration.addChildInputKeys
     * @see PivotConfiguration.removeChildInputKeys
     * @see PivotConfiguration.truncateChildInputKeys
     * @see child_input_keys
     */
    getChildInputKeysSize(): number{
        return this.unsafeChildInputKeys().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of @PivotConfiguration.child_input_keys**. Unlike [getChildInputKey](PivotConfiguration.getChildInputKey) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of @PivotConfiguration.child_input_keys**. Unlike [getChildInputKey](PivotConfiguration.getChildInputKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of @PivotConfiguration.child_input_keys**. Unlike [getChildInputKey](PivotConfiguration.getChildInputKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @PivotConfiguration.child_input_keys, you can give `-1` for example. If you want to get the first item of @PivotConfiguration.child_input_keys, you can give `0`.*
     * @category @PivotConfiguration.child_input_keys
     * @category unsafe
     * @category list
     * @category value
     * @public
     * @see PivotConfiguration.getChildInputKeys
     * @see PivotConfiguration.unsafeChildInputKeys
     * @see PivotConfiguration.getChildInputKey
     * @see PivotConfiguration.getChildInputKeysSize
     * @see PivotConfiguration.containChildInputKeys
     * @see PivotConfiguration.addChildInputKeys
     * @see PivotConfiguration.removeChildInputKeys
     * @see PivotConfiguration.truncateChildInputKeys
     * @see child_input_keys
     */
    unsafeChildInputKey(position: number): Match[]|undefined{
        if (position < 0){
            const new_position = this.getChildInputKeysSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeChildInputKey(new_position);
        }

        return this.child_input_keys[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item in @PivotConfiguration.child_input_keys**. Unlike the [unsafeChildInputKey](PivotConfiguration.unsafeChildInputKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item in @PivotConfiguration.child_input_keys**. Unlike the [unsafeChildInputKey](PivotConfiguration.unsafeChildInputKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item in @PivotConfiguration.child_input_keys**. Unlike the [unsafeChildInputKey](PivotConfiguration.unsafeChildInputKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @PivotConfiguration.child_input_keys, you can give `-1` for example. If you want to get the first item of @PivotConfiguration.child_input_keys, you can give `0`.*
     * @throw OutOfRange Throws an [OutOfRange](OutOfRange) error if the given position is out of range.
     * @category @PivotConfiguration.child_input_keys
     * @category getter
     * @category list
     * @category value
     * @public
     * @see OutOfRange
     * @see PivotConfiguration.getChildInputKeys
     * @see PivotConfiguration.unsafeChildInputKeys
     * @see PivotConfiguration.unsafeChildInputKey
     * @see PivotConfiguration.getChildInputKeysSize
     * @see PivotConfiguration.containChildInputKeys
     * @see PivotConfiguration.addChildInputKeys
     * @see PivotConfiguration.removeChildInputKeys
     * @see PivotConfiguration.truncateChildInputKeys
     * @see child_input_keys
     */
    getChildInputKey(position: number): Match[]{
        const child_input_key = this.unsafeChildInputKey(position);
        if (typeof child_input_key === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getChildInputKey");
            error.setClassname("PivotConfiguration");
            error.setProperty("child_input_keys");
            error.setPosition(position);
            throw error;
        }

        return child_input_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Add one or multiple values** to @PivotConfiguration.child_input_keys.
     * @description Allows you to **add one or multiple values** to @PivotConfiguration.child_input_keys. Each argument of in the method will be added to @PivotConfiguration.child_input_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in @PivotConfiguration.child_input_keys: the value is unique._
     * @param child_input_keys All the given values that you to add to @PivotConfiguration.child_input_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*. *If the value is already included, the method will discard the item addition.*
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.child_input_keys
     * @category associate
     * @category list
     * @public
     * @see PivotConfiguration.getChildInputKeys
     * @see PivotConfiguration.unsafeChildInputKeys
     * @see PivotConfiguration.getChildInputKey
     * @see PivotConfiguration.unsafeChildInputKey
     * @see PivotConfiguration.getChildInputKeysSize
     * @see PivotConfiguration.containChildInputKeys
     * @see PivotConfiguration.removeChildInputKeys
     * @see PivotConfiguration.truncateChildInputKeys
     * @see child_input_keys
     */
    addChildInputKeys(... child_input_keys: DeepArray<Match[]>[]): this{
        for (const child_input_key of flat(child_input_keys)){
            if (this.containChildInputKeys(child_input_key)){
                continue;
            }


            this.child_input_keys.push(child_input_key);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if the given value is included** in @PivotConfiguration.child_input_keys.
     * @description **Checks if all the given value is included** in @PivotConfiguration.child_input_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @PivotConfiguration.child_input_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @PivotConfiguration.child_input_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @return **Returns a boolean if all the given value is included** in @PivotConfiguration.child_input_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @PivotConfiguration.child_input_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @PivotConfiguration.child_input_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @param child_input_keys All the values that you want to check *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @PivotConfiguration.child_input_keys
     * @category defined
     * @category list
     * @public
     * @see PivotConfiguration.getChildInputKeys
     * @see PivotConfiguration.unsafeChildInputKeys
     * @see PivotConfiguration.getChildInputKey
     * @see PivotConfiguration.unsafeChildInputKey
     * @see PivotConfiguration.getChildInputKeysSize
     * @see PivotConfiguration.addChildInputKeys
     * @see PivotConfiguration.removeChildInputKeys
     * @see PivotConfiguration.truncateChildInputKeys
     * @see child_input_keys
     */
    containChildInputKeys(... child_input_keys: DeepArray<Match[]|undefined>[]): boolean{
        return flat(child_input_keys).every((child_input_key) => {
            if (typeof child_input_key === "undefined"){
                return false;
            }

            return this.unsafeChildInputKeys().includes(child_input_key);
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of @PivotConfiguration.child_input_keys**.
     * @description **Gives a copy of @PivotConfiguration.child_input_keys**.
     * @return **Returns a copy of @PivotConfiguration.child_input_keys**.
     * @category @PivotConfiguration.child_input_keys
     * @category getter
     * @category iterable
     * @category list
     * @public
     * @see PivotConfiguration.unsafeChildInputKeys
     * @see PivotConfiguration.getChildInputKey
     * @see PivotConfiguration.unsafeChildInputKey
     * @see PivotConfiguration.getChildInputKeysSize
     * @see PivotConfiguration.containChildInputKeys
     * @see PivotConfiguration.addChildInputKeys
     * @see PivotConfiguration.removeChildInputKeys
     * @see PivotConfiguration.truncateChildInputKeys
     * @see child_input_keys
     */
    getChildInputKeys(): Match[][]{
        return Array.from(this.unsafeChildInputKeys());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives @PivotConfiguration.child_input_keys without copy** the array.
     * @description **Gives @PivotConfiguration.child_input_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getChildInputKeys](PivotConfiguration.getChildInputKeys) method.
     * @return **Returns @PivotConfiguration.child_input_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getChildInputKeys](PivotConfiguration.getChildInputKeys) method.
     * @category @PivotConfiguration.child_input_keys
     * @category unsafe
     * @category iterable
     * @category list
     * @public
     * @see PivotConfiguration.getChildInputKeys
     * @see PivotConfiguration.getChildInputKey
     * @see PivotConfiguration.unsafeChildInputKey
     * @see PivotConfiguration.getChildInputKeysSize
     * @see PivotConfiguration.containChildInputKeys
     * @see PivotConfiguration.addChildInputKeys
     * @see PivotConfiguration.removeChildInputKeys
     * @see PivotConfiguration.truncateChildInputKeys
     * @see child_input_keys
     */
    unsafeChildInputKeys(): Match[][]{
        return this.child_input_keys;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Allows to **remove one or multiple values** from @PivotConfiguration.child_input_keys.
     * @description Allows you to **remove one or multiple values** from @PivotConfiguration.child_input_keys. Each argument of in the method will be removed from @PivotConfiguration.child_input_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @param child_input_keys All the given values that you to remove from @PivotConfiguration.child_input_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.child_input_keys
     * @category dissociate
     * @category list
     * @public
     * @see PivotConfiguration.getChildInputKeys
     * @see PivotConfiguration.unsafeChildInputKeys
     * @see PivotConfiguration.getChildInputKey
     * @see PivotConfiguration.unsafeChildInputKey
     * @see PivotConfiguration.getChildInputKeysSize
     * @see PivotConfiguration.containChildInputKeys
     * @see PivotConfiguration.addChildInputKeys
     * @see PivotConfiguration.truncateChildInputKeys
     * @see child_input_keys
     */
    removeChildInputKeys(... child_input_keys: DeepArray<Match[]>[]): this{
        for (const child_input_key of flat(child_input_keys)){
            if (!this.containChildInputKeys(child_input_key)){
                continue;
            }

            while (true){
                const index = this.child_input_keys.indexOf(child_input_key);
                if (index < 0){
                    break;
                }

                this.child_input_keys.splice(index, 1);
            }
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Truncate @PivotConfiguration.child_input_keys**.
     * @description **Remove all the values in @PivotConfiguration.child_input_keys**.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.child_input_keys
     * @category truncate
     * @category list
     * @public
     * @see PivotConfiguration.getChildInputKeys
     * @see PivotConfiguration.unsafeChildInputKeys
     * @see PivotConfiguration.getChildInputKey
     * @see PivotConfiguration.unsafeChildInputKey
     * @see PivotConfiguration.getChildInputKeysSize
     * @see PivotConfiguration.containChildInputKeys
     * @see PivotConfiguration.addChildInputKeys
     * @see PivotConfiguration.removeChildInputKeys
     * @see child_input_keys
     */
    truncateChildInputKeys(): this{
        this.removeChildInputKeys(this.unsafeChildInputKeys());
        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Save @PivotConfiguration.child_filter_keys.
     * @description Save @PivotConfiguration.child_filter_keys.
     * @category child_filter_keys
     * @category property
     * @category list
     * @protected
     * @see PivotConfiguration.getChildFilterKeys
     * @see PivotConfiguration.unsafeChildFilterKeys
     * @see PivotConfiguration.getChildFilterKey
     * @see PivotConfiguration.unsafeChildFilterKey
     * @see PivotConfiguration.getChildFilterKeysSize
     * @see PivotConfiguration.containChildFilterKeys
     * @see PivotConfiguration.addChildFilterKeys
     * @see PivotConfiguration.removeChildFilterKeys
     * @see PivotConfiguration.truncateChildFilterKeys
     */
    protected child_filter_keys: Match[][] = [];

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the size of @PivotConfiguration.child_filter_keys**.
     * @description **Gives the size of @PivotConfiguration.child_filter_keys**.
     * @return **Returns the size of @PivotConfiguration.child_filter_keys**.
     * @category @PivotConfiguration.child_filter_keys
     * @category list
     * @category length
     * @public
     * @see PivotConfiguration.getChildFilterKeys
     * @see PivotConfiguration.unsafeChildFilterKeys
     * @see PivotConfiguration.getChildFilterKey
     * @see PivotConfiguration.unsafeChildFilterKey
     * @see PivotConfiguration.containChildFilterKeys
     * @see PivotConfiguration.addChildFilterKeys
     * @see PivotConfiguration.removeChildFilterKeys
     * @see PivotConfiguration.truncateChildFilterKeys
     * @see child_filter_keys
     */
    getChildFilterKeysSize(): number{
        return this.unsafeChildFilterKeys().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of @PivotConfiguration.child_filter_keys**. Unlike [getChildFilterKey](PivotConfiguration.getChildFilterKey) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of @PivotConfiguration.child_filter_keys**. Unlike [getChildFilterKey](PivotConfiguration.getChildFilterKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of @PivotConfiguration.child_filter_keys**. Unlike [getChildFilterKey](PivotConfiguration.getChildFilterKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @PivotConfiguration.child_filter_keys, you can give `-1` for example. If you want to get the first item of @PivotConfiguration.child_filter_keys, you can give `0`.*
     * @category @PivotConfiguration.child_filter_keys
     * @category unsafe
     * @category list
     * @category value
     * @public
     * @see PivotConfiguration.getChildFilterKeys
     * @see PivotConfiguration.unsafeChildFilterKeys
     * @see PivotConfiguration.getChildFilterKey
     * @see PivotConfiguration.getChildFilterKeysSize
     * @see PivotConfiguration.containChildFilterKeys
     * @see PivotConfiguration.addChildFilterKeys
     * @see PivotConfiguration.removeChildFilterKeys
     * @see PivotConfiguration.truncateChildFilterKeys
     * @see child_filter_keys
     */
    unsafeChildFilterKey(position: number): Match[]|undefined{
        if (position < 0){
            const new_position = this.getChildFilterKeysSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeChildFilterKey(new_position);
        }

        return this.child_filter_keys[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item in @PivotConfiguration.child_filter_keys**. Unlike the [unsafeChildFilterKey](PivotConfiguration.unsafeChildFilterKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item in @PivotConfiguration.child_filter_keys**. Unlike the [unsafeChildFilterKey](PivotConfiguration.unsafeChildFilterKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item in @PivotConfiguration.child_filter_keys**. Unlike the [unsafeChildFilterKey](PivotConfiguration.unsafeChildFilterKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @PivotConfiguration.child_filter_keys, you can give `-1` for example. If you want to get the first item of @PivotConfiguration.child_filter_keys, you can give `0`.*
     * @throw OutOfRange Throws an [OutOfRange](OutOfRange) error if the given position is out of range.
     * @category @PivotConfiguration.child_filter_keys
     * @category getter
     * @category list
     * @category value
     * @public
     * @see OutOfRange
     * @see PivotConfiguration.getChildFilterKeys
     * @see PivotConfiguration.unsafeChildFilterKeys
     * @see PivotConfiguration.unsafeChildFilterKey
     * @see PivotConfiguration.getChildFilterKeysSize
     * @see PivotConfiguration.containChildFilterKeys
     * @see PivotConfiguration.addChildFilterKeys
     * @see PivotConfiguration.removeChildFilterKeys
     * @see PivotConfiguration.truncateChildFilterKeys
     * @see child_filter_keys
     */
    getChildFilterKey(position: number): Match[]{
        const child_filter_key = this.unsafeChildFilterKey(position);
        if (typeof child_filter_key === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getChildFilterKey");
            error.setClassname("PivotConfiguration");
            error.setProperty("child_filter_keys");
            error.setPosition(position);
            throw error;
        }

        return child_filter_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Add one or multiple values** to @PivotConfiguration.child_filter_keys.
     * @description Allows you to **add one or multiple values** to @PivotConfiguration.child_filter_keys. Each argument of in the method will be added to @PivotConfiguration.child_filter_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in @PivotConfiguration.child_filter_keys: the value is unique._
     * @param child_filter_keys All the given values that you to add to @PivotConfiguration.child_filter_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*. *If the value is already included, the method will discard the item addition.*
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.child_filter_keys
     * @category associate
     * @category list
     * @public
     * @see PivotConfiguration.getChildFilterKeys
     * @see PivotConfiguration.unsafeChildFilterKeys
     * @see PivotConfiguration.getChildFilterKey
     * @see PivotConfiguration.unsafeChildFilterKey
     * @see PivotConfiguration.getChildFilterKeysSize
     * @see PivotConfiguration.containChildFilterKeys
     * @see PivotConfiguration.removeChildFilterKeys
     * @see PivotConfiguration.truncateChildFilterKeys
     * @see child_filter_keys
     */
    addChildFilterKeys(... child_filter_keys: DeepArray<Match[]>[]): this{
        for (const child_filter_key of flat(child_filter_keys)){
            if (this.containChildFilterKeys(child_filter_key)){
                continue;
            }


            this.child_filter_keys.push(child_filter_key);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if the given value is included** in @PivotConfiguration.child_filter_keys.
     * @description **Checks if all the given value is included** in @PivotConfiguration.child_filter_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @PivotConfiguration.child_filter_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @PivotConfiguration.child_filter_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @return **Returns a boolean if all the given value is included** in @PivotConfiguration.child_filter_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @PivotConfiguration.child_filter_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @PivotConfiguration.child_filter_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @param child_filter_keys All the values that you want to check *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @PivotConfiguration.child_filter_keys
     * @category defined
     * @category list
     * @public
     * @see PivotConfiguration.getChildFilterKeys
     * @see PivotConfiguration.unsafeChildFilterKeys
     * @see PivotConfiguration.getChildFilterKey
     * @see PivotConfiguration.unsafeChildFilterKey
     * @see PivotConfiguration.getChildFilterKeysSize
     * @see PivotConfiguration.addChildFilterKeys
     * @see PivotConfiguration.removeChildFilterKeys
     * @see PivotConfiguration.truncateChildFilterKeys
     * @see child_filter_keys
     */
    containChildFilterKeys(... child_filter_keys: DeepArray<Match[]|undefined>[]): boolean{
        return flat(child_filter_keys).every((child_filter_key) => {
            if (typeof child_filter_key === "undefined"){
                return false;
            }

            return this.unsafeChildFilterKeys().includes(child_filter_key);
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of @PivotConfiguration.child_filter_keys**.
     * @description **Gives a copy of @PivotConfiguration.child_filter_keys**.
     * @return **Returns a copy of @PivotConfiguration.child_filter_keys**.
     * @category @PivotConfiguration.child_filter_keys
     * @category getter
     * @category iterable
     * @category list
     * @public
     * @see PivotConfiguration.unsafeChildFilterKeys
     * @see PivotConfiguration.getChildFilterKey
     * @see PivotConfiguration.unsafeChildFilterKey
     * @see PivotConfiguration.getChildFilterKeysSize
     * @see PivotConfiguration.containChildFilterKeys
     * @see PivotConfiguration.addChildFilterKeys
     * @see PivotConfiguration.removeChildFilterKeys
     * @see PivotConfiguration.truncateChildFilterKeys
     * @see child_filter_keys
     */
    getChildFilterKeys(): Match[][]{
        return Array.from(this.unsafeChildFilterKeys());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives @PivotConfiguration.child_filter_keys without copy** the array.
     * @description **Gives @PivotConfiguration.child_filter_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getChildFilterKeys](PivotConfiguration.getChildFilterKeys) method.
     * @return **Returns @PivotConfiguration.child_filter_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getChildFilterKeys](PivotConfiguration.getChildFilterKeys) method.
     * @category @PivotConfiguration.child_filter_keys
     * @category unsafe
     * @category iterable
     * @category list
     * @public
     * @see PivotConfiguration.getChildFilterKeys
     * @see PivotConfiguration.getChildFilterKey
     * @see PivotConfiguration.unsafeChildFilterKey
     * @see PivotConfiguration.getChildFilterKeysSize
     * @see PivotConfiguration.containChildFilterKeys
     * @see PivotConfiguration.addChildFilterKeys
     * @see PivotConfiguration.removeChildFilterKeys
     * @see PivotConfiguration.truncateChildFilterKeys
     * @see child_filter_keys
     */
    unsafeChildFilterKeys(): Match[][]{
        return this.child_filter_keys;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Allows to **remove one or multiple values** from @PivotConfiguration.child_filter_keys.
     * @description Allows you to **remove one or multiple values** from @PivotConfiguration.child_filter_keys. Each argument of in the method will be removed from @PivotConfiguration.child_filter_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @param child_filter_keys All the given values that you to remove from @PivotConfiguration.child_filter_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.child_filter_keys
     * @category dissociate
     * @category list
     * @public
     * @see PivotConfiguration.getChildFilterKeys
     * @see PivotConfiguration.unsafeChildFilterKeys
     * @see PivotConfiguration.getChildFilterKey
     * @see PivotConfiguration.unsafeChildFilterKey
     * @see PivotConfiguration.getChildFilterKeysSize
     * @see PivotConfiguration.containChildFilterKeys
     * @see PivotConfiguration.addChildFilterKeys
     * @see PivotConfiguration.truncateChildFilterKeys
     * @see child_filter_keys
     */
    removeChildFilterKeys(... child_filter_keys: DeepArray<Match[]>[]): this{
        for (const child_filter_key of flat(child_filter_keys)){
            if (!this.containChildFilterKeys(child_filter_key)){
                continue;
            }

            while (true){
                const index = this.child_filter_keys.indexOf(child_filter_key);
                if (index < 0){
                    break;
                }

                this.child_filter_keys.splice(index, 1);
            }
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Truncate @PivotConfiguration.child_filter_keys**.
     * @description **Remove all the values in @PivotConfiguration.child_filter_keys**.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.child_filter_keys
     * @category truncate
     * @category list
     * @public
     * @see PivotConfiguration.getChildFilterKeys
     * @see PivotConfiguration.unsafeChildFilterKeys
     * @see PivotConfiguration.getChildFilterKey
     * @see PivotConfiguration.unsafeChildFilterKey
     * @see PivotConfiguration.getChildFilterKeysSize
     * @see PivotConfiguration.containChildFilterKeys
     * @see PivotConfiguration.addChildFilterKeys
     * @see PivotConfiguration.removeChildFilterKeys
     * @see child_filter_keys
     */
    truncateChildFilterKeys(): this{
        this.removeChildFilterKeys(this.unsafeChildFilterKeys());
        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @PivotConfiguration.child_output_key.
     * @description Save @PivotConfiguration.child_output_key.
     * @category @PivotConfiguration.child_output_key
     * @category property
     * @category base
     * @protected
     * @see PivotConfiguration.setChildOutputKey
     * @see PivotConfiguration.getChildOutputKey
     * @see PivotConfiguration.unsafeChildOutputKey
     * @see PivotConfiguration.eraseChildOutputKey
     * @see PivotConfiguration.isChildOutputKeyDefined
     * @see PivotConfiguration.isChildOutputKeyEqualTo
     */
    protected child_output_key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @PivotConfiguration.child_output_key**.
     * @description Allows you to update or **assign the value of @PivotConfiguration.child_output_key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @param child_output_key The new value to assign to @PivotConfiguration.child_output_key.
     * @category @PivotConfiguration.child_output_key
     * @category setter
     * @category base
     * @public
     * @see PivotConfiguration.child_output_key
     * @see PivotConfiguration.getChildOutputKey
     * @see PivotConfiguration.unsafeChildOutputKey
     * @see PivotConfiguration.eraseChildOutputKey
     * @see PivotConfiguration.isChildOutputKeyDefined
     * @see PivotConfiguration.isChildOutputKeyEqualTo
     */
    setChildOutputKey(child_output_key: string): this{
        if (this.isChildOutputKeyEqualTo(child_output_key)){
            return this;
        }

        this.eraseChildOutputKey();
        this.child_output_key = child_output_key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @PivotConfiguration.child_output_key**. Unlike the [getChildOutputKey](PivotConfiguration.getChildOutputKey) method, **the method results an undefined value if @PivotConfiguration.child_output_key is not defined**.
     * @description **Gives the value of @PivotConfiguration.child_output_key**. Unlike the [getChildOutputKey](PivotConfiguration.getChildOutputKey) method, **the method results an undefined value if @PivotConfiguration.child_output_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @PivotConfiguration.child_output_key**. Unlike the [getChildOutputKey](PivotConfiguration.getChildOutputKey) method, **the method results an undefined value if @PivotConfiguration.child_output_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @PivotConfiguration.child_output_key
     * @category unsafe
     * @category value
     * @category base
     * @public
     * @see PivotConfiguration.child_output_key
     * @see PivotConfiguration.setChildOutputKey
     * @see PivotConfiguration.getChildOutputKey
     * @see PivotConfiguration.eraseChildOutputKey
     * @see PivotConfiguration.isChildOutputKeyDefined
     * @see PivotConfiguration.isChildOutputKeyEqualTo
     */
    unsafeChildOutputKey(): string|undefined{
        return this.child_output_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @PivotConfiguration.child_output_key**. Unlike the [unsafeChildOutputKey](PivotConfiguration.unsafeChildOutputKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.child_output_key is not defined**.
     * @description **Gives the value of @PivotConfiguration.child_output_key**. Unlike the [unsafeChildOutputKey](PivotConfiguration.unsafeChildOutputKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.child_output_key is not defined**.
     * @return **Returns the value of @PivotConfiguration.child_output_key**. Unlike the [unsafeChildOutputKey](PivotConfiguration.unsafeChildOutputKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.child_output_key is not defined**.
     * @category @PivotConfiguration.child_output_key
     * @category getter
     * @category value
     * @category base
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.child_output_key isn't defined.
     * @see PivotConfiguration.child_output_key
     * @see PivotConfiguration.setChildOutputKey
     * @see PivotConfiguration.unsafeChildOutputKey
     * @see PivotConfiguration.eraseChildOutputKey
     * @see PivotConfiguration.isChildOutputKeyDefined
     * @see PivotConfiguration.isChildOutputKeyEqualTo
     */
    getChildOutputKey(): string{
        const child_output_key = this.unsafeChildOutputKey();

        if (typeof child_output_key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getChildOutputKey");
            error.setClassname("PivotConfiguration");
            error.setProperty("child_output_key");
            throw error;
        }
        return child_output_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @PivotConfiguration.child_output_key is defined**.
     * @description **Checks if @PivotConfiguration.child_output_key is defined** or not.
     * @return **Returns a boolean if @PivotConfiguration.child_output_key is defined** or not.
     * @category @PivotConfiguration.child_output_key
     * @category defined
     * @category base
     * @public
     * @see PivotConfiguration.child_output_key
     * @see PivotConfiguration.setChildOutputKey
     * @see PivotConfiguration.getChildOutputKey
     * @see PivotConfiguration.unsafeChildOutputKey
     * @see PivotConfiguration.eraseChildOutputKey
     * @see PivotConfiguration.isChildOutputKeyEqualTo
     */
    isChildOutputKeyDefined(): boolean{
        return typeof this.child_output_key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @PivotConfiguration.child_output_key is equal to your given value**.
     * @description **Checks if @PivotConfiguration.child_output_key is equal to your given value** and if @PivotConfiguration.child_output_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @PivotConfiguration.child_output_key is equal to your given value** and if @PivotConfiguration.child_output_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param child_output_key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @PivotConfiguration.child_output_key
     * @category equality
     * @category base
     * @public
     * @see PivotConfiguration.child_output_key
     * @see PivotConfiguration.setChildOutputKey
     * @see PivotConfiguration.getChildOutputKey
     * @see PivotConfiguration.unsafeChildOutputKey
     * @see PivotConfiguration.eraseChildOutputKey
     * @see PivotConfiguration.isChildOutputKeyDefined
     */
    isChildOutputKeyEqualTo(child_output_key?: undefined|string): boolean{
        if (typeof child_output_key === "undefined"){
            return false;
        }
        return this.unsafeChildOutputKey() === child_output_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @PivotConfiguration.child_output_key**.
     * @description **Removes the assigned the value of @PivotConfiguration.child_output_key**: *@PivotConfiguration.child_output_key will be flagged as undefined*.
     * @return Return the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.child_output_key
     * @category truncate
     * @category base
     * @public
     * @see PivotConfiguration.child_output_key
     * @see PivotConfiguration.setChildOutputKey
     * @see PivotConfiguration.getChildOutputKey
     * @see PivotConfiguration.unsafeChildOutputKey
     * @see PivotConfiguration.isChildOutputKeyDefined
     * @see PivotConfiguration.isChildOutputKeyEqualTo
     */
    eraseChildOutputKey(): this{
        this.child_output_key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @PivotConfiguration.child_key.
     * @description Save @PivotConfiguration.child_key.
     * @category @PivotConfiguration.child_key
     * @category property
     * @category base
     * @protected
     * @see PivotConfiguration.setChildKey
     * @see PivotConfiguration.getChildKey
     * @see PivotConfiguration.unsafeChildKey
     * @see PivotConfiguration.eraseChildKey
     * @see PivotConfiguration.isChildKeyDefined
     * @see PivotConfiguration.isChildKeyEqualTo
     */
    protected child_key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @PivotConfiguration.child_key**.
     * @description Allows you to update or **assign the value of @PivotConfiguration.child_key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @param child_key The new value to assign to @PivotConfiguration.child_key.
     * @category @PivotConfiguration.child_key
     * @category setter
     * @category base
     * @public
     * @see PivotConfiguration.child_key
     * @see PivotConfiguration.getChildKey
     * @see PivotConfiguration.unsafeChildKey
     * @see PivotConfiguration.eraseChildKey
     * @see PivotConfiguration.isChildKeyDefined
     * @see PivotConfiguration.isChildKeyEqualTo
     */
    setChildKey(child_key: string): this{
        if (this.isChildKeyEqualTo(child_key)){
            return this;
        }

        this.eraseChildKey();
        this.child_key = child_key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @PivotConfiguration.child_key**. Unlike the [getChildKey](PivotConfiguration.getChildKey) method, **the method results an undefined value if @PivotConfiguration.child_key is not defined**.
     * @description **Gives the value of @PivotConfiguration.child_key**. Unlike the [getChildKey](PivotConfiguration.getChildKey) method, **the method results an undefined value if @PivotConfiguration.child_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @PivotConfiguration.child_key**. Unlike the [getChildKey](PivotConfiguration.getChildKey) method, **the method results an undefined value if @PivotConfiguration.child_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @PivotConfiguration.child_key
     * @category unsafe
     * @category value
     * @category base
     * @public
     * @see PivotConfiguration.child_key
     * @see PivotConfiguration.setChildKey
     * @see PivotConfiguration.getChildKey
     * @see PivotConfiguration.eraseChildKey
     * @see PivotConfiguration.isChildKeyDefined
     * @see PivotConfiguration.isChildKeyEqualTo
     */
    unsafeChildKey(): string|undefined{
        return this.child_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @PivotConfiguration.child_key**. Unlike the [unsafeChildKey](PivotConfiguration.unsafeChildKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.child_key is not defined**.
     * @description **Gives the value of @PivotConfiguration.child_key**. Unlike the [unsafeChildKey](PivotConfiguration.unsafeChildKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.child_key is not defined**.
     * @return **Returns the value of @PivotConfiguration.child_key**. Unlike the [unsafeChildKey](PivotConfiguration.unsafeChildKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.child_key is not defined**.
     * @category @PivotConfiguration.child_key
     * @category getter
     * @category value
     * @category base
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.child_key isn't defined.
     * @see PivotConfiguration.child_key
     * @see PivotConfiguration.setChildKey
     * @see PivotConfiguration.unsafeChildKey
     * @see PivotConfiguration.eraseChildKey
     * @see PivotConfiguration.isChildKeyDefined
     * @see PivotConfiguration.isChildKeyEqualTo
     */
    getChildKey(): string{
        const child_key = this.unsafeChildKey();

        if (typeof child_key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getChildKey");
            error.setClassname("PivotConfiguration");
            error.setProperty("child_key");
            throw error;
        }
        return child_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @PivotConfiguration.child_key is defined**.
     * @description **Checks if @PivotConfiguration.child_key is defined** or not.
     * @return **Returns a boolean if @PivotConfiguration.child_key is defined** or not.
     * @category @PivotConfiguration.child_key
     * @category defined
     * @category base
     * @public
     * @see PivotConfiguration.child_key
     * @see PivotConfiguration.setChildKey
     * @see PivotConfiguration.getChildKey
     * @see PivotConfiguration.unsafeChildKey
     * @see PivotConfiguration.eraseChildKey
     * @see PivotConfiguration.isChildKeyEqualTo
     */
    isChildKeyDefined(): boolean{
        return typeof this.child_key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @PivotConfiguration.child_key is equal to your given value**.
     * @description **Checks if @PivotConfiguration.child_key is equal to your given value** and if @PivotConfiguration.child_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @PivotConfiguration.child_key is equal to your given value** and if @PivotConfiguration.child_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param child_key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @PivotConfiguration.child_key
     * @category equality
     * @category base
     * @public
     * @see PivotConfiguration.child_key
     * @see PivotConfiguration.setChildKey
     * @see PivotConfiguration.getChildKey
     * @see PivotConfiguration.unsafeChildKey
     * @see PivotConfiguration.eraseChildKey
     * @see PivotConfiguration.isChildKeyDefined
     */
    isChildKeyEqualTo(child_key?: undefined|string): boolean{
        if (typeof child_key === "undefined"){
            return false;
        }
        return this.unsafeChildKey() === child_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @PivotConfiguration.child_key**.
     * @description **Removes the assigned the value of @PivotConfiguration.child_key**: *@PivotConfiguration.child_key will be flagged as undefined*.
     * @return Return the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.child_key
     * @category truncate
     * @category base
     * @public
     * @see PivotConfiguration.child_key
     * @see PivotConfiguration.setChildKey
     * @see PivotConfiguration.getChildKey
     * @see PivotConfiguration.unsafeChildKey
     * @see PivotConfiguration.isChildKeyDefined
     * @see PivotConfiguration.isChildKeyEqualTo
     */
    eraseChildKey(): this{
        this.child_key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Save @PivotConfiguration.parent_input_keys.
     * @description Save @PivotConfiguration.parent_input_keys.
     * @category parent_input_keys
     * @category property
     * @category list
     * @protected
     * @see PivotConfiguration.getParentInputKeys
     * @see PivotConfiguration.unsafeParentInputKeys
     * @see PivotConfiguration.getParentInputKey
     * @see PivotConfiguration.unsafeParentInputKey
     * @see PivotConfiguration.getParentInputKeysSize
     * @see PivotConfiguration.containParentInputKeys
     * @see PivotConfiguration.addParentInputKeys
     * @see PivotConfiguration.removeParentInputKeys
     * @see PivotConfiguration.truncateParentInputKeys
     */
    protected parent_input_keys: Match[][] = [];

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the size of @PivotConfiguration.parent_input_keys**.
     * @description **Gives the size of @PivotConfiguration.parent_input_keys**.
     * @return **Returns the size of @PivotConfiguration.parent_input_keys**.
     * @category @PivotConfiguration.parent_input_keys
     * @category list
     * @category length
     * @public
     * @see PivotConfiguration.getParentInputKeys
     * @see PivotConfiguration.unsafeParentInputKeys
     * @see PivotConfiguration.getParentInputKey
     * @see PivotConfiguration.unsafeParentInputKey
     * @see PivotConfiguration.containParentInputKeys
     * @see PivotConfiguration.addParentInputKeys
     * @see PivotConfiguration.removeParentInputKeys
     * @see PivotConfiguration.truncateParentInputKeys
     * @see parent_input_keys
     */
    getParentInputKeysSize(): number{
        return this.unsafeParentInputKeys().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of @PivotConfiguration.parent_input_keys**. Unlike [getParentInputKey](PivotConfiguration.getParentInputKey) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of @PivotConfiguration.parent_input_keys**. Unlike [getParentInputKey](PivotConfiguration.getParentInputKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of @PivotConfiguration.parent_input_keys**. Unlike [getParentInputKey](PivotConfiguration.getParentInputKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @PivotConfiguration.parent_input_keys, you can give `-1` for example. If you want to get the first item of @PivotConfiguration.parent_input_keys, you can give `0`.*
     * @category @PivotConfiguration.parent_input_keys
     * @category unsafe
     * @category list
     * @category value
     * @public
     * @see PivotConfiguration.getParentInputKeys
     * @see PivotConfiguration.unsafeParentInputKeys
     * @see PivotConfiguration.getParentInputKey
     * @see PivotConfiguration.getParentInputKeysSize
     * @see PivotConfiguration.containParentInputKeys
     * @see PivotConfiguration.addParentInputKeys
     * @see PivotConfiguration.removeParentInputKeys
     * @see PivotConfiguration.truncateParentInputKeys
     * @see parent_input_keys
     */
    unsafeParentInputKey(position: number): Match[]|undefined{
        if (position < 0){
            const new_position = this.getParentInputKeysSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeParentInputKey(new_position);
        }

        return this.parent_input_keys[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item in @PivotConfiguration.parent_input_keys**. Unlike the [unsafeParentInputKey](PivotConfiguration.unsafeParentInputKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item in @PivotConfiguration.parent_input_keys**. Unlike the [unsafeParentInputKey](PivotConfiguration.unsafeParentInputKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item in @PivotConfiguration.parent_input_keys**. Unlike the [unsafeParentInputKey](PivotConfiguration.unsafeParentInputKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @PivotConfiguration.parent_input_keys, you can give `-1` for example. If you want to get the first item of @PivotConfiguration.parent_input_keys, you can give `0`.*
     * @throw OutOfRange Throws an [OutOfRange](OutOfRange) error if the given position is out of range.
     * @category @PivotConfiguration.parent_input_keys
     * @category getter
     * @category list
     * @category value
     * @public
     * @see OutOfRange
     * @see PivotConfiguration.getParentInputKeys
     * @see PivotConfiguration.unsafeParentInputKeys
     * @see PivotConfiguration.unsafeParentInputKey
     * @see PivotConfiguration.getParentInputKeysSize
     * @see PivotConfiguration.containParentInputKeys
     * @see PivotConfiguration.addParentInputKeys
     * @see PivotConfiguration.removeParentInputKeys
     * @see PivotConfiguration.truncateParentInputKeys
     * @see parent_input_keys
     */
    getParentInputKey(position: number): Match[]{
        const parent_input_key = this.unsafeParentInputKey(position);
        if (typeof parent_input_key === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getParentInputKey");
            error.setClassname("PivotConfiguration");
            error.setProperty("parent_input_keys");
            error.setPosition(position);
            throw error;
        }

        return parent_input_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Add one or multiple values** to @PivotConfiguration.parent_input_keys.
     * @description Allows you to **add one or multiple values** to @PivotConfiguration.parent_input_keys. Each argument of in the method will be added to @PivotConfiguration.parent_input_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in @PivotConfiguration.parent_input_keys: the value is unique._
     * @param parent_input_keys All the given values that you to add to @PivotConfiguration.parent_input_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*. *If the value is already included, the method will discard the item addition.*
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.parent_input_keys
     * @category associate
     * @category list
     * @public
     * @see PivotConfiguration.getParentInputKeys
     * @see PivotConfiguration.unsafeParentInputKeys
     * @see PivotConfiguration.getParentInputKey
     * @see PivotConfiguration.unsafeParentInputKey
     * @see PivotConfiguration.getParentInputKeysSize
     * @see PivotConfiguration.containParentInputKeys
     * @see PivotConfiguration.removeParentInputKeys
     * @see PivotConfiguration.truncateParentInputKeys
     * @see parent_input_keys
     */
    addParentInputKeys(... parent_input_keys: DeepArray<Match[]>[]): this{
        for (const parent_input_key of flat(parent_input_keys)){
            if (this.containParentInputKeys(parent_input_key)){
                continue;
            }


            this.parent_input_keys.push(parent_input_key);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if the given value is included** in @PivotConfiguration.parent_input_keys.
     * @description **Checks if all the given value is included** in @PivotConfiguration.parent_input_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @PivotConfiguration.parent_input_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @PivotConfiguration.parent_input_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @return **Returns a boolean if all the given value is included** in @PivotConfiguration.parent_input_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @PivotConfiguration.parent_input_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @PivotConfiguration.parent_input_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @param parent_input_keys All the values that you want to check *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @PivotConfiguration.parent_input_keys
     * @category defined
     * @category list
     * @public
     * @see PivotConfiguration.getParentInputKeys
     * @see PivotConfiguration.unsafeParentInputKeys
     * @see PivotConfiguration.getParentInputKey
     * @see PivotConfiguration.unsafeParentInputKey
     * @see PivotConfiguration.getParentInputKeysSize
     * @see PivotConfiguration.addParentInputKeys
     * @see PivotConfiguration.removeParentInputKeys
     * @see PivotConfiguration.truncateParentInputKeys
     * @see parent_input_keys
     */
    containParentInputKeys(... parent_input_keys: DeepArray<Match[]|undefined>[]): boolean{
        return flat(parent_input_keys).every((parent_input_key) => {
            if (typeof parent_input_key === "undefined"){
                return false;
            }

            return this.unsafeParentInputKeys().includes(parent_input_key);
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of @PivotConfiguration.parent_input_keys**.
     * @description **Gives a copy of @PivotConfiguration.parent_input_keys**.
     * @return **Returns a copy of @PivotConfiguration.parent_input_keys**.
     * @category @PivotConfiguration.parent_input_keys
     * @category getter
     * @category iterable
     * @category list
     * @public
     * @see PivotConfiguration.unsafeParentInputKeys
     * @see PivotConfiguration.getParentInputKey
     * @see PivotConfiguration.unsafeParentInputKey
     * @see PivotConfiguration.getParentInputKeysSize
     * @see PivotConfiguration.containParentInputKeys
     * @see PivotConfiguration.addParentInputKeys
     * @see PivotConfiguration.removeParentInputKeys
     * @see PivotConfiguration.truncateParentInputKeys
     * @see parent_input_keys
     */
    getParentInputKeys(): Match[][]{
        return Array.from(this.unsafeParentInputKeys());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives @PivotConfiguration.parent_input_keys without copy** the array.
     * @description **Gives @PivotConfiguration.parent_input_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getParentInputKeys](PivotConfiguration.getParentInputKeys) method.
     * @return **Returns @PivotConfiguration.parent_input_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getParentInputKeys](PivotConfiguration.getParentInputKeys) method.
     * @category @PivotConfiguration.parent_input_keys
     * @category unsafe
     * @category iterable
     * @category list
     * @public
     * @see PivotConfiguration.getParentInputKeys
     * @see PivotConfiguration.getParentInputKey
     * @see PivotConfiguration.unsafeParentInputKey
     * @see PivotConfiguration.getParentInputKeysSize
     * @see PivotConfiguration.containParentInputKeys
     * @see PivotConfiguration.addParentInputKeys
     * @see PivotConfiguration.removeParentInputKeys
     * @see PivotConfiguration.truncateParentInputKeys
     * @see parent_input_keys
     */
    unsafeParentInputKeys(): Match[][]{
        return this.parent_input_keys;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Allows to **remove one or multiple values** from @PivotConfiguration.parent_input_keys.
     * @description Allows you to **remove one or multiple values** from @PivotConfiguration.parent_input_keys. Each argument of in the method will be removed from @PivotConfiguration.parent_input_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @param parent_input_keys All the given values that you to remove from @PivotConfiguration.parent_input_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.parent_input_keys
     * @category dissociate
     * @category list
     * @public
     * @see PivotConfiguration.getParentInputKeys
     * @see PivotConfiguration.unsafeParentInputKeys
     * @see PivotConfiguration.getParentInputKey
     * @see PivotConfiguration.unsafeParentInputKey
     * @see PivotConfiguration.getParentInputKeysSize
     * @see PivotConfiguration.containParentInputKeys
     * @see PivotConfiguration.addParentInputKeys
     * @see PivotConfiguration.truncateParentInputKeys
     * @see parent_input_keys
     */
    removeParentInputKeys(... parent_input_keys: DeepArray<Match[]>[]): this{
        for (const parent_input_key of flat(parent_input_keys)){
            if (!this.containParentInputKeys(parent_input_key)){
                continue;
            }

            while (true){
                const index = this.parent_input_keys.indexOf(parent_input_key);
                if (index < 0){
                    break;
                }

                this.parent_input_keys.splice(index, 1);
            }
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Truncate @PivotConfiguration.parent_input_keys**.
     * @description **Remove all the values in @PivotConfiguration.parent_input_keys**.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.parent_input_keys
     * @category truncate
     * @category list
     * @public
     * @see PivotConfiguration.getParentInputKeys
     * @see PivotConfiguration.unsafeParentInputKeys
     * @see PivotConfiguration.getParentInputKey
     * @see PivotConfiguration.unsafeParentInputKey
     * @see PivotConfiguration.getParentInputKeysSize
     * @see PivotConfiguration.containParentInputKeys
     * @see PivotConfiguration.addParentInputKeys
     * @see PivotConfiguration.removeParentInputKeys
     * @see parent_input_keys
     */
    truncateParentInputKeys(): this{
        this.removeParentInputKeys(this.unsafeParentInputKeys());
        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Save @PivotConfiguration.parent_filter_keys.
     * @description Save @PivotConfiguration.parent_filter_keys.
     * @category parent_filter_keys
     * @category property
     * @category list
     * @protected
     * @see PivotConfiguration.getParentFilterKeys
     * @see PivotConfiguration.unsafeParentFilterKeys
     * @see PivotConfiguration.getParentFilterKey
     * @see PivotConfiguration.unsafeParentFilterKey
     * @see PivotConfiguration.getParentFilterKeysSize
     * @see PivotConfiguration.containParentFilterKeys
     * @see PivotConfiguration.addParentFilterKeys
     * @see PivotConfiguration.removeParentFilterKeys
     * @see PivotConfiguration.truncateParentFilterKeys
     */
    protected parent_filter_keys: Match[][] = [];

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the size of @PivotConfiguration.parent_filter_keys**.
     * @description **Gives the size of @PivotConfiguration.parent_filter_keys**.
     * @return **Returns the size of @PivotConfiguration.parent_filter_keys**.
     * @category @PivotConfiguration.parent_filter_keys
     * @category list
     * @category length
     * @public
     * @see PivotConfiguration.getParentFilterKeys
     * @see PivotConfiguration.unsafeParentFilterKeys
     * @see PivotConfiguration.getParentFilterKey
     * @see PivotConfiguration.unsafeParentFilterKey
     * @see PivotConfiguration.containParentFilterKeys
     * @see PivotConfiguration.addParentFilterKeys
     * @see PivotConfiguration.removeParentFilterKeys
     * @see PivotConfiguration.truncateParentFilterKeys
     * @see parent_filter_keys
     */
    getParentFilterKeysSize(): number{
        return this.unsafeParentFilterKeys().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of @PivotConfiguration.parent_filter_keys**. Unlike [getParentFilterKey](PivotConfiguration.getParentFilterKey) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of @PivotConfiguration.parent_filter_keys**. Unlike [getParentFilterKey](PivotConfiguration.getParentFilterKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of @PivotConfiguration.parent_filter_keys**. Unlike [getParentFilterKey](PivotConfiguration.getParentFilterKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @PivotConfiguration.parent_filter_keys, you can give `-1` for example. If you want to get the first item of @PivotConfiguration.parent_filter_keys, you can give `0`.*
     * @category @PivotConfiguration.parent_filter_keys
     * @category unsafe
     * @category list
     * @category value
     * @public
     * @see PivotConfiguration.getParentFilterKeys
     * @see PivotConfiguration.unsafeParentFilterKeys
     * @see PivotConfiguration.getParentFilterKey
     * @see PivotConfiguration.getParentFilterKeysSize
     * @see PivotConfiguration.containParentFilterKeys
     * @see PivotConfiguration.addParentFilterKeys
     * @see PivotConfiguration.removeParentFilterKeys
     * @see PivotConfiguration.truncateParentFilterKeys
     * @see parent_filter_keys
     */
    unsafeParentFilterKey(position: number): Match[]|undefined{
        if (position < 0){
            const new_position = this.getParentFilterKeysSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeParentFilterKey(new_position);
        }

        return this.parent_filter_keys[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item in @PivotConfiguration.parent_filter_keys**. Unlike the [unsafeParentFilterKey](PivotConfiguration.unsafeParentFilterKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item in @PivotConfiguration.parent_filter_keys**. Unlike the [unsafeParentFilterKey](PivotConfiguration.unsafeParentFilterKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item in @PivotConfiguration.parent_filter_keys**. Unlike the [unsafeParentFilterKey](PivotConfiguration.unsafeParentFilterKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @PivotConfiguration.parent_filter_keys, you can give `-1` for example. If you want to get the first item of @PivotConfiguration.parent_filter_keys, you can give `0`.*
     * @throw OutOfRange Throws an [OutOfRange](OutOfRange) error if the given position is out of range.
     * @category @PivotConfiguration.parent_filter_keys
     * @category getter
     * @category list
     * @category value
     * @public
     * @see OutOfRange
     * @see PivotConfiguration.getParentFilterKeys
     * @see PivotConfiguration.unsafeParentFilterKeys
     * @see PivotConfiguration.unsafeParentFilterKey
     * @see PivotConfiguration.getParentFilterKeysSize
     * @see PivotConfiguration.containParentFilterKeys
     * @see PivotConfiguration.addParentFilterKeys
     * @see PivotConfiguration.removeParentFilterKeys
     * @see PivotConfiguration.truncateParentFilterKeys
     * @see parent_filter_keys
     */
    getParentFilterKey(position: number): Match[]{
        const parent_filter_key = this.unsafeParentFilterKey(position);
        if (typeof parent_filter_key === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getParentFilterKey");
            error.setClassname("PivotConfiguration");
            error.setProperty("parent_filter_keys");
            error.setPosition(position);
            throw error;
        }

        return parent_filter_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Add one or multiple values** to @PivotConfiguration.parent_filter_keys.
     * @description Allows you to **add one or multiple values** to @PivotConfiguration.parent_filter_keys. Each argument of in the method will be added to @PivotConfiguration.parent_filter_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in @PivotConfiguration.parent_filter_keys: the value is unique._
     * @param parent_filter_keys All the given values that you to add to @PivotConfiguration.parent_filter_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*. *If the value is already included, the method will discard the item addition.*
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.parent_filter_keys
     * @category associate
     * @category list
     * @public
     * @see PivotConfiguration.getParentFilterKeys
     * @see PivotConfiguration.unsafeParentFilterKeys
     * @see PivotConfiguration.getParentFilterKey
     * @see PivotConfiguration.unsafeParentFilterKey
     * @see PivotConfiguration.getParentFilterKeysSize
     * @see PivotConfiguration.containParentFilterKeys
     * @see PivotConfiguration.removeParentFilterKeys
     * @see PivotConfiguration.truncateParentFilterKeys
     * @see parent_filter_keys
     */
    addParentFilterKeys(... parent_filter_keys: DeepArray<Match[]>[]): this{
        for (const parent_filter_key of flat(parent_filter_keys)){
            if (this.containParentFilterKeys(parent_filter_key)){
                continue;
            }


            this.parent_filter_keys.push(parent_filter_key);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if the given value is included** in @PivotConfiguration.parent_filter_keys.
     * @description **Checks if all the given value is included** in @PivotConfiguration.parent_filter_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @PivotConfiguration.parent_filter_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @PivotConfiguration.parent_filter_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @return **Returns a boolean if all the given value is included** in @PivotConfiguration.parent_filter_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @PivotConfiguration.parent_filter_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @PivotConfiguration.parent_filter_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @param parent_filter_keys All the values that you want to check *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @PivotConfiguration.parent_filter_keys
     * @category defined
     * @category list
     * @public
     * @see PivotConfiguration.getParentFilterKeys
     * @see PivotConfiguration.unsafeParentFilterKeys
     * @see PivotConfiguration.getParentFilterKey
     * @see PivotConfiguration.unsafeParentFilterKey
     * @see PivotConfiguration.getParentFilterKeysSize
     * @see PivotConfiguration.addParentFilterKeys
     * @see PivotConfiguration.removeParentFilterKeys
     * @see PivotConfiguration.truncateParentFilterKeys
     * @see parent_filter_keys
     */
    containParentFilterKeys(... parent_filter_keys: DeepArray<Match[]|undefined>[]): boolean{
        return flat(parent_filter_keys).every((parent_filter_key) => {
            if (typeof parent_filter_key === "undefined"){
                return false;
            }

            return this.unsafeParentFilterKeys().includes(parent_filter_key);
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of @PivotConfiguration.parent_filter_keys**.
     * @description **Gives a copy of @PivotConfiguration.parent_filter_keys**.
     * @return **Returns a copy of @PivotConfiguration.parent_filter_keys**.
     * @category @PivotConfiguration.parent_filter_keys
     * @category getter
     * @category iterable
     * @category list
     * @public
     * @see PivotConfiguration.unsafeParentFilterKeys
     * @see PivotConfiguration.getParentFilterKey
     * @see PivotConfiguration.unsafeParentFilterKey
     * @see PivotConfiguration.getParentFilterKeysSize
     * @see PivotConfiguration.containParentFilterKeys
     * @see PivotConfiguration.addParentFilterKeys
     * @see PivotConfiguration.removeParentFilterKeys
     * @see PivotConfiguration.truncateParentFilterKeys
     * @see parent_filter_keys
     */
    getParentFilterKeys(): Match[][]{
        return Array.from(this.unsafeParentFilterKeys());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives @PivotConfiguration.parent_filter_keys without copy** the array.
     * @description **Gives @PivotConfiguration.parent_filter_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getParentFilterKeys](PivotConfiguration.getParentFilterKeys) method.
     * @return **Returns @PivotConfiguration.parent_filter_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getParentFilterKeys](PivotConfiguration.getParentFilterKeys) method.
     * @category @PivotConfiguration.parent_filter_keys
     * @category unsafe
     * @category iterable
     * @category list
     * @public
     * @see PivotConfiguration.getParentFilterKeys
     * @see PivotConfiguration.getParentFilterKey
     * @see PivotConfiguration.unsafeParentFilterKey
     * @see PivotConfiguration.getParentFilterKeysSize
     * @see PivotConfiguration.containParentFilterKeys
     * @see PivotConfiguration.addParentFilterKeys
     * @see PivotConfiguration.removeParentFilterKeys
     * @see PivotConfiguration.truncateParentFilterKeys
     * @see parent_filter_keys
     */
    unsafeParentFilterKeys(): Match[][]{
        return this.parent_filter_keys;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Allows to **remove one or multiple values** from @PivotConfiguration.parent_filter_keys.
     * @description Allows you to **remove one or multiple values** from @PivotConfiguration.parent_filter_keys. Each argument of in the method will be removed from @PivotConfiguration.parent_filter_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @param parent_filter_keys All the given values that you to remove from @PivotConfiguration.parent_filter_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.parent_filter_keys
     * @category dissociate
     * @category list
     * @public
     * @see PivotConfiguration.getParentFilterKeys
     * @see PivotConfiguration.unsafeParentFilterKeys
     * @see PivotConfiguration.getParentFilterKey
     * @see PivotConfiguration.unsafeParentFilterKey
     * @see PivotConfiguration.getParentFilterKeysSize
     * @see PivotConfiguration.containParentFilterKeys
     * @see PivotConfiguration.addParentFilterKeys
     * @see PivotConfiguration.truncateParentFilterKeys
     * @see parent_filter_keys
     */
    removeParentFilterKeys(... parent_filter_keys: DeepArray<Match[]>[]): this{
        for (const parent_filter_key of flat(parent_filter_keys)){
            if (!this.containParentFilterKeys(parent_filter_key)){
                continue;
            }

            while (true){
                const index = this.parent_filter_keys.indexOf(parent_filter_key);
                if (index < 0){
                    break;
                }

                this.parent_filter_keys.splice(index, 1);
            }
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Truncate @PivotConfiguration.parent_filter_keys**.
     * @description **Remove all the values in @PivotConfiguration.parent_filter_keys**.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.parent_filter_keys
     * @category truncate
     * @category list
     * @public
     * @see PivotConfiguration.getParentFilterKeys
     * @see PivotConfiguration.unsafeParentFilterKeys
     * @see PivotConfiguration.getParentFilterKey
     * @see PivotConfiguration.unsafeParentFilterKey
     * @see PivotConfiguration.getParentFilterKeysSize
     * @see PivotConfiguration.containParentFilterKeys
     * @see PivotConfiguration.addParentFilterKeys
     * @see PivotConfiguration.removeParentFilterKeys
     * @see parent_filter_keys
     */
    truncateParentFilterKeys(): this{
        this.removeParentFilterKeys(this.unsafeParentFilterKeys());
        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @PivotConfiguration.parent_output_key.
     * @description Save @PivotConfiguration.parent_output_key.
     * @category @PivotConfiguration.parent_output_key
     * @category property
     * @category base
     * @protected
     * @see PivotConfiguration.setParentOutputKey
     * @see PivotConfiguration.getParentOutputKey
     * @see PivotConfiguration.unsafeParentOutputKey
     * @see PivotConfiguration.eraseParentOutputKey
     * @see PivotConfiguration.isParentOutputKeyDefined
     * @see PivotConfiguration.isParentOutputKeyEqualTo
     */
    protected parent_output_key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @PivotConfiguration.parent_output_key**.
     * @description Allows you to update or **assign the value of @PivotConfiguration.parent_output_key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @param parent_output_key The new value to assign to @PivotConfiguration.parent_output_key.
     * @category @PivotConfiguration.parent_output_key
     * @category setter
     * @category base
     * @public
     * @see PivotConfiguration.parent_output_key
     * @see PivotConfiguration.getParentOutputKey
     * @see PivotConfiguration.unsafeParentOutputKey
     * @see PivotConfiguration.eraseParentOutputKey
     * @see PivotConfiguration.isParentOutputKeyDefined
     * @see PivotConfiguration.isParentOutputKeyEqualTo
     */
    setParentOutputKey(parent_output_key: string): this{
        if (this.isParentOutputKeyEqualTo(parent_output_key)){
            return this;
        }

        this.eraseParentOutputKey();
        this.parent_output_key = parent_output_key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @PivotConfiguration.parent_output_key**. Unlike the [getParentOutputKey](PivotConfiguration.getParentOutputKey) method, **the method results an undefined value if @PivotConfiguration.parent_output_key is not defined**.
     * @description **Gives the value of @PivotConfiguration.parent_output_key**. Unlike the [getParentOutputKey](PivotConfiguration.getParentOutputKey) method, **the method results an undefined value if @PivotConfiguration.parent_output_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @PivotConfiguration.parent_output_key**. Unlike the [getParentOutputKey](PivotConfiguration.getParentOutputKey) method, **the method results an undefined value if @PivotConfiguration.parent_output_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @PivotConfiguration.parent_output_key
     * @category unsafe
     * @category value
     * @category base
     * @public
     * @see PivotConfiguration.parent_output_key
     * @see PivotConfiguration.setParentOutputKey
     * @see PivotConfiguration.getParentOutputKey
     * @see PivotConfiguration.eraseParentOutputKey
     * @see PivotConfiguration.isParentOutputKeyDefined
     * @see PivotConfiguration.isParentOutputKeyEqualTo
     */
    unsafeParentOutputKey(): string|undefined{
        return this.parent_output_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @PivotConfiguration.parent_output_key**. Unlike the [unsafeParentOutputKey](PivotConfiguration.unsafeParentOutputKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.parent_output_key is not defined**.
     * @description **Gives the value of @PivotConfiguration.parent_output_key**. Unlike the [unsafeParentOutputKey](PivotConfiguration.unsafeParentOutputKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.parent_output_key is not defined**.
     * @return **Returns the value of @PivotConfiguration.parent_output_key**. Unlike the [unsafeParentOutputKey](PivotConfiguration.unsafeParentOutputKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.parent_output_key is not defined**.
     * @category @PivotConfiguration.parent_output_key
     * @category getter
     * @category value
     * @category base
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.parent_output_key isn't defined.
     * @see PivotConfiguration.parent_output_key
     * @see PivotConfiguration.setParentOutputKey
     * @see PivotConfiguration.unsafeParentOutputKey
     * @see PivotConfiguration.eraseParentOutputKey
     * @see PivotConfiguration.isParentOutputKeyDefined
     * @see PivotConfiguration.isParentOutputKeyEqualTo
     */
    getParentOutputKey(): string{
        const parent_output_key = this.unsafeParentOutputKey();

        if (typeof parent_output_key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getParentOutputKey");
            error.setClassname("PivotConfiguration");
            error.setProperty("parent_output_key");
            throw error;
        }
        return parent_output_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @PivotConfiguration.parent_output_key is defined**.
     * @description **Checks if @PivotConfiguration.parent_output_key is defined** or not.
     * @return **Returns a boolean if @PivotConfiguration.parent_output_key is defined** or not.
     * @category @PivotConfiguration.parent_output_key
     * @category defined
     * @category base
     * @public
     * @see PivotConfiguration.parent_output_key
     * @see PivotConfiguration.setParentOutputKey
     * @see PivotConfiguration.getParentOutputKey
     * @see PivotConfiguration.unsafeParentOutputKey
     * @see PivotConfiguration.eraseParentOutputKey
     * @see PivotConfiguration.isParentOutputKeyEqualTo
     */
    isParentOutputKeyDefined(): boolean{
        return typeof this.parent_output_key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @PivotConfiguration.parent_output_key is equal to your given value**.
     * @description **Checks if @PivotConfiguration.parent_output_key is equal to your given value** and if @PivotConfiguration.parent_output_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @PivotConfiguration.parent_output_key is equal to your given value** and if @PivotConfiguration.parent_output_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param parent_output_key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @PivotConfiguration.parent_output_key
     * @category equality
     * @category base
     * @public
     * @see PivotConfiguration.parent_output_key
     * @see PivotConfiguration.setParentOutputKey
     * @see PivotConfiguration.getParentOutputKey
     * @see PivotConfiguration.unsafeParentOutputKey
     * @see PivotConfiguration.eraseParentOutputKey
     * @see PivotConfiguration.isParentOutputKeyDefined
     */
    isParentOutputKeyEqualTo(parent_output_key?: undefined|string): boolean{
        if (typeof parent_output_key === "undefined"){
            return false;
        }
        return this.unsafeParentOutputKey() === parent_output_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @PivotConfiguration.parent_output_key**.
     * @description **Removes the assigned the value of @PivotConfiguration.parent_output_key**: *@PivotConfiguration.parent_output_key will be flagged as undefined*.
     * @return Return the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.parent_output_key
     * @category truncate
     * @category base
     * @public
     * @see PivotConfiguration.parent_output_key
     * @see PivotConfiguration.setParentOutputKey
     * @see PivotConfiguration.getParentOutputKey
     * @see PivotConfiguration.unsafeParentOutputKey
     * @see PivotConfiguration.isParentOutputKeyDefined
     * @see PivotConfiguration.isParentOutputKeyEqualTo
     */
    eraseParentOutputKey(): this{
        this.parent_output_key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @PivotConfiguration.parent_key.
     * @description Save @PivotConfiguration.parent_key.
     * @category @PivotConfiguration.parent_key
     * @category property
     * @category base
     * @protected
     * @see PivotConfiguration.setParentKey
     * @see PivotConfiguration.getParentKey
     * @see PivotConfiguration.unsafeParentKey
     * @see PivotConfiguration.eraseParentKey
     * @see PivotConfiguration.isParentKeyDefined
     * @see PivotConfiguration.isParentKeyEqualTo
     */
    protected parent_key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @PivotConfiguration.parent_key**.
     * @description Allows you to update or **assign the value of @PivotConfiguration.parent_key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @param parent_key The new value to assign to @PivotConfiguration.parent_key.
     * @category @PivotConfiguration.parent_key
     * @category setter
     * @category base
     * @public
     * @see PivotConfiguration.parent_key
     * @see PivotConfiguration.getParentKey
     * @see PivotConfiguration.unsafeParentKey
     * @see PivotConfiguration.eraseParentKey
     * @see PivotConfiguration.isParentKeyDefined
     * @see PivotConfiguration.isParentKeyEqualTo
     */
    setParentKey(parent_key: string): this{
        if (this.isParentKeyEqualTo(parent_key)){
            return this;
        }

        this.eraseParentKey();
        this.parent_key = parent_key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @PivotConfiguration.parent_key**. Unlike the [getParentKey](PivotConfiguration.getParentKey) method, **the method results an undefined value if @PivotConfiguration.parent_key is not defined**.
     * @description **Gives the value of @PivotConfiguration.parent_key**. Unlike the [getParentKey](PivotConfiguration.getParentKey) method, **the method results an undefined value if @PivotConfiguration.parent_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @PivotConfiguration.parent_key**. Unlike the [getParentKey](PivotConfiguration.getParentKey) method, **the method results an undefined value if @PivotConfiguration.parent_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @PivotConfiguration.parent_key
     * @category unsafe
     * @category value
     * @category base
     * @public
     * @see PivotConfiguration.parent_key
     * @see PivotConfiguration.setParentKey
     * @see PivotConfiguration.getParentKey
     * @see PivotConfiguration.eraseParentKey
     * @see PivotConfiguration.isParentKeyDefined
     * @see PivotConfiguration.isParentKeyEqualTo
     */
    unsafeParentKey(): string|undefined{
        return this.parent_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @PivotConfiguration.parent_key**. Unlike the [unsafeParentKey](PivotConfiguration.unsafeParentKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.parent_key is not defined**.
     * @description **Gives the value of @PivotConfiguration.parent_key**. Unlike the [unsafeParentKey](PivotConfiguration.unsafeParentKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.parent_key is not defined**.
     * @return **Returns the value of @PivotConfiguration.parent_key**. Unlike the [unsafeParentKey](PivotConfiguration.unsafeParentKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.parent_key is not defined**.
     * @category @PivotConfiguration.parent_key
     * @category getter
     * @category value
     * @category base
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @PivotConfiguration.parent_key isn't defined.
     * @see PivotConfiguration.parent_key
     * @see PivotConfiguration.setParentKey
     * @see PivotConfiguration.unsafeParentKey
     * @see PivotConfiguration.eraseParentKey
     * @see PivotConfiguration.isParentKeyDefined
     * @see PivotConfiguration.isParentKeyEqualTo
     */
    getParentKey(): string{
        const parent_key = this.unsafeParentKey();

        if (typeof parent_key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getParentKey");
            error.setClassname("PivotConfiguration");
            error.setProperty("parent_key");
            throw error;
        }
        return parent_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @PivotConfiguration.parent_key is defined**.
     * @description **Checks if @PivotConfiguration.parent_key is defined** or not.
     * @return **Returns a boolean if @PivotConfiguration.parent_key is defined** or not.
     * @category @PivotConfiguration.parent_key
     * @category defined
     * @category base
     * @public
     * @see PivotConfiguration.parent_key
     * @see PivotConfiguration.setParentKey
     * @see PivotConfiguration.getParentKey
     * @see PivotConfiguration.unsafeParentKey
     * @see PivotConfiguration.eraseParentKey
     * @see PivotConfiguration.isParentKeyEqualTo
     */
    isParentKeyDefined(): boolean{
        return typeof this.parent_key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @PivotConfiguration.parent_key is equal to your given value**.
     * @description **Checks if @PivotConfiguration.parent_key is equal to your given value** and if @PivotConfiguration.parent_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @PivotConfiguration.parent_key is equal to your given value** and if @PivotConfiguration.parent_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param parent_key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @PivotConfiguration.parent_key
     * @category equality
     * @category base
     * @public
     * @see PivotConfiguration.parent_key
     * @see PivotConfiguration.setParentKey
     * @see PivotConfiguration.getParentKey
     * @see PivotConfiguration.unsafeParentKey
     * @see PivotConfiguration.eraseParentKey
     * @see PivotConfiguration.isParentKeyDefined
     */
    isParentKeyEqualTo(parent_key?: undefined|string): boolean{
        if (typeof parent_key === "undefined"){
            return false;
        }
        return this.unsafeParentKey() === parent_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @PivotConfiguration.parent_key**.
     * @description **Removes the assigned the value of @PivotConfiguration.parent_key**: *@PivotConfiguration.parent_key will be flagged as undefined*.
     * @return Return the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.parent_key
     * @category truncate
     * @category base
     * @public
     * @see PivotConfiguration.parent_key
     * @see PivotConfiguration.setParentKey
     * @see PivotConfiguration.getParentKey
     * @see PivotConfiguration.unsafeParentKey
     * @see PivotConfiguration.isParentKeyDefined
     * @see PivotConfiguration.isParentKeyEqualTo
     */
    eraseParentKey(): this{
        this.parent_key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [BaseFieldConfiguration](BaseFieldConfiguration) and the actual [PivotConfiguration](PivotConfiguration)**.
     * @description **Saves the relation between [BaseFieldConfiguration](BaseFieldConfiguration) class instances and the actual [PivotConfiguration](PivotConfiguration) class instance**.
     * @category @PivotConfiguration.base
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category property
     * @category relation
     * @protected
     * @see PivotConfiguration.linkPivot
     * @see PivotConfiguration.hasPivotLinked
     * @see PivotConfiguration.unsafePivot
     * @see PivotConfiguration.getPivot
     * @see PivotConfiguration.unlinkPivot
     * @see PivotConfiguration.isLinkedWithPivot
     */
    protected base: BaseFieldConfiguration|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associates a [BaseFieldConfiguration](BaseFieldConfiguration) class instance**.
     * @description Allows you to **associate a [BaseFieldConfiguration](BaseFieldConfiguration) class instance** with your actual [PivotConfiguration](PivotConfiguration) class instance. *This method allows one parameter which is a [BaseFieldConfiguration](BaseFieldConfiguration) class instance that will be associated*. *Note that the method will also run [BaseFieldConfiguration.linkPivots](BaseFieldConfiguration.linkPivots) to associate the [BaseFieldConfiguration](BaseFieldConfiguration) class instance et the [PivotConfiguration](PivotConfiguration) class instance from the both parts*. *Note also that the method will also run [unlinkBase](PivotConfiguration.unlinkBase) to dissociate the current [BaseFieldConfiguration](BaseFieldConfiguration) class instance associated*.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.base
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category association
     * @category relation
     * @public
     * @see PivotConfiguration.pivot
     * @see PivotConfiguration.hasPivotLinked
     * @see PivotConfiguration.unsafePivot
     * @see PivotConfiguration.getPivot
     * @see PivotConfiguration.unlinkPivot
     * @see PivotConfiguration.isLinkedWithPivot
     * @see BaseFieldConfiguration.linkPivots
     */
    linkBase(base: BaseFieldConfiguration): this{
        if (this.isLinkedWithBase(base)){
            return this;
        }


        if (!base.arePivotsLinked(this)){
            base.linkPivots(this);

            return this;
        }


        this.unlinkBase();
        this.base = base;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if a [BaseFieldConfiguration](BaseFieldConfiguration) class instance is associated**.
     * @return **Gives a boolean if a [BaseFieldConfiguration](BaseFieldConfiguration) class instance is associated** or not to our actual [PivotConfiguration](PivotConfiguration) class instance.
     * @description **Check if a [BaseFieldConfiguration](BaseFieldConfiguration) class instance is associated** or not to our actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.base
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category defined
     * @public
     * @see PivotConfiguration.pivot
     * @see PivotConfiguration.linkPivot
     * @see PivotConfiguration.unsafePivot
     * @see PivotConfiguration.getPivot
     * @see PivotConfiguration.unlinkPivot
     * @see PivotConfiguration.isLinkedWithPivot
     */
    hasBaseLinked(): boolean{
        return typeof this.base !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instance**. Unlike the [unsafeBase](PivotConfiguration.unsafeBase) method, **the method can throw an [UnlinkedValue](UnlinkedValue) error**.
     * @description **Gives the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instance** of our actual [PivotConfiguration](PivotConfiguration) class instance. Unlike the [unsafeBase](ModelConfigurationsBasesPivots.unsafeBase) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [PivotConfiguration](PivotConfiguration) class instance doesn't have [BaseFieldConfiguration](BaseFieldConfiguration) class instance associate**.
     * @return **Results the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instance** of our actual [PivotConfiguration](PivotConfiguration) class instance. Unlike the [unsafeBase](ModelConfigurationsBasesPivots.unsafeBase) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [PivotConfiguration](PivotConfiguration) class instance doesn't have [BaseFieldConfiguration](BaseFieldConfiguration) class instance associate**.
     * @category @PivotConfiguration.base
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category value
     * @category getter
     * @public
     * @see PivotConfiguration.pivot
     * @see PivotConfiguration.linkPivot
     * @see PivotConfiguration.hasPivotLinked
     * @see PivotConfiguration.unsafePivot
     * @see PivotConfiguration.unlinkPivot
     * @see PivotConfiguration.isLinkedWithPivot
     */
    getBase(): BaseFieldConfiguration{
        const unsafe = this.unsafeBase();

        if (typeof unsafe === "undefined"){
            const error = new UnlinkedValue();
            error.setMethod("getBase");
            error.setClassname("PivotConfiguration");
            error.setWantedClassname("BaseFieldConfiguration");
            error.setProperty("base");
            throw error;
        }
        return unsafe;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instance**. Unlike the [getBase](PivotConfiguration.getBase) method, **the method can result an undefined value.
     * @description **Gives the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instance** of our actual [PivotConfiguration](PivotConfiguration) class instance. Unlike the [getBase](PivotConfiguration.getBase) method, **the method results an undefined value if the [PivotConfiguration](PivotConfiguration) class instance doesn't have [BaseFieldConfiguration](BaseFieldConfiguration) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Results the associated [BaseFieldConfiguration](BaseFieldConfiguration) class instance** of our actual [PivotConfiguration](PivotConfiguration) class instance. Unlike the [getBase](PivotConfiguration.getBase) method, **the method results an undefined value if the [PivotConfiguration](PivotConfiguration) class instance doesn't have [BaseFieldConfiguration](BaseFieldConfiguration) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @PivotConfiguration.base
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category value
     * @category unsafe
     * @public
     * @see PivotConfiguration.pivot
     * @see PivotConfiguration.linkPivot
     * @see PivotConfiguration.hasPivotLinked
     * @see PivotConfiguration.getPivot
     * @see PivotConfiguration.unlinkPivot
     * @see PivotConfiguration.isLinkedWithPivot
     */
    unsafeBase(): BaseFieldConfiguration|undefined{
        return this.base;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate the [BaseFieldConfiguration](BaseFieldConfiguration) class instance**.
     * @description **Dissociate the [BaseFieldConfiguration](BaseFieldConfiguration) class instance** from your actual [PivotConfiguration](PivotConfiguration) class instance.  *Note that the method will also run [BaseFieldConfiguration.unlinkPivots](BaseFieldConfiguration.unlinkPivots) to dissociate the [BaseFieldConfiguration](BaseFieldConfiguration) class instance et the [PivotConfiguration](PivotConfiguration) class instance from the both parts*.
     * @return Return the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.base
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category value
     * @category dissociate
     * @public
     * @see PivotConfiguration.pivot
     * @see PivotConfiguration.linkPivot
     * @see PivotConfiguration.hasPivotLinked
     * @see PivotConfiguration.unsafePivot
     * @see PivotConfiguration.getPivot
     * @see PivotConfiguration.isLinkedWithPivot
     * @see BaseFieldConfiguration.unlinkPivots
     */
    unlinkBase(): this{
        const _base = this.base;
        this.base = undefined;

        _base?.unlinkPivots(this);

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [BaseFieldConfiguration](BaseFieldConfiguration) class instance** are associated.
     * @description **Checks if the [PivotConfiguration](PivotConfiguration) class instance is associated with all the given [BaseFieldConfiguration](BaseFieldConfiguration) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Return a boolean if the [PivotConfiguration](PivotConfiguration) class instance is associated with all the given [BaseFieldConfiguration](BaseFieldConfiguration) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @category @PivotConfiguration.base
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category value
     * @category equality
     * @public
     * @see PivotConfiguration.pivot
     * @see PivotConfiguration.linkPivot
     * @see PivotConfiguration.hasPivotLinked
     * @see PivotConfiguration.unsafePivot
     * @see PivotConfiguration.getPivot
     * @see PivotConfiguration.unlinkPivot
     */
    isLinkedWithBase(base?: undefined|BaseFieldConfiguration): boolean{
        if (typeof base === "undefined"){
            return false;
        }
        return this.unsafeBase() === base;
    }
}

export default PivotConfiguration