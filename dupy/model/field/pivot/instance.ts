import BaseFieldInstance from "../base/instance";
import UnlinkedValue from "../../../error/internal/unlinked-value";
import PivotConfiguration from "./index";

class PivotInstance extends BaseFieldInstance{
    unsafeContext(): string|undefined {
        return this.getBase().unsafeContext();
    }
    setContext(context: string): this {
        this.getBase().setContext(context);
        return this;
    }
    eraseContext(): this {
        this.getBase().eraseContext();
        return this;
    }

    isPivotInstance(): this is PivotInstance {
        return true;
    }
    isPivot(): this is PivotInstance | PivotConfiguration {
        return true;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [BaseFieldInstance](BaseFieldInstance) and the actual [PivotInstance](PivotInstance)**.
     * @description **Saves the relation between [BaseFieldInstance](BaseFieldInstance) class instances and the actual [PivotInstance](PivotInstance) class instance**.
     * @category @PivotInstance.base
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category property
     * @category relation
     * @protected
     * @see PivotInstance.linkPivot
     * @see PivotInstance.hasPivotLinked
     * @see PivotInstance.unsafePivot
     * @see PivotInstance.getPivot
     * @see PivotInstance.unlinkPivot
     * @see PivotInstance.isLinkedWithPivot
     */
    protected base: BaseFieldInstance|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associates a [BaseFieldInstance](BaseFieldInstance) class instance**.
     * @description Allows you to **associate a [BaseFieldInstance](BaseFieldInstance) class instance** with your actual [PivotInstance](PivotInstance) class instance. *This method allows one parameter which is a [BaseFieldInstance](BaseFieldInstance) class instance that will be associated*. *Note that the method will also run [BaseFieldInstance.linkPivots](BaseFieldInstance.linkPivots) to associate the [BaseFieldInstance](BaseFieldInstance) class instance et the [PivotInstance](PivotInstance) class instance from the both parts*. *Note also that the method will also run [unlinkBase](PivotInstance.unlinkBase) to dissociate the current [BaseFieldInstance](BaseFieldInstance) class instance associated*.
     * @return Returns the actual [PivotInstance](PivotInstance) class instance.
     * @category @PivotInstance.base
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category association
     * @category relation
     * @public
     * @see PivotInstance.pivot
     * @see PivotInstance.hasPivotLinked
     * @see PivotInstance.unsafePivot
     * @see PivotInstance.getPivot
     * @see PivotInstance.unlinkPivot
     * @see PivotInstance.isLinkedWithPivot
     * @see BaseFieldInstance.linkPivots
     */
    linkBase(base: BaseFieldInstance): this{
        if (this.isLinkedWithBase(base)){
            return this;
        }


        if (!base.arePivotsLinked(this)){
            base.linkPivots(this);

            return this;
        }


        this.unlinkBase();
        this.base = base;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if a [BaseFieldInstance](BaseFieldInstance) class instance is associated**.
     * @return **Gives a boolean if a [BaseFieldInstance](BaseFieldInstance) class instance is associated** or not to our actual [PivotInstance](PivotInstance) class instance.
     * @description **Check if a [BaseFieldInstance](BaseFieldInstance) class instance is associated** or not to our actual [PivotInstance](PivotInstance) class instance.
     * @category @PivotInstance.base
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category defined
     * @public
     * @see PivotInstance.pivot
     * @see PivotInstance.linkPivot
     * @see PivotInstance.unsafePivot
     * @see PivotInstance.getPivot
     * @see PivotInstance.unlinkPivot
     * @see PivotInstance.isLinkedWithPivot
     */
    hasBaseLinked(): boolean{
        return typeof this.base !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [BaseFieldInstance](BaseFieldInstance) class instance**. Unlike the [unsafeBase](PivotInstance.unsafeBase) method, **the method can throw an [UnlinkedValue](UnlinkedValue) error**.
     * @description **Gives the associated [BaseFieldInstance](BaseFieldInstance) class instance** of our actual [PivotInstance](PivotInstance) class instance. Unlike the [unsafeBase](ModelInstancesBasesPivots.unsafeBase) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [PivotInstance](PivotInstance) class instance doesn't have [BaseFieldInstance](BaseFieldInstance) class instance associate**.
     * @return **Results the associated [BaseFieldInstance](BaseFieldInstance) class instance** of our actual [PivotInstance](PivotInstance) class instance. Unlike the [unsafeBase](ModelInstancesBasesPivots.unsafeBase) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [PivotInstance](PivotInstance) class instance doesn't have [BaseFieldInstance](BaseFieldInstance) class instance associate**.
     * @category @PivotInstance.base
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category value
     * @category getter
     * @public
     * @see PivotInstance.pivot
     * @see PivotInstance.linkPivot
     * @see PivotInstance.hasPivotLinked
     * @see PivotInstance.unsafePivot
     * @see PivotInstance.unlinkPivot
     * @see PivotInstance.isLinkedWithPivot
     */
    getBase(): BaseFieldInstance{
        const unsafe = this.unsafeBase();

        if (typeof unsafe === "undefined"){
            const error = new UnlinkedValue();
            error.setMethod("getBase");
            error.setClassname("PivotInstance");
            error.setWantedClassname("BaseFieldInstance");
            error.setProperty("base");
            throw error;
        }
        return unsafe;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [BaseFieldInstance](BaseFieldInstance) class instance**. Unlike the [getBase](PivotInstance.getBase) method, **the method can result an undefined value.
     * @description **Gives the associated [BaseFieldInstance](BaseFieldInstance) class instance** of our actual [PivotInstance](PivotInstance) class instance. Unlike the [getBase](PivotInstance.getBase) method, **the method results an undefined value if the [PivotInstance](PivotInstance) class instance doesn't have [BaseFieldInstance](BaseFieldInstance) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Results the associated [BaseFieldInstance](BaseFieldInstance) class instance** of our actual [PivotInstance](PivotInstance) class instance. Unlike the [getBase](PivotInstance.getBase) method, **the method results an undefined value if the [PivotInstance](PivotInstance) class instance doesn't have [BaseFieldInstance](BaseFieldInstance) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @PivotInstance.base
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category value
     * @category unsafe
     * @public
     * @see PivotInstance.pivot
     * @see PivotInstance.linkPivot
     * @see PivotInstance.hasPivotLinked
     * @see PivotInstance.getPivot
     * @see PivotInstance.unlinkPivot
     * @see PivotInstance.isLinkedWithPivot
     */
    unsafeBase(): BaseFieldInstance|undefined{
        return this.base;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate the [BaseFieldInstance](BaseFieldInstance) class instance**.
     * @description **Dissociate the [BaseFieldInstance](BaseFieldInstance) class instance** from your actual [PivotInstance](PivotInstance) class instance.  *Note that the method will also run [BaseFieldInstance.unlinkPivots](BaseFieldInstance.unlinkPivots) to dissociate the [BaseFieldInstance](BaseFieldInstance) class instance et the [PivotInstance](PivotInstance) class instance from the both parts*.
     * @return Return the actual [PivotInstance](PivotInstance) class instance.
     * @category @PivotInstance.base
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category value
     * @category dissociate
     * @public
     * @see PivotInstance.pivot
     * @see PivotInstance.linkPivot
     * @see PivotInstance.hasPivotLinked
     * @see PivotInstance.unsafePivot
     * @see PivotInstance.getPivot
     * @see PivotInstance.isLinkedWithPivot
     * @see BaseFieldInstance.unlinkPivots
     */
    unlinkBase(): this{
        const _base = this.base;
        this.base = undefined;

        _base?.unlinkPivots(this);

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [BaseFieldInstance](BaseFieldInstance) class instance** are associated.
     * @description **Checks if the [PivotInstance](PivotInstance) class instance is associated with all the given [BaseFieldInstance](BaseFieldInstance) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Return a boolean if the [PivotInstance](PivotInstance) class instance is associated with all the given [BaseFieldInstance](BaseFieldInstance) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @category @PivotInstance.base
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category value
     * @category equality
     * @public
     * @see PivotInstance.pivot
     * @see PivotInstance.linkPivot
     * @see PivotInstance.hasPivotLinked
     * @see PivotInstance.unsafePivot
     * @see PivotInstance.getPivot
     * @see PivotInstance.unlinkPivot
     */
    isLinkedWithBase(base?: undefined|BaseFieldInstance): boolean{
        if (typeof base === "undefined"){
            return false;
        }
        return this.unsafeBase() === base;
    }
}

export default PivotInstance