import OutOfRange from "../../../error/internal/out-of-range";
import DeepArray from "../../../util/deep-array/type";
import flat from "../../../util/deep-array/flat";
import UndefinedValue from "../../../error/internal/undefined-value";
import PivotConfiguration from "../pivot";
import ModelConfiguration from "../../index";
import UnlinkedValue from "../../../error/internal/unlinked-value";
import Match from "../../../util/match/type";
import DupyObject from "../../../dupy-object";
import BaseFieldInstance from "./instance";
import matcher from "../../../util/match/matcher";

abstract class BaseFieldConfiguration extends DupyObject{
    abstract unsafeContext();
    abstract unsafeTranslate();

    matchingWithFilterKeys(key?: undefined | Match) {
        return matcher(this.unsafeFilterKeys(), key)
    }

    matchingWithInputKeys(key?: undefined | Match) {
        return matcher(this.unsafeInputKeys(), key)
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseFieldConfiguration.context**. Unlike the [unsafeContext](BaseFieldConfiguration.unsafeContext) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.context is not defined**.
     * @description **Gives the value of @BaseFieldConfiguration.context**. Unlike the [unsafeContext](BaseFieldConfiguration.unsafeContext) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.context is not defined**.
     * @return **Returns the value of @BaseFieldConfiguration.context**. Unlike the [unsafeContext](BaseFieldConfiguration.unsafeContext) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.context is not defined**.
     * @category @BaseFieldConfiguration.context
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.context isn't defined.
     * @see BaseFieldConfiguration.context
     * @see BaseFieldConfiguration.setContext
     * @see BaseFieldConfiguration.unsafeContext
     * @see BaseFieldConfiguration.eraseContext
     * @see BaseFieldConfiguration.isContextDefined
     * @see BaseFieldConfiguration.isContextEqualTo
     */
    getContext(): string{
        const context = this.unsafeContext();

        if (typeof context === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getContext");
            error.setClassname("BaseFieldConfiguration");
            error.setProperty("context");
            throw error;
        }
        return context;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.context is defined**.
     * @description **Checks if @BaseFieldConfiguration.context is defined** or not.
     * @return **Returns a boolean if @BaseFieldConfiguration.context is defined** or not.
     * @category @BaseFieldConfiguration.context
     * @category defined
     * @category field
     * @public
     * @see BaseFieldConfiguration.context
     * @see BaseFieldConfiguration.setContext
     * @see BaseFieldConfiguration.getContext
     * @see BaseFieldConfiguration.unsafeContext
     * @see BaseFieldConfiguration.eraseContext
     * @see BaseFieldConfiguration.isContextEqualTo
     */
    isContextDefined(): boolean{
        return typeof this.unsafeContext() !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.context is equal to your given value**.
     * @description **Checks if @BaseFieldConfiguration.context is equal to your given value** and if @BaseFieldConfiguration.context is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @BaseFieldConfiguration.context is equal to your given value** and if @BaseFieldConfiguration.context is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param context **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @BaseFieldConfiguration.context
     * @category equality
     * @category field
     * @public
     * @see BaseFieldConfiguration.context
     * @see BaseFieldConfiguration.setContext
     * @see BaseFieldConfiguration.getContext
     * @see BaseFieldConfiguration.unsafeContext
     * @see BaseFieldConfiguration.eraseContext
     * @see BaseFieldConfiguration.isContextDefined
     */
    isContextEqualTo(context?: undefined|string): boolean{
        if (typeof context === "undefined"){
            return false;
        }
        return this.unsafeContext() === context;
    }



    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseFieldConfiguration.translate**. Unlike the [unsafeTranslate](BaseFieldConfiguration.unsafeTranslate) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.translate is not defined**.
     * @description **Gives the value of @BaseFieldConfiguration.translate**. Unlike the [unsafeTranslate](BaseFieldConfiguration.unsafeTranslate) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.translate is not defined**.
     * @return **Returns the value of @BaseFieldConfiguration.translate**. Unlike the [unsafeTranslate](BaseFieldConfiguration.unsafeTranslate) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.translate is not defined**.
     * @category @BaseFieldConfiguration.translate
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.translate isn't defined.
     * @see BaseFieldConfiguration.translate
     * @see BaseFieldConfiguration.setTranslate
     * @see BaseFieldConfiguration.unsafeTranslate
     * @see BaseFieldConfiguration.eraseTranslate
     * @see BaseFieldConfiguration.isTranslateDefined
     * @see BaseFieldConfiguration.isTranslateEqualTo
     */
    getTranslate(): string{
        const translate = this.unsafeTranslate();

        if (typeof translate === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getTranslate");
            error.setClassname("BaseFieldConfiguration");
            error.setProperty("translate");
            throw error;
        }
        return translate;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.translate is defined**.
     * @description **Checks if @BaseFieldConfiguration.translate is defined** or not.
     * @return **Returns a boolean if @BaseFieldConfiguration.translate is defined** or not.
     * @category @BaseFieldConfiguration.translate
     * @category defined
     * @category field
     * @public
     * @see BaseFieldConfiguration.translate
     * @see BaseFieldConfiguration.setTranslate
     * @see BaseFieldConfiguration.getTranslate
     * @see BaseFieldConfiguration.unsafeTranslate
     * @see BaseFieldConfiguration.eraseTranslate
     * @see BaseFieldConfiguration.isTranslateEqualTo
     */
    isTranslateDefined(): boolean{
        return typeof this.unsafeTranslate() !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.translate is equal to your given value**.
     * @description **Checks if @BaseFieldConfiguration.translate is equal to your given value** and if @BaseFieldConfiguration.translate is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @BaseFieldConfiguration.translate is equal to your given value** and if @BaseFieldConfiguration.translate is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param translate **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @BaseFieldConfiguration.translate
     * @category equality
     * @category field
     * @public
     * @see BaseFieldConfiguration.translate
     * @see BaseFieldConfiguration.setTranslate
     * @see BaseFieldConfiguration.getTranslate
     * @see BaseFieldConfiguration.unsafeTranslate
     * @see BaseFieldConfiguration.eraseTranslate
     * @see BaseFieldConfiguration.isTranslateDefined
     */
    isTranslateEqualTo(translate?: undefined|string): boolean{
        if (typeof translate === "undefined"){
            return false;
        }
        return this.unsafeTranslate() === translate;
    }


    isBaseFieldConfiguration(): this is BaseFieldConfiguration {
        return true;
    }
    isBaseField(): this is BaseFieldInstance | BaseFieldConfiguration {
        return true;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Save @BaseFieldConfiguration.input_keys.
     * @description Save @BaseFieldConfiguration.input_keys.
     * @category input_keys
     * @category property
     * @category list
     * @protected
     * @see BaseFieldConfiguration.getInputKeys
     * @see BaseFieldConfiguration.unsafeInputKeys
     * @see BaseFieldConfiguration.getInputKey
     * @see BaseFieldConfiguration.unsafeInputKey
     * @see BaseFieldConfiguration.getInputKeysSize
     * @see BaseFieldConfiguration.containInputKeys
     * @see BaseFieldConfiguration.addInputKeys
     * @see BaseFieldConfiguration.removeInputKeys
     * @see BaseFieldConfiguration.truncateInputKeys
     */
    protected input_keys: Match[] = [];

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the size of @BaseFieldConfiguration.input_keys**.
     * @description **Gives the size of @BaseFieldConfiguration.input_keys**.
     * @return **Returns the size of @BaseFieldConfiguration.input_keys**.
     * @category @BaseFieldConfiguration.input_keys
     * @category list
     * @category length
     * @public
     * @see BaseFieldConfiguration.getInputKeys
     * @see BaseFieldConfiguration.unsafeInputKeys
     * @see BaseFieldConfiguration.getInputKey
     * @see BaseFieldConfiguration.unsafeInputKey
     * @see BaseFieldConfiguration.containInputKeys
     * @see BaseFieldConfiguration.addInputKeys
     * @see BaseFieldConfiguration.removeInputKeys
     * @see BaseFieldConfiguration.truncateInputKeys
     * @see input_keys
     */
    getInputKeysSize(): number{
        return this.unsafeInputKeys().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of @BaseFieldConfiguration.input_keys**. Unlike [getInputKey](BaseFieldConfiguration.getInputKey) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of @BaseFieldConfiguration.input_keys**. Unlike [getInputKey](BaseFieldConfiguration.getInputKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of @BaseFieldConfiguration.input_keys**. Unlike [getInputKey](BaseFieldConfiguration.getInputKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @BaseFieldConfiguration.input_keys, you can give `-1` for example. If you want to get the first item of @BaseFieldConfiguration.input_keys, you can give `0`.*
     * @category @BaseFieldConfiguration.input_keys
     * @category unsafe
     * @category list
     * @category value
     * @public
     * @see BaseFieldConfiguration.getInputKeys
     * @see BaseFieldConfiguration.unsafeInputKeys
     * @see BaseFieldConfiguration.getInputKey
     * @see BaseFieldConfiguration.getInputKeysSize
     * @see BaseFieldConfiguration.containInputKeys
     * @see BaseFieldConfiguration.addInputKeys
     * @see BaseFieldConfiguration.removeInputKeys
     * @see BaseFieldConfiguration.truncateInputKeys
     * @see input_keys
     */
    unsafeInputKey(position: number): Match|undefined{
        if (position < 0){
            const new_position = this.getInputKeysSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeInputKey(new_position);
        }

        return this.input_keys[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item in @BaseFieldConfiguration.input_keys**. Unlike the [unsafeInputKey](BaseFieldConfiguration.unsafeInputKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item in @BaseFieldConfiguration.input_keys**. Unlike the [unsafeInputKey](BaseFieldConfiguration.unsafeInputKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item in @BaseFieldConfiguration.input_keys**. Unlike the [unsafeInputKey](BaseFieldConfiguration.unsafeInputKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @BaseFieldConfiguration.input_keys, you can give `-1` for example. If you want to get the first item of @BaseFieldConfiguration.input_keys, you can give `0`.*
     * @throw OutOfRange Throws an [OutOfRange](OutOfRange) error if the given position is out of range.
     * @category @BaseFieldConfiguration.input_keys
     * @category getter
     * @category list
     * @category value
     * @public
     * @see OutOfRange
     * @see BaseFieldConfiguration.getInputKeys
     * @see BaseFieldConfiguration.unsafeInputKeys
     * @see BaseFieldConfiguration.unsafeInputKey
     * @see BaseFieldConfiguration.getInputKeysSize
     * @see BaseFieldConfiguration.containInputKeys
     * @see BaseFieldConfiguration.addInputKeys
     * @see BaseFieldConfiguration.removeInputKeys
     * @see BaseFieldConfiguration.truncateInputKeys
     * @see input_keys
     */
    getInputKey(position: number): Match{
        const input_key = this.unsafeInputKey(position);
        if (typeof input_key === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getInputKey");
            error.setClassname("BaseFieldConfiguration");
            error.setProperty("input_keys");
            error.setPosition(position);
            throw error;
        }

        return input_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Add one or multiple values** to @BaseFieldConfiguration.input_keys.
     * @description Allows you to **add one or multiple values** to @BaseFieldConfiguration.input_keys. Each argument of in the method will be added to @BaseFieldConfiguration.input_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in @BaseFieldConfiguration.input_keys: the value is unique._
     * @param input_keys All the given values that you to add to @BaseFieldConfiguration.input_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*. *If the value is already included, the method will discard the item addition.*
     * @return Returns the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.input_keys
     * @category associate
     * @category list
     * @public
     * @see BaseFieldConfiguration.getInputKeys
     * @see BaseFieldConfiguration.unsafeInputKeys
     * @see BaseFieldConfiguration.getInputKey
     * @see BaseFieldConfiguration.unsafeInputKey
     * @see BaseFieldConfiguration.getInputKeysSize
     * @see BaseFieldConfiguration.containInputKeys
     * @see BaseFieldConfiguration.removeInputKeys
     * @see BaseFieldConfiguration.truncateInputKeys
     * @see input_keys
     */
    addInputKeys(... input_keys: DeepArray<Match>[]): this{
        for (const input_key of flat(input_keys)){
            if (this.containInputKeys(input_key)){
                continue;
            }


            this.input_keys.push(input_key);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if the given value is included** in @BaseFieldConfiguration.input_keys.
     * @description **Checks if all the given value is included** in @BaseFieldConfiguration.input_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @BaseFieldConfiguration.input_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @BaseFieldConfiguration.input_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @return **Returns a boolean if all the given value is included** in @BaseFieldConfiguration.input_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @BaseFieldConfiguration.input_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @BaseFieldConfiguration.input_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @param input_keys All the values that you want to check *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseFieldConfiguration.input_keys
     * @category defined
     * @category list
     * @public
     * @see BaseFieldConfiguration.getInputKeys
     * @see BaseFieldConfiguration.unsafeInputKeys
     * @see BaseFieldConfiguration.getInputKey
     * @see BaseFieldConfiguration.unsafeInputKey
     * @see BaseFieldConfiguration.getInputKeysSize
     * @see BaseFieldConfiguration.addInputKeys
     * @see BaseFieldConfiguration.removeInputKeys
     * @see BaseFieldConfiguration.truncateInputKeys
     * @see input_keys
     */
    containInputKeys(... input_keys: DeepArray<Match|undefined>[]): boolean{
        return flat(input_keys).every((input_key) => {
            if (typeof input_key === "undefined"){
                return false;
            }

            return this.unsafeInputKeys().includes(input_key);
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of @BaseFieldConfiguration.input_keys**.
     * @description **Gives a copy of @BaseFieldConfiguration.input_keys**.
     * @return **Returns a copy of @BaseFieldConfiguration.input_keys**.
     * @category @BaseFieldConfiguration.input_keys
     * @category getter
     * @category iterable
     * @category list
     * @public
     * @see BaseFieldConfiguration.unsafeInputKeys
     * @see BaseFieldConfiguration.getInputKey
     * @see BaseFieldConfiguration.unsafeInputKey
     * @see BaseFieldConfiguration.getInputKeysSize
     * @see BaseFieldConfiguration.containInputKeys
     * @see BaseFieldConfiguration.addInputKeys
     * @see BaseFieldConfiguration.removeInputKeys
     * @see BaseFieldConfiguration.truncateInputKeys
     * @see input_keys
     */
    getInputKeys(): Match[]{
        return Array.from(this.unsafeInputKeys());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives @BaseFieldConfiguration.input_keys without copy** the array.
     * @description **Gives @BaseFieldConfiguration.input_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getInputKeys](BaseFieldConfiguration.getInputKeys) method.
     * @return **Returns @BaseFieldConfiguration.input_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getInputKeys](BaseFieldConfiguration.getInputKeys) method.
     * @category @BaseFieldConfiguration.input_keys
     * @category unsafe
     * @category iterable
     * @category list
     * @public
     * @see BaseFieldConfiguration.getInputKeys
     * @see BaseFieldConfiguration.getInputKey
     * @see BaseFieldConfiguration.unsafeInputKey
     * @see BaseFieldConfiguration.getInputKeysSize
     * @see BaseFieldConfiguration.containInputKeys
     * @see BaseFieldConfiguration.addInputKeys
     * @see BaseFieldConfiguration.removeInputKeys
     * @see BaseFieldConfiguration.truncateInputKeys
     * @see input_keys
     */
    unsafeInputKeys(): Match[]{
        return this.input_keys;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Allows to **remove one or multiple values** from @BaseFieldConfiguration.input_keys.
     * @description Allows you to **remove one or multiple values** from @BaseFieldConfiguration.input_keys. Each argument of in the method will be removed from @BaseFieldConfiguration.input_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @param input_keys All the given values that you to remove from @BaseFieldConfiguration.input_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @return Returns the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.input_keys
     * @category dissociate
     * @category list
     * @public
     * @see BaseFieldConfiguration.getInputKeys
     * @see BaseFieldConfiguration.unsafeInputKeys
     * @see BaseFieldConfiguration.getInputKey
     * @see BaseFieldConfiguration.unsafeInputKey
     * @see BaseFieldConfiguration.getInputKeysSize
     * @see BaseFieldConfiguration.containInputKeys
     * @see BaseFieldConfiguration.addInputKeys
     * @see BaseFieldConfiguration.truncateInputKeys
     * @see input_keys
     */
    removeInputKeys(... input_keys: DeepArray<Match>[]): this{
        for (const input_key of flat(input_keys)){
            if (!this.containInputKeys(input_key)){
                continue;
            }

            while (true){
                const index = this.input_keys.indexOf(input_key);
                if (index < 0){
                    break;
                }

                this.input_keys.splice(index, 1);
            }
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Truncate @BaseFieldConfiguration.input_keys**.
     * @description **Remove all the values in @BaseFieldConfiguration.input_keys**.
     * @return Returns the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.input_keys
     * @category truncate
     * @category list
     * @public
     * @see BaseFieldConfiguration.getInputKeys
     * @see BaseFieldConfiguration.unsafeInputKeys
     * @see BaseFieldConfiguration.getInputKey
     * @see BaseFieldConfiguration.unsafeInputKey
     * @see BaseFieldConfiguration.getInputKeysSize
     * @see BaseFieldConfiguration.containInputKeys
     * @see BaseFieldConfiguration.addInputKeys
     * @see BaseFieldConfiguration.removeInputKeys
     * @see input_keys
     */
    truncateInputKeys(): this{
        this.removeInputKeys(this.unsafeInputKeys());
        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Save @BaseFieldConfiguration.filter_keys.
     * @description Save @BaseFieldConfiguration.filter_keys.
     * @category filter_keys
     * @category property
     * @category list
     * @protected
     * @see BaseFieldConfiguration.getFilterKeys
     * @see BaseFieldConfiguration.unsafeFilterKeys
     * @see BaseFieldConfiguration.getFilterKey
     * @see BaseFieldConfiguration.unsafeFilterKey
     * @see BaseFieldConfiguration.getFilterKeysSize
     * @see BaseFieldConfiguration.containFilterKeys
     * @see BaseFieldConfiguration.addFilterKeys
     * @see BaseFieldConfiguration.removeFilterKeys
     * @see BaseFieldConfiguration.truncateFilterKeys
     */
    protected filter_keys: Match[] = [];

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the size of @BaseFieldConfiguration.filter_keys**.
     * @description **Gives the size of @BaseFieldConfiguration.filter_keys**.
     * @return **Returns the size of @BaseFieldConfiguration.filter_keys**.
     * @category @BaseFieldConfiguration.filter_keys
     * @category list
     * @category length
     * @public
     * @see BaseFieldConfiguration.getFilterKeys
     * @see BaseFieldConfiguration.unsafeFilterKeys
     * @see BaseFieldConfiguration.getFilterKey
     * @see BaseFieldConfiguration.unsafeFilterKey
     * @see BaseFieldConfiguration.containFilterKeys
     * @see BaseFieldConfiguration.addFilterKeys
     * @see BaseFieldConfiguration.removeFilterKeys
     * @see BaseFieldConfiguration.truncateFilterKeys
     * @see filter_keys
     */
    getFilterKeysSize(): number{
        return this.unsafeFilterKeys().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of @BaseFieldConfiguration.filter_keys**. Unlike [getFilterKey](BaseFieldConfiguration.getFilterKey) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of @BaseFieldConfiguration.filter_keys**. Unlike [getFilterKey](BaseFieldConfiguration.getFilterKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of @BaseFieldConfiguration.filter_keys**. Unlike [getFilterKey](BaseFieldConfiguration.getFilterKey) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @BaseFieldConfiguration.filter_keys, you can give `-1` for example. If you want to get the first item of @BaseFieldConfiguration.filter_keys, you can give `0`.*
     * @category @BaseFieldConfiguration.filter_keys
     * @category unsafe
     * @category list
     * @category value
     * @public
     * @see BaseFieldConfiguration.getFilterKeys
     * @see BaseFieldConfiguration.unsafeFilterKeys
     * @see BaseFieldConfiguration.getFilterKey
     * @see BaseFieldConfiguration.getFilterKeysSize
     * @see BaseFieldConfiguration.containFilterKeys
     * @see BaseFieldConfiguration.addFilterKeys
     * @see BaseFieldConfiguration.removeFilterKeys
     * @see BaseFieldConfiguration.truncateFilterKeys
     * @see filter_keys
     */
    unsafeFilterKey(position: number): Match|undefined{
        if (position < 0){
            const new_position = this.getFilterKeysSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeFilterKey(new_position);
        }

        return this.filter_keys[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item in @BaseFieldConfiguration.filter_keys**. Unlike the [unsafeFilterKey](BaseFieldConfiguration.unsafeFilterKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item in @BaseFieldConfiguration.filter_keys**. Unlike the [unsafeFilterKey](BaseFieldConfiguration.unsafeFilterKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item in @BaseFieldConfiguration.filter_keys**. Unlike the [unsafeFilterKey](BaseFieldConfiguration.unsafeFilterKey) method, **if the value cannot be found at the given position, the method throws an [OutOfRange](OutOfRange) error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of @BaseFieldConfiguration.filter_keys, you can give `-1` for example. If you want to get the first item of @BaseFieldConfiguration.filter_keys, you can give `0`.*
     * @throw OutOfRange Throws an [OutOfRange](OutOfRange) error if the given position is out of range.
     * @category @BaseFieldConfiguration.filter_keys
     * @category getter
     * @category list
     * @category value
     * @public
     * @see OutOfRange
     * @see BaseFieldConfiguration.getFilterKeys
     * @see BaseFieldConfiguration.unsafeFilterKeys
     * @see BaseFieldConfiguration.unsafeFilterKey
     * @see BaseFieldConfiguration.getFilterKeysSize
     * @see BaseFieldConfiguration.containFilterKeys
     * @see BaseFieldConfiguration.addFilterKeys
     * @see BaseFieldConfiguration.removeFilterKeys
     * @see BaseFieldConfiguration.truncateFilterKeys
     * @see filter_keys
     */
    getFilterKey(position: number): Match{
        const filter_key = this.unsafeFilterKey(position);
        if (typeof filter_key === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getFilterKey");
            error.setClassname("BaseFieldConfiguration");
            error.setProperty("filter_keys");
            error.setPosition(position);
            throw error;
        }

        return filter_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Add one or multiple values** to @BaseFieldConfiguration.filter_keys.
     * @description Allows you to **add one or multiple values** to @BaseFieldConfiguration.filter_keys. Each argument of in the method will be added to @BaseFieldConfiguration.filter_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. _Note that the value cannot be added twice in @BaseFieldConfiguration.filter_keys: the value is unique._
     * @param filter_keys All the given values that you to add to @BaseFieldConfiguration.filter_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*. *If the value is already included, the method will discard the item addition.*
     * @return Returns the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.filter_keys
     * @category associate
     * @category list
     * @public
     * @see BaseFieldConfiguration.getFilterKeys
     * @see BaseFieldConfiguration.unsafeFilterKeys
     * @see BaseFieldConfiguration.getFilterKey
     * @see BaseFieldConfiguration.unsafeFilterKey
     * @see BaseFieldConfiguration.getFilterKeysSize
     * @see BaseFieldConfiguration.containFilterKeys
     * @see BaseFieldConfiguration.removeFilterKeys
     * @see BaseFieldConfiguration.truncateFilterKeys
     * @see filter_keys
     */
    addFilterKeys(... filter_keys: DeepArray<Match>[]): this{
        for (const filter_key of flat(filter_keys)){
            if (this.containFilterKeys(filter_key)){
                continue;
            }


            this.filter_keys.push(filter_key);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if the given value is included** in @BaseFieldConfiguration.filter_keys.
     * @description **Checks if all the given value is included** in @BaseFieldConfiguration.filter_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @BaseFieldConfiguration.filter_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @BaseFieldConfiguration.filter_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @return **Returns a boolean if all the given value is included** in @BaseFieldConfiguration.filter_keys. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not included in @BaseFieldConfiguration.filter_keys or are `undefined`, the method results `false`: the method result `true` only if all the values are included in @BaseFieldConfiguration.filter_keys and not undefined. _Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument exists in the list.)_
     * @param filter_keys All the values that you want to check *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @BaseFieldConfiguration.filter_keys
     * @category defined
     * @category list
     * @public
     * @see BaseFieldConfiguration.getFilterKeys
     * @see BaseFieldConfiguration.unsafeFilterKeys
     * @see BaseFieldConfiguration.getFilterKey
     * @see BaseFieldConfiguration.unsafeFilterKey
     * @see BaseFieldConfiguration.getFilterKeysSize
     * @see BaseFieldConfiguration.addFilterKeys
     * @see BaseFieldConfiguration.removeFilterKeys
     * @see BaseFieldConfiguration.truncateFilterKeys
     * @see filter_keys
     */
    containFilterKeys(... filter_keys: DeepArray<Match|undefined>[]): boolean{
        return flat(filter_keys).every((filter_key) => {
            if (typeof filter_key === "undefined"){
                return false;
            }

            return this.unsafeFilterKeys().includes(filter_key);
        });
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of @BaseFieldConfiguration.filter_keys**.
     * @description **Gives a copy of @BaseFieldConfiguration.filter_keys**.
     * @return **Returns a copy of @BaseFieldConfiguration.filter_keys**.
     * @category @BaseFieldConfiguration.filter_keys
     * @category getter
     * @category iterable
     * @category list
     * @public
     * @see BaseFieldConfiguration.unsafeFilterKeys
     * @see BaseFieldConfiguration.getFilterKey
     * @see BaseFieldConfiguration.unsafeFilterKey
     * @see BaseFieldConfiguration.getFilterKeysSize
     * @see BaseFieldConfiguration.containFilterKeys
     * @see BaseFieldConfiguration.addFilterKeys
     * @see BaseFieldConfiguration.removeFilterKeys
     * @see BaseFieldConfiguration.truncateFilterKeys
     * @see filter_keys
     */
    getFilterKeys(): Match[]{
        return Array.from(this.unsafeFilterKeys());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives @BaseFieldConfiguration.filter_keys without copy** the array.
     * @description **Gives @BaseFieldConfiguration.filter_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getFilterKeys](BaseFieldConfiguration.getFilterKeys) method.
     * @return **Returns @BaseFieldConfiguration.filter_keys without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getFilterKeys](BaseFieldConfiguration.getFilterKeys) method.
     * @category @BaseFieldConfiguration.filter_keys
     * @category unsafe
     * @category iterable
     * @category list
     * @public
     * @see BaseFieldConfiguration.getFilterKeys
     * @see BaseFieldConfiguration.getFilterKey
     * @see BaseFieldConfiguration.unsafeFilterKey
     * @see BaseFieldConfiguration.getFilterKeysSize
     * @see BaseFieldConfiguration.containFilterKeys
     * @see BaseFieldConfiguration.addFilterKeys
     * @see BaseFieldConfiguration.removeFilterKeys
     * @see BaseFieldConfiguration.truncateFilterKeys
     * @see filter_keys
     */
    unsafeFilterKeys(): Match[]{
        return this.filter_keys;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary Allows to **remove one or multiple values** from @BaseFieldConfiguration.filter_keys.
     * @description Allows you to **remove one or multiple values** from @BaseFieldConfiguration.filter_keys. Each argument of in the method will be removed from @BaseFieldConfiguration.filter_keys, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @param filter_keys All the given values that you to remove from @BaseFieldConfiguration.filter_keys *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @return Returns the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.filter_keys
     * @category dissociate
     * @category list
     * @public
     * @see BaseFieldConfiguration.getFilterKeys
     * @see BaseFieldConfiguration.unsafeFilterKeys
     * @see BaseFieldConfiguration.getFilterKey
     * @see BaseFieldConfiguration.unsafeFilterKey
     * @see BaseFieldConfiguration.getFilterKeysSize
     * @see BaseFieldConfiguration.containFilterKeys
     * @see BaseFieldConfiguration.addFilterKeys
     * @see BaseFieldConfiguration.truncateFilterKeys
     * @see filter_keys
     */
    removeFilterKeys(... filter_keys: DeepArray<Match>[]): this{
        for (const filter_key of flat(filter_keys)){
            if (!this.containFilterKeys(filter_key)){
                continue;
            }

            while (true){
                const index = this.filter_keys.indexOf(filter_key);
                if (index < 0){
                    break;
                }

                this.filter_keys.splice(index, 1);
            }
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Truncate @BaseFieldConfiguration.filter_keys**.
     * @description **Remove all the values in @BaseFieldConfiguration.filter_keys**.
     * @return Returns the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.filter_keys
     * @category truncate
     * @category list
     * @public
     * @see BaseFieldConfiguration.getFilterKeys
     * @see BaseFieldConfiguration.unsafeFilterKeys
     * @see BaseFieldConfiguration.getFilterKey
     * @see BaseFieldConfiguration.unsafeFilterKey
     * @see BaseFieldConfiguration.getFilterKeysSize
     * @see BaseFieldConfiguration.containFilterKeys
     * @see BaseFieldConfiguration.addFilterKeys
     * @see BaseFieldConfiguration.removeFilterKeys
     * @see filter_keys
     */
    truncateFilterKeys(): this{
        this.removeFilterKeys(this.unsafeFilterKeys());
        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @BaseFieldConfiguration.storage.
     * @description Save @BaseFieldConfiguration.storage.
     * @category @BaseFieldConfiguration.storage
     * @category property
     * @category field
     * @protected
     * @see BaseFieldConfiguration.setStorage
     * @see BaseFieldConfiguration.getStorage
     * @see BaseFieldConfiguration.unsafeStorage
     * @see BaseFieldConfiguration.eraseStorage
     * @see BaseFieldConfiguration.isStorageDefined
     * @see BaseFieldConfiguration.isStorageEqualTo
     */
    protected storage: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @BaseFieldConfiguration.storage**.
     * @description Allows you to update or **assign the value of @BaseFieldConfiguration.storage**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @param storage The new value to assign to @BaseFieldConfiguration.storage.
     * @category @BaseFieldConfiguration.storage
     * @category setter
     * @category field
     * @public
     * @see BaseFieldConfiguration.storage
     * @see BaseFieldConfiguration.getStorage
     * @see BaseFieldConfiguration.unsafeStorage
     * @see BaseFieldConfiguration.eraseStorage
     * @see BaseFieldConfiguration.isStorageDefined
     * @see BaseFieldConfiguration.isStorageEqualTo
     */
    setStorage(storage: string): this{
        if (this.isStorageEqualTo(storage)){
            return this;
        }

        this.eraseStorage();
        this.storage = storage;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseFieldConfiguration.storage**. Unlike the [getStorage](BaseFieldConfiguration.getStorage) method, **the method results an undefined value if @BaseFieldConfiguration.storage is not defined**.
     * @description **Gives the value of @BaseFieldConfiguration.storage**. Unlike the [getStorage](BaseFieldConfiguration.getStorage) method, **the method results an undefined value if @BaseFieldConfiguration.storage is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @BaseFieldConfiguration.storage**. Unlike the [getStorage](BaseFieldConfiguration.getStorage) method, **the method results an undefined value if @BaseFieldConfiguration.storage is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @BaseFieldConfiguration.storage
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see BaseFieldConfiguration.storage
     * @see BaseFieldConfiguration.setStorage
     * @see BaseFieldConfiguration.getStorage
     * @see BaseFieldConfiguration.eraseStorage
     * @see BaseFieldConfiguration.isStorageDefined
     * @see BaseFieldConfiguration.isStorageEqualTo
     */
    unsafeStorage(): string|undefined{
        return this.storage;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseFieldConfiguration.storage**. Unlike the [unsafeStorage](BaseFieldConfiguration.unsafeStorage) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.storage is not defined**.
     * @description **Gives the value of @BaseFieldConfiguration.storage**. Unlike the [unsafeStorage](BaseFieldConfiguration.unsafeStorage) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.storage is not defined**.
     * @return **Returns the value of @BaseFieldConfiguration.storage**. Unlike the [unsafeStorage](BaseFieldConfiguration.unsafeStorage) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.storage is not defined**.
     * @category @BaseFieldConfiguration.storage
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.storage isn't defined.
     * @see BaseFieldConfiguration.storage
     * @see BaseFieldConfiguration.setStorage
     * @see BaseFieldConfiguration.unsafeStorage
     * @see BaseFieldConfiguration.eraseStorage
     * @see BaseFieldConfiguration.isStorageDefined
     * @see BaseFieldConfiguration.isStorageEqualTo
     */
    getStorage(): string{
        const storage = this.unsafeStorage();

        if (typeof storage === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getStorage");
            error.setClassname("BaseFieldConfiguration");
            error.setProperty("storage");
            throw error;
        }
        return storage;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.storage is defined**.
     * @description **Checks if @BaseFieldConfiguration.storage is defined** or not.
     * @return **Returns a boolean if @BaseFieldConfiguration.storage is defined** or not.
     * @category @BaseFieldConfiguration.storage
     * @category defined
     * @category field
     * @public
     * @see BaseFieldConfiguration.storage
     * @see BaseFieldConfiguration.setStorage
     * @see BaseFieldConfiguration.getStorage
     * @see BaseFieldConfiguration.unsafeStorage
     * @see BaseFieldConfiguration.eraseStorage
     * @see BaseFieldConfiguration.isStorageEqualTo
     */
    isStorageDefined(): boolean{
        return typeof this.storage !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.storage is equal to your given value**.
     * @description **Checks if @BaseFieldConfiguration.storage is equal to your given value** and if @BaseFieldConfiguration.storage is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @BaseFieldConfiguration.storage is equal to your given value** and if @BaseFieldConfiguration.storage is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param storage **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @BaseFieldConfiguration.storage
     * @category equality
     * @category field
     * @public
     * @see BaseFieldConfiguration.storage
     * @see BaseFieldConfiguration.setStorage
     * @see BaseFieldConfiguration.getStorage
     * @see BaseFieldConfiguration.unsafeStorage
     * @see BaseFieldConfiguration.eraseStorage
     * @see BaseFieldConfiguration.isStorageDefined
     */
    isStorageEqualTo(storage?: undefined|string): boolean{
        if (typeof storage === "undefined"){
            return false;
        }
        return this.unsafeStorage() === storage;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @BaseFieldConfiguration.storage**.
     * @description **Removes the assigned the value of @BaseFieldConfiguration.storage**: *@BaseFieldConfiguration.storage will be flagged as undefined*.
     * @return Return the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.storage
     * @category truncate
     * @category field
     * @public
     * @see BaseFieldConfiguration.storage
     * @see BaseFieldConfiguration.setStorage
     * @see BaseFieldConfiguration.getStorage
     * @see BaseFieldConfiguration.unsafeStorage
     * @see BaseFieldConfiguration.isStorageDefined
     * @see BaseFieldConfiguration.isStorageEqualTo
     */
    eraseStorage(): this{
        this.storage = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @BaseFieldConfiguration.output_key.
     * @description Save @BaseFieldConfiguration.output_key.
     * @category @BaseFieldConfiguration.output_key
     * @category property
     * @category field
     * @protected
     * @see BaseFieldConfiguration.setOutputKey
     * @see BaseFieldConfiguration.getOutputKey
     * @see BaseFieldConfiguration.unsafeOutputKey
     * @see BaseFieldConfiguration.eraseOutputKey
     * @see BaseFieldConfiguration.isOutputKeyDefined
     * @see BaseFieldConfiguration.isOutputKeyEqualTo
     */
    protected output_key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @BaseFieldConfiguration.output_key**.
     * @description Allows you to update or **assign the value of @BaseFieldConfiguration.output_key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @param output_key The new value to assign to @BaseFieldConfiguration.output_key.
     * @category @BaseFieldConfiguration.output_key
     * @category setter
     * @category field
     * @public
     * @see BaseFieldConfiguration.output_key
     * @see BaseFieldConfiguration.getOutputKey
     * @see BaseFieldConfiguration.unsafeOutputKey
     * @see BaseFieldConfiguration.eraseOutputKey
     * @see BaseFieldConfiguration.isOutputKeyDefined
     * @see BaseFieldConfiguration.isOutputKeyEqualTo
     */
    setOutputKey(output_key: string): this{
        if (this.isOutputKeyEqualTo(output_key)){
            return this;
        }

        this.eraseOutputKey();
        this.output_key = output_key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseFieldConfiguration.output_key**. Unlike the [getOutputKey](BaseFieldConfiguration.getOutputKey) method, **the method results an undefined value if @BaseFieldConfiguration.output_key is not defined**.
     * @description **Gives the value of @BaseFieldConfiguration.output_key**. Unlike the [getOutputKey](BaseFieldConfiguration.getOutputKey) method, **the method results an undefined value if @BaseFieldConfiguration.output_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @BaseFieldConfiguration.output_key**. Unlike the [getOutputKey](BaseFieldConfiguration.getOutputKey) method, **the method results an undefined value if @BaseFieldConfiguration.output_key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @BaseFieldConfiguration.output_key
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see BaseFieldConfiguration.output_key
     * @see BaseFieldConfiguration.setOutputKey
     * @see BaseFieldConfiguration.getOutputKey
     * @see BaseFieldConfiguration.eraseOutputKey
     * @see BaseFieldConfiguration.isOutputKeyDefined
     * @see BaseFieldConfiguration.isOutputKeyEqualTo
     */
    unsafeOutputKey(): string|undefined{
        return this.output_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseFieldConfiguration.output_key**. Unlike the [unsafeOutputKey](BaseFieldConfiguration.unsafeOutputKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.output_key is not defined**.
     * @description **Gives the value of @BaseFieldConfiguration.output_key**. Unlike the [unsafeOutputKey](BaseFieldConfiguration.unsafeOutputKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.output_key is not defined**.
     * @return **Returns the value of @BaseFieldConfiguration.output_key**. Unlike the [unsafeOutputKey](BaseFieldConfiguration.unsafeOutputKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.output_key is not defined**.
     * @category @BaseFieldConfiguration.output_key
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.output_key isn't defined.
     * @see BaseFieldConfiguration.output_key
     * @see BaseFieldConfiguration.setOutputKey
     * @see BaseFieldConfiguration.unsafeOutputKey
     * @see BaseFieldConfiguration.eraseOutputKey
     * @see BaseFieldConfiguration.isOutputKeyDefined
     * @see BaseFieldConfiguration.isOutputKeyEqualTo
     */
    getOutputKey(): string{
        const output_key = this.unsafeOutputKey();

        if (typeof output_key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getOutputKey");
            error.setClassname("BaseFieldConfiguration");
            error.setProperty("output_key");
            throw error;
        }
        return output_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.output_key is defined**.
     * @description **Checks if @BaseFieldConfiguration.output_key is defined** or not.
     * @return **Returns a boolean if @BaseFieldConfiguration.output_key is defined** or not.
     * @category @BaseFieldConfiguration.output_key
     * @category defined
     * @category field
     * @public
     * @see BaseFieldConfiguration.output_key
     * @see BaseFieldConfiguration.setOutputKey
     * @see BaseFieldConfiguration.getOutputKey
     * @see BaseFieldConfiguration.unsafeOutputKey
     * @see BaseFieldConfiguration.eraseOutputKey
     * @see BaseFieldConfiguration.isOutputKeyEqualTo
     */
    isOutputKeyDefined(): boolean{
        return typeof this.output_key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.output_key is equal to your given value**.
     * @description **Checks if @BaseFieldConfiguration.output_key is equal to your given value** and if @BaseFieldConfiguration.output_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @BaseFieldConfiguration.output_key is equal to your given value** and if @BaseFieldConfiguration.output_key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param output_key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @BaseFieldConfiguration.output_key
     * @category equality
     * @category field
     * @public
     * @see BaseFieldConfiguration.output_key
     * @see BaseFieldConfiguration.setOutputKey
     * @see BaseFieldConfiguration.getOutputKey
     * @see BaseFieldConfiguration.unsafeOutputKey
     * @see BaseFieldConfiguration.eraseOutputKey
     * @see BaseFieldConfiguration.isOutputKeyDefined
     */
    isOutputKeyEqualTo(output_key?: undefined|string): boolean{
        if (typeof output_key === "undefined"){
            return false;
        }
        return this.unsafeOutputKey() === output_key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @BaseFieldConfiguration.output_key**.
     * @description **Removes the assigned the value of @BaseFieldConfiguration.output_key**: *@BaseFieldConfiguration.output_key will be flagged as undefined*.
     * @return Return the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.output_key
     * @category truncate
     * @category field
     * @public
     * @see BaseFieldConfiguration.output_key
     * @see BaseFieldConfiguration.setOutputKey
     * @see BaseFieldConfiguration.getOutputKey
     * @see BaseFieldConfiguration.unsafeOutputKey
     * @see BaseFieldConfiguration.isOutputKeyDefined
     * @see BaseFieldConfiguration.isOutputKeyEqualTo
     */
    eraseOutputKey(): this{
        this.output_key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @BaseFieldConfiguration.key.
     * @description Save @BaseFieldConfiguration.key.
     * @category @BaseFieldConfiguration.key
     * @category property
     * @category field
     * @protected
     * @see BaseFieldConfiguration.setKey
     * @see BaseFieldConfiguration.getKey
     * @see BaseFieldConfiguration.unsafeKey
     * @see BaseFieldConfiguration.eraseKey
     * @see BaseFieldConfiguration.isKeyDefined
     * @see BaseFieldConfiguration.isKeyEqualTo
     */
    protected key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @BaseFieldConfiguration.key**.
     * @description Allows you to update or **assign the value of @BaseFieldConfiguration.key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @param key The new value to assign to @BaseFieldConfiguration.key.
     * @category @BaseFieldConfiguration.key
     * @category setter
     * @category field
     * @public
     * @see BaseFieldConfiguration.key
     * @see BaseFieldConfiguration.getKey
     * @see BaseFieldConfiguration.unsafeKey
     * @see BaseFieldConfiguration.eraseKey
     * @see BaseFieldConfiguration.isKeyDefined
     * @see BaseFieldConfiguration.isKeyEqualTo
     */
    setKey(key: string): this{
        if (this.isKeyEqualTo(key)){
            return this;
        }

        this.eraseKey();
        this.key = key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseFieldConfiguration.key**. Unlike the [getKey](BaseFieldConfiguration.getKey) method, **the method results an undefined value if @BaseFieldConfiguration.key is not defined**.
     * @description **Gives the value of @BaseFieldConfiguration.key**. Unlike the [getKey](BaseFieldConfiguration.getKey) method, **the method results an undefined value if @BaseFieldConfiguration.key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @BaseFieldConfiguration.key**. Unlike the [getKey](BaseFieldConfiguration.getKey) method, **the method results an undefined value if @BaseFieldConfiguration.key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @BaseFieldConfiguration.key
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see BaseFieldConfiguration.key
     * @see BaseFieldConfiguration.setKey
     * @see BaseFieldConfiguration.getKey
     * @see BaseFieldConfiguration.eraseKey
     * @see BaseFieldConfiguration.isKeyDefined
     * @see BaseFieldConfiguration.isKeyEqualTo
     */
    unsafeKey(): string|undefined{
        return this.key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseFieldConfiguration.key**. Unlike the [unsafeKey](BaseFieldConfiguration.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.key is not defined**.
     * @description **Gives the value of @BaseFieldConfiguration.key**. Unlike the [unsafeKey](BaseFieldConfiguration.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.key is not defined**.
     * @return **Returns the value of @BaseFieldConfiguration.key**. Unlike the [unsafeKey](BaseFieldConfiguration.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.key is not defined**.
     * @category @BaseFieldConfiguration.key
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @BaseFieldConfiguration.key isn't defined.
     * @see BaseFieldConfiguration.key
     * @see BaseFieldConfiguration.setKey
     * @see BaseFieldConfiguration.unsafeKey
     * @see BaseFieldConfiguration.eraseKey
     * @see BaseFieldConfiguration.isKeyDefined
     * @see BaseFieldConfiguration.isKeyEqualTo
     */
    getKey(): string{
        const key = this.unsafeKey();

        if (typeof key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getKey");
            error.setClassname("BaseFieldConfiguration");
            error.setProperty("key");
            throw error;
        }
        return key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.key is defined**.
     * @description **Checks if @BaseFieldConfiguration.key is defined** or not.
     * @return **Returns a boolean if @BaseFieldConfiguration.key is defined** or not.
     * @category @BaseFieldConfiguration.key
     * @category defined
     * @category field
     * @public
     * @see BaseFieldConfiguration.key
     * @see BaseFieldConfiguration.setKey
     * @see BaseFieldConfiguration.getKey
     * @see BaseFieldConfiguration.unsafeKey
     * @see BaseFieldConfiguration.eraseKey
     * @see BaseFieldConfiguration.isKeyEqualTo
     */
    isKeyDefined(): boolean{
        return typeof this.key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.key is equal to your given value**.
     * @description **Checks if @BaseFieldConfiguration.key is equal to your given value** and if @BaseFieldConfiguration.key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @BaseFieldConfiguration.key is equal to your given value** and if @BaseFieldConfiguration.key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @BaseFieldConfiguration.key
     * @category equality
     * @category field
     * @public
     * @see BaseFieldConfiguration.key
     * @see BaseFieldConfiguration.setKey
     * @see BaseFieldConfiguration.getKey
     * @see BaseFieldConfiguration.unsafeKey
     * @see BaseFieldConfiguration.eraseKey
     * @see BaseFieldConfiguration.isKeyDefined
     */
    isKeyEqualTo(key?: undefined|string): boolean{
        if (typeof key === "undefined"){
            return false;
        }
        return this.unsafeKey() === key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @BaseFieldConfiguration.key**.
     * @description **Removes the assigned the value of @BaseFieldConfiguration.key**: *@BaseFieldConfiguration.key will be flagged as undefined*.
     * @return Return the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.key
     * @category truncate
     * @category field
     * @public
     * @see BaseFieldConfiguration.key
     * @see BaseFieldConfiguration.setKey
     * @see BaseFieldConfiguration.getKey
     * @see BaseFieldConfiguration.unsafeKey
     * @see BaseFieldConfiguration.isKeyDefined
     * @see BaseFieldConfiguration.isKeyEqualTo
     */
    eraseKey(): this{
        this.key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @BaseFieldConfiguration.is_primary.
     * @description Save @BaseFieldConfiguration.is_primary.
     * @category @BaseFieldConfiguration.is_primary
     * @category property
     * @category field
     * @protected
     * @see BaseFieldConfiguration.setIsPrimary
     * @see BaseFieldConfiguration.getIsPrimary
     * @see BaseFieldConfiguration.isPrimary
     * @see BaseFieldConfiguration.eraseIsPrimary
     * @see BaseFieldConfiguration.isIsPrimaryDefined
     * @see BaseFieldConfiguration.isIsPrimaryEqualTo
     */
    protected is_primary: boolean = false;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @BaseFieldConfiguration.is_primary**.
     * @description Allows you to update or **assign the value of @BaseFieldConfiguration.is_primary**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @param is_primary The new value to assign to @BaseFieldConfiguration.is_primary.
     * @category @BaseFieldConfiguration.is_primary
     * @category setter
     * @category field
     * @public
     * @see BaseFieldConfiguration.is_primary
     * @see BaseFieldConfiguration.getIsPrimary
     * @see BaseFieldConfiguration.isPrimary
     * @see BaseFieldConfiguration.eraseIsPrimary
     * @see BaseFieldConfiguration.isIsPrimaryDefined
     * @see BaseFieldConfiguration.isIsPrimaryEqualTo
     */
    setIsPrimary(is_primary: boolean): this{
        if (this.isIsPrimaryEqualTo(is_primary)){
            return this;
        }

        this.is_primary = is_primary;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseFieldConfiguration.is_primary**. Unlike the [getIsPrimary](BaseFieldConfiguration.getIsPrimary) method, **the method results an undefined value if @BaseFieldConfiguration.is_primary is not defined**.
     * @description **Gives the value of @BaseFieldConfiguration.is_primary**. Unlike the [getIsPrimary](BaseFieldConfiguration.getIsPrimary) method, **the method results an undefined value if @BaseFieldConfiguration.is_primary is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @BaseFieldConfiguration.is_primary**. Unlike the [getIsPrimary](BaseFieldConfiguration.getIsPrimary) method, **the method results an undefined value if @BaseFieldConfiguration.is_primary is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @BaseFieldConfiguration.is_primary
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see BaseFieldConfiguration.is_primary
     * @see BaseFieldConfiguration.setIsPrimary
     * @see BaseFieldConfiguration.getIsPrimary
     * @see BaseFieldConfiguration.eraseIsPrimary
     * @see BaseFieldConfiguration.isIsPrimaryDefined
     * @see BaseFieldConfiguration.isIsPrimaryEqualTo
     */
    isPrimary(): boolean{
        return this.is_primary;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.is_primary is equal to your given value**.
     * @description **Checks if @BaseFieldConfiguration.is_primary is equal to your given value** and if @BaseFieldConfiguration.is_primary is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @BaseFieldConfiguration.is_primary is equal to your given value** and if @BaseFieldConfiguration.is_primary is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param is_primary **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @BaseFieldConfiguration.is_primary
     * @category equality
     * @category field
     * @public
     * @see BaseFieldConfiguration.is_primary
     * @see BaseFieldConfiguration.setIsPrimary
     * @see BaseFieldConfiguration.getIsPrimary
     * @see BaseFieldConfiguration.isPrimary
     * @see BaseFieldConfiguration.eraseIsPrimary
     * @see BaseFieldConfiguration.isIsPrimaryDefined
     */
    isIsPrimaryEqualTo(is_primary?: undefined|boolean): boolean{
        if (typeof is_primary === "undefined"){
            return false;
        }
        return this.isPrimary() === is_primary;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @BaseFieldConfiguration.is_auto_increment.
     * @description Save @BaseFieldConfiguration.is_auto_increment.
     * @category @BaseFieldConfiguration.is_auto_increment
     * @category property
     * @category field
     * @protected
     * @see BaseFieldConfiguration.setIsAutoIncrement
     * @see BaseFieldConfiguration.getIsAutoIncrement
     * @see BaseFieldConfiguration.isAutoIncrement
     * @see BaseFieldConfiguration.eraseIsAutoIncrement
     * @see BaseFieldConfiguration.isIsAutoIncrementDefined
     * @see BaseFieldConfiguration.isIsAutoIncrementEqualTo
     */
    protected is_auto_increment: boolean = false;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @BaseFieldConfiguration.is_auto_increment**.
     * @description Allows you to update or **assign the value of @BaseFieldConfiguration.is_auto_increment**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @param is_auto_increment The new value to assign to @BaseFieldConfiguration.is_auto_increment.
     * @category @BaseFieldConfiguration.is_auto_increment
     * @category setter
     * @category field
     * @public
     * @see BaseFieldConfiguration.is_auto_increment
     * @see BaseFieldConfiguration.getIsAutoIncrement
     * @see BaseFieldConfiguration.isAutoIncrement
     * @see BaseFieldConfiguration.eraseIsAutoIncrement
     * @see BaseFieldConfiguration.isIsAutoIncrementDefined
     * @see BaseFieldConfiguration.isIsAutoIncrementEqualTo
     */
    setIsAutoIncrement(is_auto_increment: boolean): this{
        if (this.isIsAutoIncrementEqualTo(is_auto_increment)){
            return this;
        }

        this.is_auto_increment = is_auto_increment;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @BaseFieldConfiguration.is_auto_increment**. Unlike the [getIsAutoIncrement](BaseFieldConfiguration.getIsAutoIncrement) method, **the method results an undefined value if @BaseFieldConfiguration.is_auto_increment is not defined**.
     * @description **Gives the value of @BaseFieldConfiguration.is_auto_increment**. Unlike the [getIsAutoIncrement](BaseFieldConfiguration.getIsAutoIncrement) method, **the method results an undefined value if @BaseFieldConfiguration.is_auto_increment is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @BaseFieldConfiguration.is_auto_increment**. Unlike the [getIsAutoIncrement](BaseFieldConfiguration.getIsAutoIncrement) method, **the method results an undefined value if @BaseFieldConfiguration.is_auto_increment is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @BaseFieldConfiguration.is_auto_increment
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see BaseFieldConfiguration.is_auto_increment
     * @see BaseFieldConfiguration.setIsAutoIncrement
     * @see BaseFieldConfiguration.getIsAutoIncrement
     * @see BaseFieldConfiguration.eraseIsAutoIncrement
     * @see BaseFieldConfiguration.isIsAutoIncrementDefined
     * @see BaseFieldConfiguration.isIsAutoIncrementEqualTo
     */
    isAutoIncrement(): boolean{
        return this.is_auto_increment;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @BaseFieldConfiguration.is_auto_increment is equal to your given value**.
     * @description **Checks if @BaseFieldConfiguration.is_auto_increment is equal to your given value** and if @BaseFieldConfiguration.is_auto_increment is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @BaseFieldConfiguration.is_auto_increment is equal to your given value** and if @BaseFieldConfiguration.is_auto_increment is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param is_auto_increment **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @BaseFieldConfiguration.is_auto_increment
     * @category equality
     * @category field
     * @public
     * @see BaseFieldConfiguration.is_auto_increment
     * @see BaseFieldConfiguration.setIsAutoIncrement
     * @see BaseFieldConfiguration.getIsAutoIncrement
     * @see BaseFieldConfiguration.isAutoIncrement
     * @see BaseFieldConfiguration.eraseIsAutoIncrement
     * @see BaseFieldConfiguration.isIsAutoIncrementDefined
     */
    isIsAutoIncrementEqualTo(is_auto_increment?: undefined|boolean): boolean{
        if (typeof is_auto_increment === "undefined"){
            return false;
        }
        return this.isAutoIncrement() === is_auto_increment;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [PivotConfiguration](PivotConfiguration) and the actual [BaseFieldConfiguration](BaseFieldConfiguration)**.
     * @description **An array of saved relations between [PivotConfiguration](PivotConfiguration) class instances and the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance**.
     * @category @PivotConfiguration.field
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category property
     * @protected
     * @see BaseFieldConfiguration.arePivotsLinked
     * @see BaseFieldConfiguration.disposePivots
     * @see BaseFieldConfiguration.unlinkPivots
     * @see BaseFieldConfiguration.unsafePivots
     * @see BaseFieldConfiguration.getPivots
     * @see BaseFieldConfiguration.linkPivots
     * @see BaseFieldConfiguration.getPivot
     * @see BaseFieldConfiguration.unsafePivot
     * @see BaseFieldConfiguration.getPivotsSize
     */
    protected pivots: PivotConfiguration[] = [];


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the number of the associated [PivotConfiguration](PivotConfiguration) class instances**.
     * @description **Gives the size of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances**.
     * @return **Returns the number of the associated [PivotConfiguration](PivotConfiguration) class instances**.
     * @category @PivotConfiguration.field
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category length
     * @public
     * @see BaseFieldConfiguration.arePivotsLinked
     * @see BaseFieldConfiguration.disposePivots
     * @see BaseFieldConfiguration.unlinkPivots
     * @see BaseFieldConfiguration.unsafePivots
     * @see BaseFieldConfiguration.getPivots
     * @see BaseFieldConfiguration.linkPivots
     * @see BaseFieldConfiguration.getPivot
     * @see BaseFieldConfiguration.unsafePivot
     * @see BaseFieldConfiguration.pivots
     */
    getPivotsSize(): number{
        return this.unsafePivots().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration)** class instances. Unlike [getPivot](BaseFieldConfiguration.getPivot) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances**. Unlike [getPivot](BaseFieldConfiguration.getPivot) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances**. Unlike [getPivot](BaseFieldConfiguration.getPivot) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration), you can give `-1` for example. If you want to get the first item of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration), you can give `0`.*
     * @category @PivotConfiguration.field
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category unsafe
     * @category value
     * @public
     * @see BaseFieldConfiguration.arePivotsLinked
     * @see BaseFieldConfiguration.disposePivots
     * @see BaseFieldConfiguration.unlinkPivots
     * @see BaseFieldConfiguration.unsafePivots
     * @see BaseFieldConfiguration.getPivots
     * @see BaseFieldConfiguration.linkPivots
     * @see BaseFieldConfiguration.getPivot
     * @see BaseFieldConfiguration.getPivotsSize
     * @see BaseFieldConfiguration.pivots
     */
    unsafePivot(position: number): PivotConfiguration|undefined{
        if (position < 0){
            const new_position = this.getPivotsSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafePivot(new_position);
        }

        return this.pivots[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Field--unsafePivot) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Field--unsafePivot) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances**. Unlike the [unsafeTask](#one-to-many-Field--unsafePivot) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration), you can give `-1` for example. If you want to get the first item of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration), you can give `0`.*
     * @category @PivotConfiguration.field
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category getter
     * @category value
     * @public
     * @see BaseFieldConfiguration.arePivotsLinked
     * @see BaseFieldConfiguration.disposePivots
     * @see BaseFieldConfiguration.unlinkPivots
     * @see BaseFieldConfiguration.unsafePivots
     * @see BaseFieldConfiguration.getPivots
     * @see BaseFieldConfiguration.linkPivots
     * @see BaseFieldConfiguration.unsafePivot
     * @see BaseFieldConfiguration.getPivotsSize
     * @see BaseFieldConfiguration.pivots
     */
    getPivot(position: number): PivotConfiguration{
        const pivot = this.unsafePivot(position);
        if (typeof pivot === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getPivot");
            error.setClassname("BaseFieldConfiguration");
            error.setProperty("pivots");
            error.setPosition(position);
            throw error;
        }
        return pivot;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associate one or multiple [PivotConfiguration](PivotConfiguration) class instances** from your actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @description Allows you to **associate one or multiple [PivotConfiguration](PivotConfiguration) class instances** from your actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate [BaseFieldConfiguration](BaseFieldConfiguration) class instance** ([PivotConfiguration.unlinkField](PivotConfiguration.unlinkField)), and **then associate the  [PivotConfiguration](PivotConfiguration) class instances** to the [BaseFieldConfiguration](BaseFieldConfiguration) class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [PivotConfiguration.linkField](PivotConfiguration.linkField) to associate the [BaseFieldConfiguration](BaseFieldConfiguration) class instance and the [PivotConfiguration](PivotConfiguration) class instances from the both parts*.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @param pivots All the given [PivotConfiguration](PivotConfiguration) class instances that you to associate to your actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @PivotConfiguration.field
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category associate
     * @public
     * @see BaseFieldConfiguration.arePivotsLinked
     * @see BaseFieldConfiguration.disposePivots
     * @see BaseFieldConfiguration.unlinkPivots
     * @see BaseFieldConfiguration.unsafePivots
     * @see BaseFieldConfiguration.getPivots
     * @see BaseFieldConfiguration.getPivot
     * @see BaseFieldConfiguration.unsafePivot
     * @see BaseFieldConfiguration.getPivotsSize
     * @see BaseFieldConfiguration.pivots
     * @see PivotConfiguration.linkField
     */
    linkPivots(... pivots: DeepArray<PivotConfiguration>[]): this{
        for (const pivot of flat(pivots)){
            if (this.arePivotsLinked(pivot)){
                continue;
            }


            this.pivots.push(pivot);


            pivot.linkBase(this);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances**.
     * @description **Gives a copy of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances**.
     * @return **Returns a copy of the [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances**.
     * @category @PivotConfiguration.field
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category getter
     * @category iterable
     * @public
     * @see BaseFieldConfiguration.arePivotsLinked
     * @see BaseFieldConfiguration.disposePivots
     * @see BaseFieldConfiguration.unlinkPivots
     * @see BaseFieldConfiguration.unsafePivots
     * @see BaseFieldConfiguration.linkPivots
     * @see BaseFieldConfiguration.getPivot
     * @see BaseFieldConfiguration.unsafePivot
     * @see BaseFieldConfiguration.getPivotsSize
     * @see BaseFieldConfiguration.pivots
     */
    getPivots(): PivotConfiguration[]{
        return Array.from(this.unsafePivots());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives an [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances without copy**.
     * @description **Gives an [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getPivots](BaseFieldConfiguration.getPivots) method.
     * @return **Returns an [array](BaseFieldConfiguration.pivots) of the associated [PivotConfiguration](PivotConfiguration) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getPivots](BaseFieldConfiguration.getPivots) method.
     * @category @PivotConfiguration.field
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category unsafe
     * @category iterable
     * @public
     * @see BaseFieldConfiguration.arePivotsLinked
     * @see BaseFieldConfiguration.disposePivots
     * @see BaseFieldConfiguration.unlinkPivots
     * @see BaseFieldConfiguration.getPivots
     * @see BaseFieldConfiguration.linkPivots
     * @see BaseFieldConfiguration.getPivot
     * @see BaseFieldConfiguration.unsafePivot
     * @see BaseFieldConfiguration.getPivotsSize
     * @see BaseFieldConfiguration.pivots
     */
    unsafePivots(): PivotConfiguration[]{
        return this.pivots;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate one or multiple [PivotConfiguration](PivotConfiguration) class instances**.
     * @description Allows you to **dissociate one or multiple [PivotConfiguration](PivotConfiguration) class instances** from your actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the [BaseFieldConfiguration](BaseFieldConfiguration) class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [PivotConfiguration.unlinkField](PivotConfiguration.unlinkField) to dissociate the [BaseFieldConfiguration](BaseFieldConfiguration) class instance and the given [PivotConfiguration](PivotConfiguration) class instances from the both parts*.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @param pivots All the given [PivotConfiguration](PivotConfiguration) class instances that you to dissociate from your actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @PivotConfiguration.field
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category dissociate
     * @public
     * @see BaseFieldConfiguration.arePivotsLinked
     * @see BaseFieldConfiguration.disposePivots
     * @see BaseFieldConfiguration.unsafePivots
     * @see BaseFieldConfiguration.getPivots
     * @see BaseFieldConfiguration.linkPivots
     * @see BaseFieldConfiguration.getPivot
     * @see BaseFieldConfiguration.unsafePivot
     * @see BaseFieldConfiguration.getPivotsSize
     * @see BaseFieldConfiguration.pivots
     * @see PivotConfiguration.unlinkField
     */
    unlinkPivots(... pivots: DeepArray<PivotConfiguration>[]): this{
        for (const pivot of flat(pivots)){
            if (!this.arePivotsLinked(pivot)){
                continue;
            }

            while (true){
                const index = this.pivots.indexOf(pivot);
                if (index < 0){
                    break;
                }

                this.pivots.splice(index, 1);
            }

            pivot.unlinkBase();
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate all the associated [PivotConfiguration](PivotConfiguration) class instances**.
     * @description **Dissociate all the associated [PivotConfiguration](PivotConfiguration) class instances**.  *Note that all the associated [PivotConfiguration](PivotConfiguration) class instances will lose their [BaseFieldConfiguration](BaseFieldConfiguration) class instance association*.
     * @return Returns the actual [PivotConfiguration](PivotConfiguration) class instance.
     * @category @PivotConfiguration.field
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category truncate
     * @public
     * @see BaseFieldConfiguration.arePivotsLinked
     * @see BaseFieldConfiguration.unlinkPivots
     * @see BaseFieldConfiguration.unsafePivots
     * @see BaseFieldConfiguration.getPivots
     * @see BaseFieldConfiguration.linkPivots
     * @see BaseFieldConfiguration.getPivot
     * @see BaseFieldConfiguration.unsafePivot
     * @see BaseFieldConfiguration.getPivotsSize
     * @see BaseFieldConfiguration.pivots
     */
    disposePivots(): this{
        this.unlinkPivots(this.unsafePivots());
        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [PivotConfiguration](PivotConfiguration) class instances are associated**.
     * @description **Checks if all the given [PivotConfiguration](PivotConfiguration) class instances are associated** with your actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [BaseFieldConfiguration](BaseFieldConfiguration) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [PivotConfiguration](PivotConfiguration) class instances are associated with your [BaseFieldConfiguration](BaseFieldConfiguration) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @return **Return a boolean if all the given [PivotConfiguration](PivotConfiguration) class instances are associated** with your actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [BaseFieldConfiguration](BaseFieldConfiguration) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [PivotConfiguration](PivotConfiguration) class instances are associated with your [BaseFieldConfiguration](BaseFieldConfiguration) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @param pivots All the given [PivotConfiguration](PivotConfiguration) class instances that you want to check their relation *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @PivotConfiguration.field
     * @category Relation between BaseFieldConfiguration and PivotConfiguration
     * @category relation
     * @category defined
     * @public
     * @see BaseFieldConfiguration.disposePivots
     * @see BaseFieldConfiguration.unlinkPivots
     * @see BaseFieldConfiguration.unsafePivots
     * @see BaseFieldConfiguration.getPivots
     * @see BaseFieldConfiguration.linkPivots
     * @see BaseFieldConfiguration.getPivot
     * @see BaseFieldConfiguration.unsafePivot
     * @see BaseFieldConfiguration.getPivotsSize
     * @see BaseFieldConfiguration.pivots
     */
    arePivotsLinked(... pivots: DeepArray<PivotConfiguration|undefined>[]): boolean{
        return flat(pivots).every((pivot) => {
            if (typeof pivot === "undefined"){
                return false;
            }

            return this.unsafePivots().includes(pivot);
        });
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [ModelConfiguration](ModelConfiguration) and the actual [BaseFieldConfiguration](BaseFieldConfiguration)**.
     * @description **Saves the relation between [ModelConfiguration](ModelConfiguration) class instances and the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance**.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category property
     * @category relation
     * @protected
     * @see BaseFieldConfiguration.linkField
     * @see BaseFieldConfiguration.hasFieldLinked
     * @see BaseFieldConfiguration.unsafeField
     * @see BaseFieldConfiguration.getField
     * @see BaseFieldConfiguration.unlinkField
     * @see BaseFieldConfiguration.isLinkedWithField
     */
    protected model: ModelConfiguration|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associates a [ModelConfiguration](ModelConfiguration) class instance**.
     * @description Allows you to **associate a [ModelConfiguration](ModelConfiguration) class instance** with your actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance. *This method allows one parameter which is a [ModelConfiguration](ModelConfiguration) class instance that will be associated*. *Note that the method will also run [ModelConfiguration.linkFields](ModelConfiguration.linkFields) to associate the [ModelConfiguration](ModelConfiguration) class instance et the [BaseFieldConfiguration](BaseFieldConfiguration) class instance from the both parts*. *Note also that the method will also run [unlinkModel](BaseFieldConfiguration.unlinkModel) to dissociate the current [ModelConfiguration](ModelConfiguration) class instance associated*.
     * @return Returns the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category association
     * @category relation
     * @public
     * @see BaseFieldConfiguration.field
     * @see BaseFieldConfiguration.hasFieldLinked
     * @see BaseFieldConfiguration.unsafeField
     * @see BaseFieldConfiguration.getField
     * @see BaseFieldConfiguration.unlinkField
     * @see BaseFieldConfiguration.isLinkedWithField
     * @see ModelConfiguration.linkFields
     */
    linkModel(model: ModelConfiguration): this{
        if (this.isLinkedWithModel(model)){
            return this;
        }


        if (!model.areFieldsLinked(this)){
            model.linkFields(this);

            return this;
        }


        this.unlinkModel();
        this.model = model;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if a [ModelConfiguration](ModelConfiguration) class instance is associated**.
     * @return **Gives a boolean if a [ModelConfiguration](ModelConfiguration) class instance is associated** or not to our actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @description **Check if a [ModelConfiguration](ModelConfiguration) class instance is associated** or not to our actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category defined
     * @public
     * @see BaseFieldConfiguration.field
     * @see BaseFieldConfiguration.linkField
     * @see BaseFieldConfiguration.unsafeField
     * @see BaseFieldConfiguration.getField
     * @see BaseFieldConfiguration.unlinkField
     * @see BaseFieldConfiguration.isLinkedWithField
     */
    hasModelLinked(): boolean{
        return typeof this.model !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [ModelConfiguration](ModelConfiguration) class instance**. Unlike the [unsafeModel](BaseFieldConfiguration.unsafeModel) method, **the method can throw an [UnlinkedValue](UnlinkedValue) error**.
     * @description **Gives the associated [ModelConfiguration](ModelConfiguration) class instance** of our actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance. Unlike the [unsafeModel](ModelConfigurationsFields.unsafeModel) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [BaseFieldConfiguration](BaseFieldConfiguration) class instance doesn't have [ModelConfiguration](ModelConfiguration) class instance associate**.
     * @return **Results the associated [ModelConfiguration](ModelConfiguration) class instance** of our actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance. Unlike the [unsafeModel](ModelConfigurationsFields.unsafeModel) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [BaseFieldConfiguration](BaseFieldConfiguration) class instance doesn't have [ModelConfiguration](ModelConfiguration) class instance associate**.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category value
     * @category getter
     * @public
     * @see BaseFieldConfiguration.field
     * @see BaseFieldConfiguration.linkField
     * @see BaseFieldConfiguration.hasFieldLinked
     * @see BaseFieldConfiguration.unsafeField
     * @see BaseFieldConfiguration.unlinkField
     * @see BaseFieldConfiguration.isLinkedWithField
     */
    getModel(): ModelConfiguration{
        const unsafe = this.unsafeModel();

        if (typeof unsafe === "undefined"){
            const error = new UnlinkedValue();
            error.setMethod("getModel");
            error.setClassname("BaseFieldConfiguration");
            error.setWantedClassname("ModelConfiguration");
            error.setProperty("model");
            throw error;
        }
        return unsafe;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [ModelConfiguration](ModelConfiguration) class instance**. Unlike the [getModel](BaseFieldConfiguration.getModel) method, **the method can result an undefined value.
     * @description **Gives the associated [ModelConfiguration](ModelConfiguration) class instance** of our actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance. Unlike the [getModel](BaseFieldConfiguration.getModel) method, **the method results an undefined value if the [BaseFieldConfiguration](BaseFieldConfiguration) class instance doesn't have [ModelConfiguration](ModelConfiguration) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Results the associated [ModelConfiguration](ModelConfiguration) class instance** of our actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance. Unlike the [getModel](BaseFieldConfiguration.getModel) method, **the method results an undefined value if the [BaseFieldConfiguration](BaseFieldConfiguration) class instance doesn't have [ModelConfiguration](ModelConfiguration) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category value
     * @category unsafe
     * @public
     * @see BaseFieldConfiguration.field
     * @see BaseFieldConfiguration.linkField
     * @see BaseFieldConfiguration.hasFieldLinked
     * @see BaseFieldConfiguration.getField
     * @see BaseFieldConfiguration.unlinkField
     * @see BaseFieldConfiguration.isLinkedWithField
     */
    unsafeModel(): ModelConfiguration|undefined{
        return this.model;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate the [ModelConfiguration](ModelConfiguration) class instance**.
     * @description **Dissociate the [ModelConfiguration](ModelConfiguration) class instance** from your actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.  *Note that the method will also run [ModelConfiguration.unlinkFields](ModelConfiguration.unlinkFields) to dissociate the [ModelConfiguration](ModelConfiguration) class instance et the [BaseFieldConfiguration](BaseFieldConfiguration) class instance from the both parts*.
     * @return Return the actual [BaseFieldConfiguration](BaseFieldConfiguration) class instance.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category value
     * @category dissociate
     * @public
     * @see BaseFieldConfiguration.field
     * @see BaseFieldConfiguration.linkField
     * @see BaseFieldConfiguration.hasFieldLinked
     * @see BaseFieldConfiguration.unsafeField
     * @see BaseFieldConfiguration.getField
     * @see BaseFieldConfiguration.isLinkedWithField
     * @see ModelConfiguration.unlinkFields
     */
    unlinkModel(): this{
        const _model = this.model;
        this.model = undefined;

        _model?.unlinkFields(this);

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [ModelConfiguration](ModelConfiguration) class instance** are associated.
     * @description **Checks if the [BaseFieldConfiguration](BaseFieldConfiguration) class instance is associated with all the given [ModelConfiguration](ModelConfiguration) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Return a boolean if the [BaseFieldConfiguration](BaseFieldConfiguration) class instance is associated with all the given [ModelConfiguration](ModelConfiguration) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @category @BaseFieldConfiguration.model
     * @category Relation between ModelConfiguration and BaseFieldConfiguration
     * @category relation
     * @category value
     * @category equality
     * @public
     * @see BaseFieldConfiguration.field
     * @see BaseFieldConfiguration.linkField
     * @see BaseFieldConfiguration.hasFieldLinked
     * @see BaseFieldConfiguration.unsafeField
     * @see BaseFieldConfiguration.getField
     * @see BaseFieldConfiguration.unlinkField
     */
    isLinkedWithModel(model?: undefined|ModelConfiguration): boolean{
        if (typeof model === "undefined"){
            return false;
        }
        return this.unsafeModel() === model;
    }
}

export default BaseFieldConfiguration;