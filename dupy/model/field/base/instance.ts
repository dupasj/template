import PivotInstance from "../pivot/instance";
import ModelInstance from "../../instance";
import UnlinkedValue from "../../../error/internal/unlinked-value";
import DeepArray from "../../../util/deep-array/type";
import flat from "../../../util/deep-array/flat";
import OutOfRange from "../../../error/internal/out-of-range";
import DupyObject from "../../../dupy-object";
import BaseFieldConfiguration from "./index";

abstract class BaseFieldInstance<Configuration extends BaseFieldConfiguration = BaseFieldConfiguration> extends DupyObject{
    configuration: Configuration;
    constructor(configuration: Configuration) {
        super();

        this.configuration = configuration;
    }
    getConfiguration(): Configuration {
        return this.configuration;
    }
    isConfigurationEqualTo(configuration?: undefined|Configuration): boolean{
        if (typeof configuration === "undefined"){
            return false;
        }
        return this.getConfiguration() === configuration;
    }

    isBaseField(): this is BaseFieldInstance | BaseFieldConfiguration {
        return true;
    }
    isBaseFieldInstance(): this is BaseFieldInstance {
        return true;
    }

    getTranslate(){
        return this.getConfiguration().getTranslate();
    }


    abstract unsafeContext(): string|undefined;
    abstract setContext(context: string): this;
    abstract eraseContext(): this;


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [PivotInstance](PivotInstance) and the actual [BaseFieldInstance](BaseFieldInstance)**.
     * @description **An array of saved relations between [PivotInstance](PivotInstance) class instances and the actual [BaseFieldInstance](BaseFieldInstance) class instance**.
     * @category @PivotInstance.field
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category property
     * @protected
     * @see BaseFieldInstance.arePivotsLinked
     * @see BaseFieldInstance.disposePivots
     * @see BaseFieldInstance.unlinkPivots
     * @see BaseFieldInstance.unsafePivots
     * @see BaseFieldInstance.getPivots
     * @see BaseFieldInstance.linkPivots
     * @see BaseFieldInstance.getPivot
     * @see BaseFieldInstance.unsafePivot
     * @see BaseFieldInstance.getPivotsSize
     */
    protected pivots: PivotInstance[] = [];


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the number of the associated [PivotInstance](PivotInstance) class instances**.
     * @description **Gives the size of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances**.
     * @return **Returns the number of the associated [PivotInstance](PivotInstance) class instances**.
     * @category @PivotInstance.field
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category length
     * @public
     * @see BaseFieldInstance.arePivotsLinked
     * @see BaseFieldInstance.disposePivots
     * @see BaseFieldInstance.unlinkPivots
     * @see BaseFieldInstance.unsafePivots
     * @see BaseFieldInstance.getPivots
     * @see BaseFieldInstance.linkPivots
     * @see BaseFieldInstance.getPivot
     * @see BaseFieldInstance.unsafePivot
     * @see BaseFieldInstance.pivots
     */
    getPivotsSize(): number{
        return this.unsafePivots().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance)** class instances. Unlike [getPivot](BaseFieldInstance.getPivot) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances**. Unlike [getPivot](BaseFieldInstance.getPivot) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances**. Unlike [getPivot](BaseFieldInstance.getPivot) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance), you can give `-1` for example. If you want to get the first item of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance), you can give `0`.*
     * @category @PivotInstance.field
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category unsafe
     * @category value
     * @public
     * @see BaseFieldInstance.arePivotsLinked
     * @see BaseFieldInstance.disposePivots
     * @see BaseFieldInstance.unlinkPivots
     * @see BaseFieldInstance.unsafePivots
     * @see BaseFieldInstance.getPivots
     * @see BaseFieldInstance.linkPivots
     * @see BaseFieldInstance.getPivot
     * @see BaseFieldInstance.getPivotsSize
     * @see BaseFieldInstance.pivots
     */
    unsafePivot(position: number): PivotInstance|undefined{
        if (position < 0){
            const new_position = this.getPivotsSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafePivot(new_position);
        }

        return this.pivots[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances**. Unlike the [unsafeTask](#one-to-many-Field--unsafePivot) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances**. Unlike the [unsafeTask](#one-to-many-Field--unsafePivot) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances**. Unlike the [unsafeTask](#one-to-many-Field--unsafePivot) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance), you can give `-1` for example. If you want to get the first item of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance), you can give `0`.*
     * @category @PivotInstance.field
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category getter
     * @category value
     * @public
     * @see BaseFieldInstance.arePivotsLinked
     * @see BaseFieldInstance.disposePivots
     * @see BaseFieldInstance.unlinkPivots
     * @see BaseFieldInstance.unsafePivots
     * @see BaseFieldInstance.getPivots
     * @see BaseFieldInstance.linkPivots
     * @see BaseFieldInstance.unsafePivot
     * @see BaseFieldInstance.getPivotsSize
     * @see BaseFieldInstance.pivots
     */
    getPivot(position: number): PivotInstance{
        const pivot = this.unsafePivot(position);
        if (typeof pivot === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getPivot");
            error.setClassname("BaseFieldInstance");
            error.setProperty("pivots");
            error.setPosition(position);
            throw error;
        }
        return pivot;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associate one or multiple [PivotInstance](PivotInstance) class instances** from your actual [BaseFieldInstance](BaseFieldInstance) class instance.
     * @description Allows you to **associate one or multiple [PivotInstance](PivotInstance) class instances** from your actual [BaseFieldInstance](BaseFieldInstance) class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate [BaseFieldInstance](BaseFieldInstance) class instance** ([PivotInstance.unlinkField](PivotInstance.unlinkField)), and **then associate the  [PivotInstance](PivotInstance) class instances** to the [BaseFieldInstance](BaseFieldInstance) class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [PivotInstance.linkField](PivotInstance.linkField) to associate the [BaseFieldInstance](BaseFieldInstance) class instance and the [PivotInstance](PivotInstance) class instances from the both parts*.
     * @return Returns the actual [PivotInstance](PivotInstance) class instance.
     * @param pivots All the given [PivotInstance](PivotInstance) class instances that you to associate to your actual [BaseFieldInstance](BaseFieldInstance) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @PivotInstance.field
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category associate
     * @public
     * @see BaseFieldInstance.arePivotsLinked
     * @see BaseFieldInstance.disposePivots
     * @see BaseFieldInstance.unlinkPivots
     * @see BaseFieldInstance.unsafePivots
     * @see BaseFieldInstance.getPivots
     * @see BaseFieldInstance.getPivot
     * @see BaseFieldInstance.unsafePivot
     * @see BaseFieldInstance.getPivotsSize
     * @see BaseFieldInstance.pivots
     * @see PivotInstance.linkField
     */
    linkPivots(... pivots: DeepArray<PivotInstance>[]): this{
        for (const pivot of flat(pivots)){
            if (this.arePivotsLinked(pivot)){
                continue;
            }


            this.pivots.push(pivot);


            pivot.linkBase(this);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances**.
     * @description **Gives a copy of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances**.
     * @return **Returns a copy of the [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances**.
     * @category @PivotInstance.field
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category getter
     * @category iterable
     * @public
     * @see BaseFieldInstance.arePivotsLinked
     * @see BaseFieldInstance.disposePivots
     * @see BaseFieldInstance.unlinkPivots
     * @see BaseFieldInstance.unsafePivots
     * @see BaseFieldInstance.linkPivots
     * @see BaseFieldInstance.getPivot
     * @see BaseFieldInstance.unsafePivot
     * @see BaseFieldInstance.getPivotsSize
     * @see BaseFieldInstance.pivots
     */
    getPivots(): PivotInstance[]{
        return Array.from(this.unsafePivots());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives an [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances without copy**.
     * @description **Gives an [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getPivots](BaseFieldInstance.getPivots) method.
     * @return **Returns an [array](BaseFieldInstance.pivots) of the associated [PivotInstance](PivotInstance) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getPivots](BaseFieldInstance.getPivots) method.
     * @category @PivotInstance.field
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category unsafe
     * @category iterable
     * @public
     * @see BaseFieldInstance.arePivotsLinked
     * @see BaseFieldInstance.disposePivots
     * @see BaseFieldInstance.unlinkPivots
     * @see BaseFieldInstance.getPivots
     * @see BaseFieldInstance.linkPivots
     * @see BaseFieldInstance.getPivot
     * @see BaseFieldInstance.unsafePivot
     * @see BaseFieldInstance.getPivotsSize
     * @see BaseFieldInstance.pivots
     */
    unsafePivots(): PivotInstance[]{
        return this.pivots;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate one or multiple [PivotInstance](PivotInstance) class instances**.
     * @description Allows you to **dissociate one or multiple [PivotInstance](PivotInstance) class instances** from your actual [BaseFieldInstance](BaseFieldInstance) class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the [BaseFieldInstance](BaseFieldInstance) class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [PivotInstance.unlinkField](PivotInstance.unlinkField) to dissociate the [BaseFieldInstance](BaseFieldInstance) class instance and the given [PivotInstance](PivotInstance) class instances from the both parts*.
     * @return Returns the actual [PivotInstance](PivotInstance) class instance.
     * @param pivots All the given [PivotInstance](PivotInstance) class instances that you to dissociate from your actual [BaseFieldInstance](BaseFieldInstance) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @PivotInstance.field
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category dissociate
     * @public
     * @see BaseFieldInstance.arePivotsLinked
     * @see BaseFieldInstance.disposePivots
     * @see BaseFieldInstance.unsafePivots
     * @see BaseFieldInstance.getPivots
     * @see BaseFieldInstance.linkPivots
     * @see BaseFieldInstance.getPivot
     * @see BaseFieldInstance.unsafePivot
     * @see BaseFieldInstance.getPivotsSize
     * @see BaseFieldInstance.pivots
     * @see PivotInstance.unlinkField
     */
    unlinkPivots(... pivots: DeepArray<PivotInstance>[]): this{
        for (const pivot of flat(pivots)){
            if (!this.arePivotsLinked(pivot)){
                continue;
            }

            while (true){
                const index = this.pivots.indexOf(pivot);
                if (index < 0){
                    break;
                }

                this.pivots.splice(index, 1);
            }

            pivot.unlinkBase();
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate all the associated [PivotInstance](PivotInstance) class instances**.
     * @description **Dissociate all the associated [PivotInstance](PivotInstance) class instances**.  *Note that all the associated [PivotInstance](PivotInstance) class instances will lose their [BaseFieldInstance](BaseFieldInstance) class instance association*.
     * @return Returns the actual [PivotInstance](PivotInstance) class instance.
     * @category @PivotInstance.field
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category truncate
     * @public
     * @see BaseFieldInstance.arePivotsLinked
     * @see BaseFieldInstance.unlinkPivots
     * @see BaseFieldInstance.unsafePivots
     * @see BaseFieldInstance.getPivots
     * @see BaseFieldInstance.linkPivots
     * @see BaseFieldInstance.getPivot
     * @see BaseFieldInstance.unsafePivot
     * @see BaseFieldInstance.getPivotsSize
     * @see BaseFieldInstance.pivots
     */
    disposePivots(): this{
        this.unlinkPivots(this.unsafePivots());
        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [PivotInstance](PivotInstance) class instances are associated**.
     * @description **Checks if all the given [PivotInstance](PivotInstance) class instances are associated** with your actual [BaseFieldInstance](BaseFieldInstance) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [BaseFieldInstance](BaseFieldInstance) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [PivotInstance](PivotInstance) class instances are associated with your [BaseFieldInstance](BaseFieldInstance) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @return **Return a boolean if all the given [PivotInstance](PivotInstance) class instances are associated** with your actual [BaseFieldInstance](BaseFieldInstance) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [BaseFieldInstance](BaseFieldInstance) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [PivotInstance](PivotInstance) class instances are associated with your [BaseFieldInstance](BaseFieldInstance) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @param pivots All the given [PivotInstance](PivotInstance) class instances that you want to check their relation *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @PivotInstance.field
     * @category Relation between BaseFieldInstance and PivotInstance
     * @category relation
     * @category defined
     * @public
     * @see BaseFieldInstance.disposePivots
     * @see BaseFieldInstance.unlinkPivots
     * @see BaseFieldInstance.unsafePivots
     * @see BaseFieldInstance.getPivots
     * @see BaseFieldInstance.linkPivots
     * @see BaseFieldInstance.getPivot
     * @see BaseFieldInstance.unsafePivot
     * @see BaseFieldInstance.getPivotsSize
     * @see BaseFieldInstance.pivots
     */
    arePivotsLinked(... pivots: DeepArray<PivotInstance|undefined>[]): boolean{
        return flat(pivots).every((pivot) => {
            if (typeof pivot === "undefined"){
                return false;
            }

            return this.unsafePivots().includes(pivot);
        });
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [ModelInstance](ModelInstance) and the actual [BaseFieldInstance](BaseFieldInstance)**.
     * @description **Saves the relation between [ModelInstance](ModelInstance) class instances and the actual [BaseFieldInstance](BaseFieldInstance) class instance**.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category property
     * @category relation
     * @protected
     * @see BaseFieldInstance.linkField
     * @see BaseFieldInstance.hasFieldLinked
     * @see BaseFieldInstance.unsafeField
     * @see BaseFieldInstance.getField
     * @see BaseFieldInstance.unlinkField
     * @see BaseFieldInstance.isLinkedWithField
     */
    protected modelInstance: ModelInstance|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associates a [ModelInstance](ModelInstance) class instance**.
     * @description Allows you to **associate a [ModelInstance](ModelInstance) class instance** with your actual [BaseFieldInstance](BaseFieldInstance) class instance. *This method allows one parameter which is a [ModelInstance](ModelInstance) class instance that will be associated*. *Note that the method will also run [ModelInstance.linkFields](ModelInstance.linkFields) to associate the [ModelInstance](ModelInstance) class instance et the [BaseFieldInstance](BaseFieldInstance) class instance from the both parts*. *Note also that the method will also run [unlinkModelInstance](BaseFieldInstance.unlinkModelInstance) to dissociate the current [ModelInstance](ModelInstance) class instance associated*.
     * @return Returns the actual [BaseFieldInstance](BaseFieldInstance) class instance.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category association
     * @category relation
     * @public
     * @see BaseFieldInstance.field
     * @see BaseFieldInstance.hasFieldLinked
     * @see BaseFieldInstance.unsafeField
     * @see BaseFieldInstance.getField
     * @see BaseFieldInstance.unlinkField
     * @see BaseFieldInstance.isLinkedWithField
     * @see ModelInstance.linkFields
     */
    linkModelInstance(modelInstance: ModelInstance): this{
        if (this.isLinkedWithModelInstance(modelInstance)){
            return this;
        }


        if (!modelInstance.areFieldsLinked(this)){
            modelInstance.linkFields(this);

            return this;
        }


        this.unlinkModelInstance();
        this.modelInstance = modelInstance;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if a [ModelInstance](ModelInstance) class instance is associated**.
     * @return **Gives a boolean if a [ModelInstance](ModelInstance) class instance is associated** or not to our actual [BaseFieldInstance](BaseFieldInstance) class instance.
     * @description **Check if a [ModelInstance](ModelInstance) class instance is associated** or not to our actual [BaseFieldInstance](BaseFieldInstance) class instance.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category defined
     * @public
     * @see BaseFieldInstance.field
     * @see BaseFieldInstance.linkField
     * @see BaseFieldInstance.unsafeField
     * @see BaseFieldInstance.getField
     * @see BaseFieldInstance.unlinkField
     * @see BaseFieldInstance.isLinkedWithField
     */
    hasModelInstanceLinked(): boolean{
        return typeof this.modelInstance !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [ModelInstance](ModelInstance) class instance**. Unlike the [unsafeModelInstance](BaseFieldInstance.unsafeModelInstance) method, **the method can throw an [UnlinkedValue](UnlinkedValue) error**.
     * @description **Gives the associated [ModelInstance](ModelInstance) class instance** of our actual [BaseFieldInstance](BaseFieldInstance) class instance. Unlike the [unsafeModelInstance](ModelInstancesFields.unsafeModelInstance) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [BaseFieldInstance](BaseFieldInstance) class instance doesn't have [ModelInstance](ModelInstance) class instance associate**.
     * @return **Results the associated [ModelInstance](ModelInstance) class instance** of our actual [BaseFieldInstance](BaseFieldInstance) class instance. Unlike the [unsafeModelInstance](ModelInstancesFields.unsafeModelInstance) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [BaseFieldInstance](BaseFieldInstance) class instance doesn't have [ModelInstance](ModelInstance) class instance associate**.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category value
     * @category getter
     * @public
     * @see BaseFieldInstance.field
     * @see BaseFieldInstance.linkField
     * @see BaseFieldInstance.hasFieldLinked
     * @see BaseFieldInstance.unsafeField
     * @see BaseFieldInstance.unlinkField
     * @see BaseFieldInstance.isLinkedWithField
     */
    getModelInstance(): ModelInstance{
        const unsafe = this.unsafeModelInstance();

        if (typeof unsafe === "undefined"){
            const error = new UnlinkedValue();
            error.setMethod("getModelInstance");
            error.setClassname("BaseFieldInstance");
            error.setWantedClassname("ModelInstance");
            error.setProperty("modelInstance");
            throw error;
        }
        return unsafe;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [ModelInstance](ModelInstance) class instance**. Unlike the [getModelInstance](BaseFieldInstance.getModelInstance) method, **the method can result an undefined value.
     * @description **Gives the associated [ModelInstance](ModelInstance) class instance** of our actual [BaseFieldInstance](BaseFieldInstance) class instance. Unlike the [getModelInstance](BaseFieldInstance.getModelInstance) method, **the method results an undefined value if the [BaseFieldInstance](BaseFieldInstance) class instance doesn't have [ModelInstance](ModelInstance) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Results the associated [ModelInstance](ModelInstance) class instance** of our actual [BaseFieldInstance](BaseFieldInstance) class instance. Unlike the [getModelInstance](BaseFieldInstance.getModelInstance) method, **the method results an undefined value if the [BaseFieldInstance](BaseFieldInstance) class instance doesn't have [ModelInstance](ModelInstance) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category value
     * @category unsafe
     * @public
     * @see BaseFieldInstance.field
     * @see BaseFieldInstance.linkField
     * @see BaseFieldInstance.hasFieldLinked
     * @see BaseFieldInstance.getField
     * @see BaseFieldInstance.unlinkField
     * @see BaseFieldInstance.isLinkedWithField
     */
    unsafeModelInstance(): ModelInstance|undefined{
        return this.modelInstance;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate the [ModelInstance](ModelInstance) class instance**.
     * @description **Dissociate the [ModelInstance](ModelInstance) class instance** from your actual [BaseFieldInstance](BaseFieldInstance) class instance.  *Note that the method will also run [ModelInstance.unlinkFields](ModelInstance.unlinkFields) to dissociate the [ModelInstance](ModelInstance) class instance et the [BaseFieldInstance](BaseFieldInstance) class instance from the both parts*.
     * @return Return the actual [BaseFieldInstance](BaseFieldInstance) class instance.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category value
     * @category dissociate
     * @public
     * @see BaseFieldInstance.field
     * @see BaseFieldInstance.linkField
     * @see BaseFieldInstance.hasFieldLinked
     * @see BaseFieldInstance.unsafeField
     * @see BaseFieldInstance.getField
     * @see BaseFieldInstance.isLinkedWithField
     * @see ModelInstance.unlinkFields
     */
    unlinkModelInstance(): this{
        const _modelInstance = this.modelInstance;
        this.modelInstance = undefined;

        _modelInstance?.unlinkFields(this);

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [ModelInstance](ModelInstance) class instance** are associated.
     * @description **Checks if the [BaseFieldInstance](BaseFieldInstance) class instance is associated with all the given [ModelInstance](ModelInstance) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Return a boolean if the [BaseFieldInstance](BaseFieldInstance) class instance is associated with all the given [ModelInstance](ModelInstance) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @category @BaseFieldInstance.modelInstance
     * @category Relation between ModelInstance and BaseFieldInstance
     * @category relation
     * @category value
     * @category equality
     * @public
     * @see BaseFieldInstance.field
     * @see BaseFieldInstance.linkField
     * @see BaseFieldInstance.hasFieldLinked
     * @see BaseFieldInstance.unsafeField
     * @see BaseFieldInstance.getField
     * @see BaseFieldInstance.unlinkField
     */
    isLinkedWithModelInstance(modelInstance?: undefined|ModelInstance): boolean{
        if (typeof modelInstance === "undefined"){
            return false;
        }
        return this.unsafeModelInstance() === modelInstance;
    }
}

export default BaseFieldInstance