import FieldConfiguration from "./index";
import BaseFieldInstance from "../base/instance";
import UndefinedValue from "../../../error/internal/undefined-value";

class FieldInstance extends BaseFieldInstance<FieldConfiguration>{
    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @FieldInstance.context.
     * @description Save @FieldInstance.context.
     * @category @FieldInstance.context
     * @category property
     * @category field
     * @protected
     * @see FieldInstance.setContext
     * @see FieldInstance.getContext
     * @see FieldInstance.unsafeContext
     * @see FieldInstance.eraseContext
     * @see FieldInstance.isContextDefined
     * @see FieldInstance.isContextEqualTo
     */
    protected context: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @FieldInstance.context**.
     * @description Allows you to update or **assign the value of @FieldInstance.context**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [FieldInstance](FieldInstance) class instance.
     * @param context The new value to assign to @FieldInstance.context.
     * @category @FieldInstance.context
     * @category setter
     * @category field
     * @public
     * @see FieldInstance.context
     * @see FieldInstance.getContext
     * @see FieldInstance.unsafeContext
     * @see FieldInstance.eraseContext
     * @see FieldInstance.isContextDefined
     * @see FieldInstance.isContextEqualTo
     */
    setContext(context: string): this{
        if (this.isContextEqualTo(context)){
            return this;
        }

        this.eraseContext();
        this.context = context;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @FieldInstance.context**. Unlike the [getContext](FieldInstance.getContext) method, **the method results an undefined value if @FieldInstance.context is not defined**.
     * @description **Gives the value of @FieldInstance.context**. Unlike the [getContext](FieldInstance.getContext) method, **the method results an undefined value if @FieldInstance.context is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @FieldInstance.context**. Unlike the [getContext](FieldInstance.getContext) method, **the method results an undefined value if @FieldInstance.context is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @FieldInstance.context
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see FieldInstance.context
     * @see FieldInstance.setContext
     * @see FieldInstance.getContext
     * @see FieldInstance.eraseContext
     * @see FieldInstance.isContextDefined
     * @see FieldInstance.isContextEqualTo
     */
    unsafeContext(): string|undefined{
        return this.context;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @FieldInstance.context**. Unlike the [unsafeContext](FieldInstance.unsafeContext) method, **we will throw an [UndefinedValue](UndefinedValue) error if @FieldInstance.context is not defined**.
     * @description **Gives the value of @FieldInstance.context**. Unlike the [unsafeContext](FieldInstance.unsafeContext) method, **we will throw an [UndefinedValue](UndefinedValue) error if @FieldInstance.context is not defined**.
     * @return **Returns the value of @FieldInstance.context**. Unlike the [unsafeContext](FieldInstance.unsafeContext) method, **we will throw an [UndefinedValue](UndefinedValue) error if @FieldInstance.context is not defined**.
     * @category @FieldInstance.context
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @FieldInstance.context isn't defined.
     * @see FieldInstance.context
     * @see FieldInstance.setContext
     * @see FieldInstance.unsafeContext
     * @see FieldInstance.eraseContext
     * @see FieldInstance.isContextDefined
     * @see FieldInstance.isContextEqualTo
     */
    getContext(): string{
        const context = this.unsafeContext();

        if (typeof context === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getContext");
            error.setClassname("FieldInstance");
            error.setProperty("context");
            throw error;
        }
        return context;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @FieldInstance.context is defined**.
     * @description **Checks if @FieldInstance.context is defined** or not.
     * @return **Returns a boolean if @FieldInstance.context is defined** or not.
     * @category @FieldInstance.context
     * @category defined
     * @category field
     * @public
     * @see FieldInstance.context
     * @see FieldInstance.setContext
     * @see FieldInstance.getContext
     * @see FieldInstance.unsafeContext
     * @see FieldInstance.eraseContext
     * @see FieldInstance.isContextEqualTo
     */
    isContextDefined(): boolean{
        return typeof this.context !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @FieldInstance.context is equal to your given value**.
     * @description **Checks if @FieldInstance.context is equal to your given value** and if @FieldInstance.context is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @FieldInstance.context is equal to your given value** and if @FieldInstance.context is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param context **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @FieldInstance.context
     * @category equality
     * @category field
     * @public
     * @see FieldInstance.context
     * @see FieldInstance.setContext
     * @see FieldInstance.getContext
     * @see FieldInstance.unsafeContext
     * @see FieldInstance.eraseContext
     * @see FieldInstance.isContextDefined
     */
    isContextEqualTo(context?: undefined|string): boolean{
        if (typeof context === "undefined"){
            return false;
        }
        return this.unsafeContext() === context;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @FieldInstance.context**.
     * @description **Removes the assigned the value of @FieldInstance.context**: *@FieldInstance.context will be flagged as undefined*.
     * @return Return the actual [FieldInstance](FieldInstance) class instance.
     * @category @FieldInstance.context
     * @category truncate
     * @category field
     * @public
     * @see FieldInstance.context
     * @see FieldInstance.setContext
     * @see FieldInstance.getContext
     * @see FieldInstance.unsafeContext
     * @see FieldInstance.isContextDefined
     * @see FieldInstance.isContextEqualTo
     */
    eraseContext(): this{
        this.context = undefined;

        return this;
    }
}

export default FieldInstance;