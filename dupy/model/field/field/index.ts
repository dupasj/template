import BaseFieldConfiguration from "../base";

class FieldConfiguration extends BaseFieldConfiguration{
    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @FieldConfiguration.translate.
     * @description Save @FieldConfiguration.translate.
     * @category @FieldConfiguration.translate
     * @category property
     * @category field
     * @protected
     * @see FieldConfiguration.setTranslate
     * @see FieldConfiguration.getTranslate
     * @see FieldConfiguration.unsafeTranslate
     * @see FieldConfiguration.eraseTranslate
     * @see FieldConfiguration.isTranslateDefined
     * @see FieldConfiguration.isTranslateEqualTo
     */
    protected translate: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @FieldConfiguration.translate**.
     * @description Allows you to update or **assign the value of @FieldConfiguration.translate**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [FieldConfiguration](FieldConfiguration) class instance.
     * @param translate The new value to assign to @FieldConfiguration.translate.
     * @category @FieldConfiguration.translate
     * @category setter
     * @category field
     * @public
     * @see FieldConfiguration.translate
     * @see FieldConfiguration.getTranslate
     * @see FieldConfiguration.unsafeTranslate
     * @see FieldConfiguration.eraseTranslate
     * @see FieldConfiguration.isTranslateDefined
     * @see FieldConfiguration.isTranslateEqualTo
     */
    setTranslate(translate: string): this{
        if (this.isTranslateEqualTo(translate)){
            return this;
        }

        this.eraseTranslate();
        this.translate = translate;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @FieldConfiguration.translate**. Unlike the [getTranslate](FieldConfiguration.getTranslate) method, **the method results an undefined value if @FieldConfiguration.translate is not defined**.
     * @description **Gives the value of @FieldConfiguration.translate**. Unlike the [getTranslate](FieldConfiguration.getTranslate) method, **the method results an undefined value if @FieldConfiguration.translate is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @FieldConfiguration.translate**. Unlike the [getTranslate](FieldConfiguration.getTranslate) method, **the method results an undefined value if @FieldConfiguration.translate is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @FieldConfiguration.translate
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see FieldConfiguration.translate
     * @see FieldConfiguration.setTranslate
     * @see FieldConfiguration.getTranslate
     * @see FieldConfiguration.eraseTranslate
     * @see FieldConfiguration.isTranslateDefined
     * @see FieldConfiguration.isTranslateEqualTo
     */
    unsafeTranslate(): string|undefined{
        return this.translate;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @FieldConfiguration.translate**.
     * @description **Removes the assigned the value of @FieldConfiguration.translate**: *@FieldConfiguration.translate will be flagged as undefined*.
     * @return Return the actual [FieldConfiguration](FieldConfiguration) class instance.
     * @category @FieldConfiguration.translate
     * @category truncate
     * @category field
     * @public
     * @see FieldConfiguration.translate
     * @see FieldConfiguration.setTranslate
     * @see FieldConfiguration.getTranslate
     * @see FieldConfiguration.unsafeTranslate
     * @see FieldConfiguration.isTranslateDefined
     * @see FieldConfiguration.isTranslateEqualTo
     */
    eraseTranslate(): this{
        this.translate = undefined;

        return this;
    }



    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @FieldConfiguration.context.
     * @description Save @FieldConfiguration.context.
     * @category @FieldConfiguration.context
     * @category property
     * @category field
     * @protected
     * @see FieldConfiguration.setContext
     * @see FieldConfiguration.getContext
     * @see FieldConfiguration.unsafeContext
     * @see FieldConfiguration.eraseContext
     * @see FieldConfiguration.isContextDefined
     * @see FieldConfiguration.isContextEqualTo
     */
    protected context: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @FieldConfiguration.context**.
     * @description Allows you to update or **assign the value of @FieldConfiguration.context**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [FieldConfiguration](FieldConfiguration) class instance.
     * @param context The new value to assign to @FieldConfiguration.context.
     * @category @FieldConfiguration.context
     * @category setter
     * @category field
     * @public
     * @see FieldConfiguration.context
     * @see FieldConfiguration.getContext
     * @see FieldConfiguration.unsafeContext
     * @see FieldConfiguration.eraseContext
     * @see FieldConfiguration.isContextDefined
     * @see FieldConfiguration.isContextEqualTo
     */
    setContext(context: string): this{
        if (this.isContextEqualTo(context)){
            return this;
        }

        this.eraseContext();
        this.context = context;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @FieldConfiguration.context**. Unlike the [getContext](FieldConfiguration.getContext) method, **the method results an undefined value if @FieldConfiguration.context is not defined**.
     * @description **Gives the value of @FieldConfiguration.context**. Unlike the [getContext](FieldConfiguration.getContext) method, **the method results an undefined value if @FieldConfiguration.context is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @FieldConfiguration.context**. Unlike the [getContext](FieldConfiguration.getContext) method, **the method results an undefined value if @FieldConfiguration.context is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @FieldConfiguration.context
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see FieldConfiguration.context
     * @see FieldConfiguration.setContext
     * @see FieldConfiguration.getContext
     * @see FieldConfiguration.eraseContext
     * @see FieldConfiguration.isContextDefined
     * @see FieldConfiguration.isContextEqualTo
     */
    unsafeContext(): string|undefined{
        return this.context;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @FieldConfiguration.context**.
     * @description **Removes the assigned the value of @FieldConfiguration.context**: *@FieldConfiguration.context will be flagged as undefined*.
     * @return Return the actual [FieldConfiguration](FieldConfiguration) class instance.
     * @category @FieldConfiguration.context
     * @category truncate
     * @category field
     * @public
     * @see FieldConfiguration.context
     * @see FieldConfiguration.setContext
     * @see FieldConfiguration.getContext
     * @see FieldConfiguration.unsafeContext
     * @see FieldConfiguration.isContextDefined
     * @see FieldConfiguration.isContextEqualTo
     */
    eraseContext(): this{
        this.context = undefined;

        return this;
    }
}

export default FieldConfiguration;