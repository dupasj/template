import Project from "./project";
import ModelConfiguration from "./model";
import ModelInstance from "./model/instance";
import BaseFieldConfiguration from "./model/field/base";
import BaseFieldInstance from "./model/field/base/instance";
import PivotConfiguration from "./model/field/pivot";
import PivotInstance from "./model/field/pivot/instance";
import BaseDupyError from "./error/base";
import DupyError from "./error";
import BundleError from "./error/bundle";
import ModelError from "./error/model";
import ActionError from "./error/action";
import FieldError from "./error/field";

abstract class DupyObject{
    isBaseError(): this is BaseDupyError {
        return false;
    }
    isError(): this is DupyError {
        return false;
    }
    isBundleError(): this is BundleError {
        return false;
    }
    isModelError(): this is ModelError {
        return false;
    }
    isActionModel(): this is ActionError {
        return false;
    }
    isFieldError(): this is FieldError {
        return false;
    }

    isDupyObject(): this is DupyObject {
        return true;
    }
    isProject(): this is Project {
        return false;
    }
    isModelConfiguration(): this is ModelConfiguration {
        return false;
    }
    isModelInstance(): this is ModelInstance {
        return false;
    }
    isModel(): this is ModelInstance|ModelConfiguration{
        return false;
    }


    isBaseFieldConfiguration(): this is BaseFieldConfiguration {
        return false;
    }
    isBaseFieldInstance(): this is BaseFieldInstance {
        return false;
    }
    isBaseField(): this is BaseFieldInstance|BaseFieldConfiguration{
        return false;
    }

    isPivotConfiguration(): this is PivotConfiguration {
        return false;
    }
    isPivotInstance(): this is PivotInstance {
        return false;
    }
    isPivot(): this is PivotInstance|PivotConfiguration{
        return false;
    }
}

export default DupyObject;
