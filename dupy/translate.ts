import DeepArray from "./util/deep-array/type";
import DupyObject from "./dupy-object";

abstract class Translate extends DupyObject{
    /**
     * @description
     * @protected
     * @return
     * @see setTranslate
     * @see hasTranslate
     * @see getTranslate
     * @see removeTranslate
     * @see getTranslates
     * @see getTranslateValues
     * @see getTranslateKeys
     */
    protected static translate: {[key: string]: string } = {};

    /**
     * @description
     * @return The current {@link Translate } instance
     * @param key
     * @param value
     * @see translate
     * @see hasTranslate
     * @see getTranslate
     * @see removeTranslate
     * @see getTranslates
     * @see getTranslateValues
     * @see getTranslateKeys
     */
    static setTranslate(key: {[key: string]: string}|string, value?: string): typeof Translate{
        if (typeof key !== "string"){
            if (typeof value !== "undefined"){
                throw new Error("Unexpected the value argument if the given value is a map or an object");
            }

            for (const _key in key){
                this.setTranslate(_key, key[_key]);
            }

            return this;
        }

        if (typeof value === "undefined"){
            throw new Error("Expected a value argument value if the given value is a string");
        }

        this.removeTranslate(key);
        this.translate[key] = value;

        return this;
    }

    /**
     * @description
     * @return
     * @param key
     * @see translate
     * @see setTranslate
     * @see getTranslate
     * @see removeTranslate
     * @see getTranslates
     * @see getTranslateValues
     * @see getTranslateKeys
     */
    static hasTranslate(... key: DeepArray<string>[]): boolean{
        return key.every((item) => {
            if (Array.isArray(item)){
                return this.hasTranslate(... item);
            }

            return item in this.translate;
        });
    }

    /**
     * @description
     * @return
     * @param key
     * @see translate
     * @see setTranslate
     * @see hasTranslate
     * @see removeTranslate
     * @see getTranslates
     * @see getTranslateValues
     * @see getTranslateKeys
     */
    static getTranslate(key: string): string{
        if (!(key in this.translate)){
            throw new Error(`Your given key "${key}" is not defined into the \`translate\`'s ${this.constructor.name} instance`);
        }

        return this.translate[key]!;
    }

    static unsafeTranslate(key: string): string|undefined{
        return this.translate[key]!;
    }

    /**
     * @description
     * @return The current {@link Translate } instance
     * @param keys
     * @see translate
     * @see setTranslate
     * @see hasTranslate
     * @see getTranslate
     * @see getTranslates
     * @see getTranslateValues
     * @see getTranslateKeys
     */
    static removeTranslate(... keys: DeepArray<string>[]): typeof Translate{
        for (const key of keys){
            if (Array.isArray(key)){
                this.removeTranslate(... key);
                continue;
            }

            delete this.translate[key];
        }

        return this;
    }

    /**
     * @description
     * @return
     * @see setTranslate
     * @see hasTranslate
     * @see getTranslate
     * @see removeTranslate
     * @see getTranslates
     * @see getTranslateValues
     * @see translate
     */
    static getTranslateKeys(): string[]{
        return Object.keys(this.translate);
    }

    /**
     * @description
     * @return
     * @see setTranslate
     * @see hasTranslate
     * @see getTranslate
     * @see removeTranslate
     * @see getTranslates
     * @see getTranslateKeys
     * @see translate
     */
    static getTranslateValues(): string[]{
        return Object.values(this.translate);
    }

    /**
     * @description
     * @return
     * @see setTranslate
     * @see hasTranslate
     * @see getTranslate
     * @see removeTranslate
     * @see translate
     * @see getTranslateValues
     * @see getTranslateKeys
     */
    static getTranslates(): [string, string][]{
        return Object.entries(this.translate);
    }
}

export default Translate;