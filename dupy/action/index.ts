import UndefinedValue from "../error/internal/undefined-value";
import DeepArray from "../util/deep-array/type";
import flat from "../util/deep-array/flat";
import Route from "../route";
import OutOfRange from "../error/internal/out-of-range";
import Project from "../project";
import UnlinkedValue from "../error/internal/unlinked-value";

class ActionConfiguration{
    /**
     * @todo check/write the documentation
     * @todo check/write the test unit
     *
     * @summary Save @ActionConfiguration.key.
     * @description Save @ActionConfiguration.key.
     * @category @ActionConfiguration.key
     * @category property
     * @category field
     * @protected
     * @see ActionConfiguration.setKey
     * @see ActionConfiguration.getKey
     * @see ActionConfiguration.unsafeKey
     * @see ActionConfiguration.eraseKey
     * @see ActionConfiguration.isKeyDefined
     * @see ActionConfiguration.isKeyEqualTo
     */
    protected key: string|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Assign the value the @ActionConfiguration.key**.
     * @description Allows you to update or **assign the value of @ActionConfiguration.key**. *This method allows one parameter which is the value that will be assigned.*
     * @return Return the actual [ActionConfiguration](ActionConfiguration) class instance.
     * @param key The new value to assign to @ActionConfiguration.key.
     * @category @ActionConfiguration.key
     * @category setter
     * @category field
     * @public
     * @see ActionConfiguration.key
     * @see ActionConfiguration.getKey
     * @see ActionConfiguration.unsafeKey
     * @see ActionConfiguration.eraseKey
     * @see ActionConfiguration.isKeyDefined
     * @see ActionConfiguration.isKeyEqualTo
     */
    setKey(key: string): this{
        if (this.isKeyEqualTo(key)){
            return this;
        }

        this.eraseKey();
        this.key = key;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @ActionConfiguration.key**. Unlike the [getKey](ActionConfiguration.getKey) method, **the method results an undefined value if @ActionConfiguration.key is not defined**.
     * @description **Gives the value of @ActionConfiguration.key**. Unlike the [getKey](ActionConfiguration.getKey) method, **the method results an undefined value if @ActionConfiguration.key is not defined**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Return the value of @ActionConfiguration.key**. Unlike the [getKey](ActionConfiguration.getKey) method, **the method results an undefined value if @ActionConfiguration.key is not defined**: *you should take a look about [??](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @ActionConfiguration.key
     * @category unsafe
     * @category value
     * @category field
     * @public
     * @see ActionConfiguration.key
     * @see ActionConfiguration.setKey
     * @see ActionConfiguration.getKey
     * @see ActionConfiguration.eraseKey
     * @see ActionConfiguration.isKeyDefined
     * @see ActionConfiguration.isKeyEqualTo
     */
    unsafeKey(): string|undefined{
        return this.key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the value of @ActionConfiguration.key**. Unlike the [unsafeKey](ActionConfiguration.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ActionConfiguration.key is not defined**.
     * @description **Gives the value of @ActionConfiguration.key**. Unlike the [unsafeKey](ActionConfiguration.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ActionConfiguration.key is not defined**.
     * @return **Returns the value of @ActionConfiguration.key**. Unlike the [unsafeKey](ActionConfiguration.unsafeKey) method, **we will throw an [UndefinedValue](UndefinedValue) error if @ActionConfiguration.key is not defined**.
     * @category @ActionConfiguration.key
     * @category getter
     * @category value
     * @category field
     * @public
     * @throw UndefinedValue Throws an [UndefinedValue](UndefinedValue) error if @ActionConfiguration.key isn't defined.
     * @see ActionConfiguration.key
     * @see ActionConfiguration.setKey
     * @see ActionConfiguration.unsafeKey
     * @see ActionConfiguration.eraseKey
     * @see ActionConfiguration.isKeyDefined
     * @see ActionConfiguration.isKeyEqualTo
     */
    getKey(): string{
        const key = this.unsafeKey();

        if (typeof key === "undefined"){
            const error = new UndefinedValue();
            error.setMethod("getKey");
            error.setClassname("ActionConfiguration");
            error.setProperty("key");
            throw error;
        }
        return key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @ActionConfiguration.key is defined**.
     * @description **Checks if @ActionConfiguration.key is defined** or not.
     * @return **Returns a boolean if @ActionConfiguration.key is defined** or not.
     * @category @ActionConfiguration.key
     * @category defined
     * @category field
     * @public
     * @see ActionConfiguration.key
     * @see ActionConfiguration.setKey
     * @see ActionConfiguration.getKey
     * @see ActionConfiguration.unsafeKey
     * @see ActionConfiguration.eraseKey
     * @see ActionConfiguration.isKeyEqualTo
     */
    isKeyDefined(): boolean{
        return typeof this.key !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if @ActionConfiguration.key is equal to your given value**.
     * @description **Checks if @ActionConfiguration.key is equal to your given value** and if @ActionConfiguration.key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Returns a boolean if @ActionConfiguration.key is equal to your given value** and if @ActionConfiguration.key is defined. *Note that if the given value is `undefined`, the method always results `false`*.
     * @param key **The given value to check**, if this value is `undefined`, the method returns `false`.
     * @category @ActionConfiguration.key
     * @category equality
     * @category field
     * @public
     * @see ActionConfiguration.key
     * @see ActionConfiguration.setKey
     * @see ActionConfiguration.getKey
     * @see ActionConfiguration.unsafeKey
     * @see ActionConfiguration.eraseKey
     * @see ActionConfiguration.isKeyDefined
     */
    isKeyEqualTo(key?: undefined|string): boolean{
        if (typeof key === "undefined"){
            return false;
        }
        return this.unsafeKey() === key;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Removes the assigned the value of @ActionConfiguration.key**.
     * @description **Removes the assigned the value of @ActionConfiguration.key**: *@ActionConfiguration.key will be flagged as undefined*.
     * @return Return the actual [ActionConfiguration](ActionConfiguration) class instance.
     * @category @ActionConfiguration.key
     * @category truncate
     * @category field
     * @public
     * @see ActionConfiguration.key
     * @see ActionConfiguration.setKey
     * @see ActionConfiguration.getKey
     * @see ActionConfiguration.unsafeKey
     * @see ActionConfiguration.isKeyDefined
     * @see ActionConfiguration.isKeyEqualTo
     */
    eraseKey(): this{
        this.key = undefined;

        return this;
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [Route](Route) and the actual [ActionConfiguration](ActionConfiguration)**.
     * @description **An array of saved relations between [Route](Route) class instances and the actual [ActionConfiguration](ActionConfiguration) class instance**.
     * @category @Route.action
     * @category Relation between ActionConfiguration and Route
     * @category relation
     * @category property
     * @protected
     * @see ActionConfiguration.areRoutesLinked
     * @see ActionConfiguration.disposeRoutes
     * @see ActionConfiguration.unlinkRoutes
     * @see ActionConfiguration.unsafeRoutes
     * @see ActionConfiguration.getRoutes
     * @see ActionConfiguration.linkRoutes
     * @see ActionConfiguration.getRoute
     * @see ActionConfiguration.unsafeRoute
     * @see ActionConfiguration.getRoutesSize
     */
    protected routes: Route[] = [];


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the number of the associated [Route](Route) class instances**.
     * @description **Gives the size of the [array](ActionConfiguration.routes) of the associated [Route](Route) class instances**.
     * @return **Returns the number of the associated [Route](Route) class instances**.
     * @category @Route.action
     * @category Relation between ActionConfiguration and Route
     * @category relation
     * @category length
     * @public
     * @see ActionConfiguration.areRoutesLinked
     * @see ActionConfiguration.disposeRoutes
     * @see ActionConfiguration.unlinkRoutes
     * @see ActionConfiguration.unsafeRoutes
     * @see ActionConfiguration.getRoutes
     * @see ActionConfiguration.linkRoutes
     * @see ActionConfiguration.getRoute
     * @see ActionConfiguration.unsafeRoute
     * @see ActionConfiguration.routes
     */
    getRoutesSize(): number{
        return this.unsafeRoutes().length;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](ActionConfiguration.routes) of the associated [Route](Route)** class instances. Unlike [getRoute](ActionConfiguration.getRoute) method, **the method results `undefined` if no value can be found at the given position**.
     * @description **Gives a positioned item of the [array](ActionConfiguration.routes) of the associated [Route](Route) class instances**. Unlike [getRoute](ActionConfiguration.getRoute) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](ActionConfiguration.routes) of the associated [Route](Route) class instances**. Unlike [getRoute](ActionConfiguration.getRoute) method, **the method results `undefined` if no value can be found at the given position**. *You should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](ActionConfiguration.routes) of the associated [Route](Route), you can give `-1` for example. If you want to get the first item of the [array](ActionConfiguration.routes) of the associated [Route](Route), you can give `0`.*
     * @category @Route.action
     * @category Relation between ActionConfiguration and Route
     * @category relation
     * @category unsafe
     * @category value
     * @public
     * @see ActionConfiguration.areRoutesLinked
     * @see ActionConfiguration.disposeRoutes
     * @see ActionConfiguration.unlinkRoutes
     * @see ActionConfiguration.unsafeRoutes
     * @see ActionConfiguration.getRoutes
     * @see ActionConfiguration.linkRoutes
     * @see ActionConfiguration.getRoute
     * @see ActionConfiguration.getRoutesSize
     * @see ActionConfiguration.routes
     */
    unsafeRoute(position: number): Route|undefined{
        if (position < 0){
            const new_position = this.getRoutesSize() + position;

            if (new_position < 0){
                return undefined;
            }

            return this.unsafeRoute(new_position);
        }

        return this.routes[position];
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a positioned item of the [array](ActionConfiguration.routes) of the associated [Route](Route) class instances**. Unlike the [unsafeTask](#one-to-many-Action--unsafeRoute) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @description **Gives a positioned item of the [array](ActionConfiguration.routes) of the associated [Route](Route) class instances**. Unlike the [unsafeTask](#one-to-many-Action--unsafeRoute) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @return **Returns a positioned item of the [array](ActionConfiguration.routes) of the associated [Route](Route) class instances**. Unlike the [unsafeTask](#one-to-many-Action--unsafeRoute) method, **if the value cannot be found at the given position, the method throws an error**. The method allows one numeric argument who is the expected item position. If the given position is negative, the given position will start from the end.
     * @param position The given position. If the given position is negative, the position starts from the end. *If you want the last item of the [array](ActionConfiguration.routes) of the associated [Route](Route), you can give `-1` for example. If you want to get the first item of the [array](ActionConfiguration.routes) of the associated [Route](Route), you can give `0`.*
     * @category @Route.action
     * @category Relation between ActionConfiguration and Route
     * @category relation
     * @category getter
     * @category value
     * @public
     * @see ActionConfiguration.areRoutesLinked
     * @see ActionConfiguration.disposeRoutes
     * @see ActionConfiguration.unlinkRoutes
     * @see ActionConfiguration.unsafeRoutes
     * @see ActionConfiguration.getRoutes
     * @see ActionConfiguration.linkRoutes
     * @see ActionConfiguration.unsafeRoute
     * @see ActionConfiguration.getRoutesSize
     * @see ActionConfiguration.routes
     */
    getRoute(position: number): Route{
        const route = this.unsafeRoute(position);
        if (typeof route === "undefined"){
            const error = new OutOfRange();
            error.setMethod("getRoute");
            error.setClassname("ActionConfiguration");
            error.setProperty("routes");
            error.setPosition(position);
            throw error;
        }
        return route;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associate one or multiple [Route](Route) class instances** from your actual [ActionConfiguration](ActionConfiguration) class instance.
     * @description Allows you to **associate one or multiple [Route](Route) class instances** from your actual [ActionConfiguration](ActionConfiguration) class instance. The method accepts an array of values, **each argument** of in the method **will dissociate his current associate [ActionConfiguration](ActionConfiguration) class instance** ([Route.unlinkAction](Route.unlinkAction)), and **then associate the  [Route](Route) class instances** to the [ActionConfiguration](ActionConfiguration) class instance. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [Route.linkAction](Route.linkAction) to associate the [ActionConfiguration](ActionConfiguration) class instance and the [Route](Route) class instances from the both parts*.
     * @return Returns the actual [Route](Route) class instance.
     * @param routes All the given [Route](Route) class instances that you to associate to your actual [ActionConfiguration](ActionConfiguration) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @Route.action
     * @category Relation between ActionConfiguration and Route
     * @category relation
     * @category associate
     * @public
     * @see ActionConfiguration.areRoutesLinked
     * @see ActionConfiguration.disposeRoutes
     * @see ActionConfiguration.unlinkRoutes
     * @see ActionConfiguration.unsafeRoutes
     * @see ActionConfiguration.getRoutes
     * @see ActionConfiguration.getRoute
     * @see ActionConfiguration.unsafeRoute
     * @see ActionConfiguration.getRoutesSize
     * @see ActionConfiguration.routes
     * @see Route.linkAction
     */
    linkRoutes(... routes: DeepArray<Route>[]): this{
        for (const route of flat(routes)){
            if (this.areRoutesLinked(route)){
                continue;
            }


            this.routes.push(route);


            route.linkAction(this);
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives a copy of the [array](ActionConfiguration.routes) of the associated [Route](Route) class instances**.
     * @description **Gives a copy of the [array](ActionConfiguration.routes) of the associated [Route](Route) class instances**.
     * @return **Returns a copy of the [array](ActionConfiguration.routes) of the associated [Route](Route) class instances**.
     * @category @Route.action
     * @category Relation between ActionConfiguration and Route
     * @category relation
     * @category getter
     * @category iterable
     * @public
     * @see ActionConfiguration.areRoutesLinked
     * @see ActionConfiguration.disposeRoutes
     * @see ActionConfiguration.unlinkRoutes
     * @see ActionConfiguration.unsafeRoutes
     * @see ActionConfiguration.linkRoutes
     * @see ActionConfiguration.getRoute
     * @see ActionConfiguration.unsafeRoute
     * @see ActionConfiguration.getRoutesSize
     * @see ActionConfiguration.routes
     */
    getRoutes(): Route[]{
        return Array.from(this.unsafeRoutes());
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives an [array](ActionConfiguration.routes) of the associated [Route](Route) class instances without copy**.
     * @description **Gives an [array](ActionConfiguration.routes) of the associated [Route](Route) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getRoutes](ActionConfiguration.getRoutes) method.
     * @return **Returns an [array](ActionConfiguration.routes) of the associated [Route](Route) class instances without copy** the array. The array is not copied, so if you modify this array, you will modify the master array (who this can give unexpected features). **You should use it read-only features**, otherwise use the [getRoutes](ActionConfiguration.getRoutes) method.
     * @category @Route.action
     * @category Relation between ActionConfiguration and Route
     * @category relation
     * @category unsafe
     * @category iterable
     * @public
     * @see ActionConfiguration.areRoutesLinked
     * @see ActionConfiguration.disposeRoutes
     * @see ActionConfiguration.unlinkRoutes
     * @see ActionConfiguration.getRoutes
     * @see ActionConfiguration.linkRoutes
     * @see ActionConfiguration.getRoute
     * @see ActionConfiguration.unsafeRoute
     * @see ActionConfiguration.getRoutesSize
     * @see ActionConfiguration.routes
     */
    unsafeRoutes(): Route[]{
        return this.routes;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate one or multiple [Route](Route) class instances**.
     * @description Allows you to **dissociate one or multiple [Route](Route) class instances** from your actual [ActionConfiguration](ActionConfiguration) class instance. The method accepts an array of values,  **each argument** of in the method **will be dissociated from the [ActionConfiguration](ActionConfiguration) class instance**, and the method accepts an array of values. *The value can be a deep array, you should take a look to [flat](flat) function*.  *Note that the method will also run [Route.unlinkAction](Route.unlinkAction) to dissociate the [ActionConfiguration](ActionConfiguration) class instance and the given [Route](Route) class instances from the both parts*.
     * @return Returns the actual [Route](Route) class instance.
     * @param routes All the given [Route](Route) class instances that you to dissociate from your actual [ActionConfiguration](ActionConfiguration) class instance *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @Route.action
     * @category Relation between ActionConfiguration and Route
     * @category relation
     * @category dissociate
     * @public
     * @see ActionConfiguration.areRoutesLinked
     * @see ActionConfiguration.disposeRoutes
     * @see ActionConfiguration.unsafeRoutes
     * @see ActionConfiguration.getRoutes
     * @see ActionConfiguration.linkRoutes
     * @see ActionConfiguration.getRoute
     * @see ActionConfiguration.unsafeRoute
     * @see ActionConfiguration.getRoutesSize
     * @see ActionConfiguration.routes
     * @see Route.unlinkAction
     */
    unlinkRoutes(... routes: DeepArray<Route>[]): this{
        for (const route of flat(routes)){
            if (!this.areRoutesLinked(route)){
                continue;
            }

            while (true){
                const index = this.routes.indexOf(route);
                if (index < 0){
                    break;
                }

                this.routes.splice(index, 1);
            }

            route.unlinkAction();
        }

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate all the associated [Route](Route) class instances**.
     * @description **Dissociate all the associated [Route](Route) class instances**.  *Note that all the associated [Route](Route) class instances will lose their [ActionConfiguration](ActionConfiguration) class instance association*.
     * @return Returns the actual [Route](Route) class instance.
     * @category @Route.action
     * @category Relation between ActionConfiguration and Route
     * @category relation
     * @category truncate
     * @public
     * @see ActionConfiguration.areRoutesLinked
     * @see ActionConfiguration.unlinkRoutes
     * @see ActionConfiguration.unsafeRoutes
     * @see ActionConfiguration.getRoutes
     * @see ActionConfiguration.linkRoutes
     * @see ActionConfiguration.getRoute
     * @see ActionConfiguration.unsafeRoute
     * @see ActionConfiguration.getRoutesSize
     * @see ActionConfiguration.routes
     */
    disposeRoutes(): this{
        this.unlinkRoutes(this.unsafeRoutes());
        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [Route](Route) class instances are associated**.
     * @description **Checks if all the given [Route](Route) class instances are associated** with your actual [ActionConfiguration](ActionConfiguration) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [ActionConfiguration](ActionConfiguration) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [Route](Route) class instances are associated with your [ActionConfiguration](ActionConfiguration) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @return **Return a boolean if all the given [Route](Route) class instances are associated** with your actual [ActionConfiguration](ActionConfiguration) class instance. Each argument of in the method going to be checked, and the method accepts an array of values *(the value can be a deep array, you should take a look to [flat](flat) function)*. If one of thus elements is not associated with your [ActionConfiguration](ActionConfiguration) class instance or are `undefined`, the method results `false`: the method result `true` only if all the [Route](Route) class instances are associated with your [ActionConfiguration](ActionConfiguration) class instance and not undefined. *Note that, if no arguments is given in this method, the method results `true` (in fact, all the argument is associated.)*
     * @param routes All the given [Route](Route) class instances that you want to check their relation *(the value can be a deep array, you should take a look to [flat](flat) function)*.
     * @category @Route.action
     * @category Relation between ActionConfiguration and Route
     * @category relation
     * @category defined
     * @public
     * @see ActionConfiguration.disposeRoutes
     * @see ActionConfiguration.unlinkRoutes
     * @see ActionConfiguration.unsafeRoutes
     * @see ActionConfiguration.getRoutes
     * @see ActionConfiguration.linkRoutes
     * @see ActionConfiguration.getRoute
     * @see ActionConfiguration.unsafeRoute
     * @see ActionConfiguration.getRoutesSize
     * @see ActionConfiguration.routes
     */
    areRoutesLinked(... routes: DeepArray<Route|undefined>[]): boolean{
        return flat(routes).every((route) => {
            if (typeof route === "undefined"){
                return false;
            }

            return this.unsafeRoutes().includes(route);
        });
    }


    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Saves the relation between [Project](Project) and the actual [ActionConfiguration](ActionConfiguration)**.
     * @description **Saves the relation between [Project](Project) class instances and the actual [ActionConfiguration](ActionConfiguration) class instance**.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category property
     * @category relation
     * @protected
     * @see ActionConfiguration.linkAction
     * @see ActionConfiguration.hasActionLinked
     * @see ActionConfiguration.unsafeAction
     * @see ActionConfiguration.getAction
     * @see ActionConfiguration.unlinkAction
     * @see ActionConfiguration.isLinkedWithAction
     */
    protected project: Project|undefined = undefined;

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Associates a [Project](Project) class instance**.
     * @description Allows you to **associate a [Project](Project) class instance** with your actual [ActionConfiguration](ActionConfiguration) class instance. *This method allows one parameter which is a [Project](Project) class instance that will be associated*. *Note that the method will also run [Project.linkActions](Project.linkActions) to associate the [Project](Project) class instance et the [ActionConfiguration](ActionConfiguration) class instance from the both parts*. *Note also that the method will also run [unlinkProject](ActionConfiguration.unlinkProject) to dissociate the current [Project](Project) class instance associated*.
     * @return Returns the actual [ActionConfiguration](ActionConfiguration) class instance.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category association
     * @category relation
     * @public
     * @see ActionConfiguration.action
     * @see ActionConfiguration.hasActionLinked
     * @see ActionConfiguration.unsafeAction
     * @see ActionConfiguration.getAction
     * @see ActionConfiguration.unlinkAction
     * @see ActionConfiguration.isLinkedWithAction
     * @see Project.linkActions
     */
    linkProject(project: Project): this{
        if (this.isLinkedWithProject(project)){
            return this;
        }


        if (!project.areActionsLinked(this)){
            project.linkActions(this);

            return this;
        }


        this.unlinkProject();
        this.project = project;

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if a [Project](Project) class instance is associated**.
     * @return **Gives a boolean if a [Project](Project) class instance is associated** or not to our actual [ActionConfiguration](ActionConfiguration) class instance.
     * @description **Check if a [Project](Project) class instance is associated** or not to our actual [ActionConfiguration](ActionConfiguration) class instance.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category defined
     * @public
     * @see ActionConfiguration.action
     * @see ActionConfiguration.linkAction
     * @see ActionConfiguration.unsafeAction
     * @see ActionConfiguration.getAction
     * @see ActionConfiguration.unlinkAction
     * @see ActionConfiguration.isLinkedWithAction
     */
    hasProjectLinked(): boolean{
        return typeof this.project !== "undefined";
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [Project](Project) class instance**. Unlike the [unsafeProject](ActionConfiguration.unsafeProject) method, **the method can throw an [UnlinkedValue](UnlinkedValue) error**.
     * @description **Gives the associated [Project](Project) class instance** of our actual [ActionConfiguration](ActionConfiguration) class instance. Unlike the [unsafeProject](ActionConfigurations.unsafeProject) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [ActionConfiguration](ActionConfiguration) class instance doesn't have [Project](Project) class instance associate**.
     * @return **Results the associated [Project](Project) class instance** of our actual [ActionConfiguration](ActionConfiguration) class instance. Unlike the [unsafeProject](ActionConfigurations.unsafeProject) method, **we will throw an [UnlinkedValue](UnlinkedValue) error if the [ActionConfiguration](ActionConfiguration) class instance doesn't have [Project](Project) class instance associate**.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category value
     * @category getter
     * @public
     * @see ActionConfiguration.action
     * @see ActionConfiguration.linkAction
     * @see ActionConfiguration.hasActionLinked
     * @see ActionConfiguration.unsafeAction
     * @see ActionConfiguration.unlinkAction
     * @see ActionConfiguration.isLinkedWithAction
     */
    getProject(): Project{
        const unsafe = this.unsafeProject();

        if (typeof unsafe === "undefined"){
            const error = new UnlinkedValue();
            error.setMethod("getProject");
            error.setClassname("ActionConfiguration");
            error.setWantedClassname("Project");
            error.setProperty("project");
            throw error;
        }
        return unsafe;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Gives the associated [Project](Project) class instance**. Unlike the [getProject](ActionConfiguration.getProject) method, **the method can result an undefined value.
     * @description **Gives the associated [Project](Project) class instance** of our actual [ActionConfiguration](ActionConfiguration) class instance. Unlike the [getProject](ActionConfiguration.getProject) method, **the method results an undefined value if the [ActionConfiguration](ActionConfiguration) class instance doesn't have [Project](Project) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @return **Results the associated [Project](Project) class instance** of our actual [ActionConfiguration](ActionConfiguration) class instance. Unlike the [getProject](ActionConfiguration.getProject) method, **the method results an undefined value if the [ActionConfiguration](ActionConfiguration) class instance doesn't have [Project](Project) class instance associate**: *you should take a look about [??](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) operator or to the [optional chaining](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/Optional_chaining)*.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category value
     * @category unsafe
     * @public
     * @see ActionConfiguration.action
     * @see ActionConfiguration.linkAction
     * @see ActionConfiguration.hasActionLinked
     * @see ActionConfiguration.getAction
     * @see ActionConfiguration.unlinkAction
     * @see ActionConfiguration.isLinkedWithAction
     */
    unsafeProject(): Project|undefined{
        return this.project;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Dissociate the [Project](Project) class instance**.
     * @description **Dissociate the [Project](Project) class instance** from your actual [ActionConfiguration](ActionConfiguration) class instance.  *Note that the method will also run [Project.unlinkActions](Project.unlinkActions) to dissociate the [Project](Project) class instance et the [ActionConfiguration](ActionConfiguration) class instance from the both parts*.
     * @return Return the actual [ActionConfiguration](ActionConfiguration) class instance.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category value
     * @category dissociate
     * @public
     * @see ActionConfiguration.action
     * @see ActionConfiguration.linkAction
     * @see ActionConfiguration.hasActionLinked
     * @see ActionConfiguration.unsafeAction
     * @see ActionConfiguration.getAction
     * @see ActionConfiguration.isLinkedWithAction
     * @see Project.unlinkActions
     */
    unlinkProject(): this{
        const _project = this.project;
        this.project = undefined;

        _project?.unlinkActions(this);

        return this;
    }

    /**
     * @todo check/write the documentation
     * @todo check/write the test unit for
     *
     * @summary **Checks if all the given [Project](Project) class instance** are associated.
     * @description **Checks if the [ActionConfiguration](ActionConfiguration) class instance is associated with all the given [Project](Project) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @return **Return a boolean if the [ActionConfiguration](ActionConfiguration) class instance is associated with all the given [Project](Project) class instance**. *Note that if the given value is `undefined`, the method always results `false`*.
     * @category @ActionConfiguration.project
     * @category Relation between Project and ActionConfiguration
     * @category relation
     * @category value
     * @category equality
     * @public
     * @see ActionConfiguration.action
     * @see ActionConfiguration.linkAction
     * @see ActionConfiguration.hasActionLinked
     * @see ActionConfiguration.unsafeAction
     * @see ActionConfiguration.getAction
     * @see ActionConfiguration.unlinkAction
     */
    isLinkedWithProject(project?: undefined|Project): boolean{
        if (typeof project === "undefined"){
            return false;
        }
        return this.unsafeProject() === project;
    }
}


export default ActionConfiguration;