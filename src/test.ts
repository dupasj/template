import DeepArray from "../result/util/deep-array/type";

const flat = <T = any>(value: DeepArray<T>): T[] => {
    if (!Array.isArray(value)){
        return [value];
    }

    const done: DeepArray<T>[][] = [];
    const result: T[] = [];
    const _flat = (value: DeepArray<T>[]) => {
        if (done.includes(value)){
            return
        }

        done.push(value);

        for(const item of value){
            if (Array.isArray(item)){
                _flat(item);
                continue;
            }

            if (result.includes(item)){
                continue
            }

            result.push(item);
        }
    }

    _flat(value)

    return result;
};

export default flat;