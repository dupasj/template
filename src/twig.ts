import Twig from "twig";
import * as ChangeCase from "change-case";
import pluralize from "pluralize";

Twig.extendFilter("pascal", (value, args) => {
	return ChangeCase.pascalCase(value);
});
Twig.extendFilter("snake", (value, args) => {
	const r = ChangeCase.snakeCase(value);

	if (["switch","function","typeof"].includes(r)){
		return "_"+r;
	}

	return r;
});
Twig.extendFilter("pluralize", (value, args) => {
	if (Array.isArray(args) && args.length > 0 && !args[0]){
		return ChangeCase.snakeCase(value).split("_").map(val => pluralize.singular(val)).join("_");
	}

	const split = ChangeCase.snakeCase(value).split("_");

	return [
		... split.slice(0,-1).map(val => pluralize.singular(val)),
		... split.slice(-1).map(val => pluralize(val)),
	].join("_");
});
Twig.extendFilter("singular", (value, args) => {
	return ChangeCase.snakeCase(value).split("_").map(val => pluralize.singular(val)).join("_");
});

export const relation_trim = (trim,value) => {
	let pascal = ChangeCase.snakeCase(value).split("_").map(pluralize.singular);
	const split = ChangeCase.snakeCase(trim).split("_").map(pluralize.singular);

	for(let i=0;i<split.length;i++){
		if (pascal.length <= 1){
			break;
		}
		if (pascal[0] !== split[i]){
			break;
		}

		pascal.splice(0,1);
	}

	return pascal.join("_");
}

Twig.extendFilter("relation_trim", (value, args) => {
	return ChangeCase.pascalCase(relation_trim(args[0],value));
});

export default Twig;