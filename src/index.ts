import pluralize from "pluralize";
import Express from "express";
import Twig, {relation_trim} from './twig';
import * as Path from "path";
import {ESLint} from "eslint";
import bodyParser from "body-parser";
import * as ChangeCase from "change-case";
import eslint_config from "./eslint";

const eslint = new ESLint(eslint_config);

const app = Express();

// @ts-ignore
app.use(bodyParser.json());


app.get("/field",async (req,res) => {
    const parameters = {
        name: typeof req.query.name === "string" ? req.query.name : "name",
        classname: typeof req.query.classname === "string" ? req.query.classname : "classname",
        type: (req.query.type || "any").toString().split("|"),
        function: req.query.function === "1" || req.query.function === "true",
        parent: typeof req.query.parent === "string" ? req.query.parent : null,
        default: typeof req.query.default === "string" ? req.query.default : null,
    };

    const classes = {
        [parameters.classname]: {
            fields: {
                [parameters.name]: {
                    type: parameters.type,
                    list: false,
                    function: parameters.function,
                    default: parameters.default,
                    parent: parameters.parent,
                }
            },
            relation: {
                child: {},
                parent: {}
            }
        }
    }

    console.dir(classes,{
        depth: null
    });

    Twig.renderFile(Path.join(__dirname,"resources/json.twig"), {
        data: classes,
    }, async (err, js) => {
        if (err){
            console.log(err);
        }else{
            const beautify = await eslint.lintText(js);

            if (typeof beautify[0].output !== "string"){
                res.setHeader("Content-Type","text/plain");
                res.status(200).end(js,"utf8");
                return;
            }

            res.setHeader("Content-Type","text/plain");
            res.status(200).end(beautify[0].output,"utf8");
        }
    });
});

app.get("/map",async (req,res) => {
    const parameters = {
        name: typeof req.query.name === "string" ? req.query.name : "name",
        classname: typeof req.query.classname === "string" ? req.query.classname : "classname",
        type: req.query.type || "any",
    };

    console.log(parameters);

    Twig.renderFile(Path.join(__dirname,"resources/map.twig"), parameters, async (err, js) => {
        if (err){
            console.log(err);
        }else{
            const beautify = await eslint.lintText(js);

            if (typeof beautify[0].output !== "string"){
                res.setHeader("Content-Type","text/plain");
                res.status(200).end(js,"utf8");
                return;
            }

            res.setHeader("Content-Type","text/plain");
            res.status(200).end(beautify[0].output,"utf8");
        }
    });
});

app.get("/list",async (req,res) => {
    const parameters = {
        name: typeof req.query.name === "string" ? req.query.name : "name",
        classname: typeof req.query.classname === "string" ? req.query.classname : "classname",
        type: (req.query.type || "any").toString().split("|"),
        function: req.query.function === "1" || req.query.function === "true",
        parent: typeof req.query.parent === "string" ? req.query.parent : null,
        default: typeof req.query.default === "string" ? req.query.default : null,
    };

    const classes = {
        [parameters.classname]: {
            fields: {
                [parameters.name]: {
                    type: parameters.type,
                    list: true,
                    function: parameters.function,
                    default: parameters.default,
                    parent: parameters.parent,
                }
            },
            relation: {
                child: {},
                parent: {}
            }
        }
    }

    console.dir(classes,{
        depth: null
    });

    Twig.renderFile(Path.join(__dirname,"resources/json.twig"), {
        data: classes,
    }, async (err, js) => {
        console.log(js)
        if (err){
            console.log(err);
        }else{
            const beautify = await eslint.lintText(js);

            if (typeof beautify[0].output !== "string"){
                res.setHeader("Content-Type","text/plain");
                res.status(200).end(js,"utf8");
                return;
            }

            res.setHeader("Content-Type","text/plain");
            res.status(200).end(beautify[0].output,"utf8");
        }
    });
});

app.get("/relation",async (req,res) => {
    const parameters = {
        child: typeof req.query.child === "string" ? req.query.child : "child",
        parent: typeof req.query.parent === "string" ? req.query.parent : "parent",
        classname: typeof req.query.classname === "string" ? req.query.classname : "Classname"
    };

    const list = {
        child: parameters.child.trim().endsWith("[]"),
        parent: parameters.parent.trim().endsWith("[]"),
    }
    const name = {
        child: list.child ? parameters.child.slice(0,parameters.child.lastIndexOf("[]")).trim() : parameters.child.trim(),
        parent: list.parent ? parameters.parent.slice(0,parameters.parent.lastIndexOf("[]")).trim() : parameters.parent.trim(),
    }

    const classes = {
        [name.parent]: {
            fields: {},
            relation: {
                parent:{},
                child:{
                    [name.child]: {
                        list: list.child
                    }
                },
            }
        },
        [name.child]: {
            fields: {},
            relation: {
                child:{},
                parent:{
                    [name.parent]: {
                        list: list.parent
                    }
                },
            }
        },
    }

    console.dir(classes,{
        depth: null
    });

    Twig.renderFile(Path.join(__dirname,"resources/json.twig"), {
        data: classes,
    }, async (err, js) => {
        if (err){
            console.log(err);
        }else{
            const beautify = await eslint.lintText(js);

            if (typeof beautify[0].output !== "string"){
                res.setHeader("Content-Type","text/plain");
                res.status(200).end(js,"utf8");
                return;
            }

            res.setHeader("Content-Type","text/plain");
            res.status(200).end(beautify[0].output,"utf8");
        }
    });
});

app.post("/json",async (req,res) => {
    const getType = (value): string[] => {
        if (typeof value === "number"){
            return ["number"];
        }
        if (typeof value === "string"){
            return [value];
        }
        if (typeof value === "boolean"){
            return ["boolean"];
        }
        if (Array.isArray(value)){
            const result: string[] = [];
            for(const item of value){
                const types = getType(item);

                for(const type of types){
                    if (result.includes(type)){
                        continue;
                    }

                    result.push(type);
                }
            }

            return result;
        }

        return ["object"];
    }

    const statics = ((req.query.static ?? "") as string).toLowerCase().split(',').map(item => item.trim()).filter(item => item.length > 0);
    const statics_undone = new Set(statics);
    const statics_debug = new Set();

    const classname = typeof req.query.classname === "string" ? req.query.classname : "Classname";

    const classes = {};

    const map = (json,classname,path: string[] = []) => {
            if (Array.isArray(json)){
                for(const item of json){
                    map(item,classname,path);
                }
                return;
            }

          if (!(classname in classes)){
              classes[classname] = {
                  fields: {},
                  relation: {
                      child: {},
                      parent: {}
                  }
              }
          }

        for(const key in json){
            const types = getType(json[key]);
            if (types.includes("object")){
                const new_classname = classname+ChangeCase.pascalCase(key);
                const ctx = ChangeCase.snakeCase(Array.isArray(json[key]) ? pluralize(key) : pluralize.singular(key));

                for(const item of (Array.isArray(json[key]) ? json[key] : [json[key]])){
                    map(item,new_classname,[... path,ctx]);
                }

                if (Array.isArray(json[key]) && !(new_classname in classes)){
                    classes[classname] = {
                        fields: {},
                        relation: {
                            child: {},
                            parent: {}
                        }
                    }
                }

                statics_undone.delete([... path,ctx].join("."));
                statics_undone.delete([... path,ctx,ChangeCase.snakeCase(relation_trim(new_classname,classname))].join("."));

                statics_debug.add([... path,ctx].join("."));
                statics_debug.add([... path,ctx,ChangeCase.snakeCase(relation_trim(new_classname,classname))].join("."));

                classes[classname].relation.child[new_classname] = {
                    is_static: statics.includes([... path,ctx].join(".")),
                    list: Array.isArray(json[key])
                }
                classes[new_classname].relation.parent[classname] = {
                    is_static: statics.includes([... path,ctx,ChangeCase.snakeCase(relation_trim(new_classname,classname))].join(".")),
                    list: false
                }

                continue;
            }

            if (!(key in classes[classname].fields)){
                statics_debug.add([... path,key].join("."));
                statics_undone.delete([... path,key].join("."));

                classes[classname].fields[key] = {
                    type: types,
                    is_static: statics.includes([... path,key].join(".")),
                    list: Array.isArray(json[key]),
                    default: null,
                    parent: null,
                };
            }

            for(const type of types){
                if (!classes[classname].fields[key].type.includes(type)){
                    classes[classname].fields[key].type.push(type);
                }
            }
            classes[classname].fields[key].list = classes[classname].fields[key].list || Array.isArray(json[key]);
        }
    };

    map(req.body,classname);

    if (statics_undone.size > 0){
        res.setHeader("Content-Type","text/plain");
        res.status(417).end("The following key ("+Array.from(statics_undone).map((i) => "'"+i+"'").join(',')+") paths do not match with the following keys dictionary: "+Array.from(statics_debug).join(','),"utf8");
        return;
    }

    Twig.renderFile(Path.join(__dirname,"resources/json.twig"), {
        data: classes,
    }, async (err, js) => {
        if (err){
            console.log(err);
        }else{
            const beautify = await eslint.lintText(js);

            if (typeof beautify[0].output !== "string"){
                res.setHeader("Content-Type","text/plain");
                res.status(200).end(js,"utf8");
                return;
            }

            res.setHeader("Content-Type","text/plain");
            res.status(200).end(beautify[0].output,"utf8");
        }
    });
});
const port = process.env.PORT || 80;
app.listen(port,() => {
    console.log("Running on http://localhost:"+port);
});